import 'core-js';
import 'regenerator-runtime/runtime';

import React from 'react';
import ReactDOM from 'react-dom';
import App from './pages/App';
import DependencyProvider from './setup/DependencyProvider';

ReactDOM.render(
	<React.StrictMode>
		<DependencyProvider>
			<App />
		</DependencyProvider>
	</React.StrictMode>,
	document.getElementById('root'),
);
