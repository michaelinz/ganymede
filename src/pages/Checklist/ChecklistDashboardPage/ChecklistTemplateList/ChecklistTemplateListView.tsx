import React, { useState } from 'react';
import { IChecklist, IChecklistTemplate } from '@mccarthyfinch/public-api-client';
import { ListRow, ListWrapper, MenuItem } from '../../components/DashboardList';
import { getChecklistEditorURL } from '../../ChecklistRouter';
import { useHistory } from 'react-router-dom';
import LoadingOverlay from 'src/components/LoadingOverlay';
import { UseQueryResult } from 'react-query';
import QueryStatusWrapper from 'src/components/QueryStatusWrapper';
import CircularProgress from 'src/components/CircularProgress';
import ChecklistTemplateListRow from './ChecklistTemplateListRow';
import { ascend, sortWith } from 'ramda';

const sortChecklistTemplates = sortWith<IChecklistTemplate>([
	ascend(checklistTemplate => checklistTemplate.name)
]);

interface IChecklistTemplateListViewProps {
	createChecklist: (checklist: IChecklist) => Promise<IChecklist>;
	checklistTemplates: IChecklistTemplate[];
	queryResult: UseQueryResult<IChecklistTemplate[], any>;
}

export default function ChecklistTemplateListView({ createChecklist, checklistTemplates, queryResult }: IChecklistTemplateListViewProps) {
	const history = useHistory();
	const [isLoading, setIsLoading] = useState(false);
	const _checklistTemplates = checklistTemplates ? sortChecklistTemplates(checklistTemplates) : [];

	const onCreateChecklist = async () => {
		setIsLoading(true);
		try {
			const checklist = await createChecklist({ name: 'Blank Checklist' });
			history.push(getChecklistEditorURL(checklist.uuid));
		} catch (e) {
			console.error(e);
			setIsLoading(false);
		}
	};

	if (isLoading) {
		return <LoadingOverlay />;
	}
	return (
		<QueryStatusWrapper
			queryResults={[queryResult]}
			renderLoading={() => <div style={{padding: '1rem'}}><CircularProgress /></div>}
			renderError={() => <></>}
			data-ted="checklist-list"
		>
			<ListWrapper>
				{
					[<ListRow key='__create' title="Create Blank Checklist" onClick={onCreateChecklist} />]
					.concat(_checklistTemplates
						.map((item) => (
							<ChecklistTemplateListRow
								key={item.uuid}
								checklistTemplate={item}
							/>
						))
					)
				}
			</ListWrapper>
		</QueryStatusWrapper>
	);
}
