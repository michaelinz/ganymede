import React from 'react';
import ChecklistService from 'src/models/service/checklistService/ChecklistService';
import { IChecklist } from '@mccarthyfinch/public-api-client';
import ChecklistTemplateListView from './ChecklistTemplateListView';
import ChecklistTemplateService from 'src/models/service/checklistService/ChecklistTemplateService';
import useFileUUID from '../../hooks/useFileUUID';


export default function ChecklistTemplateList() {
	const [create] = ChecklistService.useCreateChecklist();
	const [, fileUUID, setFileUUID] = useFileUUID();
	const checklistTemplates = ChecklistTemplateService.useAllChecklistTemplates();

	const createChecklist = async (checklist: IChecklist) => {
		const createdChecklist = await create({
			fileUUID,
			...checklist,
		});
		if (createdChecklist.fileUUID) {
			setFileUUID(createdChecklist.fileUUID);
		} else {
			console.warn('create checklist file uuid was null?');
		}
		return createdChecklist;
	};

	return (
		<ChecklistTemplateListView
			createChecklist={createChecklist}
			checklistTemplates={checklistTemplates.data}
			queryResult={checklistTemplates}
		/>
	);
}
