import React, { useState } from 'react';
import { IChecklistTemplate, PartialIChecklistTemplate } from '@mccarthyfinch/public-api-client';
import { ListRow, MenuItem } from '../../components/DashboardList';
import Modal from '../../components/Modal';
import format from 'date-fns/format';
import styled from 'styled-components';

interface IProps {
	checklistTemplate: IChecklistTemplate;
	deleteOperation: () => void;
	updateOperation: (updatedChecklistTemplate: PartialIChecklistTemplate) => void;
	startProcessingChecklistTemplate: () => Promise<void>;
}

export default function ChecklistTemplateListRowView({ checklistTemplate, deleteOperation, updateOperation, startProcessingChecklistTemplate }: IProps) {
	const [rename, setRename] = useState(false);
	const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);
	const [_name, setTitle] = useState(checklistTemplate.name);

	const onDeleteChecklistTemplate = async () => {
		setShowDeleteConfirmation(true);
	};

	const onDeleteConfirmation = async () => {
		await deleteOperation();
		setShowDeleteConfirmation(false);
	};

	const onDeleteCancel = () => {
		setShowDeleteConfirmation(false);
	};

	const renameMenuItemClickHandler = () => {
		setRename(true);
	};

	const renameInputBlurHandler = () => {
		saveRename();
		setRename(false);
	};

	const renameInputKeyDownHandler = (e: React.KeyboardEvent<HTMLInputElement>) => {
		if (e.key === 'Enter') {
			saveRename();
			setRename(false);
		}

		if (e.key === 'Escape') {
			setTitle(checklistTemplate.name);
			setRename(false);
		}
	};

	const saveRename = () => {
		if (checklistTemplate.name !== _name) {
			updateOperation({ name: _name });
		}
	};

	const renameInputChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
		setTitle(e.target.value);
	};

	const clickHandler = () => {
		// do something
		startProcessingChecklistTemplate();
	};

	return (
		<>
			{
				!rename ?
					<ListRow
						title={_name}
						subtitle={`Modified ${format(checklistTemplate.lastUpdatedAt, 'DD MMM YYYY')}`}
						onClick={clickHandler}
						menuItems={[
							<MenuItem key='Rename' onClick={renameMenuItemClickHandler}>Rename</MenuItem>,
							<MenuItem key='Delete' onClick={onDeleteChecklistTemplate}>Delete</MenuItem>
						]}
					/>
					:
					<ListRow
						title={<InputWrapper value={_name} onChange={renameInputChangeHandler} onBlur={renameInputBlurHandler} onKeyDown={renameInputKeyDownHandler} autoFocus />}
						subtitle={`Modified ${format(checklistTemplate.lastUpdatedAt, 'DD MMM YYYY')}`}
						onClick={clickHandler}
					/>
			}
			{
				showDeleteConfirmation ?
					<Modal
						titleText="Delete Checklist"
						bodyElement={
							<>
								<p>Are you sure you want to delete {checklistTemplate.name}.</p>
								<p>This will be <strong>permanently deleted</strong> for everybody in your organisation.</p>
							</>
						}
						okButtonText="Delete"
						ok={onDeleteConfirmation}
						cancel={onDeleteCancel}
					/>
					:
					null
			}
		</>
	);
}

const InputWrapper = styled.input`
	font-size: 1rem;
	font-weight: bold;
`;
