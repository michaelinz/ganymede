import { IChecklistTemplate, PartialIChecklistTemplate } from '@mccarthyfinch/public-api-client';
import React from 'react';
import ChecklistTemplateService from 'src/models/service/checklistService/ChecklistTemplateService';
import useProcessChecklistStore, { isProcessing } from 'src/models/service/process/useProcessChecklistStore';
import ChecklistTemplateListRowView from './ChecklistTemplateListRowView';

interface IProps {
	checklistTemplate: IChecklistTemplate;
}

export default function ChecklistTemplateListRow({ checklistTemplate }: IProps) {
	const [deleteApi] = ChecklistTemplateService.useDeleteChecklistTemplate(checklistTemplate.uuid);
	const [updateApi] = ChecklistTemplateService.useUpdateChecklistTemplate(checklistTemplate.uuid);
	const processChecklistStore = useProcessChecklistStore();

	const deleteOperation = async () => {
		await deleteApi();
	};

	const updateOperation = async (updatedChecklistTemplate: PartialIChecklistTemplate) => {
		await updateApi(updatedChecklistTemplate);
	};

	const startProcessingChecklistTemplate = async () => {
		if (!isProcessing(processChecklistStore.processingState)) {
			processChecklistStore.startProcessing(checklistTemplate?.uuid);
		}
	};

	return (
		<ChecklistTemplateListRowView
			checklistTemplate={checklistTemplate}
			deleteOperation={deleteOperation}
			updateOperation={updateOperation}
			startProcessingChecklistTemplate={startProcessingChecklistTemplate}
		/>
	);
}
