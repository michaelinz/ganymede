import { Heading, Paragraph } from '@mccarthyfinch/author-ui-components';
import React from 'react';
import { ProcessingState } from 'src/models/service/process/useProcessChecklistStore';
import CircularProgress from 'src/components/CircularProgress';
import styled from 'styled-components';

export default function ProcessingView({ processingState }: { processingState: ProcessingState }) {
	const getMessage = () => {
		switch (processingState) {
			case ProcessingState.Fetching:
				return 'Fetching results...';
			case ProcessingState.Processing:
				return 'Processing document...';
			case ProcessingState.Uploading:
				return 'Uploading document...';
			case ProcessingState.Highlighting:
				return 'Highlighting document...';
			default:
				return '';
		}
	};
	return (
		<Wrapper>
			<CircularProgress />
			<Heading fontWeight='bold'>One moment</Heading>
			<Paragraph>{getMessage()}</Paragraph>
		</Wrapper>
	);
}

const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	margin-top: 3rem;
	align-items: center;

	& > * {
		margin-bottom: 0.5rem;
	}
`;
