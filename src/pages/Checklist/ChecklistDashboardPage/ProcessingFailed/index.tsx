import React from 'react';
import { Paragraph, Heading } from '@mccarthyfinch/author-ui-components';
import failure from './img-failed-processing.svg';
import styled from 'styled-components';
import Zelda from 'src/components/Zelda';
import { SecondaryButton } from 'src/components/molecules/Buttons';

interface IProcessingFailed {
	onOK: () => void;
}

export default function ProcessingFailed({ onOK }: IProcessingFailed) {
	return (
		<Wrapper>
			<Heading fontWeight='bold'>Document processing failed</Heading>
			<img src={failure} />
			<Paragraph>
				This document was unable to process.<br />
				Try reopening the checklist or <Zelda to={'https://support.mccarthyfinch.com/hc/en-us/requests/new'}>report a bug</Zelda>
			</Paragraph>
			<SecondaryButton onClick={onOK}>OK</SecondaryButton>
		</Wrapper>
	);
}

const Wrapper = styled.div`
	text-align: center;
	padding: 1rem;
	width: 100%;
	display: flex;
	flex-direction: column;
	align-items: center;

	a {
		color: black;
		text-decoration: underline;
		font-weight: ${({ theme }) => theme.fontWeight.bold};
	}

	& > * {
		margin: 1rem 0;
	}
`;
