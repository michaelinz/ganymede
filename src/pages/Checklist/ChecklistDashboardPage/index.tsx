import { faHome } from '@fortawesome/pro-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Annotation, Heading } from '@mccarthyfinch/author-ui-components';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import Header, { HeaderColumn, PrimaryHeaderBar } from 'src/components/Layout/Header';
import { Content, PageWrapper } from 'src/components/Layout/Page';
import LoadingOverlay from 'src/components/LoadingOverlay';
import Zelda from 'src/components/Zelda';
import useProcessChecklistStore, { isProcessing, ProcessingState } from 'src/models/service/process/useProcessChecklistStore';
import { AppRoutes } from 'src/pages/App/AppRouter';
import checklistDashboardBackground from '../../../../public/assets/checklist/img-checklist-dashboard.svg';
import { getChecklistEditorURL } from '../ChecklistRouter';
import useFileUUID from '../hooks/useFileUUID';
import ChecklistList from './ChecklistList';
import ChecklistTemplateList from './ChecklistTemplateList';
import ProcessingFailed from './ProcessingFailed';
import ProcessingView from './ProcessingView';
import { Section } from './styles';

export default function ChecklistDashboard() {
	const [isLoadingFileUUID, fileUUID] = useFileUUID();
	const history = useHistory();
	const processChecklistStore = useProcessChecklistStore();
	const [showError, setShowError] = useState(false); // TODO: use routing instead?

	useEffect(() => {
		if (processChecklistStore?.shouldRedirectToChecklistOnComplete) {
			if (processChecklistStore?.processingState === ProcessingState.Error) {
				processChecklistStore?.setShouldRedirectToChecklistOnComplete(false);
				setShowError(true);
			}
			if (processChecklistStore?.processingState === ProcessingState.Complete) {
				processChecklistStore?.setShouldRedirectToChecklistOnComplete(false);
				history.push(getChecklistEditorURL(processChecklistStore?.checklistUUID));
				return;
			}
		}
	}, [processChecklistStore?.processingState]);


	if (isLoadingFileUUID) {
		return <LoadingOverlay />;
	}
	return (
		<PageWrapper>
			<Header background={checklistDashboardBackground}>
				<PrimaryHeaderBar>
					<HeaderColumn>
						<Zelda to={AppRoutes.Dashboard.getHref()}>
							<FontAwesomeIcon icon={faHome} />
						</Zelda>
					</HeaderColumn>
					<HeaderColumn align='center'>
						<Heading>Checklist</Heading>
					</HeaderColumn>
					<HeaderColumn align='right'>
						{/* Will eventually show what's new when we update the app
						<Zelda to=''>
							<FontAwesomeIcon icon={faGift} />
						</Zelda>
						*/}
						{/* <Zelda to={ChecklistRoutes.ChecklistDashboard.getHref()}>
							<FontAwesomeIcon icon={faThLarge} />
						</Zelda> */}
					</HeaderColumn>
				</PrimaryHeaderBar>
				<>{/* Placeholder for the checklist search */}</>
			</Header>

			<Content>
				{
					showError ?
					<ProcessingFailed onOK={() => setShowError(false)} />
					:
						(
							isProcessing(processChecklistStore?.processingState) ?
							<ProcessingView processingState={processChecklistStore?.processingState} /> :
							<Lists fileUUID={fileUUID} />
						)
				}

			</Content>
		</PageWrapper>
	);
}

function Lists({ fileUUID }: { fileUUID: string }) {
	return (
		<Section>
			{fileUUID && (
				<Section>
					<Annotation fontWeight="bold" color="fontSecondary">
						THIS DOCUMENT
							</Annotation>
					<ChecklistList fileUUID={fileUUID} />
				</Section>
			)}
			<Section>
				{fileUUID && (
					<Annotation fontWeight="bold" color="fontSecondary">
						TEMPLATES
					</Annotation>
				)}
				<ChecklistTemplateList />
			</Section>
		</Section>
	);
}
