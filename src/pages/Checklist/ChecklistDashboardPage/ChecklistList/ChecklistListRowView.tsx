import React, { useState } from 'react';
import { IChecklist, PartialIChecklist } from '@mccarthyfinch/public-api-client';
import { ListRow, MenuItem } from '../../components/DashboardList';
import Modal from '../../components/Modal';
import format from 'date-fns/format';
import styled from 'styled-components';
import { getChecklistEditorURL } from '../../ChecklistRouter';
import { useHistory } from 'react-router-dom';

interface IProps {
	checklist: IChecklist,
	updateOperation: (updatedChecklist: PartialIChecklist) => void
}

export default function ChecklistListRowView({ checklist, updateOperation }: IProps) {
	const history = useHistory();
	const [rename, setRename] = useState(false);
	const [showDeleteConfirmation, setShowDeleteConfirmation] = useState(false);
	const [_name, setTitle] = useState(checklist.name);

	const deleteMenuItemClickHandler = (e) => {
		e.stopPropagation();
		setShowDeleteConfirmation(true);
	}

	const onDeleteConfirmation = async () => {
		updateOperation({ isActive: false });
		setShowDeleteConfirmation(false);
	}

	const onDeleteCancel = () => {
		setShowDeleteConfirmation(false);
	}

	const renameMenuItemClickHandler = (e) => {
		e.stopPropagation();
		setRename(true);
	}

	const renameInputBlurHandler = () => {
		saveRename();
		setRename(false);
	}

	const renameInputKeyDownHandler = (e: React.KeyboardEvent<HTMLInputElement>) => {
		if (e.key === 'Enter') {
			saveRename();
			setRename(false);
		}

		if (e.key === 'Escape') {
			setTitle(checklist.name);
			setRename(false);
		}
	}

	const saveRename = () => {
		if (checklist.name !== _name) {
			updateOperation({ name: _name });
		}
	}

	const renameInputChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
		setTitle(e.target.value);
	}

	const checklistClickHandler = () => {
		history.push(getChecklistEditorURL(checklist.uuid));		
	}

	return (
		<>
			{
				!rename ?
					<ListRow
						data-ted={checklist.uuid}
						title={_name}
						subtitle={`Modified ${format(checklist.lastUpdatedAt, 'DD MMM YYYY')}`}
						onClick={checklistClickHandler}
						menuItems={[
							<MenuItem key='Rename' onClick={renameMenuItemClickHandler}>Rename</MenuItem>,
							<MenuItem key='Delete' onClick={deleteMenuItemClickHandler}>Delete</MenuItem>
						]}
					/>
					:
					<ListRow
						data-ted={checklist.uuid}
						title={<InputWrapper value={_name} onChange={renameInputChangeHandler} onBlur={renameInputBlurHandler} onKeyDown={renameInputKeyDownHandler} autoFocus />}
						subtitle={`Modified ${format(checklist.lastUpdatedAt, 'DD MMM YYYY')}`}
						onClick={checklistClickHandler}
					/>
			}
			{
				showDeleteConfirmation ?
					<Modal
						titleText="Delete Checklist"
						bodyElement={
							<>
								<p>Are you sure you want to delete {checklist.name}.</p>
								<p>This will be <strong>permanently deleted</strong> for everybody in your organisation.</p>
							</>
						}
						okButtonText="Delete"
						ok={onDeleteConfirmation}
						cancel={onDeleteCancel}
					/>
					:
					null
			}
		</>
	);
}

const InputWrapper = styled.input`
	font-size: 1rem;
	font-weight: bold;
`;