import { IChecklist } from '@mccarthyfinch/public-api-client';
import React from 'react';
import ChecklistService from 'src/models/service/checklistService/ChecklistService';
import ChecklistListView from './ChecklistListView';

function ChecklistList({ fileUUID }: { fileUUID: string }) {
	const checklists = ChecklistService.useChecklists(fileUUID);

	return <ChecklistListView checklists={checklists.data} queryResult={checklists} />;
}

export default ChecklistList;
