import { IChecklist, PartialIChecklist } from '@mccarthyfinch/public-api-client';
import compareDesc from 'date-fns/compare_desc';
import React from 'react';
import { UseQueryResult } from 'react-query';
import CircularProgress from 'src/components/CircularProgress';
import QueryStatusWrapper from 'src/components/QueryStatusWrapper';
import { ListWrapper } from '../../components/DashboardList';
import ChecklistListRow from './ChecklistListRow';
export interface IChecklistListViewProps {
	checklists: IChecklist[];
	queryResult: UseQueryResult<IChecklist[], any>;
}

const ChecklistListView = ({ checklists, queryResult }: IChecklistListViewProps) => {
	const _checklists = checklists?.filter((item) => item.uuid && item.isActive)
		.sort((a, b) => compareDesc(a.lastUpdatedAt, b.lastUpdatedAt)) || [];

	return (
		<QueryStatusWrapper
			queryResults={[queryResult]}
			renderLoading={() => <div style={{padding: '1rem'}}><CircularProgress /></div>}
			renderError={() => <></>}
			data-ted="checklist-list"
		>
			<ListWrapper>
				{_checklists
					.map((item) => (
						<ChecklistListRow
							key={item.uuid}
							checklist={item}
						/>
					))}
			</ListWrapper>
		</QueryStatusWrapper>
	);
};

export default ChecklistListView;
