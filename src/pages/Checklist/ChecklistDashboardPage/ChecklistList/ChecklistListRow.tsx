import React from 'react';
import { IChecklist, PartialIChecklist } from '@mccarthyfinch/public-api-client';
import ChecklistService from 'src/models/service/checklistService/ChecklistService';
import ChecklistListRowView from './ChecklistListRowView'

interface IProps {
	checklist: IChecklist
}

export default function ChecklistListRow({ checklist }: IProps) {
	const [updateApi] = ChecklistService.useUpdateChecklist(checklist.uuid);

	const updateOperation = async (updatedChecklist: PartialIChecklist) => {
		await updateApi(updatedChecklist);
	}

	return (
		<ChecklistListRowView
			checklist={checklist}
			updateOperation={updateOperation}
		/>
	);
}