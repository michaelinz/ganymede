import React, { Suspense } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import { ProcessChecklistStoreProvider } from 'src/models/service/process/useProcessChecklistStore';
import LoadingOverlay from '../../components/LoadingOverlay';
import ChecklistRouter from './ChecklistRouter';
import composeWrappers from 'src/utils/composeWrappers';

// https://projects.invisionapp.com/share/82ZEFNDJ3TB#/screens/437214845

const ContextProviders = composeWrappers([ProcessChecklistStoreProvider]);
export default function Checklist() {
	const match = useRouteMatch();
	return (
		<ContextProviders>
			<Suspense fallback={<LoadingOverlay />}>
				<Switch>
					{Object.values(ChecklistRouter.childRoutes).map((siteUrl) => {
						return <Route key={siteUrl.path} path={`${match?.url || ''}${siteUrl.path}`} component={siteUrl.component} exact={siteUrl.path === '/'} />;
					})}
				</Switch>
			</Suspense>
		</ContextProviders>
	);
}
