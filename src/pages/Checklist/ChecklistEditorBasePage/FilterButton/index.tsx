import React, { ReactEventHandler } from 'react';
import { ButtonWrapper } from './styles';

interface FilterButtonProps {
	selected?: boolean;
	disabled?: boolean;
	onClick?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
	children: React.ReactChild;
}

export default function FilterButton({ selected, disabled, children, onClick }: FilterButtonProps) {
	return (
		<ButtonWrapper selected={selected} disabled={disabled} onClick={disabled ? () => {} : onClick}>
			<span>{children}</span>
		</ButtonWrapper>
	);
}
