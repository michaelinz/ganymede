import styled from 'styled-components';

export const ButtonWrapper = styled.button<{ selected: boolean; disabled: boolean }>`
	background-color: white;
	border-radius: 0.875rem;
	text-align: center;
	padding: 0.375rem 0.75rem;
	color: ${({ theme, selected, disabled }) => {
		if (disabled) { return theme.colorPalette.snow[600]; }
		if (selected) { return theme.color.fontPrimary; }
		return theme.colorPalette.smoke[600];
	}};
	font-weight: ${({ theme }) => theme.fontWeight.bold};
	border: none;
	font-size: ${({ theme }) => theme.typography.caption.fontSize};

	:not(:disabled) {
		cursor: pointer;
		box-shadow: 0 0 4px 0 rgba(34, 34, 34, 0.4);
	}

	:active:not(:disabled) {
		box-shadow: none;
	}

	:hover:not(:disabled) {
		> span {
			opacity: 0.7;
			transition: all 0.3s;
		}
	}
`;
