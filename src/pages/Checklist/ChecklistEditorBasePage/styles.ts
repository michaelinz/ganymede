import styled from 'styled-components';

export const Heading = styled.div`
	background: white;
	min-width: 6.6rem;
	border-radius: 1rem;
	padding: 0.5rem 1rem;
	box-shadow: 0 0 4px 0 rgba(34, 34, 34, 0.4);

	span[role='input'] {
		color: black;
		font-family: ${({ theme }) => theme.fontFamily};
		font-weight: ${({ theme }) => theme.fontWeight.bold};
		font-size: ${({ theme }) => theme.typography.caption.fontSize};
		line-height: ${({ theme }) => theme.typography.caption.lineHeight};
	}
`;
