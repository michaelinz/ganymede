import React, { Suspense, useEffect, useState } from 'react';
import { observer } from 'mobx-react';
import { Route, Switch, useLocation, useParams, useRouteMatch } from 'react-router-dom';
import { ChecklistEditorRoutes, ChecklistRoutes } from '../ChecklistRouter';
import { Content, PageWrapper } from 'src/components/Layout/Page';
import Header, { PrimaryHeaderBar, SecondaryHeaderBar, HeaderColumn } from 'src/components/Layout/Header';
import UnderlineTextInput from 'src/components/molecules/UnderlineTextInput';
import LoadingOverlay from 'src/components/LoadingOverlay';
import ChecklistService from 'src/models/service/checklistService/ChecklistService';
import Zelda from 'src/components/Zelda';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faThLarge, faHighlighter, faHome, faBook } from '@fortawesome/pro-solid-svg-icons';
import FilterButton from './FilterButton';
import { useFilter } from '../hooks/useFilters';
import { QuickFilter } from 'src/models/store/UIStore';
import checklistBackground from '../../../../public/assets/checklist/img-checklist-dashboard.svg';
import { Heading } from './styles';
import IssueCategoryService from 'src/models/service/checklistService/IssueCategoryService';
import IssueService from 'src/models/service/checklistService/IssueService';
import useChecklistUUID from '../hooks/useChecklistUUID';
import useDocumentUUID from '../hooks/useDocumentUUID';
import { AppRoutes } from 'src/pages/App/AppRouter';
import HighlightsShelf from 'src/pages/Highlights/HighlightsShelf';
import ReferenceShelf from 'src/pages/Highlights/ReferenceShelf';
import ComparisonShelf from 'src/modules/ClauseLibrary/ComparisonShelf';
import { IconButtonBorderless } from 'src/components/molecules/Buttons';
import { ClauseLibraryUIView, useComparisonShelfUI, ComparisonShelfUIProvider } from 'src/modules/ClauseLibrary/useClauseLibraryUI';

// TODO: this page needs to have things split up better to reduce re-renders
const ChecklistPageContainer = () => {
	const match = useRouteMatch();
	const location = useLocation();
	const [quickFilters, setQuickFilters] = useFilter('quick');
	const [showHighlightsShelf, setShowHighlightsShelf] = useState(false);
	const { checklistUUID } = useParams<{ checklistUUID: string }>();
	const comparisonShelf = useComparisonShelfUI();

	// Set the checklistUUID as a setting on the document so we re-open the last opened checklist
	const [isLoadingChecklistUUID, settingsChecklistUUID, setSettingsChecklistUUID ] = useChecklistUUID();
	useEffect(() => {
		if (!isLoadingChecklistUUID && settingsChecklistUUID !== checklistUUID) {
			setSettingsChecklistUUID(checklistUUID);
			console.log('Set checklistUUID on document settings', checklistUUID);
		}
	}, [isLoadingChecklistUUID]);

	const { data: checklist } = ChecklistService.useChecklist(checklistUUID);
	const { data: issueCategories} = IssueCategoryService.useIssueCategoriesForChecklist(checklistUUID);
	const { data: issues } = IssueService.useIssuesForChecklist(checklistUUID);
	const [update] = ChecklistService.useUpdateChecklist(checklistUUID);

	// Set the documentUUID as a setting on the document
	const [isLoadingSettingsDocumentUUID, settingsDocumentUUID, setSettingsDocumentUUID ] = useDocumentUUID();
	useEffect(() => {
		if (!isLoadingSettingsDocumentUUID && checklist?.documentUUID && (settingsDocumentUUID !== checklist?.documentUUID)) {
				setSettingsDocumentUUID(checklist.documentUUID);
		}
	}, [isLoadingSettingsDocumentUUID, checklist?.documentUUID]);

	const toggleQuickFilter = (filter: QuickFilter) => {
		// DONE and TODO cannot be selected simultaenously
		if (quickFilters.includes(filter)) {
			setQuickFilters(quickFilters.filter(qf => qf !== filter));
			return;
		}
		if (filter === QuickFilter.DONE && quickFilters.includes(QuickFilter.TODO)) {
			setQuickFilters(quickFilters.filter(qf => qf !== QuickFilter.TODO).concat(filter));
			return;
		}
		if (filter === QuickFilter.TODO && quickFilters.includes(QuickFilter.DONE)) {
			setQuickFilters(quickFilters.filter(qf => qf !== QuickFilter.DONE).concat(filter));
			return;
		}
		setQuickFilters(quickFilters.concat(filter));
	};

	const getFilterButtonProps = (filter: QuickFilter) => ({
		selected: quickFilters.includes(filter),
		disabled: !issueCategories?.length && !issues?.length,
		onClick: () => toggleQuickFilter(filter),
	});

	const onComparisonShelfClick = () => {
		if (comparisonShelf?.filters?.filters?.clauseUUIDs?.length) {
			comparisonShelf?.filters?.setFilters({
				...comparisonShelf?.filters?.filters,
				clauseUUIDs: [],
			});
		}
		comparisonShelf?.view?.setClauseLibraryUIView(ClauseLibraryUIView.Browse, true);
	};

	// TODO: Move shelves and their toggles separately to reduce re-rendering the rest of the content on toggle
	return (
		<PageWrapper>
			{
				checklist?.documentUUID &&
				<ReferenceShelf documentUUID={checklist?.documentUUID} />
			}
			<ComparisonShelf />
			<HighlightsShelf isOpen={showHighlightsShelf} onDismiss={() => setShowHighlightsShelf(!showHighlightsShelf)} />
			<Header background={checklistBackground}>
				<PrimaryHeaderBar>
					<HeaderColumn>
						<Heading>
							{checklist && (
								<UnderlineTextInput
									value={checklist?.name || ''}
									onChange={(e) => update({ uuid: checklist?.uuid, name: e.target.value })}
								/>
							)}
						</Heading>
					</HeaderColumn>
					<HeaderColumn align='right'>
						<Zelda to={AppRoutes.Dashboard.getHref()}>
							<FontAwesomeIcon icon={faHome} />
						</Zelda>
						<IconButtonBorderless onClick={() => setShowHighlightsShelf(!showHighlightsShelf)}>
							<FontAwesomeIcon icon={faHighlighter} />
						</IconButtonBorderless>
						<IconButtonBorderless onClick={onComparisonShelfClick} >
							<FontAwesomeIcon icon={faBook} />
						</IconButtonBorderless>
						<Zelda to={ChecklistRoutes.ChecklistDashboard.getHref()}>
							<FontAwesomeIcon icon={faThLarge} />
						</Zelda>
					</HeaderColumn>
				</PrimaryHeaderBar>

				{!location.pathname.includes('issue') && (
					<SecondaryHeaderBar>
						<FilterButton {...getFilterButtonProps(QuickFilter.TODO)}>To do</FilterButton>
						{/* <FilterButton {...getFilterButtonProps(QuickFilter.MINE)}>Mine</FilterButton> */}
						<FilterButton {...getFilterButtonProps(QuickFilter.IMPORTANT)}>Important</FilterButton>
						<FilterButton {...getFilterButtonProps(QuickFilter.DONE)}>Done</FilterButton>
					</SecondaryHeaderBar>
				)}
			</Header>

			<Content style={{ backgroundColor: '#F9F9F9', minHeight: 0 }}>
				<Suspense fallback={<LoadingOverlay />}>
					<Switch>
						{Object.values(ChecklistEditorRoutes).map((siteUrl) => {
							return <Route key={siteUrl.path} path={`${match?.path || ''}${siteUrl.path}`} component={siteUrl.component} exact={siteUrl.path === '/'} />;
						})}
					</Switch>
				</Suspense>
			</Content>
		</PageWrapper>
	);
};


function ChecklistPage() {
	return (
		<ComparisonShelfUIProvider>
			<ChecklistPageContainer />
		</ComparisonShelfUIProvider>
	);
}

export default observer(ChecklistPage);
