import styled from 'styled-components';

export const PageWrapper = styled.div`
	display: flex;
	flex-direction: column;
	align-items: stretch;
	flex: 1 0;
	width: 100%;
	height: 100%;
`;
