import uuid from 'uuid/v4';
import { getBlankFormattedText } from '@mccarthyfinch/author-ui-components';
import { IIssue, IIssueCategory } from '@mccarthyfinch/public-api-client';

export function getBlankPosition() {
	return {
		uuid: uuid(),
		name: '',
		content: {
			plainText: '',
			formattedText: JSON.stringify(getBlankFormattedText()),
		},
		isUsed: false,
		isActive: true,
	};
}

export function getBlankIssueCategory(category: Partial<IIssueCategory> & Pick<IIssueCategory, 'checklistUUID' | 'rank'>): IIssueCategory {
	return {
		uuid: uuid(),
		name: '',
		rank: 0,
		isActive: true,
		...category,
	};
}

export function getBlankIssue(issue: Partial<IIssue> & Pick<IIssue, 'checklistUUID' | 'rank'>): IIssue {
	return {
		uuid: uuid(),
		name: '',
		rank: 0,
		isActive: true,
		isChecked: false,
		...issue,
	};
}

