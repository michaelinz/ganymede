import React, { useEffect, useState } from 'react';
import { getEditor } from 'src/editor/EditorFactory';

export default function useChecklistUUID(): [boolean, string, React.Dispatch<React.SetStateAction<string>>] {
	const [checklistUUID, _setChecklistUUID] = useState<string>();
	const [isLoading, setIsLoading] = useState(true);
	const editor = getEditor();
	useEffect(() => {
		const getChecklistUUID = async () => {
			const checklistUUID = await editor.Settings.getChecklistUUID();
			if (checklistUUID) {
				return checklistUUID;
			}
			console.log('No checklist uuid found');
			return undefined;
		};
		getChecklistUUID().then((checklistUUID) => {
			if (checklistUUID) {
				setChecklistUUID(checklistUUID);
			}
			setIsLoading(false);
		});
	}, []);

	const setChecklistUUID = (checklistUUID: string) => {
		editor.Settings.setChecklistUUID(checklistUUID);
		return _setChecklistUUID(checklistUUID);
	};
	return [isLoading, checklistUUID, setChecklistUUID];
}
