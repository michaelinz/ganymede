import React, { useEffect, useState } from 'react';
import { getEditor } from 'src/editor/EditorFactory';
import useLastProcessed from './useLastProcessed';

export default function useDocumentUUID(): [boolean, string, React.Dispatch<React.SetStateAction<string>>] {
	const [documentUUID, _setDocumentUUID] = useState<string>();
	const [isLoading, setIsLoading] = useState(true);

	const [ isLoadingLastProcessed, lastProcessed, checkLastProcessed ] = useLastProcessed();
	const editor = getEditor();

	useEffect(() => {
		if (!isLoadingLastProcessed) {
			_setDocumentUUID(lastProcessed.documentUUID);
			setIsLoading(false);
		}
	}, [ isLoadingLastProcessed ]);

	const setDocumentUUID = (documentUUID: string, setLastProcessedTime = true) => {
		editor.Settings.setDocumentUUID(documentUUID);
		if (documentUUID && setLastProcessedTime) {
			editor.Settings.setLastProcessedDocumentUUIDChecklist(new Date());
		}
		checkLastProcessed();
		return _setDocumentUUID(documentUUID);
	};
	return [isLoading, documentUUID, setDocumentUUID, ];
}
