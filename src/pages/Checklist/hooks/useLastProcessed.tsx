import React, { useEffect, useState } from 'react';
import Editor from 'src/editor/base/Editor';
import { getEditor } from 'src/editor/EditorFactory';


interface ILastProcessed {
	app: App;
	time?: number; // unix
	documentUUID?: string;
}

export default function useLastProcessed(): [ boolean, ILastProcessed, () => Promise<void> ] {
	const [lastProcessed, _setLastProcessed] = useState<ILastProcessed>();
	const [isLoading, setIsLoading] = useState(true);
	const editor = getEditor();

	const checkLastProcessed = async () => {
		const result = await getLastProcessed(editor);
		if (result) {
			_setLastProcessed(result);
		}
	};

	useEffect(() => {
		checkLastProcessed().then(() => {
			setIsLoading(false);
		});
	}, []);

	return [ isLoading, lastProcessed, checkLastProcessed ];
}


export enum App {
	APPROVE = 'approve',
	CHECKLIST = 'checklist',
	NONE = 'none',
}

async function getLastProcessed(editor: Editor): Promise<ILastProcessed> {
	// checklistLastProcessed is set as 1 since we prefer checklist over approve
	const [ checklistDocumentUUID, checklistLastProcessed = 1, approveDocumentUUID, approveLastProcessed = 0 ] = await Promise.all([
		editor.Settings.getDocumentUUID(),
		editor.Settings.getLastProcessedTimeChecklist(),

		editor.Settings.getLastProcessedDocumentUUIDApprove(),
		editor.Settings.getLastProcessedTimeApprove(),
	]);

	const getLastProcessedApp = () => {
		const apps: ILastProcessed[] = [
			{
				app: App.CHECKLIST,
				documentUUID: checklistDocumentUUID,
				time: checklistLastProcessed,
			},
			{
				app: App.APPROVE,
				documentUUID: approveDocumentUUID,
				time: approveLastProcessed,
			}
		];
		const processedApps = apps.filter(app => app.documentUUID).sort((a, b) => a.time - b.time);
		if (processedApps.length) {
			return processedApps[0];
		}
		const noneApp: ILastProcessed = {
			app: App.NONE,
		};
		return noneApp;
	};

	return getLastProcessedApp();
}
