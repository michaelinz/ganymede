import React from 'react';
import { useUIStore, Filters, QuickFilter, defaultFilterState } from 'src/models/store/UIStore';


type FilterType = keyof Filters;


export function useFilters(): [Filters, React.Dispatch<React.SetStateAction<Filters>>, boolean] {
	const uiStore = useUIStore();

	const filters = uiStore.filters;
	const setFilters = uiStore.setFilters;
	const isLoading = !filters;

	return [filters, setFilters, isLoading];
}


export function useFilter<T extends FilterType>(type?: T): [Filters[T], React.Dispatch<React.SetStateAction<Filters[T]>>, boolean] {
	const [filters, setFilters, isLoading] = useFilters();

	const setFilter = (filterValue: Filters[T]) => {
		const newFilters = {
			...filters,
			[type]: filterValue,
		};
		setFilters(newFilters);
	};

	return [filters[type], setFilter, isLoading];
}


export function useFilterOverallState(): [boolean, () => void] {
	const [filters, setFilters, isLoading] = useFilters();

	// True if filters aren't set to the defaults
	const areFiltersEnabled = !isLoading
		&& filters.quick.length !== defaultFilterState.quick.length;

	// Set filters to defaults
	const removeAllFilters = () => {
		setFilters(defaultFilterState);
	};

	return [areFiltersEnabled, removeAllFilters];
}


export function useFilterChangeOnCreateItem(): [(itemType: 'Issue' | 'IssueCategory') => void] {
	const [_filters, setFilters, isLoading] = useFilters();

	const changeFiltersOnCreateItem = (itemType: 'Issue' | 'IssueCategory') => {
		// Can't change filters before they've loaded
		if (isLoading) { return; }

		let filters = { ..._filters };

		switch (itemType) {
			case 'Issue':
				// Remove done and important filter (as new issues or categories will never be important or done and will thus disappear)
				filters.quick = filters.quick.filter(qf => ![QuickFilter.DONE, QuickFilter.IMPORTANT].includes(qf));
				break;

			case 'IssueCategory':
				// Remove todo filter for categories (as categories will never be todo-able and will thus disappear)
				filters.quick = filters.quick.filter(qf => ![QuickFilter.DONE, QuickFilter.IMPORTANT, QuickFilter.TODO].includes(qf));
				break;

			default:
		}

		setFilters(filters);
	};

	return [changeFiltersOnCreateItem];
}
