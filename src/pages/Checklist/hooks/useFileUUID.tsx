import React, { useEffect, useState } from 'react';
import { getEditor } from 'src/editor/EditorFactory';

export default function useFileUUID(): [boolean, string, React.Dispatch<React.SetStateAction<string>>] {
	const [fileUUID, _setFileUUID] = useState<string>();
	const [isLoading, setIsLoading] = useState(true);
	const editor = getEditor();
	useEffect(() => {
		const getFileUUID = async () => {
			const fileUUID = await editor.Settings.getFileUUID();
			if (fileUUID) {
				return fileUUID;
			}
			console.log('No file uuid found');
			return undefined;
		};
		getFileUUID().then((fileUUID) => {
			if (fileUUID) {
				setFileUUID(fileUUID);
			}
			setIsLoading(false);
		});
	}, []);

	const setFileUUID = (fileUUID: string) => {
		editor.Settings.setFileUUID(fileUUID);
		return _setFileUUID(fileUUID);
	};
	return [isLoading, fileUUID, setFileUUID];
}
