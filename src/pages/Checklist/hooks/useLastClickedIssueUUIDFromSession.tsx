import React, { useEffect, useState } from 'react';
import SessionStorage from 'src/models/utils/appState/sessionStorage';

export default function useLastClickedIssueUUIDFromSession(): [boolean, string, React.Dispatch<React.SetStateAction<string>>] {
	const [issueUUID, _setIssueUUID] = useState<string>();
	const [isLoading, setIsLoading] = useState(true);

	const SESSION_LAST_ISSUE_CLICKED_UUID_KEY = 'last_clicked_issue_uuid';

	useEffect(() => {
		const getIssueUUIDFromSession = async () => {
			const issueUUID = await SessionStorage.getAll<string>(SESSION_LAST_ISSUE_CLICKED_UUID_KEY);
			if (issueUUID) {
				return issueUUID;
			}
			console.log('No issue uuid found');
			return undefined;
		};
		getIssueUUIDFromSession().then((issueUUID) => {
			if (issueUUID) {
				setIssueUUIDToSession(issueUUID);
			}
			setIsLoading(false);
		});
	}, []);

	const setIssueUUIDToSession = (issueUUID: string) => {
		SessionStorage.setAll(SESSION_LAST_ISSUE_CLICKED_UUID_KEY, issueUUID);
		return _setIssueUUID(issueUUID);
	};
    
	return [isLoading, issueUUID, setIssueUUIDToSession];
}