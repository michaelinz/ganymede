import React from 'react';
import styled, { css } from 'styled-components';
import { Row, Column } from 'src/components/Layout';
import { Subtitle, Caption } from '@mccarthyfinch/author-ui-components';
import Zelda from 'src/components/Zelda';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle, faEllipsisV } from '@fortawesome/pro-regular-svg-icons';

interface IListRow {
	thumbnail?: React.ReactChild;
	title: string | React.ReactElement;
	subtitle?: string;
	onClick?: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
	href?: string;
	menuItems?: React.ReactElement[];
}

export function ListRow({ href, ...rest }: IListRow) {
	if (href) {
		return (
			<Zelda to={href}>
				<InnerListRow enableOnHoverEffect={true} {...rest} />
			</Zelda>
		);
	}
	return <InnerListRow enableOnHoverEffect={Boolean(rest.onClick)} {...rest} />;
}

function InnerListRow({ thumbnail, title, subtitle, onClick, enableOnHoverEffect, menuItems }: IListRow & { enableOnHoverEffect: boolean }) {
	return (
		<ListRowWrapper enableOnHoverEffect={enableOnHoverEffect} onClick={onClick}>
			<Thumbnail>{thumbnail || <FontAwesomeIcon icon={faCheckCircle} />}</Thumbnail>
			<Column>
				{
					typeof(title) === 'string' ?
						<Subtitle fontWeight="bold">{title}</Subtitle>
				 		:
				 		title
				}
				<Caption color="fontSecondary">{subtitle}</Caption>
			</Column>
			{
				menuItems ?
					<>
						<MenuIcon><FontAwesomeIcon icon={faEllipsisV} /></MenuIcon>
						<Menu>
							{menuItems}
						</Menu>
					</>
					:
					null
			}
		</ListRowWrapper>
	);
}

export const ListWrapper = styled.div`
	display: flex;
	flex-direction: column;

	& > *:not(:last-child) {
		border-bottom: 1px solid ${({ theme }) => theme.color.border};
	}
`;

const ListRowWrapper = styled(Row)<{ enableOnHoverEffect: boolean }>`
	height: 4.62rem;
	width: 100%;
	padding: 0.375rem;
	transition: 0.3s all;

	${({ enableOnHoverEffect }) => {
		if (enableOnHoverEffect) {
			return css`
				cursor: pointer;

				&:hover {
					background-color: #f0f0f0;
				}
			`;
		}
	}}
`;

const Thumbnail = styled.div`
	height: 2.56rem;
	width: 2.56rem;
	border-radius: 0.75rem;
	margin-right: 1rem;
	flex: 0 0 2.56rem;
	box-shadow: 0 0 8px 0 rgba(34, 34, 34, 0.3);
	background: white;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;

	& > svg {
		font-size: 1.2rem;
	}
`;

const MenuIcon = styled.div`
	padding: 0.25rem;
	padding-left: 0.5rem;
	visibility: hidden;
	opacity: 0;
	transition: 0.3s all;

	*:hover > & {
		visibility: visible;
		opacity: 1;
	}
`;

const Menu = styled.div`
	position: absolute;
	background-color: #fff;
	right: 0;
	top: 2.5rem;
	z-index: 100;
	width: 5rem;
	visibility: hidden;
	opacity: 0;
	border-radius: 0.125rem;
	border: solid 0.125rem #e3e6eb;
	transition: 0.3s all;

	*:hover + &, &:hover {
		visibility: visible;
		opacity: 1;
	}
`;

export const MenuItem = styled.div`
	padding: 0.5rem;
	transition: 0.3s all;
	background-color: white;

	&:hover {
		background-color: #f0f0f0;
	}
`;
