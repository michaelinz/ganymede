import styled, { css, ThemeColor } from "styled-components";

interface IInputWrapper {
	color: ThemeColor;
	disabled: boolean;
	checked: boolean;
	size: React.ReactText;
	shadow?: boolean;
}

export const InputWrapper = styled.div<IInputWrapper>`
	padding: 0;
	margin: 0;
	border-radius: 50%;
	height: ${({ size = '1rem' }) => size};
	width: ${({ size = '1rem' }) => size};
	line-height: ${({ size = '1rem' }) => size};
	font-weight: ${({ theme }) => theme.fontWeight.light};
	color: ${({ theme, color, disabled }) => disabled ? theme.color.disabled : theme.color[color]};
	background-color: ${({ disabled, theme }) => disabled ? theme.color.background : 'transparent'};

	${({ shadow }) => shadow ? css`box-shadow: inset 0 3.5px 4px 0 rgba(164, 175, 191, 0.5);` : ''}

	flex-shrink: 0;
	display: flex;
	flex-direction: row;
	justify-content: center;
	align-items: center;
	opacity: 1;
	transition: 0.3s all;

	svg {
		height: ${({ size = '1rem' }) => size} !important;
		width: ${({ size = '1rem' }) => size} !important;
	}

	&:hover {
		${({ disabled }) => !disabled && css`cursor: pointer;`}
	}
`;
