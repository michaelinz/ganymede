import React from 'react';
import CustomCheckbox, { ICustomCheckboxProps } from 'src/components/CustomCheckbox';
import { PlaceholderCheckboxWrapper } from '../../ChecklistIssueEditorPage/DocumentReferenceSection/styles';

interface IPlaceholderCheckboxProps extends Omit<ICustomCheckboxProps, 'children'> {
}

const PlaceholderCheckbox = ({ disabled, ...props }: IPlaceholderCheckboxProps) => {
	const size = '0.875rem';

	return (
		<CustomCheckbox
			data-ted={'placeholder-isChecked'}
			style={{ marginRight: '0.5rem' }}
			disabled={true}
			checked={false}
			{...props}
		>
			{() => <PlaceholderCheckboxWrapper size={size} />}
		</CustomCheckbox>
	);
};

export default PlaceholderCheckbox;
