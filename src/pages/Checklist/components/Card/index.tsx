import styled from 'styled-components';

const Card = styled.div`
	border-radius: 0.75rem;
	border: solid 1px #bfc7d2;
	display: flex;
	flex-direction: column;

	background: white;

	min-width: 18rem;
	padding: 0.5rem;
`;

export default Card;
