import { IconButtonBorderless } from '@mccarthyfinch/author-ui-components';
import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBookmark } from '@fortawesome/pro-light-svg-icons';
import { faBookmark as faBookmarkSolid } from '@fortawesome/pro-solid-svg-icons';

interface IProps {
    isImportant: boolean;
    toggleIsImportant: ()=>Promise<void>;
    style?: React.CSSProperties;
}

export default function ImportantFlag({isImportant, toggleIsImportant, style}: IProps) {
    return (
        <IconButtonBorderless onClick={toggleIsImportant} style={style}>
            {
                isImportant ?
                    <FontAwesomeIcon icon={faBookmarkSolid} style={{ color: '#222222' }} />
                    :
                    <FontAwesomeIcon icon={faBookmark} />
            }
        </IconButtonBorderless>

    );
}
