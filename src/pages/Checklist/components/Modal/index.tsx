import React, { useRef } from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/pro-light-svg-icons';

interface IProps {
    titleText: string,
    bodyElement: React.ReactElement,
    okButtonText: string,
	cancel: () => void,
	ok: () => void
}

export default function Modal({ titleText, bodyElement, okButtonText, cancel, ok }: IProps) {
    const closeRef = useRef(null);
    const okButtonRef= useRef(null);

    const onBeginBlockFocusHandler = () => {
        okButtonRef.current.focus();
    };

    const onEndBlockFocusHandler = () => {
        closeRef.current.focus();
    };

	return (
		<>
			<ModalOverlay onClick={cancel} />
			<ModalDialog>
                <BlockFocus tabIndex={0} onFocus={onBeginBlockFocusHandler}/>
				<ModalClose onClick={cancel} tabIndex={0} ref={closeRef}>
					<FontAwesomeIcon icon={faTimes} style={{ height: "100%" }} />
				</ModalClose>
				<ModalTitle>
					<div>{titleText}</div>
				</ModalTitle>
				<ModalBody>
                    {bodyElement}
				</ModalBody>
				<ModalFooter>
					<CancelButton onClick={cancel} autoFocus>Cancel</CancelButton>
					<OkButton onClick={ok} ref={okButtonRef}>{okButtonText}</OkButton>
				</ModalFooter>
                <BlockFocus tabIndex={0} onFocus={onEndBlockFocusHandler} />
			</ModalDialog>
		</>
	);
}

const BlockFocus = styled.div`
    width: 0;
    height: 0;
`;

const ModalOverlay = styled.div`
	position:fixed;
	top: 0;
	left: 0;
	height: 100vh;
	width: 100vw;
	background-color: #000;
	opacity: 0.5;
	z-index: 1000;
`;

const ModalDialog = styled.div`
	position: fixed;
	top: 10%;
	left: 1rem;
	width: calc(100% - 2rem);
    max-height: 80%;
	padding: 1.5rem;
	border-radius: 0.75rem;
	border: solid 1px ${({ theme }) => theme.color.border};
	background-color: #ffffff;
    overflow-y: auto;
	z-index: 1001;
`;

const ModalClose = styled.div`
	float: right;
	height: 1rem;
	cursor: pointer;

	:hover {
		opacity: 0.6;
		transition: all 0.2s;
	}
`;

const ModalTitle = styled.div`
	font-size: 1rem;
	font-weight: 600;
	color: #222222;
`;

const ModalBody = styled.div`
	margin: 1rem 0;
`;

const ModalFooter = styled.div`
	text-align: right;
	margin: 2.5rem 0 0 0;
`;

const CancelButton = styled.button`
	width: 6.875rem;
	height: 3rem;
	margin: 0 0.5rem 0 0;
	padding: 1rem;
	border-radius: 0.5rem;
	border: solid 2px ${({ theme }) => theme.color.border};
	background-color: #fff;
	color: #222222;
	cursor: pointer;

	:hover {
		opacity: 0.6;
		transition: all 0.2s;
	}

    @media (max-width: 19.375rem) {
        margin: 0 0 0.5rem 0;
    }
`;

const OkButton = styled.button`
	width: 6.875rem;
	height: 3rem;
	margin: 0;
	padding: 1rem;
	border-radius: 0.5rem;
	border: solid 2px ${({ theme }) => theme.color.border};
	background-color: #222222;
	color: #fff;
	cursor: pointer;

	:hover {
		opacity: 0.6;
		transition: all 0.2s;
	}
`;