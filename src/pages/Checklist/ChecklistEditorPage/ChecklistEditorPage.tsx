import { Heading, Paragraph } from '@mccarthyfinch/author-ui-components';
import { IChecklist, IIssue, IIssueCategory } from '@mccarthyfinch/public-api-client';
import React, { useState } from 'react';
import FloatingActionButton, { IMenuItem } from 'src/components/FloatingActionButton';
import styled, { css } from 'styled-components';
import { getBlankIssue, getBlankIssueCategory } from '../utils';
import SortableCheckList from './SortableCheckList';
import { CheckListItem } from './utils';
import { CleanSlate, EmptyChecklistImage } from './styles';
import { useFilterChangeOnCreateItem } from '../hooks/useFilters';

/**
 * https://projects.invisionapp.com/share/82ZEFNDJ3TB#/screens/437214845
 */
interface IChecklistEditorPageProps {
	checklist: IChecklist;
	checklistItems: CheckListItem[]; // Filtered list
	allChecklistItems: CheckListItem[]; // Unfiltered list
	createIssueCategory: (issueCategory: IIssueCategory) => Promise<IIssueCategory>;
	createIssue: (issue: IIssue) => Promise<IIssue>;
	updateIssueCategory: (issue: Partial<IIssueCategory>) => Promise<IIssueCategory>;
	updateIssue: (issueCategory: Partial<IIssue>) => Promise<IIssue>;
}

export default function ChecklistEditorPage({
	checklist,
	checklistItems,
	allChecklistItems,
	createIssueCategory,
	createIssue,
	updateIssueCategory,
	updateIssue,
}: IChecklistEditorPageProps) {
	const [newItemUUIDs, setNewItemUUIDs] = useState([]);
	const [changeFiltersOnCreateItem] = useFilterChangeOnCreateItem();

	const createNewIssue = (issueCategoryUUID?: string, _rank?: number) => {
		changeFiltersOnCreateItem('Issue');
		const rank = allChecklistItems?.length ? allChecklistItems[allChecklistItems.length - 1].item.rank + 1 : 0;
		const newIssue = getBlankIssue({ checklistUUID: checklist.uuid, rank, issueCategoryUUID });
		createIssue(newIssue);
		setNewItemUUIDs((items) => [...items, newIssue.uuid]);
	};

	const createNewIssueCategory = async () => {
		changeFiltersOnCreateItem('IssueCategory');
		const rank = allChecklistItems?.length ? allChecklistItems[allChecklistItems.length - 1].item.rank + 1 : 0;
		const newIssueCategory = getBlankIssueCategory({ checklistUUID: checklist.uuid, rank });
		createIssueCategory(newIssueCategory);
		setNewItemUUIDs((items) => [...items, newIssueCategory.uuid]);
	};

	const menu: IMenuItem[] = [
		{
			key: 'category',
			renderContent: () => <>Add group</>,
			onClick: createNewIssueCategory,
		},
		{
			key: 'issue',
			renderContent: () => <>Add Issue</>,
			onClick: () => createNewIssue(),
		},
	];

	return (
		<Wrapper>
			{allChecklistItems.length ? (
				<ListWrapper data-ted='list-wrapper'>
					<SortableCheckList
						checklistItems={checklistItems}
						checklistUUID={checklist.uuid}
						newItemUUIDs={newItemUUIDs}
						setNewItemUUIDs={setNewItemUUIDs}
						updateIssueCategory={updateIssueCategory}
						updateIssue={updateIssue}
					/>
				</ListWrapper>
			) : (
				<CleanSlate>
					<div style={{ textAlign: 'center' }}>
						<Heading fontWeight="bold" style={{ marginTop: '1rem' }}>
							A clean slate
						</Heading>
						<Paragraph style={{ marginTop: '0.3rem' }} fontWeight='light'>Anything to add?</Paragraph>
					</div>
					<EmptyChecklistImage />
				</CleanSlate>
			)}

			<FloatingActionButton menu={menu} />
		</Wrapper>
	);
}

const Wrapper = styled.div<{ isDraggingOver?: boolean }>`
	display: flex;
	padding: 0.25rem 0 0;
	flex-direction: column;
	align-items: stretch;
	width: 100%;
	max-width: 100%;
	height: 100%;
	position: relative;
	flex-grow: 1;

	${({ isDraggingOver, theme }) => {
		if (isDraggingOver) {
			return css`
				background-color: ${theme.color.contrast};
			`;
		}
	}}
`;

const ListWrapper = styled.div`
	padding-bottom: 4rem;

	& > *:not(:last-child) {
		margin-bottom: 1rem;
	}

	.ghost {
		opacity: 0.6;
	}
`;
