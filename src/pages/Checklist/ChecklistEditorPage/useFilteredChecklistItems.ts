import { useMemo } from 'react';
import { IIssue, IIssueCategory } from '@mccarthyfinch/public-api-client';
import { useFilters } from '../hooks/useFilters';
import { CheckListItem, denormalizedCheckListItems, filterIssuesAndCategories } from './utils';
import { useAuthStore } from 'src/models/store/AuthStore';
import IssueCategoryService from 'src/models/service/checklistService/IssueCategoryService';
import IssueService from 'src/models/service/checklistService/IssueService';
import { QueryObserverResult } from 'react-query';

export default function useFilteredChecklistItems(checklistUUID: string): [CheckListItem[], CheckListItem[], QueryObserverResult<IIssue[], unknown>, QueryObserverResult<IIssueCategory[], unknown>] {
	const issuesQuery = IssueService.useIssuesForChecklist(checklistUUID);
	const issueCategoriesQuery = IssueCategoryService.useIssueCategoriesForChecklist(checklistUUID);

	const [filters] = useFilters();
	const authStore = useAuthStore();

	const getChecklistItems = (filtered?: boolean) => {
		if (filtered) {
			const filtered = filterIssuesAndCategories(issuesQuery.data, issueCategoriesQuery.data, filters, authStore.user?.uuid);
			return denormalizedCheckListItems(filtered.issues, filtered.issueCategories);
		}
		return denormalizedCheckListItems(issuesQuery.data, issueCategoriesQuery.data);
	};

	// Recalculate when issues, categories or filters change
	const filteredChecklistItems = useMemo(() => {
		if (issuesQuery.data && issueCategoriesQuery.data) {
			return getChecklistItems(true);
		} else {
			return [];
		}
	}, [issuesQuery.data, issueCategoriesQuery.data, filters]);

	// Recalculate when issues or categories change
	const unfilteredChecklistItems = useMemo(() => {
		if (issuesQuery.data && issueCategoriesQuery.data) {
			return getChecklistItems();
		} else {
			return [];
		}
	}, [issuesQuery.data, issueCategoriesQuery.data]);

	return [filteredChecklistItems, unfilteredChecklistItems, issuesQuery, issueCategoriesQuery];
}
