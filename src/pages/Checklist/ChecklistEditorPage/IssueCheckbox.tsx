import React from 'react';
import CustomCheckbox, { ICustomCheckboxProps } from 'src/components/CustomCheckbox';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle as faCheckCircleSolid } from '@fortawesome/pro-solid-svg-icons';
import { faCircle } from '@fortawesome/pro-regular-svg-icons';
import { InputWrapper } from '../components/Checkbox';

interface IIssueCheckboxProps extends Omit<ICustomCheckboxProps, 'children'> {
	prechecked?: boolean;
	warning?: boolean;
}

const IssueCheckbox = (props: IIssueCheckboxProps) => {
	const size = '1rem';

	return (
		<CustomCheckbox
			data-ted={'issue-isChecked'}
			style={{ marginRight: '0.5rem' }}
			{...props}
		>
			{(checked, disabled) => {

				if (checked) {
					return (
						<InputWrapper color='checked' checked={checked} disabled={disabled} size={size}>
							<FontAwesomeIcon icon={faCheckCircleSolid} />
						</InputWrapper>
					);
				}

				if (props.warning) {
					return (
						<InputWrapper color='danger' checked={checked} disabled={disabled} size={size} shadow>
							<FontAwesomeIcon icon={faCircle} />
						</InputWrapper>
					);
				}

				if (props.prechecked) {
					return (
						<InputWrapper color='checked' checked={checked} disabled={disabled} size={size} shadow>
							<FontAwesomeIcon icon={faCircle} />
						</InputWrapper>
					);
				}

				return (
					<InputWrapper color='fontSecondary' checked={checked} disabled={disabled} size={size} shadow>
						<FontAwesomeIcon icon={faCircle} />
					</InputWrapper>
				);

			}}
		</CustomCheckbox>
	);
};

export default IssueCheckbox;
