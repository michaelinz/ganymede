import { observer } from 'mobx-react';
import React from 'react';
import { useParams } from 'react-router-dom';
import { Content } from 'src/components/Layout/Page';
import LoadingOverlay from 'src/components/LoadingOverlay';
import QueryStatusWrapper from 'src/components/QueryStatusWrapper';
import ChecklistService from 'src/models/service/checklistService/ChecklistService';
import IssueCategoryService from 'src/models/service/checklistService/IssueCategoryService';
import IssueService from 'src/models/service/checklistService/IssueService';
import ChecklistEditorPage from './ChecklistEditorPage';
import useFilteredChecklistItems from './useFilteredChecklistItems';

const ChecklistEditorPageContainer = () => {
	const { checklistUUID } = useParams<{ checklistUUID: string }>();

	const checklistQuery = ChecklistService.useChecklist(checklistUUID);
	const [filteredChecklistItems, allChecklistItems, issuesQuery, issueCategoriesQuery] = useFilteredChecklistItems(checklistUUID);

	const [createIssueCategory] = IssueCategoryService.useCreateIssueCategory(checklistUUID);
	const [createIssue] = IssueService.useCreateIssue(checklistUUID);
	const [updateIssueCategory] = IssueCategoryService.useUpdateIssueCategory(checklistUUID);
	const [updateIssue] = IssueService.useUpdateIssue(checklistUUID);

	return (
		<Content style={{ backgroundColor: 'transparent' }}>
			<QueryStatusWrapper
				queryResults={[checklistQuery, issueCategoriesQuery, issuesQuery]}
				renderLoading={() => <LoadingOverlay />}
				renderError={(errors) => <span>Error: {JSON.stringify(errors)}</span>}
			>
				<ChecklistEditorPage
					checklist={checklistQuery.data}
					checklistItems={filteredChecklistItems}
					allChecklistItems={allChecklistItems}
					createIssueCategory={createIssueCategory}
					createIssue={createIssue}
					updateIssueCategory={updateIssueCategory}
					updateIssue={updateIssue}
				/>
			</QueryStatusWrapper>
		</Content>
	);
};

export default observer(ChecklistEditorPageContainer);
