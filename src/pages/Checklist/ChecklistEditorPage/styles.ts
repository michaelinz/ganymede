import styled, { css } from 'styled-components';
import BorderlessTextInput from 'src/components/molecules/BorderlessTextInput';
import { HR, IconButtonBorderless, McfCheckbox, Paragraph } from '@mccarthyfinch/author-ui-components';
import blankChecklistImage from '../../../../public/assets/checklist/img-blank-checklist.png';
import _DocumentReferenceList from '../ChecklistIssueEditorPage/DocumentReferenceSection/DocumentReferenceList';

export const CategoryAction = styled.div<{ alwaysShow?: boolean }>`
	min-width: 20px;
	max-width: 20px;

	transition: 0.3s all;
	visibility: ${({ alwaysShow }) => alwaysShow ? 'visible' : 'hidden'};
	opacity: ${({ alwaysShow }) => alwaysShow ? 1 : 0};

	${IconButtonBorderless} {
		:hover {
			svg {
				color: ${({ theme }) => theme.color.fontPrimary};
			}
		}
	}
`;

export const CategoryWrapper = styled.div`
	transition: 0.2s all;

	:hover {
		${CategoryAction} {
			visibility: visible;
			opacity: 1;
		}
	}
`;

export const CategoryTitleWrapper = styled.div`
	*[role='input'] {
		font-size: ${({ theme }) => theme.typography.categoryTitle.fontSize};
		line-height: ${({ theme }) => theme.typography.categoryTitle.lineHeight};
		font-weight: ${({ theme }) => theme.fontWeight.bold};
	}
`;

export const CategoryTitle = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	padding: 1rem;
`;

export const EditableCategoryTitle = styled(BorderlessTextInput)`
	height: 100%;

	> span {
		height: 100%;
		padding-top: 4px;
	}
`;

export const Divider = styled(HR)`
	margin: 0 0.5rem;
	width: auto;
`;

export const ButtonWrapper = styled.div<{ alwaysShow?: boolean }>`
	min-width: 20px;
	max-width: 20px;
	transition: 0.3s all;

	visibility: ${({ alwaysShow }) => alwaysShow ? 'visible' : 'hidden'};
	opacity: ${({ alwaysShow }) => alwaysShow ? 1 : 0};

	${IconButtonBorderless} {
		:hover {
			svg {
				color: ${({ theme }) => theme.color.fontPrimary};
			}
		}
	}
`;

export const IssueEditorWrapper = styled.div<{ isExpanded: boolean }>`
	transition: 0.2s all;
	padding: 1rem;

	${({ isExpanded }) => (isExpanded ? 'padding-bottom: 0.5rem;' : '')}

	:hover {
		${ButtonWrapper} {
			visibility: visible;
			opacity: 1;
		}
	}
`;

export const IssueRow = styled.div<{ isLast: boolean }>`
	${({ isLast }) => (isLast ? 'border-radius: 0 0 0.7rem 0.7rem;' : '')}

	:hover {
		box-shadow: 0 0 6px 1px rgba(59, 73, 98, 0.2);
	}
`;

export const CleanSlate = styled.div`
	flex: 1;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;
	overflow-x: hidden;
	max-width: 100%;
	padding-top: 1rem;
`;

export const EmptyChecklistImage = styled.img.attrs({
	src: blankChecklistImage,
})`
	align-self: flex-end;
`;

export const IssueCheckbox = styled(McfCheckbox)`
`;

export const IssueText = styled(Paragraph)<{ isChecked: boolean }>`
	width: 100%;

	${({ isChecked, theme }) => {
		if (isChecked) {
			return css`
				text-decoration: line-through;
				color: ${theme.colorPalette.smoke[600]};
			`;
		}
	}}

	:hover {
		opacity: 0.5;
		transition: all 0.3s;
	}
`;

export const DocumentReferenceList = styled(_DocumentReferenceList)`
	margin-left: 1.6875rem;
`;

export const IssueTextWrapper = styled(IssueText)<{isExpandable: boolean}>`
	width: 100%;
	${({isExpandable}) => {
		if (isExpandable) {
			return css`
				cursor: pointer;
				opacity: 1;
			`;
		}
	}}
`;
