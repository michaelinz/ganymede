import { IIssue, IIssueCategory } from '@mccarthyfinch/public-api-client';
import { ItemInterface } from 'src/components/react-sortablejs/types';
import { Filters, QuickFilter } from 'src/models/store/UIStore';


export interface CheckListItem extends ItemInterface {
	type: 'Issue' | 'IssueCategory';
	item: IIssueCategory | IIssue;
	children: CheckListItem[];
}

export function denormalizedCheckListItems(issues: IIssue[], issueCategories: IIssueCategory[]) {
	const denormalized: CheckListItem[] = [];
	const issueCategoryChecklistItems: CheckListItem[] = issueCategories
		.filter((ic) => ic.isActive)
		.map((item) => ({
			id: item.uuid,
			type: 'IssueCategory',
			item,
			children: [],
		}));

	issues.forEach((item) => {
		if (!item.isActive) {
			return;
		}
		const checklistItem: CheckListItem = {
			id: item.uuid,
			type: 'Issue',
			item,
			children: [],
		};
		if (!item.issueCategoryUUID) {
			denormalized.push(checklistItem);
			return;
		}
		const ic = issueCategoryChecklistItems.find((ic) => ic.item.uuid === item.issueCategoryUUID);
		if (!ic) {
			console.warn(`Issue ${item.uuid} has missing parent issue category ${item.issueCategoryUUID}. Acting as if it has no parent.`);
			denormalized.push(checklistItem);
			return;
		}
		ic.children.push(checklistItem);
	});

	issueCategoryChecklistItems.forEach((item) => item.children.sort((a, b) => a.item.rank - b.item.rank));
	denormalized.push(...issueCategoryChecklistItems);
	denormalized.sort((a, b) => a.item.rank - b.item.rank);
	return denormalized;
}

export function getIndexPath(list: CheckListItem[], index: number, itemToFind: CheckListItem, previousPath: number[] = []): number[] {
	if (list[index]?.id === itemToFind.id) {
		return [...previousPath, index];
	}
	const path: number[] = [];
	for (const [childIndex, item] of list.entries()) {
		if (item?.children) {
			path.push(...getIndexPath(item.children, index, itemToFind, [...previousPath, childIndex]));
		}
	}
	return path;
}


export function filterIssuesAndCategories(issues: IIssue[], issueCategories: IIssueCategory[], filters: Filters, userUUID?: string) {
	const { quick } = filters;

	const filteredIssues = issues.filter(issue => {
		if (!issue.isActive) { return false; }

		// AND filtering when more than one quick filter is selected
		if (quick.includes(QuickFilter.TODO) && issue.isChecked) {
			return false;
		}
		if (quick.includes(QuickFilter.DONE) && !issue.isChecked) {
			return false;
		}
		if (quick.includes(QuickFilter.MINE) && userUUID && issue.createdBy && issue.createdBy.uuid !== userUUID /* Nothing is assigned yet so filter on creator */) {
			return false;
		}
		if (quick.includes(QuickFilter.IMPORTANT) /* Nothing has importance flag yet so filter out everything */) {
			return issue.isImportant;
		}
		return true;
	});

	const filteredCategories = issueCategories.filter(ic => {
		if (!ic.isActive) { return false; }

		// If there is at least one filtered issue in the category, keep the category regardless of filters
		if (filteredIssues.some(issue => issue.issueCategoryUUID === ic.uuid)) {
			return true;
		}
		if (quick.includes(QuickFilter.TODO)) {
			return false;
		}
		if (quick.includes(QuickFilter.DONE)) {
			return false;
		}
		if (quick.includes(QuickFilter.MINE) && ic.createdBy && ic.createdBy.uuid !== userUUID /* Nothing is assigned yet so filter on creator */) {
			return false;
		}
		if (quick.includes(QuickFilter.IMPORTANT) /* Nothing has importance flag yet so filter out everything */) {
			return ic.isImportant;
		}
		return true;
	});

	return { issues: filteredIssues, issueCategories: filteredCategories };
}

export interface saveSortChangesProps {
	sortableItems: CheckListItem[];
	newIndex: number;
	droppedItem: CheckListItem;
	updateIssue: (issue: Partial<IIssue>) => Promise<IIssue>;
	updateIssueCategory: (issue: Partial<IIssueCategory>) => Promise<IIssueCategory>;
}

export function saveSortChanges(props: saveSortChangesProps) {
	try {
		const { sortableItems, newIndex, droppedItem, updateIssue, updateIssueCategory } = props;
		const indexPath = getIndexPath(sortableItems, newIndex, droppedItem);
		if (!indexPath) {
			console.error('indexPath was null', sortableItems, newIndex, droppedItem);
			return;
		}
		indexPath.pop();

		let currentList: CheckListItem[] = sortableItems;
		let parentItem: CheckListItem = null;
		if (indexPath.length) {
			while (indexPath.length) {
				parentItem = currentList[indexPath.pop()];
				currentList = parentItem.children;
			}
		}

		const previousItem = newIndex === 0 ? undefined : currentList[newIndex - 1];
		const nextItem = newIndex >= currentList.length - 1 ? undefined : currentList[newIndex + 1];
		const newRank = getNewRank(previousItem?.item?.rank, nextItem?.item?.rank);
		const changedItem = currentList[newIndex];

		if (changedItem.type === 'Issue') {
			return updateIssue({ uuid: changedItem.item.uuid, rank: newRank, issueCategoryUUID: parentItem?.item?.uuid || null });
		}
		if (changedItem.type === 'IssueCategory') {
			return updateIssueCategory({ uuid: changedItem.item.uuid, rank: newRank });
		}
	} catch (e) {
		console.error(e);
	}
}

function getNewRank(previousItemRank: number, nextItemRank: number) {
	if (previousItemRank === undefined) {
		return nextItemRank === undefined ? 0 : Math.trunc(nextItemRank) - 1;
	}
	if (nextItemRank === undefined) {
		return Math.trunc(previousItemRank) + 1;
	}
	return (previousItemRank + nextItemRank) / 2;
}
