import { faTrash } from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { HR, IconButtonBorderless } from '@mccarthyfinch/author-ui-components';
import { IIssue, IIssueCategory } from '@mccarthyfinch/public-api-client';
import { observer } from 'mobx-react';
import React from 'react';
import useUpsert from 'src/components/hooks/useUpsert';
import AddItemButton from 'src/components/molecules/AddItemButton';
import { useFilter } from '../hooks/useFilters';
import { CategoryAction, CategoryTitle, CategoryTitleWrapper, CategoryWrapper, EditableCategoryTitle, Divider } from './styles';
import ImportantFlag from '../components/ImportantFlag/index';
import Tooltip from 'src/components/Tooltip';

interface IIssueCategoryEditorProps {
	checklistUUID: string;
	issueCategory: IIssueCategory;
	createIssue?: (issue: Partial<IIssue>) => Promise<IIssue>;
	createIssueCategory?: (issueCategory: Partial<IIssueCategory>) => Promise<IIssueCategory>;
	updateIssueCategory?: (issueCategory: Partial<IIssueCategory>) => Promise<IIssueCategory>;
	autoFocus?: boolean;
	issues: IIssue[];
	children?: React.ReactChild;
	toggleIssueCategoryIsImportant: () => Promise<void>;
}

function IssueCategoryEditor({ issueCategory, autoFocus, children, createIssue, createIssueCategory, updateIssueCategory, issues, toggleIssueCategoryIsImportant }: IIssueCategoryEditorProps) {
	const [item, onUpsertItem] = useUpsert<IIssueCategory>({ item: issueCategory, createItem: createIssueCategory, updateItem: updateIssueCategory });
	const [quickFilters] = useFilter('quick');

	const deleteIssueCategory = () => {
		updateIssueCategory({ uuid: issueCategory.uuid, isActive: false });
	};

	if (item && !item.isActive) {
		return null;
	}

	return (
		<CategoryWrapper>
			<CategoryTitleWrapper data-ted={issueCategory.uuid}>
				<CategoryTitle>
					<EditableCategoryTitle
						autoFocus={autoFocus}
						data-ted="name"
						value={item?.name}
						placeholder="Write a group title"
						onChange={async (e) => {
							onUpsertItem({ name: e.target.value });
						}}
						debounceDelay={0}
					/>
					<CategoryAction style={{ marginRight: '0.25rem' }}>
						{
							!issues?.length && !quickFilters.length &&
							<Tooltip title='Delete'>
								<div>
									<IconButtonBorderless onClick={deleteIssueCategory}>
										<FontAwesomeIcon icon={faTrash} />
									</IconButtonBorderless>
								</div>
							</Tooltip>
						}
					</CategoryAction>
					<CategoryAction style={{ marginRight: '0.25rem' }}>
						<Tooltip title='Add issue'>
							<div>
								<AddItemButton onClick={() => createIssue({ issueCategoryUUID: issueCategory.uuid })} />
							</div>
						</Tooltip>
					</CategoryAction>
					<CategoryAction alwaysShow={issueCategory.isImportant}>
						<Tooltip title={issueCategory.isImportant ? 'Marked as important' : 'Mark as important'}>
							<div>
								<ImportantFlag isImportant={issueCategory.isImportant} toggleIsImportant={toggleIssueCategoryIsImportant} />
							</div>
						</Tooltip>
					</CategoryAction>
				</CategoryTitle>
			</CategoryTitleWrapper>
			<Divider />
			{children}
		</CategoryWrapper>
	);
}

export default observer(IssueCategoryEditor);
