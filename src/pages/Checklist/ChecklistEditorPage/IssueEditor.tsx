import { HR, IconButtonBorderless } from '@mccarthyfinch/author-ui-components';
import { IIssue } from '@mccarthyfinch/public-api-client';
import React, { useEffect, useState, useRef } from 'react';
import useUpsert from 'src/components/hooks/useUpsert';
import { Row } from 'src/components/Layout';
import BorderlessTextInput from 'src/components/molecules/BorderlessTextInput';
import Zelda from 'src/components/Zelda';
import IssueService from 'src/models/service/checklistService/IssueService';
import { getChecklistIssueURL } from '../ChecklistRouter';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowCircleRight } from '@fortawesome/pro-light-svg-icons';
import { faArrowCircleRight as faArrowCircleRightSolid } from '@fortawesome/pro-solid-svg-icons';
import { IssueEditorWrapper, IssueTextWrapper, ButtonWrapper, DocumentReferenceList } from './styles';
import DocumentReferenceService from 'src/models/service/checklistService/DocumentReferenceService';
import ImportantFlag from '../components/ImportantFlag/index';
import useLastClickedIssueUUIDFromSession from '../hooks/useLastClickedIssueUUIDFromSession';
import Tooltip from '../../../components/Tooltip';
import IssueCheckbox from './IssueCheckbox';
import { goToDocRefInDoc } from '../ChecklistIssueEditorPage/DocumentReferenceSection/utils';

interface IIssueCreator {
	checklistUUID: string;
	issue: IIssue;
	isNew?: boolean;
	autoFocus?: boolean;
}

function usePrevious<T>(value: T): T {
  const ref = useRef<T>();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
}

function IssueEditor({ checklistUUID, issue, isNew, autoFocus }: IIssueCreator) {
	const documentReferencesQuery = DocumentReferenceService.useDocumentReferencesForIssue(issue.uuid);

	const [createItem] = IssueService.useCreateIssue(checklistUUID);
	const [updateItem] = IssueService.useUpdateIssue(checklistUUID);
	const [item, onUpdateItem] = useUpsert<IIssue>({ item: issue, createItem, updateItem });
	const [, lastClickedIssueUUID, setLastClickedIssueUUIDToSession] = useLastClickedIssueUUIDFromSession();

	const [editable, setEditable] = useState(isNew);
	const [containsAlert, setContainsAlert] = useState(false);
	const [containsPreCheck, setContainsPreCheck] = useState(false);
	const [isHoveringDetails, setIsHoveringDetails] = useState(false);

	const isExpandable = !editable && (documentReferencesQuery.data
		? documentReferencesQuery.data.filter(dr => dr.isActive).length > 0
		: issue.documentReferenceUUIDs?.length > 0);

	const [isExpanded, setIsExpanded] = useState(isExpandable);
	const previousIsExpanded = usePrevious(isExpanded);

	const issueRef = useRef(null);


	if (item && !item.isActive) {
		return null;
	}

	useEffect(() => {
		// When component mounts, if the issue has no name, let the name be editable
		if (!editable && !item.name) {
			setEditable(true);
		}
	}, []);

	// useEffect(() => {
	// 	if (!previousIsExpanded && isExpanded) {
	// 		// When the issue is expanded, if there are doc refs in the list, jump to the first doc ref in the document
	// 		if (isExpanded && documentReferencesQuery.data?.length) {
	// 			goToDocRefInDoc(documentReferencesQuery.data[0]);
	// 		}
	// 	}
	// }, [isExpanded]);

	useEffect(() => {
		// When the document reference list changes, if the issue is no longer expandable, collapse it
		if (!isExpandable && isExpanded) {
			setIsExpanded(false);
		}
	}, [documentReferencesQuery.data, issue.documentReferenceUUIDs]);

	useEffect(() => {
		// When the last clicked issue changes, if this issue is the last clicked issue, scroll it into view
		if (lastClickedIssueUUID && issueRef && issueRef.current.dataset.ted === lastClickedIssueUUID) {
			issueRef.current.scrollIntoView({ block: "start" });
		}
	}, [lastClickedIssueUUID]);


	const toggleExpanded = () => {
		setIsExpanded(!isExpanded);
	};

	const toggleIssueIsImportant = async () => {
		await updateItem({ uuid: issue.uuid, isImportant: !issue.isImportant });
	};

	return (
		<IssueEditorWrapper data-ted={issue.uuid} ref={issueRef} isExpanded={isExpanded}>
			<Row
				onBlur={() => {
					if (editable && item.name) {
						setEditable(false);
					}
				}}
				style={{ minHeight: '1.5rem', justifyContent: 'space-between' }}
			>
				<Row style={{ flex: '1' }}>
					<IssueCheckbox
						data-ted={'isChecked'}
						checked={item.isChecked}
						prechecked={containsPreCheck}
						warning={containsAlert}
						onClick={() => onUpdateItem({ isChecked: !item.isChecked })}
						disabled={editable}
					/>
					<IssueTextWrapper isChecked={item?.isChecked} as='div' isExpandable={isExpandable} onClick={isExpandable ? toggleExpanded : null}>
						<BorderlessTextInput
							autoFocus={editable && autoFocus}
							data-ted="name"
							value={item?.name}
							placeholder="Describe an issue"
							onChange={(e) => onUpdateItem({ name: e.target.value })}
							debounceDelay={0}
							disabled={!editable}
						/>
					</IssueTextWrapper>
				</Row>
				<Row style={{ width: '2.75rem', flex: '0 0 2.75rem', minWidth: '2.75rem' }}>
					<ButtonWrapper style={{ marginRight: '0.25rem' }}>
						{
							!editable ?
								<Zelda
									data-ted={issue.uuid}
									to={getChecklistIssueURL(checklistUUID, issue.uuid)}
									onMouseEnter={()=> setIsHoveringDetails(true)}
									onMouseLeave={()=> setIsHoveringDetails(false)}
									onClick={() => setLastClickedIssueUUIDToSession(issue.uuid)}
								>
									<Tooltip title='Details'>
										<div>
											<IconButtonBorderless style={{ paddingRight: '0' }}>
												<FontAwesomeIcon
													icon={isHoveringDetails ? faArrowCircleRightSolid: faArrowCircleRight}
													style={{ transition: '0.3s all' }}
												/>
											</IconButtonBorderless>
										</div>
									</Tooltip>
								</Zelda>
								:
								null
						}
					</ButtonWrapper>
					<ButtonWrapper alwaysShow={issue.isImportant}>
						<Tooltip title={issue.isImportant ? 'Marked as important' : 'Mark as important'}>
							<div>
								<ImportantFlag isImportant={issue.isImportant} toggleIsImportant={toggleIssueIsImportant} />
							</div>
						</Tooltip>
					</ButtonWrapper>
				</Row>
			</Row>

			{isExpanded && (
				<>
					<HR style={{ width: 'auto', margin: '1rem 2.5rem 0.25rem 1.6785rem' }} />
					<DocumentReferenceList
						issueUUID={issue.uuid}
						documentReferenceUUIDs={issue.documentReferenceUUIDs || []}
						documentReferences={documentReferencesQuery.data}
						setContainsAlert={setContainsAlert}
						setContainsPreCheck={setContainsPreCheck}
						shortDividers
					/>
				</>
			)}
		</IssueEditorWrapper>
	);
}

export default IssueEditor;
