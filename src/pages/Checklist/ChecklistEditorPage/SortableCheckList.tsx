import { IIssue, IIssueCategory } from '@mccarthyfinch/public-api-client';
import React, { useEffect, useState } from 'react';
import { Options } from 'sortablejs';
import { ReactSortable } from 'src/components/react-sortablejs';
import IssueCategoryService from 'src/models/service/checklistService/IssueCategoryService';
import IssueService from 'src/models/service/checklistService/IssueService';
import styled, { css } from 'styled-components';
import { getBlankIssue } from '../utils';
import IssueCategoryEditor from './IssueCategoryEditor';
import IssueEditor from './IssueEditor';
import { CheckListItem, saveSortChanges, saveSortChangesProps } from './utils';
import { Divider, IssueRow } from './styles';
import { useFilterChangeOnCreateItem } from '../hooks/useFilters';

interface ISortableChecklist {
	checklistItems: CheckListItem[];
	checklistUUID: string;
	newItemUUIDs: string[];
	setNewItemUUIDs: React.Dispatch<React.SetStateAction<any[]>>;
	updateIssue: (issue: Partial<IIssue>) => Promise<IIssue>;
	updateIssueCategory: (issue: Partial<IIssueCategory>) => Promise<IIssueCategory>;
}

type onEnd = (props: Pick<saveSortChangesProps, 'newIndex' | 'sortableItems' | 'droppedItem'>) => Promise<IIssueCategory> | Promise<IIssue>;

const sortableOptions: Options = {
	easing: 'cubic-bezier(0.2, 0, 0, 1)',
	animation: 150,
	fallbackOnBody: true,
	swapThreshold: 0.65,
	ghostClass: 'ghost',
	group: 'shared',
};

// Based on https://codesandbox.io/s/react-sortable-js-nested-tu8nn?file=/src/App.js
export default function SortableCheckList({
	checklistItems,
	checklistUUID,
	newItemUUIDs,
	setNewItemUUIDs,
	updateIssue,
	updateIssueCategory,
}: ISortableChecklist) {
	const [sortableItems, setSortableItems] = useSortableItemCache(checklistItems);

	const onEnd: onEnd = (props) => {
		return saveSortChanges({ ...props, updateIssue, updateIssueCategory });
	};

	if (!checklistItems.length) {
		return <div style={{ padding: '1rem', width: '100%', textAlign: 'center' }}>No items. Try removing a filter.</div>;
	}

	return (
		<ReactSortable
			list={sortableItems}
			setList={setSortableItems}
			{...sortableOptions}
			onEnd={(e, sortable, store) => {
				const droppedItem = store.previousChosenItem as CheckListItem;
				const newIndex = e.newIndex;
				onEnd({
					newIndex,
					sortableItems,
					droppedItem,
				});
			}}
		>
			{checklistItems.map((checklistItem, index) => {
				return (
					<Card key={checklistItem.item.uuid}>
						<ChecklistItemFactory
							checklistUUID={checklistUUID}
							checklistItem={checklistItem}
							checklistItemIndex={[index]}
							newItemUUIDs={newItemUUIDs}
							setNewItemUUIDs={setNewItemUUIDs}
							sortableItems={sortableItems}
							setSortableItems={setSortableItems}
							onEnd={onEnd}
						/>
					</Card>
				);
			})}
		</ReactSortable>
	);
}


interface IChecklistItemViewProps {
	checklistItem: CheckListItem;
	checklistUUID: string;
	checklistItemIndex: number[];
	newItemUUIDs: string[];
	setNewItemUUIDs: React.Dispatch<React.SetStateAction<any[]>>;

	sortableItems: CheckListItem[];
	setSortableItems: React.Dispatch<React.SetStateAction<CheckListItem[]>>;
	onEnd: onEnd;
}

function ChecklistItemFactory({ checklistItem, checklistUUID, newItemUUIDs, ...rest }: IChecklistItemViewProps) {
	if (checklistItem.type === 'Issue') {
		const issue = checklistItem.item as IIssue;
		return <IssueEditor issue={issue} checklistUUID={checklistUUID} isNew={newItemUUIDs.includes(checklistItem.item.uuid)} autoFocus={newItemUUIDs[newItemUUIDs.length - 1] === issue.uuid} />;
	}
	return <IssueEditorWrapper checklistItem={checklistItem} checklistUUID={checklistUUID} newItemUUIDs={newItemUUIDs} {...rest} />;
}

function IssueEditorWrapper({
	checklistItem,
	checklistUUID,
	checklistItemIndex,
	sortableItems,
	setSortableItems,
	newItemUUIDs,
	setNewItemUUIDs,
	onEnd,
}: IChecklistItemViewProps) {
	const issueCategory = checklistItem.item as IIssueCategory;
	const [createIssueCategory] = IssueCategoryService.useCreateIssueCategory(checklistUUID);
	const [updateIssueCategory] = IssueCategoryService.useUpdateIssueCategory(checklistUUID);
	const [ _createIssue ] = IssueService.useCreateIssue(checklistUUID);
	const [changeFiltersOnCreateItem] = useFilterChangeOnCreateItem();

	const createIssue = async (issue: Partial<IIssue>) => {
		changeFiltersOnCreateItem('Issue');
		const rank = checklistItem.children?.length ? checklistItem.children[checklistItem.children.length - 1].item.rank + 1 : 0;
		const newIssue = getBlankIssue({ checklistUUID: checklistUUID, rank, ...issue });
		const request = _createIssue(newIssue);
		setNewItemUUIDs((items) => [...items, newIssue.uuid]);
		return request;
	};

	const toggleIssueCategoryIsImportant = async () => {
		await updateIssueCategory({ uuid: issueCategory.uuid, isImportant: !(issueCategory.isImportant ?? false) });
	};

	return (
		<IssueCategoryEditor
			key={issueCategory.uuid}
			checklistUUID={checklistUUID}
			issueCategory={issueCategory}
			autoFocus={!issueCategory.name && newItemUUIDs[newItemUUIDs.length - 1] === issueCategory.uuid}
			createIssueCategory={createIssueCategory}
			updateIssueCategory={updateIssueCategory}
			createIssue={createIssue}
			issues={checklistItem.children.map(child => child.item) as IIssue[]}
			toggleIssueCategoryIsImportant={toggleIssueCategoryIsImportant}
		>
			<ReactSortable
				key={issueCategory.uuid}
				list={checklistItem.children}
				setList={(currentList) => {
					setSortableItems((sourceList) => {
						// Check if the change is due to filters/parent list change
						if (checklistItemIndex?.length && sourceList?.length) {
							if (sourceList[checklistItemIndex[0]].id !== checklistItem.id) {
								// Since the change was due to filters/parent list change we don't change the list
								return sourceList;
							}
						}
						// Change was not due to filters/parent list change so continue
						const newList = [...sourceList];
						const _checklistItemIndex = [...checklistItemIndex];
						const lastIndex = _checklistItemIndex.pop();
						const lastArr = _checklistItemIndex.reduce((arr, i) => arr[i].children, newList);
						lastArr[lastIndex].children = currentList;
						return newList;
					});
				}}
				{...sortableOptions}
				onEnd={(e, sortable, store) => {
					const droppedItem = store.previousChosenItem as CheckListItem;
					const newIndex = e.newIndex;
					onEnd({ droppedItem, newIndex, sortableItems });
				}}
			>
				{checklistItem?.children?.map((item, i, self) => {
					const issue = item.item as IIssue;
					return (
						<IssueRow key={issue.uuid} isLast={i === self.length - 1}>
							<IssueEditor key={issue.uuid} issue={issue} checklistUUID={checklistUUID} isNew={newItemUUIDs.includes(checklistItem.item.uuid)} autoFocus={newItemUUIDs[newItemUUIDs.length - 1] === issue.uuid} />
							{i !== self.length - 1 && <Divider />}
						</IssueRow>
					);
				})}
			</ReactSortable>
		</IssueCategoryEditor>
	);
}

/**
 * Cache for items that can be updated
 * When the items props changes then we merge the changes
 * @param items
 * @param filterItemFunc
 */
function useSortableItemCache(
	items: CheckListItem[],
	filterItemFunc?: (items: CheckListItem[]) => CheckListItem[],
): [CheckListItem[], React.Dispatch<React.SetStateAction<CheckListItem[]>>] {
	const defaultFilterItems = (items: CheckListItem[]) => items;
	const _filterItems = filterItemFunc || defaultFilterItems;
	const [sortableItems, setSortableItems] = useState(_filterItems(items));

	useEffect(() => {
		const _items = _filterItems(items);
		// TODO if this is slow we can optimise stuff like index looping
		// eg, since the indexes of the matching items should be close to each other we can search like that
		const matchedItemUUIDs: string[] = [];
		const newSortableItems = sortableItems
			.map((sortableItem) => {
				const matchedItem = _items.find((item) => item.item.uuid === sortableItem.item.uuid);
				// sortedItem should be removed since it doesn't exist in items anymore
				if (!matchedItem) {
					return null;
				}
				// Merge changes
				matchedItemUUIDs.push(matchedItem.item.uuid);
				return { ...sortableItem, ...matchedItem };
			})
			.filter((i) => i);

		const newItems = _items.filter((item) => !matchedItemUUIDs.includes(item.item.uuid));
		newSortableItems.push(...newItems);
		setSortableItems(newSortableItems);
	}, [items]);

	return [sortableItems, setSortableItems];
}


const Card = styled.div<{ isDraggingOver?: boolean }>`
	border-radius: 0.75rem;
	border: solid 1px #bfc7d2;
	display: flex;
	flex-direction: column;

	background: white;

	min-width: 18rem;
	margin: 1rem;

	:hover {
		box-shadow: 0 0 6px 1px rgba(59, 73, 98, 0.2);
	}

	${({ isDraggingOver, theme }) => {
		if (isDraggingOver) {
			return css`
				background-color: ${theme.color.contrast};
			`;
		}
	}}
`;
