import htmlToText from '../htmlToText';

describe('htmlToText', () => {
	test('Word online small selection', () => {
		const html = `<HTML>
		<HEAD></HEAD>
		<BODY>
		<div class="OutlineGroup"><div class="OutlineElement Ltr"><div class="ParaWrappingDiv"><p class="Paragraph" paraid="0" paraeid="{794e446d-a4af-42c2-87b1-e2edc3f04054}{106}" style="font-weight: normal; font-style: normal; vertical-align: baseline; font-family: &quot;Segoe UI&quot;, Tahoma, Verdana, Sans-Serif; font-kerning: none; background-color: transparent; color: windowtext; text-align: left; margin: 0px 0px 0px 18px; padding-left: 39px; padding-right: 0px; text-indent: -39px; font-size: 6pt;"><span data-contrast="none" xml:lang="EN-US" lang="EN-US" class="TextRun" style="color: rgb(26, 26, 26); font-size: 9.5pt; line-height: 15.1083px; font-family: WordVisi_MSFontService, Arial, Arial_EmbeddedFont, Arial_MSFontService, sans-serif;"><span class="NormalTextRun" style="background-color: inherit;">confidence</span></span><span class="EOP" style="font-size: 9.5pt; line-height: 15.1083px; font-family: WordVisiPilcrow_MSFontService, Arial, Arial_EmbeddedFont, Arial_MSFontService, sans-serif; color: rgb(26, 26, 26);">&nbsp;</span></p></div></div></div><span class="WACImageGroupContainer"></span><span data-contrast="none" xml:lang="EN-US" lang="EN-US" class="TextRun" style="color: rgb(26, 26, 26); font-size: 9.5pt; line-height: 15.1083px; font-family: WordVisi_MSFontService, Arial, Arial_EmbeddedFont, Arial_MSFontService, sans-serif;"></span><span class="NormalTextRun" style="background-color: inherit;"></span>
		</BODY>
		</HTML>`;
		expect(htmlToText(html).trim()).toEqual('confidence');
	});

	test('Word desktop paragraph selection', () => {
		const html = `<html>



		<head>

		<meta http-equiv=Content-Type content="text/html; charset=windows-1252">

		<meta name=Generator content="Microsoft Word 15 (filtered)">

		<style>

		<!--

		 /* Font Definitions */

		 @font-face

			{font-family:"Cambria Math";

			panose-1:2 4 5 3 5 4 6 3 2 4;}

		@font-face

			{font-family:Calibri;

			panose-1:2 15 5 2 2 2 4 3 2 4;}

		 /* Style Definitions */

		 p.MsoNormal, li.MsoNormal, div.MsoNormal

			{margin-top:0cm;

			margin-right:0cm;

			margin-bottom:8.0pt;

			margin-left:0cm;

			line-height:107%;

			font-size:11.0pt;

			font-family:"Calibri",sans-serif;}

		.MsoChpDefault

			{font-family:"Calibri",sans-serif;}

		.MsoPapDefault

			{margin-bottom:8.0pt;

			line-height:107%;}

		@page WordSection1

			{size:612.0pt 792.0pt;

			margin:72.0pt 72.0pt 72.0pt 72.0pt;}

		div.WordSection1

			{page:WordSection1;}

		-->

		</style>



		</head>



		<body lang=EN-NZ style='word-wrap:break-word'>



		<div class=WordSection1>



		<p class=MsoNormal style='margin-left:17.5pt;text-align:justify'><span

		lang=EN-GB style='font-size:9.5pt;line-height:107%;font-family:"Arial",sans-serif'>1.4

		The Supplier retains the intellectual property rights in the Software and may

		re-use the Software for other customers provided that the customer’s business

		is not in competition with PNA or any of its related companies or associates at

		that time. </span></p>



		<p class=MsoNormal><span lang=EN-GB>&nbsp;</span></p>



		</div>



		</body>



		</html>`;
		const expected = `1.4 The Supplier retains the intellectual property rights in the Software and may re-use the Software for other customers provided that the customer’s business is not in competition with PNA or any of its related companies or associates at that time.`;
		expect(htmlToText(html).trim()).toEqual(expected);
	});

});


