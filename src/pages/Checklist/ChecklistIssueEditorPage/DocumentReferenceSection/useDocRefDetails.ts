import { ICheck, IDocumentReference } from "@mccarthyfinch/public-api-client";
import { useEffect, useState } from "react";
import CheckService from '../../../../models/service/checklistService/CheckService';
import { getDocRefDescription, getDocRefExplanation } from './utils';

export default function useDocRefDetails(docRef?: IDocumentReference): [string, string, boolean, ICheck] {
	const [description, setDescription] = useState('');
	const [explanation, setExplanation] = useState('');

	const checkQuery = CheckService.useCheck(docRef?.checkUUID);

	useEffect(() => {
		if (!docRef) { return; }

		const _description = getDocRefDescription(docRef, checkQuery.data);
		const _explanation = getDocRefExplanation(docRef, checkQuery.data);

		setDescription(_description);
		setExplanation(_explanation);
	}, [docRef, checkQuery.data]);

	return [description, explanation, checkQuery.isLoading, checkQuery.data];
}
