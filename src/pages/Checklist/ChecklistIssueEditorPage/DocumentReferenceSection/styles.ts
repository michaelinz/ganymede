import styled, { css, ThemeColor } from 'styled-components';
import { IconButtonBorderless } from '@mccarthyfinch/author-ui-components';
import UnstyledDrawer from '../../../../components/Drawer';
import { Column } from 'src/components/Layout';
import { loadingPlaceholderAnimation } from '../../../../styles/animations';

export const DocRefListWrapper = styled(Column)`
	overflow-y: auto;
	overflow-x: hidden;
	height: 100%;
	width: auto;
`;

export const ButtonWrapper = styled.div<{ alwaysShow?: boolean }>`
	min-width: 20px;
	max-width: 20px;
	transition: 0.3s all;
	visibility: ${({ alwaysShow }) => alwaysShow ? 'visible' : 'hidden'};
	opacity: ${({ alwaysShow }) => alwaysShow ? 1 : 0};

	${IconButtonBorderless} {
		:hover {
			svg {
				color: ${({ theme }) => theme.color.fontPrimary};
			}
		}
	}
`;


export const DocRefRowWrapper = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	margin: 0.5rem 0;
	min-height: 1.375rem;
	flex: 1 0;

	> div:first-child {
		padding-top: 2px;
	}

	:hover {
		${ButtonWrapper} {
			visibility: visible;
			opacity: 1;
		}
	}
`;

export const DocRefText = styled.div<{ isChecked?: boolean, isItalic?: boolean }>`
	cursor: pointer;
	font-size: 12px;
	flex: 1;

	/* ${({ isChecked, theme }) => {
		if (isChecked) {
			return css`
				text-decoration: line-through;
				color: ${theme.colorPalette.smoke[600]};
			`;
		}
	}} */

	${({ isItalic }) => (isItalic ? 'font-style: italic;' : '')}

	&:hover {
		opacity: 0.5;
		transition: all 0.3s;
	}
`;

export const DrawerTitleAction = styled(IconButtonBorderless)`
	font-size: 16px;
	padding-top: 0;
	padding-bottom: 0;
	margin: 0;
`;

export const Drawer = styled(UnstyledDrawer)`
	height: max-content;
	max-height: 40%;
`;

export const DrawerContentWrapper = styled.div`
	padding: 0 0 0 1.5rem;
	display: flex;
	flex-direction: column;
	flex: 1;
	position: relative;
	overflow: hidden;
`;

export const DrawerTitle = styled.h3`
	font-family: Montserrat;
	font-weight: 600;
	font-size: ${({ theme }) => theme.typography.paragraph.fontSize};
	line-height: ${({ theme }) => theme.typography.paragraph.lineHeight};
	min-height: 18px;
	color: ${({ theme }) => theme.color.fontPrimary};
	margin: 0;
`;

export const EmptyReferences = styled.div`
	border-radius: 8px;
	border: dashed 1px #bfc7d2;
	padding: 0.75rem;
	font-family: 'Montserrat';
	font-weight: 300;
	font-size: 13px;
	line-height: 16px;
	color: ${({ theme }) => theme.color.fontPrimary};
	width: 100%;
`;

export const DrawerContent = styled.div`
	overflow: scroll;
	height: 100%;
	margin-top: 1rem;
	padding-right: 0.5rem;
	padding-bottom: 1rem;
`;


export const PlaceholderCheckboxWrapper = styled.div<{ size: React.ReactText }>`
	${loadingPlaceholderAnimation}

	padding: 0;
	margin: 0;
	border-radius: 50%;
	height: ${({ size = '1rem' }) => size};
	width: ${({ size = '1rem' }) => size};
	flex-shrink: 0;
	display: flex;
	flex-direction: row;
	justify-content: center;
	align-items: center;
	opacity: 1;
	transition: 0.3s all;
`;

export const PlaceholderDocRefText = styled.div`
	${loadingPlaceholderAnimation}

	flex: 1;
	height: 0.875rem;
	border-radius: 0.4375rem;
`;
