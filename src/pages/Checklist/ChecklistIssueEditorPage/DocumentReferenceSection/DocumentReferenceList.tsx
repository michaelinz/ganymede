import React from 'react';
import { IDocumentReference } from '@mccarthyfinch/public-api-client';
import DocumentReferenceService from 'src/models/service/checklistService/DocumentReferenceService';
import DocRefRow from './DocRefRow';
import { goToDocRefInDoc, removeDocRefInDoc } from './utils';
import { DocRefListWrapper } from './styles';
import { HR } from '@mccarthyfinch/author-ui-components';

interface DocumentReferenceListProps {
	issueUUID: string;
	documentReferenceUUIDs: string[];
	documentReferences: IDocumentReference[];
	setContainsAlert?: (warning: boolean) => void;
	setContainsPreCheck?: (warning: boolean) => void;
	className?: string;
	style?: React.CSSProperties;
	shortDividers?: boolean;
}

export default function DocumentReferenceList({ issueUUID, documentReferenceUUIDs, documentReferences, setContainsAlert, setContainsPreCheck, shortDividers, ...rest }: DocumentReferenceListProps) {
	const [updateDocumentReference] = DocumentReferenceService.useUpdateDocumentReference(issueUUID);

	const setChecked = (docRef: IDocumentReference) => {
		return updateDocumentReference({
			uuid: docRef.uuid,
			isChecked: !docRef.isChecked,
		});
	};

	const goToDocRef = async (docRef: IDocumentReference) => {
		goToDocRefInDoc(docRef);
	};

	const onDeleteDocRef = async (docRef: IDocumentReference) => {
		removeDocRefInDoc(docRef.tag);
		return updateDocumentReference({ uuid: docRef.uuid, isActive: false });
	};

	return (
		<DocRefListWrapper {...rest}>
			{documentReferences
				?.filter((docRef) => docRef.isActive)
				.map((docRef, i) => (
					<React.Fragment key={docRef.uuid}>
						{i !== 0 && <HR key={`${docRef.uuid}-hr`} style={{ margin: `0.25rem ${shortDividers ? '2.5rem' : '0'} 0.25rem 0`, width: 'auto' }} />}
						<DocRefRow
							key={docRef.uuid}
							issueUUID={issueUUID}
							docRefUUID={docRef.uuid}
							docRef={docRef}
							setContainsAlert={setContainsAlert}
							setContainsPreCheck={setContainsPreCheck}
							onDelete={() => onDeleteDocRef(docRef)}
							onCheck={() => setChecked(docRef)}
							onGoTo={() => goToDocRef(docRef)}
						/>
					</React.Fragment>
				))
				|| documentReferenceUUIDs.map((docRefUUID, i) => (
					<React.Fragment key={docRefUUID}>
						{i !== 0 && <HR key={`${docRefUUID}-hr`} style={{ margin: `0.25rem ${shortDividers ? '2.5rem' : '0'} 0.25rem 0`, width: 'auto' }} />}
						<DocRefRow
							key={docRefUUID}
							issueUUID={issueUUID}
							docRefUUID={docRefUUID}
							docRef={null}
							setContainsAlert={setContainsAlert}
							setContainsPreCheck={setContainsPreCheck}
							onDelete={null}
							onCheck={null}
							onGoTo={null}
						/>
					</React.Fragment>
				))
			}
		</DocRefListWrapper>
	);
}
