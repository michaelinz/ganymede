import uuid from 'uuid/v4';
import { getEditor } from 'src/editor/EditorFactory';
import { escape } from 'html-escaper';
import htmlToText from './htmlToText';
import { IEditorParagraph } from 'src/editor/base/EditorSelection';
import {
	ActionTypes,
	DocumentReferenceType,
	ICheck,
	IDocumentReference,
	IParagraphContent,
	RuleChecks,
	RulerCategory,
} from '@mccarthyfinch/public-api-client';

export function shortenText(text: string, maxLength = 80) {
	const processedText = text.replace(/\s/g, ' ');
	return processedText.length > maxLength
		? processedText.slice(0, maxLength - 3).trim().concat('...')
		: processedText;
}

export async function createDocRefInDoc(issueUUID: string): Promise<IDocumentReference> {
	const editor = getEditor();

	let html: string, paragraphs: IEditorParagraph[];
	try {
		html = await editor.Selection.getSelectionHtml();
		paragraphs = await editor.Selection.getSelectionParagraphs();
	} catch (err) {
		console.error('DocumentReferenceError: No selection available (createDocRefInDoc)');
		console.error(err);
		return null;
	}

	const tag = `mcf-doc-ref-${uuid()}`;
	const text = htmlToText(html);
	const name = shortenText(`${text}`);

	try {
		await editor.DocumentReference.createDocRefFromSelection(tag);
	} catch (err) {
		console.error('DocumentReferenceError: Could not create docRef from selection (Content Control)');
		console.error(err);
		throw err;
	}

	return {
		uuid: uuid(),
		name,
		isChecked: false,
		isPreChecked: false,
		isActive: true,
		issueUUID,
		type: DocumentReferenceType.Custom,
		tag,
		content: {
			plainText: text,
			html: escape(html),
		},
		paragraphs : paragraphs.map((wp, wpIndex) => ({
			uuid: uuid(),
			text: wp.text,
			index: wp.index,
		} as IParagraphContent)),
	};
}

export async function removeDocRefInDoc(tag: string) {
	if (tag === `mcf-doc-ref-undefined`) {
		console.error('DocumentReferenceError: Undefined document reference (removeDocRefInDoc)');
		return;
	}
	const editor = getEditor();
	await editor.DocumentReference.deleteDocRef(tag);
}

export async function goToDocRefInDoc(docRef: IDocumentReference) {
	if (docRef.tag === `mcf-doc-ref-undefined`) {
		console.error('DocumentReferenceError: Undefined document reference (goToDocRefInDoc)');
		return;
	}
	const editor = getEditor();
	editor.DocumentReference.goToDocRef(docRef);
}

export async function goToDefinitionInDoc(text: string) {
	const editor = getEditor();
	editor.DocumentReference.goToDefinition(text);
}

export async function goToClauseInDoc(text: string[]) {
	const editor = getEditor();
	editor.DocumentReference.goToClause(text);
}

export function isCustomDocRef(docRef: IDocumentReference) {
	return docRef.type === 'Custom';
}

export function isAFindBasedRule(check: ICheck) {
	return [RuleChecks.Match, RuleChecks.Contains, RuleChecks.Exist].includes(check.theIf.condition);
}

export function isFoundRule(check: ICheck) {
	return isAFindBasedRule(check) && !check.theIf.isNegated;
}

export function isNotFoundRule(check: ICheck) {
	return isAFindBasedRule(check) && check.theIf.isNegated;
}

export function isSearch(check: ICheck) {
	return check.theIf.condition === RuleChecks.Match;
}

export function isDetailComparison(check: ICheck) {
	return check.theIf.category === RulerCategory.Detail
		&& [
			RuleChecks.Equals,
			RuleChecks.GreaterThan,
			RuleChecks.GreaterThanEquals,
			RuleChecks.LessThan,
			RuleChecks.LessThanEquals,
		].includes(check.theIf.condition);
}

export function hasAIAction(check: ICheck) {
	return [
		ActionTypes.Delete,
		ActionTypes.Update,
		ActionTypes.Insert,
	].includes(check.theThen.type);
}

export function getDocRefDescription(docRef: IDocumentReference, check: ICheck) {
	if (check?.theThen?.inAppComment) {
		return check?.theThen?.inAppComment;
	}
	if (isCustomDocRef(docRef)) {
		// Custom document references
		return docRef.name;
	}

	if (check && hasAIAction(check)) {
		// Document references with AI actions (not alert)
		let action: string;
		if (docRef.action === 'delete') { action = 'deletion'; }
		else if (docRef.action === 'insert') { action = 'insertion'; }
		else { action = 'update'; }
		return `Suggested ${action}${docRef.hasDocumentComment ? ' and added comment' : ''}`;
	}

	// Alerts only

	if (check && isFoundRule(check)) {
		// Return the first paragraph as the description
		const description = docRef.paragraphs?.length && docRef.paragraphs[0].text;
		if (description) { return shortenText(description); }
	}

	if (check && isNotFoundRule(check)) {
		if (check.theIf.condition === RuleChecks.Exist) {
			return `${check.theIf.categoryType?.name || check.theIf.category} not found`;
		}
		if (check.theIf.condition === RuleChecks.Match) {
			return `${check.name || check.theIf.value} not found`;
		}
		return `${check.theIf.value} not found`;
	}

	return docRef.hasDocumentComment ? 'Added comment' : docRef.name;
}

export function getDocRefExplanation(docRef: IDocumentReference, check: ICheck) {
	if (!check) { return ''; } // Doc refs without checks don't need explanations

	if (isSearch(check)) {
		const query = check.theIf.value;
		const type = check.theIf.categoryType?.name || 'The document';
		const isNegated = check.theIf.isNegated;
		return isNegated
			? `${type} ${hasAIAction(check) ? 'did not match' : 'does not match'} the query: ${query}`
			: `${type} ${hasAIAction(check) ? 'matched' : 'matches'} the query: ${query}`;
	}

	if (isAFindBasedRule(check)) {
		const isNegated = check.theIf.isNegated;
		const type = check.theIf.categoryType?.name
			|| (check.theIf.category === RulerCategory.Concept && 'A clause')
			|| (check.theIf.category === RulerCategory.Detail && 'A detail')
			|| 'An item of interest';
		return check.theIf.condition === RuleChecks.Exist
			? `${type} was ${isNegated ? 'not ' : ''}found in the document`
			: `${type} ${isNegated ? `${hasAIAction(check) ? 'did' : 'does'} not contain` : `contain${hasAIAction(check) ? 'ed' : 's'}`} the words: ${check.theIf.value}`;
	}

	if (isDetailComparison(check)) {
		let comparison;
		if (check.theIf.condition === RuleChecks.Equals) { comparison = 'equal to'; }
		else if (check.theIf.condition === RuleChecks.GreaterThan) { comparison = 'greater than'; }
		else if (check.theIf.condition === RuleChecks.GreaterThanEquals) { comparison = 'greater than or equal to'; }
		else if (check.theIf.condition === RuleChecks.LessThan) { comparison = 'less than'; }
		else if (check.theIf.condition === RuleChecks.LessThanEquals) { comparison = 'less than or equal to'; }
		const type = check.theIf.categoryType?.name || 'A detail';
		const tense = hasAIAction(check) ? 'was' : 'is';
		const value = `${check.theIf.value}${check.theIf.unit.includes('date') ? '' : check.theIf.unit}`;
		return `${type} ${tense} ${comparison} ${value}`;
	}

	return `The "${docRef.name}" rule ${hasAIAction(check) ? 'passed' : 'passes'}`;
}
