import React, { useEffect, useState } from 'react';
import { ActionTypes, AlertTypes, ICheck, IDocumentReference, RulerCategory } from '@mccarthyfinch/public-api-client';
import { DocRefRowWrapper, DocRefText, ButtonWrapper, PlaceholderDocRefText } from './styles';
import { IconButtonBorderless } from '@mccarthyfinch/author-ui-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faThumbsDown } from '@fortawesome/pro-light-svg-icons';
import { LoadingIcon } from '../IssueToolbar/styles';
import Tooltip from 'src/components/Tooltip';
import DocRefCheckbox, { CheckboxType } from './DocRefCheckbox';
import { faQuestionCircle as faQuestionCircleSolid } from '@fortawesome/pro-solid-svg-icons';
import { faBook, faQuestionCircle } from '@fortawesome/pro-regular-svg-icons';
import useDocRefDetails from './useDocRefDetails';
import { PlaceholderCheckbox } from '../../components/Checkbox';
import { hasAIAction, isCustomDocRef, isFoundRule } from './utils';
import { ClauseLibraryUIView, useComparisonShelfUI } from 'src/modules/ClauseLibrary/useClauseLibraryUI';

interface IDocRefRow {
	issueUUID: string;
	docRefUUID: string;
	docRef?: IDocumentReference;
	setContainsAlert?: (warning: boolean) => void;
	setContainsPreCheck?: (warning: boolean) => void;
	onDelete: () => Promise<IDocumentReference>;
	onGoTo: () => void;
	onCheck: () => Promise<IDocumentReference>;
}

export default function DocRefRow({ docRefUUID, docRef, onDelete, onGoTo, onCheck, setContainsAlert, setContainsPreCheck }: IDocRefRow) {
	const [isDeleting, setIsDeleting] = useState(false);
	const [isHoveringDetails, setIsHoveringDetails] = useState(false);
	const comparisonShelf = useComparisonShelfUI();

	const [description, explanation, isLoading, check] = useDocRefDetails(docRef);

	const showPlaceholder = !docRef || isLoading;

	const shouldItalicise = (dr: IDocumentReference, ch: ICheck) => isCustomDocRef(dr) || (isFoundRule(ch) && !hasAIAction(ch));

	const getCheckboxType = (dr: IDocumentReference, ch: ICheck): CheckboxType => {
		if (ch?.theThen.type === ActionTypes.Alert && ch.theThen.alertType) {
			return ch.theThen.alertType as string as CheckboxType;
		}
		return dr.action ? CheckboxType.PreCheck : CheckboxType.Display;
	};

	const _onDelete = () => {
		setIsDeleting(true);
		return onDelete();
	}

	const relatedConceptUUID = check?.theIf?.category === RulerCategory.Concept ? check?.theIf?.categoryTypeUUID : null;
	const showComparisonShelf = () => {
		comparisonShelf?.filters?.setFilters({
			...comparisonShelf?.filters?.filters,
			clauseUUIDs: [ relatedConceptUUID ],
		});
		comparisonShelf?.view.setClauseLibraryUIView(ClauseLibraryUIView.Browse);
	};

	useEffect(() => {
		// Tell a parent component that the docRef is an alert or precheck
		if (docRef && check) {
			const type = getCheckboxType(docRef, check);
			if (type === CheckboxType.Alert && setContainsAlert) {
				setContainsAlert(true);
			} else if (type === CheckboxType.PreCheck && setContainsPreCheck) {
				setContainsPreCheck(true);
			}
		}
	}, [docRef, check]);

	return (
		<DocRefRowWrapper data-ted={docRefUUID}>
			{showPlaceholder
				? (
					<>
						<PlaceholderCheckbox />
						<PlaceholderDocRefText />
						<ButtonWrapper />
						<ButtonWrapper />
					</>
				)
				: (
					<>
						<DocRefCheckbox
							id={docRef.uuid}
							checked={!!docRef.isChecked}
							onClick={onCheck}
							checkboxType={getCheckboxType(docRef, check)}
						/>
						<DocRefText isChecked={docRef.isChecked} onClick={onGoTo} isItalic={shouldItalicise(docRef, check)}>
							{description}
						</DocRefText>

						{
							relatedConceptUUID &&
							<ButtonWrapper>
								<Tooltip title='Related clauses'>
									<div>
										<IconButtonBorderless onClick={showComparisonShelf}>
											<FontAwesomeIcon icon={faBook} />
										</IconButtonBorderless>
									</div>
								</Tooltip>
							</ButtonWrapper>
						}

						<ButtonWrapper
							onMouseEnter={()=> setIsHoveringDetails(true)}
							onMouseLeave={()=> setIsHoveringDetails(false)}
							style={{ marginRight: '0.25rem' }}
						>
							{docRef.type !== 'Custom' && (
								<Tooltip title={explanation} enterDelay={0}>
									<div>
										<IconButtonBorderless>
											<FontAwesomeIcon icon={isHoveringDetails ? faQuestionCircleSolid : faQuestionCircle} />
										</IconButtonBorderless>
									</div>
								</Tooltip>
							)}
						</ButtonWrapper>

						<ButtonWrapper>
							<Tooltip title='Irrelevant'>
								<div>
									<IconButtonBorderless onClick={_onDelete} disabled={isDeleting}>
										{isDeleting ? <LoadingIcon /> : <FontAwesomeIcon icon={faThumbsDown} />}
									</IconButtonBorderless>
								</div>
							</Tooltip>
						</ButtonWrapper>
					</>
				)}
		</DocRefRowWrapper>
	);
}
