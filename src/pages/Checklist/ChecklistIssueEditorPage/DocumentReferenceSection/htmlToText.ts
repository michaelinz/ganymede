import { htmlToText as _htmlToText } from 'html-to-text';

export default function htmlToText(html: string) {
	return _htmlToText(html, { wordwrap: null });
}
