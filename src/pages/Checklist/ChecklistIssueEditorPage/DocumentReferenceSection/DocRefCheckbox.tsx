import React from 'react';
import CustomCheckbox, { ICustomCheckboxProps } from 'src/components/CustomCheckbox';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle as faCheckCircleSolid } from '@fortawesome/pro-solid-svg-icons';
import { faCheckCircle, faExclamationCircle, faCircle } from '@fortawesome/pro-regular-svg-icons';
import { InputWrapper } from '../../components/Checkbox';

export enum CheckboxType {
	Alert = 'alert',
	PreCheck = 'pre-check',
	Display = 'display',
}

interface IDocRefCheckboxProps extends Omit<ICustomCheckboxProps, 'children'> {
	checkboxType: CheckboxType;
}

const DocRefCheckbox = (props: IDocRefCheckboxProps) => {
	const size = '0.875rem';

	return (
		<CustomCheckbox
			data-ted={'docref-isChecked'}
			style={{ marginRight: '0.5rem' }}
			{...props}
		>
			{(checked, disabled) => {

				if (checked) {
					return (
						<InputWrapper color='checked' checked={checked} disabled={disabled} size={size}>
							<FontAwesomeIcon icon={faCheckCircleSolid} />
						</InputWrapper>
					);
				}

				switch (props.checkboxType) {
					case CheckboxType.Alert:
						return (
							<InputWrapper color='danger' checked={checked} disabled={disabled} size={size} shadow>
								<FontAwesomeIcon icon={faExclamationCircle} />
							</InputWrapper>
						);
					case CheckboxType.PreCheck:
						return (
							<InputWrapper color='fontSecondary' checked={checked} disabled={disabled} size={size} shadow>
								<FontAwesomeIcon icon={faCheckCircle} />
							</InputWrapper>
						);
					case CheckboxType.Display:
					default:
						return (
							<InputWrapper color='fontSecondary' checked={checked} disabled={disabled} size={size} shadow>
								<FontAwesomeIcon icon={faCircle} />
							</InputWrapper>
						);
				}

			}}
		</CustomCheckbox>
	);
};

export default DocRefCheckbox;
