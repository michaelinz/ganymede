import React, { useState } from 'react';
import { IDocumentReference, IIssue } from '@mccarthyfinch/public-api-client';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/pro-light-svg-icons';
import { Row } from 'src/components/Layout';
import FeatureFlags from '../../../../config/FeatureFlags';
import DocumentReferenceList from './DocumentReferenceList';
import { createDocRefInDoc } from './utils';
import { Drawer, DrawerContentWrapper, DrawerTitle, DrawerTitleAction, EmptyReferences, DrawerContent } from './styles';
import DocumentReferenceService from 'src/models/service/checklistService/DocumentReferenceService';
import QueryStatusWrapper from 'src/components/QueryStatusWrapper';

interface IDocumentReferenceSectionProps {
	issue: IIssue;
}

export default function DocumentReferenceSection({ issue }: IDocumentReferenceSectionProps) {
	const [isOpen, setIsOpen] = useState(true);
	const documentReferencesQuery = DocumentReferenceService.useDocumentReferencesForIssue(issue.uuid);
	const [createDocumentReference] = DocumentReferenceService.useCreateDocumentReference(issue.uuid);

	const onCreateDocRef = async () => {
		setIsOpen(true);
		const docRef: IDocumentReference = await createDocRefInDoc(issue.uuid);
		if (docRef) {
			return createDocumentReference(docRef);
		}
		return null;
	};

	const docRefLength = documentReferencesQuery?.data?.length ? documentReferencesQuery?.data?.filter((docRef) => docRef.isActive)?.length : 0;

	return (
		<Drawer isOpen={isOpen} setIsOpen={setIsOpen}>
			<QueryStatusWrapper queryResults={[documentReferencesQuery]}>
				<DrawerContentWrapper>
					<Row style={{ justifyContent: 'space-between', paddingRight: '1.5rem' }}>
						<DrawerTitle>
							{isOpen && !docRefLength ? 'Add document references' : 'Document references'} ({docRefLength})
						</DrawerTitle>
						<DrawerTitleAction onClick={onCreateDocRef}>
							<FontAwesomeIcon icon={faPlusCircle} />
						</DrawerTitleAction>
					</Row>
					<DrawerContent>
						{isOpen && (
							<>
								{docRefLength ? (
									<DocumentReferenceList
										issueUUID={issue.uuid}
										documentReferenceUUIDs={issue.documentReferenceUUIDs || []}
										documentReferences={documentReferencesQuery.data?.filter((docRef) => docRef.isActive)}
									/>
								) : (
									<Row style={{ margin: '0' }}>
										<EmptyReferences>
											Select any text in your document and press the <FontAwesomeIcon icon={faPlusCircle} /> button
											{FeatureFlags.ENABLE_DRAG_N_DROP_DOC_REFS && ' or drag and drop onto this shelf'}.
										</EmptyReferences>
									</Row>
								)}
							</>
						)}
					</DrawerContent>

				</DrawerContentWrapper>
			</QueryStatusWrapper>
		</Drawer>
	);
}
