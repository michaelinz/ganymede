import styled from 'styled-components';
import { SecondaryButtonOutline } from '@mccarthyfinch/author-ui-components';
import { Column } from 'src/components/Layout';
import CircularProgress from 'src/components/CircularProgress';

export const ToolbarColumn = styled(Column)`
	width: fit-content;
	display: flex;
	flex-direction: column;
	margin-right: 0.5rem;
	flex-grow: 0;
	flex-shrink: 0;
`;

export const CompleteButton = styled.button<{isChecked: boolean}>`
	outline: none;
	display: flex;
	flex-direction: row;
	align-content: center;
	justify-content: center;
	align-items: center;
	justify-items: center;
	transition: all 0.3s;
	position: relative;
	border: 1px solid;

	padding: 0.5em 1em;
	font-size: ${({ theme }) => theme.typography.caption.fontSize};
	font-weight: ${({ theme }) => theme.fontWeight.bold};
	width: fit-content;
	border-radius: 0.375rem;
	color: ${({ theme, isChecked }) => isChecked ? 'white' : theme.color.fontPrimary};
	border-color: ${({ theme, isChecked }) => isChecked ? theme.color.success : theme.colorPalette.smoke[300]};
	background: ${({ theme, isChecked }) => isChecked ? theme.color.success : 'white'};
	transition: 0.3s all;


	color: ${({ theme, isChecked }) => isChecked ? 'white' : theme.color.fontPrimary};

	& > svg {
		color: ${({ theme, isChecked }) => isChecked ? 'white' : theme.color.fontPrimary};
	}

	&:hover:not(:disabled) {
		cursor: pointer;
		opacity: 0.5;
		/* color: ${ ({ theme }) => theme.color.primary}; */
		/* border-color: ${ ({ theme }) => theme.colorPalette.primary['100']}; */
	}

`;

export const BackButtonWrapper = styled.div`
	color: ${({ theme }) => theme.colorPalette.smoke[600]};
	font-size: ${({ theme }) => theme.typography.subtitle.fontSize};
`;

export const LoadingIcon = styled(CircularProgress)`
	height: 12px;
	width: 12px;
	transform-origin: 6px 6px 6px;
`;
