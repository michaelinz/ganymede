import React, { useState } from 'react';
import { faArrowLeft, faCheck, faTrash } from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconButtonBorderless } from '@mccarthyfinch/author-ui-components';
import { IIssue } from '@mccarthyfinch/public-api-client';
import Zelda from 'src/components/Zelda';
import { Row } from 'src/components/Layout';
import { getChecklistEditorURL } from '../../ChecklistRouter';
import { ToolbarColumn, CompleteButton, BackButtonWrapper, LoadingIcon } from './styles';
import ImportantFlag from '../../components/ImportantFlag/index';

interface IIssueToolbarProps {
	checklistUUID: string;
	issue: IIssue;
	toggleIssueIsChecked: () => void;
	deleteIssue: () => Promise<void>;
	isDeleting: boolean;
	toggleIssueIsImportant: () => Promise<void>;
}

export default function IssueToolbar({ checklistUUID, issue, toggleIssueIsChecked, deleteIssue, isDeleting, toggleIssueIsImportant }: IIssueToolbarProps) {
	return (
		<Row style={{ paddingBottom: '2rem' }}>
			<ToolbarColumn style={{ flex: '1 1 0px' }}>
				<Zelda to={getChecklistEditorURL(checklistUUID)} tabIndex={0} style={{ width: 'fit-content' }}>
					<BackButtonWrapper>
						<FontAwesomeIcon icon={faArrowLeft} />
					</BackButtonWrapper>
				</Zelda>
			</ToolbarColumn>

			<ToolbarColumn style={{width: 'auto'}}>
				<Row>
					<CompleteButton isChecked={issue.isChecked} onClick={toggleIssueIsChecked}>
						{issue.isChecked ? (
							<><FontAwesomeIcon style={{ marginRight: '0.5em' }} icon={faCheck} /> Completed</>
						) : (
							<>
								<FontAwesomeIcon style={{ marginRight: '0.5em' }} icon={faCheck} /> Mark complete
							</>
						)}
					</CompleteButton>
					<ImportantFlag isImportant={issue.isImportant} toggleIsImportant={toggleIssueIsImportant} style={{ marginLeft: '0.5rem' }} />
					<IconButtonBorderless onClick={deleteIssue} disabled={isDeleting}>
						{isDeleting ? <LoadingIcon /> : <FontAwesomeIcon icon={faTrash} />}
					</IconButtonBorderless>
				</Row>
			</ToolbarColumn>
		</Row>
	);
}
