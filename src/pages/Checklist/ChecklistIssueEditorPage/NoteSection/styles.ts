import styled from 'styled-components';
import { McfTextArea } from '@mccarthyfinch/author-ui-components';

export const NotesArea = styled(McfTextArea).attrs({
	cols: 62,
	rows: 22,
})`
	font-family: Montserrat;
	font-size: 13px;
	color: ${({ theme }) => theme.color.fontPrimary};
	resize: vertical;
	border-color: ${({ theme }) => theme.color.fontSecondary};
	padding: 0;
	padding: 0.5rem;

	:hover,
	:focus {
		box-shadow: none;
		border-color: ${({ theme }) => theme.color.fontSecondary};
	}
`;

export const NotesDisplay = styled.div`
	font-family: Montserrat;
	font-size: 13px;
	color: ${({ theme }) => theme.color.fontPrimary};
	white-space: pre-wrap;
	cursor: text;
`;
