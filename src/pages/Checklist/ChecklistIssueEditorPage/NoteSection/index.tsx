import React, { useRef, useState } from 'react';
import { IIssue } from '@mccarthyfinch/public-api-client';
import { convertFormattedStringifiedJSON, FormattedText, useOnClickOutside } from '@mccarthyfinch/author-ui-components';
import { NotesArea, NotesDisplay } from './styles';

interface INoteSection {
	issue: IIssue;
	updateIssue: (issue: Partial<IIssue>) => Promise<IIssue>;
	isEditing: boolean;
	setIsEditing: (isEditing: boolean) => void;
}

export default function NoteSection({ issue, updateIssue, isEditing, setIsEditing }: INoteSection) {
	const [plainText, setPlainText] = useState(issue.notes?.plainText || '');
	const [notes, setNotes] = useState(convertFormattedStringifiedJSON(issue.notes?.formattedText, issue.notes?.plainText || ''));

	const notesRef = useRef();

	useOnClickOutside(notesRef, () => {
		if (isEditing) {
			setIsEditing(false);
		}
	});

	const onNoteChange = (value: FormattedText, plainText: string) => {
		setPlainText(plainText);
		setNotes(value);
		updateIssue({
			notes: {
				plainText: plainText,
				formattedText: value ? JSON.stringify(value) : undefined,
			},
		});
	};

	return isEditing
		? (
			<div ref={notesRef} style={{ display: 'flex', flex: 1 }}>
				<NotesArea
					data-ted='note-section'
					placeholder="Write notes here..."
					value={plainText}
					onChange={e => onNoteChange(undefined, e.target.value)}
					style={{ flex: 1 }}
				/>
			</div>
		) : (
			<NotesDisplay onClick={() => setIsEditing(true)}>{plainText || 'Click here to add notes...'}</NotesDisplay>
		);
}
