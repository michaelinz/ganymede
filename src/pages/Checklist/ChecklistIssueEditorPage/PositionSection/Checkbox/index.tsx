import { faSquare } from '@fortawesome/pro-light-svg-icons';
import { faCheckSquare } from '@fortawesome/pro-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Caption } from '@mccarthyfinch/author-ui-components';
import React from 'react';
import { CheckboxInputWrapper, Input, InputIcon, Label } from './styles';

export interface ICheckboxInput extends React.InputHTMLAttributes<HTMLInputElement> {
	label?: string;
}

const McfCheckbox = (props: ICheckboxInput) => {
	const id = props.id ? props.id : `${props.name}_${props.value}`;
	const { label, className, onClick, disabled = false, ...rest } = props;
	const _onClick = onClick && !disabled ? onClick : undefined;
	return (
		<CheckboxInputWrapper
			data-ted="McfCheckbox"
			className={className}
			onClick={(e) => {
				e.preventDefault();
				return _onClick && _onClick(e as any);
			}}
			aria-checked={props.checked}
		>
			<Label htmlFor={id} checked={props.checked} disabled={disabled}>
				<InputIcon checked={props.checked} disabled={disabled}>
					<FontAwesomeIcon icon={props.checked ? faCheckSquare : faSquare} />
				</InputIcon>
				<Caption aria-label={label}>{label}</Caption>
			</Label>
			{ /** readOnly to remove errors since we are using onClick instead of onChange */}
			<Input tabIndex={0} id={id} type="checkbox" disabled={disabled} readOnly {...rest} />
		</CheckboxInputWrapper>
	);
};

export default McfCheckbox;
