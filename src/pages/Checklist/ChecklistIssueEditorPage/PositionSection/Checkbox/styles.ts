import styled, { css } from 'styled-components';
import { Caption } from '@mccarthyfinch/author-ui-components';

export const CheckboxInputWrapper = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: center;
	align-items: center;
`;

export const Label = styled.label<{ checked: boolean; disabled?: boolean }>`
	display: flex;
	flex-direction: row;
	justify-content: center;
	align-items: center;
	transition: 0.2s all;

	${Caption} {
		margin-left: 0.5em;
		opacity: 1;
		transition: 0.2s all;
		${({ theme, checked }) => {
			if (checked) {
				return css`
					font-weight: ${theme.fontWeight.bold};
				`;
			}
		}}
	}

	${({ disabled }) => {
		if (!disabled) {
			return css`
				&:hover:not(:disabled) {
					cursor: pointer;
				}
			`;
		}
	}}
`;

export const Input = styled.input`
	appearance: none;
	opacity: 0;
	width: 0;
	height: 0;
	position: absolute;
`;

export const InputIcon = styled.i<{ checked: boolean; disabled: boolean }>`
	padding: 0;
	margin: 0;
	height: 1rem;
	width: 1rem;
	line-height: 1rem;
	font-weight: ${({ theme }) => theme.fontWeight.light};
	color: ${({ theme, checked }) => (checked ? theme.color.checked : theme.color.fontSecondary)};
	border-radius: 50%;
	flex-shrink: 0;

	display: flex;
	flex-direction: row;
	justify-content: center;
	align-items: center;
	opacity: 1;
	transition: 0.3s all;

	svg {
		height: 0.9rem !important;
		width: 0.9rem !important;
	}

	&:hover {
		${({ disabled, checked }) =>
			!disabled &&
			!checked &&
			css`
				background-color: transparent;
			`}
	}

	span {
		font-size: 11px;
		line-height: 14px;
		vertical-align: top;
		margin-left: 10px;
		opacity: 1;
		transition: 0.2s all;
		color: ${({ theme }) => theme.color.fontPrimary};
	}
`;
