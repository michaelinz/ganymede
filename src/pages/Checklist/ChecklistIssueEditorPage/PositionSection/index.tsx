import React, { useEffect, useState } from 'react';
import { HR } from '@mccarthyfinch/author-ui-components';
import { IPosition } from '@mccarthyfinch/public-api-client';
import PositionService from 'src/models/service/checklistService/PositionService';
import PositionEditor from './PositionEditor';
import PositionView from './PositionView';
import { Column } from 'src/components/Layout';

/**
 * https://projects.invisionapp.com/share/82ZEFNDJ3TB#/screens/437244246
 *
 * https://projects.invisionapp.com/share/82ZEFNDJ3TB#/screens/437214844
 *
 */
interface IPositionSection {
	issueUUID: string;
	positions: IPosition[];
	showEditor: boolean;
	setShowEditor: (show: boolean) => void;
}

export default function PositionSection({ issueUUID, positions, showEditor, setShowEditor }: IPositionSection) {
	const [positionToEdit, setPositionToEdit] = useState<IPosition>(null);

	// If the editor is set to close, remove the position being edited
	useEffect(() => {
		if (!showEditor) {
			setPositionToEdit(null);
		}
	}, [showEditor]);

	const [create] = PositionService.useCreatePosition(issueUUID);
	const [update] = PositionService.useUpdatePosition(issueUUID);

	const editPosition = (position: IPosition) => {
		setPositionToEdit(position);
		setShowEditor(true);
	};

	const createPosition = (position: IPosition) => {
		return create({
			...position,
			issueUUID,
		});
	};

	const _positions = positions?.filter((position) => position.isActive) || [];

	return showEditor ? (
		<PositionEditor position={positionToEdit || undefined} updatePosition={update} createPosition={createPosition} closeEditor={() => setShowEditor(false)} />
	) : (
		<Column>
			{_positions
				.sort(() => 0) // TODO: These should be ordered by a rank since the order is user controlled
				.map<React.ReactNode>((position) => (
					<PositionView
						key={position.uuid}
						position={position}
						updatePosition={update}
						setIsEditing={(isEditing) => editPosition(isEditing ? position : null)}
					/>
				))
				.reduce((acc, component, i) => (acc === null ? [component] : [acc, <HR key={i} style={{ margin: '0 0 2rem' }} />, component]), null)}
		</Column>
	);
}
