import React, { useRef, useState } from 'react';
import { faTrash } from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FormattedText, IconButtonBorderless, PrimaryButton, SecondaryButtonOutline, useOnClickOutside } from '@mccarthyfinch/author-ui-components';
import { IPosition, PartialIPosition } from '@mccarthyfinch/public-api-client';
import { getBlankPosition } from '../../../utils';
import { Wrapper, PositionTitleInput, PositionDescriptionInput } from './styles';
import PositionService from 'src/models/service/checklistService/PositionService';
import useUpsert from 'src/components/hooks/useUpsert';
import { Row } from 'src/components/Layout';

interface IPositionCreator {
	position?: IPosition;
	createPosition?: (position: IPosition) => Promise<IPosition>;
	updatePosition: (position: PartialIPosition) => Promise<IPosition>;
	closeEditor: () => void;
}

export default function PositionEditor({ position, createPosition, updatePosition, closeEditor }: IPositionCreator) {
	const [item, onUpdateItem] = useUpsert<IPosition>({ item: position || getBlankPosition(), createItem: createPosition, updateItem: updatePosition, isNew: !position });
	const ref = useRef();

	useOnClickOutside(ref, () => {
		closeEditor();
	});

	if (item && !item.isActive) {
		return null;
	}

	const isEditing = !!position;
	const onContentChange = (value: FormattedText, plainText?: string) => {
		onUpdateItem({
			content: {
				plainText,
				formattedText: value ? JSON.stringify(value) : undefined,
			},
		});
	};

	const onDelete = async () => {
		onUpdateItem({
			isActive: false,
		});
		closeEditor();
	};

	return (
		<Wrapper ref={ref}>
			<PositionTitleInput data-ted="name" onChange={(e) => onUpdateItem({ name: e.target.value })} value={item?.name} placeholder="Add position title" />
			<PositionDescriptionInput
				data-ted="content"
				onChange={(e) => onContentChange(undefined, e.target.value)}
				value={item?.content?.plainText}
				placeholder="Add description"
			/>
			{/* <McfRichTextArea
				data-ted='content'
				value={convertFormattedStringifiedJSON(_position.content?.formattedText, _position.content?.plainText)}
				onChange={onContentChange}
				placeholder='Add description'
			/> */}
			{/* <PrimaryButton style={{ marginBottom: '0.5rem' }} onClick={() => onUpdateItem(position)}>
				{isEditing ? 'Save' : 'Create'}
			</PrimaryButton> */}
			{isEditing && (
				<Row style={{flexDirection: 'row-reverse'}}>
					<IconButtonBorderless onClick={() => onDelete()} style={{ paddingRight: '0' }}>
						<FontAwesomeIcon icon={faTrash} />
					</IconButtonBorderless>
				</Row>

				// <SecondaryButtonOutline onClick={onDelete}>
				// 	<FontAwesomeIcon icon={faTrash} style={{ marginRight: '0.25rem' }} />
				// 	Delete
				// </SecondaryButtonOutline>
			)}
		</Wrapper>
	);
}
