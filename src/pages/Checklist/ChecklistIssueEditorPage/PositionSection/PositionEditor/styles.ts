import styled from 'styled-components';
import { McfTextArea, McfTextInput } from '@mccarthyfinch/author-ui-components';

export const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
`;

export const PositionTitleInput = styled(McfTextInput)`
	font-size: ${({ theme }) => theme.typography.paragraph.fontSize};
	line-height: ${({ theme }) => theme.typography.paragraph.lineHeight};
	margin-bottom: 1rem;

	input {
		padding: 0.5rem;
		border-radius: 4px;
		border: solid 1px #e8eaee;
		background-color: #fcfcfd;
		font-family: Montserrat;
	}
`;

export const PositionDescriptionInput = styled(McfTextArea).attrs({
	rows: 6,
})`
	margin-bottom: 1rem;
	resize: none;

	padding: 0.5rem;
	border-radius: 4px;
	border: solid 1px #e8eaee;
	background-color: #fcfcfd;
	font-family: Montserrat;
`;
