import React, { useState } from 'react';
import { faPencil } from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconButtonBorderless, Paragraph, Subtitle } from '@mccarthyfinch/author-ui-components';
import { IPosition } from '@mccarthyfinch/public-api-client';
import { Column, Row } from 'src/components/Layout';
import PositionEditor from './PositionEditor';
import Checkbox from './Checkbox';
import { PositionViewWrapper } from './styles';

interface IPositionView {
	position: IPosition;
	updatePosition: (position: Partial<IPosition> & Required<{ uuid: string }>) => Promise<IPosition>;
	setIsEditing: (editing: boolean) => void;
}

export default function PositionView({ position, updatePosition, setIsEditing }: IPositionView) {
	return (
		<PositionViewWrapper data-ted={position.uuid}>
			<Row style={{ justifyContent: 'space-between', paddingBottom: '1rem' }}>
				<Row style={{ flex: '1 0' }}>
					<Subtitle fontWeight='bold'>{position.name}</Subtitle>
					<IconButtonBorderless onClick={() => setIsEditing(true)}>
						<FontAwesomeIcon icon={faPencil} />
					</IconButtonBorderless>
				</Row>
				<Row style={{ flex: 0 }}>
					<Checkbox
						label="Used" checked={position.isUsed}
						onClick={() => updatePosition({ uuid: position.uuid, isUsed: !position.isUsed })}
						readOnly
					/>
				</Row>
			</Row>
			<Row>
				<Paragraph>{position.content?.plainText}</Paragraph>
			</Row>
		</PositionViewWrapper>
	);
}

// import { convertFormattedStringifiedJSON, McfRichTextDisplay, McfTextArea, Paragraph, Subtitle, useOnClickOutside } from '@mccarthyfinch/author-ui-components';
// import { IPosition } from '@mccarthyfinch/public-api-client';
// import React, { useRef, useState } from 'react';
// import { getBlankPosition } from './utils';

// interface IPositionView {
// 	position: IPosition,
// 	updatePosition: (position: Partial<IPosition> & Required<{uuid: string}>) => Promise<IPosition>;
// }

// export default function PositionView({ position: _position, updatePosition }: IPositionView) {
// 	const [position, setPosition] = useState<IPosition>(_position || getBlankPosition());
// 	const [isEditingName, setIsEditingName] = useState(false);
// 	const [isEditingContent, setIsEditingContent] = useState(false);

// 	const nameRef = useRef();
// 	const contentRef = useRef();

// 	useOnClickOutside(nameRef, () => {
// 		if (isEditingName) {
// 			setIsEditingName(false);
// 		}
// 	});

// 	useOnClickOutside(contentRef, () => {
// 		if (isEditingContent) {
// 			setIsEditingContent(false);
// 		}
// 	});

// 	const update = async (field: string) => {
// 		// todo: debounce this
// 		await updatePosition({
// 			uuid: position.uuid,
// 			[field]: position[field],
// 		});
// 	}

// 	return (
// 		<div data-ted={position.uuid}>
// 			{
// 				isEditingName ?
// 					<div ref={nameRef}>
// 						<McfTextArea
// 							value={position.name}
// 							onChange={(e) => {
// 								setPosition({ ...position, name: e.target.value })
// 								update('name');
// 							}}
// 						/>
// 					</div>
// 					:
// 					<Subtitle onClick={() => setIsEditingName(true)}>{position.name}</Subtitle>
// 			}
// 			{/* <McfRichTextDisplay
// 				value={convertFormattedStringifiedJSON(position.content?.formattedText, position.content?.plainText)}
// 			/> */}
// 			{
// 				isEditingContent ?
// 					<div ref={contentRef}>
// 						<McfTextArea
// 							value={position?.content?.plainText}
// 							onChange={e => {
// 								setPosition({
// 									...position, content: {
// 										plainText: e.target.value
// 									}
// 								});
// 								update('content');
// 							}}
// 						/>
// 					</div>
// 					:
// 					<Paragraph style={{minHeight: '2rem'}} onClick={() => setIsEditingContent(true)}>{position.content?.plainText}</Paragraph>
// 			}
// 		</div>
// 	);
// }
