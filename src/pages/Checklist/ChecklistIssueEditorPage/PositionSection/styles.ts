import styled from 'styled-components';
import { Column } from 'src/components/Layout';
import { IconButtonBorderless } from '@mccarthyfinch/author-ui-components';

export const PositionViewWrapper = styled(Column)`
	padding-right: 1rem;
	margin-bottom: 2rem;

	${IconButtonBorderless} {
		display: none;
	}

	:hover {
		${IconButtonBorderless} {
			display: flex;
		}
	}
`;
