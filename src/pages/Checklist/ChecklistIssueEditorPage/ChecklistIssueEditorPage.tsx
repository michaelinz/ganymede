import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { IDocumentReference, IIssue, IPosition } from '@mccarthyfinch/public-api-client';
import { IconButtonBorderless, Title } from '@mccarthyfinch/author-ui-components';
import { Row } from 'src/components/Layout';
import IssueToolbar from './IssueToolbar';
import PositionSection from './PositionSection';
import NoteSection from './NoteSection';
import { getChecklistEditorURL } from '../ChecklistRouter';
import IssueTabSection, { IssueTab } from './IssueTabSection';
import { ITab } from './IssueTabSection/Tab';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle, faTimesCircle } from '@fortawesome/pro-light-svg-icons';
import DocumentReferenceSection from './DocumentReferenceSection';
import { ContentWrapper } from './styles';
import UnderlineTextInput from 'src/components/molecules/UnderlineTextInput';
import { QueryObserverResult } from 'react-query';
import { removeDocRefInDoc } from './DocumentReferenceSection/utils';
import { DrawerPageWrapper, DrawerPageContentWrapper } from 'src/components/Drawer';

interface IChecklistEditorPageProps {
	issue: IIssue;
	positions: IPosition[];
	checklistUUID: string;
	documentReferencesQuery: QueryObserverResult<IDocumentReference[], unknown>;
	updateIssue: (issue: Partial<IIssue>) => Promise<IIssue>;
	updatePosition: (position: Partial<IPosition>) => Promise<IPosition>;
	updateDocumentReference: (position: Partial<IDocumentReference>) => Promise<IDocumentReference>;
}

function ChecklistIssueEditorPage({ checklistUUID, positions, documentReferencesQuery, issue, updateIssue, updatePosition, updateDocumentReference }: IChecklistEditorPageProps) {
	const [isEditingNotes, setIsEditingNotes] = useState(false);
	const [showPositionEditor, setShowPositionEditor] = useState(false);
	const [isDeleting, setIsDeleting] = useState(false);

	const history = useHistory();

	const visibleTabs = [IssueTab.Notes];

	const tabs: ITab[] = visibleTabs.map((tabType) => {
		let label: string;
		switch (tabType) {
			case IssueTab.Positions:
				label = `${tabType} (${positions?.filter((p) => p.isActive).length || 0})`;
				break;
			default:
				label = tabType;
		}
		return {
			value: tabType,
			label,
		};
	});

	const onDeleteIssue = async () => {
		// Can't delete again
		if (!issue.isActive) {
			console.warn('Issue cannot be deleted as it is already inactive', issue);
			return;
		}

		// Don't need to go back to false when done as we're redirecting
		setIsDeleting(true);

		const waitForUpdates: Promise<any>[] = [
			updateIssue({ isActive: false })
		];

		documentReferencesQuery.data.forEach(docRef => waitForUpdates.push(removeDocRefInDoc(docRef.tag)));

		await Promise.all(waitForUpdates);

		history.push(getChecklistEditorURL(checklistUUID));
	};

	const toggleIssueIsImportant = async () => {
		await updateIssue({ uuid: issue.uuid, isImportant: !issue.isImportant });
	};

	return (
		<DrawerPageWrapper>
			<DrawerPageContentWrapper>
				<ContentWrapper style={{ height: 'auto'}}>
					<IssueToolbar
						checklistUUID={checklistUUID}
						issue={issue}
						toggleIssueIsChecked={() => updateIssue({ isChecked: !issue.isChecked })}
						deleteIssue={onDeleteIssue}
						isDeleting={isDeleting}
						toggleIssueIsImportant={toggleIssueIsImportant}
					/>

					<Row data-ted='name'>
						<Title style={{ width: '100%' }} fontWeight="bold">
							<UnderlineTextInput
								data-ted="name"
								value={issue?.name}
								placeholder="Describe an issue"
								onChange={async (e) => {
									updateIssue({ name: e.target.value });
								}}
							/>
						</Title>
					</Row>
				</ContentWrapper>

				<ContentWrapper style={{ maxHeight: '100%', minHeight: 0, flexGrow: 1, position: 'relative' }}>
					<IssueTabSection
						tabs={tabs}
						renderAction={(currentTab) => {
							switch (currentTab) {
								case IssueTab.Positions:
									return (
										<IconButtonBorderless onClick={() => setShowPositionEditor(!showPositionEditor)} style={{ fontSize: '16px' }}>
											<FontAwesomeIcon icon={showPositionEditor ? faTimesCircle : faPlusCircle} />
										</IconButtonBorderless>
									);
								default:
									return null;
							}
						}}
					>
						{(currentTab) => {
							switch (currentTab) {
								case IssueTab.Notes:
									return <NoteSection issue={issue} updateIssue={updateIssue} isEditing={isEditingNotes} setIsEditing={setIsEditingNotes} />;
								case IssueTab.Positions:
									return <PositionSection issueUUID={issue.uuid} positions={positions} showEditor={showPositionEditor} setShowEditor={setShowPositionEditor} />;
								case IssueTab.ClauseBank:
									return <div>:(</div>;
								default:
									return null;
							}
						}}
					</IssueTabSection>
				</ContentWrapper>
			</DrawerPageContentWrapper>
			<DocumentReferenceSection issue={issue} />
		</DrawerPageWrapper>
	);
}

export default ChecklistIssueEditorPage;
