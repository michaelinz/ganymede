import { McfTextInput } from '@mccarthyfinch/author-ui-components';
import styled from 'styled-components';

export const ContentWrapper = styled.div`
	display: flex;
	padding: 1rem 1.5rem;
	flex-direction: column;
	position: relative;
	flex-grow: 0;
	flex-shrink: 0;
	height: fit-content;
`;

export const IssueNameEditor = styled(McfTextInput)`
	font-size: 18px;
	line-height: 1.1em;
`;
