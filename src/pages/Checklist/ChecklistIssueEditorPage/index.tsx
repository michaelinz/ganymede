import React from 'react';
import { useParams } from 'react-router-dom';
import CircularProgress from 'src/components/CircularProgress';
import { Content } from 'src/components/Layout/Page';
import QueryStatusWrapper from 'src/components/QueryStatusWrapper';
import DocumentReferenceService from 'src/models/service/checklistService/DocumentReferenceService';
import IssueService from 'src/models/service/checklistService/IssueService';
import PositionService from 'src/models/service/checklistService/PositionService';
import ChecklistIssueEditorPage from './ChecklistIssueEditorPage';
import { ContentWrapper } from './styles';

function ChecklistIssueEditorPageContainer() {
	const { checklistUUID, issueUUID } = useParams<{ checklistUUID: string; issueUUID: string }>();
	const issueQuery = IssueService.useIssue(checklistUUID, issueUUID);
	const positionsQuery = PositionService.usePositionsForIssue(issueUUID);
	const documentReferencesQuery = DocumentReferenceService.useDocumentReferencesForIssue(issueUUID);
	const [updateIssue] = IssueService.useUpdateIssue(checklistUUID);
	const [updatePosition] = PositionService.useUpdatePosition(issueUUID);
	const [updateDocumentReference] = DocumentReferenceService.useUpdateDocumentReference(issueUUID);

	return (
		<Content style={{ backgroundColor: 'white', minHeight: 0 }}>
			<QueryStatusWrapper
				queryResults={[issueQuery, positionsQuery]}
				renderLoading={() => (
					<ContentWrapper style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flex: '1' }}>
						<CircularProgress />
					</ContentWrapper>
				)}
				renderError={() => <div>Something went wrong?</div>}
			>
				<ChecklistIssueEditorPage
					checklistUUID={checklistUUID}
					issue={issueQuery.data}
					positions={positionsQuery.data}
					updateIssue={(partialItem) => updateIssue({ uuid: issueQuery.data.uuid, ...partialItem })}
					updatePosition={updatePosition}
					updateDocumentReference={updateDocumentReference}
					documentReferencesQuery={documentReferencesQuery}
				/>
			</QueryStatusWrapper>
		</Content>
	);
}

export default ChecklistIssueEditorPageContainer;
