import { Caption } from '@mccarthyfinch/author-ui-components';
import styled from 'styled-components';

export const TabsWrapper = styled.div.attrs({
	role: 'tablist',
})`
	display: flex;
	flex-direction: row;
	border-bottom: solid 2px #e8eaee;
	width: 100%;
	justify-content: space-between;
	align-items: stretch;
`;

export const TabLabel = styled(Caption).attrs({
	fontWeight: 'bold',
})<{ selected: boolean, disabled: boolean }>`
	font-size: ${({ theme }) => theme.typography.caption.fontSize};
	color: ${({ selected, disabled, theme }) => {
		if (disabled) {
			return theme.color.disabled;
		}
		if (selected) {
			return theme.color.fontPrimary;
		}
		return theme.color.fontSecondary;
	}};
`;

export const TabWrapper = styled.button.attrs({
	role: 'tab',
})<{ selected: boolean; disabled }>`
	border: none;
	background-color: transparent;
	border-bottom: ${({ selected, theme }) => (selected ? `solid 3px ${theme.color.fontPrimary}` : `solid 3px transparent`)};
	padding: 0 0 0.5rem 0;
	margin-right: 1rem;
	cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer')};

	:focus {
		outline: none;
		${TabLabel} {
			color: ${({ theme }) => theme.color.primary};
		}
	}
`;
