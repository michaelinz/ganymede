import React, { useEffect, useRef } from 'react';
import { IssueTab, getTabID, getPanelID } from './index';
import { TabWrapper, TabLabel } from './styles';

export interface ITab {
	label: string;
	value: IssueTab;
}

interface ITabProps {
	tab: ITab;
	selected: boolean;
	disabled?: boolean;
	onClickTab: () => void;
	onKeyboard: (e: React.KeyboardEvent<HTMLButtonElement>) => void;
}

export default function Tab({ tab, selected, disabled, onClickTab, onKeyboard }: ITabProps) {
	const id = getTabID(tab.value);
	const ref = useRef(null);

	useEffect(() => {
		if (selected) {
			// Move element into view when it is focused
			ref.current.focus();
		}
	}, [selected]);

	return (
		<TabWrapper
			id={id}
			selected={selected}
			disabled={disabled}
			onClick={onClickTab}
			aria-selected={selected}
			aria-disabled={disabled}
			aria-controls={getPanelID(tab.value)}
			tabIndex={selected ? 0 : -1}
			data-ted={id}
			onKeyDown={onKeyboard}
			ref={ref}
		>
			<TabLabel selected={selected} disabled={disabled}>
				{tab.label}
			</TabLabel>
		</TabWrapper>
	);
}
