import React, { useEffect, useState } from 'react';
import { Row } from 'src/components/Layout';
import Tab, { ITab } from './Tab';
import { TabsWrapper } from './styles';

export enum IssueTab {
	Notes = 'Notes',
	Positions = 'Positions',
	ClauseBank = 'ClauseBank',
}

export const getTabID = (tab: IssueTab) => `${tab.toLowerCase()}-tab`;
export const getPanelID = (tab: IssueTab) => `${tab.toLowerCase()}-panel`;

interface IIssueTabSectionProps {
	tabs: ITab[];
	children: (currentTab: IssueTab) => React.ReactChild;
	renderAction?: (currentTab: IssueTab) => React.ReactChild;
}

export default function IssueTabSection({ tabs, children, renderAction }: IIssueTabSectionProps) {
	const [currentTab, setCurrentTab] = useState(IssueTab.Notes);

	const tabNames = tabs.map((t) => t.value);
	const currIndex = tabNames.indexOf(currentTab);

	const selectFirstTab = () => setCurrentTab(tabNames[0]);
	const selectLastTab = () => setCurrentTab(tabNames[tabNames.length - 1]);
	const selectNextTab = () => (currIndex === tabNames.length - 1 ? selectFirstTab() : setCurrentTab(tabNames[currIndex + 1]));
	const selectPrevTab = () => (currIndex === 0 ? selectLastTab() : setCurrentTab(tabNames[currIndex - 1]));

	useEffect(() => {
		// Prevent user from seeing a tab not on the list
		if (!tabNames.includes(currentTab)) {
			selectFirstTab();
		}
	});

	const onKeyboard = (e: React.KeyboardEvent<HTMLButtonElement>) => {
		switch (e.key) {
			case 'ArrowLeft':
				selectPrevTab();
				break;
			case 'ArrowRight':
				selectNextTab();
				break;
			case 'Home':
				selectFirstTab();
				break;
			case 'End':
				selectLastTab();
				break;
			default:
				return;
		}
	};

	return (
		<>
			<Row>
				<TabsWrapper aria-label="Issue Details">
					<div>
						{tabs.map((tab) => (
							<Tab key={tab.value} tab={tab} selected={currentTab === tab.value} onClickTab={() => setCurrentTab(tab.value)} onKeyboard={onKeyboard} />
						))}
					</div>
					<div>{renderAction && renderAction(currentTab)}</div>
				</TabsWrapper>
			</Row>

			<Row
				id={getPanelID(currentTab)}
				role="tabpanel"
				// tabIndex={0} // Recommended if there are no focusable elements inside the panel
				aria-labelledby={getTabID(currentTab)}
				style={{ paddingTop: '2rem', paddingBottom: '2rem', overflow: 'auto', flexShrink: 1, display: 'block' }}
			>
				{children(currentTab)}
			</Row>
		</>
	);
}
