import React from 'react';
import { generatePath } from 'react-router-dom';
import SiteUrl from 'src/components/SiteUrl/SiteUrl';

const ChecklistDashboardPage = React.lazy(() => import('./ChecklistDashboardPage'));
const Checklist = React.lazy(() => import('./index'));
const ChecklistEditorBasePage = React.lazy(() => import('./ChecklistEditorBasePage'));
const ChecklistEditorPage = React.lazy(() => import('./ChecklistEditorPage'));
const ChecklistIssueEditorPage = React.lazy(() => import('./ChecklistIssueEditorPage'));
const ChecklistRedirectToPrevious = React.lazy(() => import('./ChecklistRedirectToPrevious'));

// Routes
export const ChecklistEditorRoutes = {
	ChecklistIssueEditor: new SiteUrl({
		name: 'ChecklistIssueEditor',
		path: '/issue/:issueUUID',
		component: ChecklistIssueEditorPage,
	}),
	ChecklistEditor: new SiteUrl({
		name: 'ChecklistEditor',
		path: '/',
		component: ChecklistEditorPage,
	}),
};

export const ChecklistRoutes = {
	ChecklistEditorPage: new SiteUrl({
		name: 'Checklist',
		path: '/editor/:checklistUUID',
		component: ChecklistEditorBasePage,
		childRoutes: ChecklistEditorRoutes,
	}),
	ChecklistDashboard: new SiteUrl({
		name: 'ChecklistDashboard',
		path: '/dashboard',
		component: ChecklistDashboardPage,
	}),
	ChecklistRedirectToPrevious: new SiteUrl({
		name: 'ChecklistRedirectToPrevious',
		path: '/',
		component: ChecklistRedirectToPrevious,
	}),
};

const ChecklistRouter = new SiteUrl({
	name: 'Checklist',
	path: '/checklist',
	component: Checklist,
	childRoutes: ChecklistRoutes,
});

// URL builder utils
export const getChecklistEditorURL = (checklistUUID: string) => generatePath(ChecklistEditorRoutes.ChecklistEditor.getHref(), { checklistUUID });
export const getChecklistIssueURL = (checklistUUID: string, issueUUID: string) =>
	generatePath(ChecklistEditorRoutes.ChecklistIssueEditor.getHref(), { checklistUUID, issueUUID });
export const ChecklistRedirectToPreviousURL = () => generatePath(ChecklistRoutes.ChecklistRedirectToPrevious.getHref());

export default ChecklistRouter;
