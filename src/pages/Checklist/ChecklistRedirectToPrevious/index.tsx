import React, { useEffect, useState } from 'react';

import useChecklistUUID from '../hooks/useChecklistUUID';
import { Redirect } from 'react-router-dom';
import { ChecklistRoutes, getChecklistEditorURL } from '../ChecklistRouter';
import LoadingOverlay from 'src/components/LoadingOverlay';
import useLastProcessed, { App } from '../hooks/useLastProcessed';
import ChecklistService from 'src/models/service/checklistService/ChecklistService';

export default function ChecklistRedirectToPrevious() {
	const [ isLoadingLastProcessed, lastProcessed ] = useLastProcessed();
	const [ isLoadingChecklistUUID, settingsChecklistUUID, setSettingsChecklistUUID ] = useChecklistUUID();
	const [ checklistUUIDForApp, setChecklistUUIDForApp ] = useState<string>();
	const [ isLoading, setIsLoading ] = useState(true);

	useEffect(() => {
		if (!isLoadingLastProcessed && !isLoadingChecklistUUID) {
			console.log('Last processed app', lastProcessed.app, lastProcessed);
			if (lastProcessed.app === App.CHECKLIST) {
				setChecklistUUIDForApp(settingsChecklistUUID);
				setIsLoading(false);
				return;
			}
			if (lastProcessed.app === App.APPROVE) {
				ChecklistService.getChecklistByDocumentUUID(lastProcessed.documentUUID)
					.then((checklist) => {
						if (checklist?.uuid) {
							setChecklistUUIDForApp(checklist.uuid);
							setSettingsChecklistUUID(checklist.uuid);
						} else {
							console.error('Could not find checklist for document uuid', lastProcessed.documentUUID);
						}
					})
					.finally(() => {
						setIsLoading(false);
					});
				return;
			}
			setIsLoading(false);
			return;
		}
	}, [ isLoadingLastProcessed, isLoadingChecklistUUID ]);

	if (isLoading) {
		return <LoadingOverlay />;
	}

	if (checklistUUIDForApp) {
		return <Redirect to={getChecklistEditorURL(checklistUUIDForApp)} />;
	}
	return <Redirect to={ChecklistRoutes.ChecklistDashboard.getHref()} />;
}
