import { FormattedText, getBlankFormattedText, McfRichTextArea } from '@mccarthyfinch/author-ui-components';
import React, { useState } from 'react';

export const Simple = () => {
	const [value, setValue] = useState<FormattedText>(getBlankFormattedText());
	return (
		<>
			<button
				onClick={() => {
					setValue(getBlankFormattedText());
					setTimeout(() => {
						setValue(getBlankFormattedText());
					}, 501);
				}}
			>
				Clear text
			</button>

			<McfRichTextArea value={value} onChange={setValue} placeholder="Placeholder text..." debounceDelay={500} />
			<h3>Debounced formatted value:</h3>
			<p style={{ whiteSpace: 'pre-wrap' }}>{JSON.stringify(value, null, '\t')}</p>
		</>
	);
};

export default function Playground() {
	return <Simple />;
}

// import { observable } from 'mobx';
// import { observer } from 'mobx-react';
// import React from 'react';
// import { Redirect } from 'react-router-dom';
// import { getEditor } from 'src/editor/EditorFactory';
// import ChecklistService from 'src/models/service/checklistService/ChecklistService';
// import { LoginStatus, useAuthStore } from 'src/models/store/AuthStore';
// import { AppRoutes } from '../App/AppRouter';

// interface IPlaygroundProps {
// }

// function Playground({ }: IPlaygroundProps) {
// 	const authStore = useAuthStore();

// 	console.log(authStore.loginStatus);
// 	if (authStore.loginStatus !== LoginStatus.LoggedIn) {
// 		return (<Redirect to={AppRoutes.CheckIn.getHref()} />);
// 	}
// 	const editor = getEditor();
// 	const tagName = 'ted-test';

// 	const createLocation = async () => {
// 		const html = await editor.Selection.getSelectionHtml();
// 		console.log(html);
// 		await editor.DocumentReference.createDocRefFromSelection(tagName);
// 	};

// 	const goToLocation = () => {
// 		editor.DocumentReference.goToDocRef(tagName);
// 	};

// 	const clearLocation = () => {
// 		editor.DocumentReference.deleteDocRef(tagName);
// 	};

// 	const onDrop = async (e: React.DragEvent<HTMLDivElement>) => {
// 		/**
// 		 * Note: this seems to delete the dropped text from word when dropped onto the browser
// 		 * but not when dropped into the word task pane.
// 		 */
// 		e.preventDefault();
// 		const html = await editor.Selection.getSelectionHtml();
// 		console.log(html);
// 		return false;
// 	};

// 	const onLogout = async () => {
// 		authStore.logOut();
// 	};

// 	return (
// 		<div onDrop={onDrop} onDragOver={e => e.preventDefault()}>
// 			<h1>Playground</h1>

// 			<h2>Play</h2>
// 			<div style={{display: 'flex', flexDirection: 'column'}}>
// 				<button onClick={onLogout}>Logout</button>

// 				<button onClick={createLocation}>create</button>
// 				<button onClick={goToLocation}>Go to</button>
// 				<button onClick={clearLocation}>clear</button>
// 				{/* <button onClick={getParagraphs}>getParagraphs</button> */}

// 			</div>

// 		</div>
// 	);
// }

// export default observer(Playground);
