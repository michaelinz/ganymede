import React from 'react';
import styled from 'styled-components';
import { Annotation } from '@mccarthyfinch/author-ui-components';
import Zelda from 'src/components/Zelda';
import { IconDefinition } from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

interface IModuleCardProps {
	backgroundImage: string; // image
	label: string;
	href: string;
	icon: IconDefinition;
}

export default function ModuleCard({backgroundImage, label, href, icon}: IModuleCardProps) {
	return (
		<Zelda to={href}>
			<CardWrapper backgroundImage={backgroundImage}>
				<LabelWrapper>
					<Annotation>{label}</Annotation>
				</LabelWrapper>
				<IconWrapper><FontAwesomeIcon icon={icon} /></IconWrapper>
			</CardWrapper>
		</Zelda>
	);
}

const CardWrapper = styled.div<{backgroundImage: string}>`
	border-radius: 0.5rem;
	background-color: transparent;
	background-image: url(${({ backgroundImage }) => backgroundImage});
	background-position: left top;
	background-repeat: no-repeat;
	background-size: contain;

	object-fit: contain;
  box-shadow: 0 0 16px 0 rgba(59, 73, 98, 0.2);

	transition: all 0.2s;

	width: 8.625rem;
	height: 4.5rem;

	display: flex;
	flex-direction: row;
	padding: 0.75rem;

	&:hover {
		opacity: 0.7;
	}
`;

const LabelWrapper = styled.div`
	${Annotation} {
		color: white;
		font-weight: ${({ theme }) => theme.fontWeight.medium};
	}
	display: flex;
	flex-direction: column;
	flex-flow: column-reverse;
	flex-grow: 1;

`;

const IconWrapper = styled.div`
	display: flex;

	svg {
		color: white;
		font-size: 1rem;
	}
`;

export const ModuleCardGrid = styled.div`
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	width: 100%;
	margin: -0.5rem 0 0 -0.5rem;

	& > * {
		margin: 0.5rem 0 0 0.5rem;
	}
`;
