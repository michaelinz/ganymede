import { faTasks, faBook } from '@fortawesome/pro-solid-svg-icons';
import { Heading, Paragraph } from '@mccarthyfinch/author-ui-components';
import { IUser } from '@mccarthyfinch/public-api-client';
import { observer } from 'mobx-react';
import React from 'react';
import Header, { HeaderColumn, PrimaryHeaderBar, SecondaryHeaderBar } from 'src/components/Layout/Header';
import { Content, PageWrapper } from 'src/components/Layout/Page';
import { useAuthStore } from 'src/models/store/AuthStore';
import { AppRoutes } from 'src/pages/App/AppRouter';
import dashboardBackground from '../../../../public/assets/dashboard/dashboard-01.jpg';
import checklistBackground from '../../../../public/assets/dashboard/module-checklists.jpg';
import clauseBankBackground from '../../../../public/assets/dashboard/module-clauselibrary.jpg';
import ModuleCard, { ModuleCardGrid } from './ModuleCard';

function DashboardPage() {
	const authStore = useAuthStore();
	return <DashboardPageView user={authStore.user} />;
}

export default observer(DashboardPage);

interface IDashboardPageView {
	user: IUser;
}
function DashboardPageView({ user }: IDashboardPageView) {
	return (
		<PageWrapper>
			<Header background={dashboardBackground}>
				<PrimaryHeaderBar>
					<HeaderColumn>
						<Heading fontWeight="bold">Hi {user.name}</Heading>
					</HeaderColumn>
				</PrimaryHeaderBar>
				<SecondaryHeaderBar />
			</Header>

			<Content style={{ padding: '1rem' }}>
				<Paragraph fontWeight="bold">Get started</Paragraph>
				<br />

				<ModuleCardGrid>
					<ModuleCard href={AppRoutes.Checklist.getHref()} label="Checklist" icon={faTasks} backgroundImage={checklistBackground} />
					<ModuleCard href={AppRoutes.ClauseLibrary.getHref()} label="Clause Library" icon={faBook} backgroundImage={clauseBankBackground} />
				</ModuleCardGrid>
			</Content>
		</PageWrapper>
	);
}
