import React, { Suspense } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import LoadingOverlay from '../../components/LoadingOverlay';
import DashboardRouter from './DashboardRouter';

export default function Dashboard() {
	const match = useRouteMatch();
	return (
		<Suspense fallback={<LoadingOverlay />}>
			<Switch>
				{Object.values(DashboardRouter.childRoutes).map((siteUrl) => {
					return <Route key={siteUrl.path} path={`${match?.url || ''}${siteUrl.path}`} component={siteUrl.component} exact={siteUrl.path === '/'} />;
				})}
			</Switch>
		</Suspense>
	);
}
