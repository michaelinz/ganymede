import React from 'react';
import { generatePath } from 'react-router-dom';
import SiteUrl from 'src/components/SiteUrl/SiteUrl';

const DashboardPage = React.lazy(() => import('./DashboardPage'));
const Dashboard = React.lazy(() => import('./index'));

export const DashboardRoutes = {
	Dashboard: new SiteUrl({
		name: 'DashboardPage',
		path: '/',
		component: DashboardPage,
	}),
};

const DashboardRouter = new SiteUrl({
	name: 'Dashboard',
	path: '/dashboard',
	component: Dashboard,
	childRoutes: DashboardRoutes,
});

export default DashboardRouter;
