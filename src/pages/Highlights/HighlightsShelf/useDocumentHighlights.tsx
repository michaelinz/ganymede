import React, { useEffect, useState } from 'react';
import { getEditor } from 'src/editor/EditorFactory';
import { IHighlights } from '@mccarthyfinch/public-api-client';
import useLastProcessed from 'src/pages/Checklist/hooks/useLastProcessed';
import UserPreferenceService from 'src/models/service/userPreferenceService/UserPreferenceService';

export default function useDocumentHighlights() {
	const [documentHighlights, _setDocumentHighlights] = useState<IHighlights>();
	const [isLoading, setIsLoading] = useState(true);
	const [, lastProcessed, checkLastProcessed] = useLastProcessed();
	const documentUUID = lastProcessed?.documentUUID;

	useEffect(() => {
		const init = async () => {
			const [userHighlights, docHighlights] = await Promise.all([
				getUserPreferences(),
				getDocumentHighlights(),
			]);
			if (shouldProcessDocument(docHighlights, userHighlights)) {
				setDocumentHighlights(userHighlights);
			}
			else {
				_setDocumentHighlights({
					referenceOn: userHighlights.referenceOn,
					definitionTipOn: userHighlights.definitionTipOn,
					searchMatchOn: userHighlights.searchMatchOn
				});
				setIsLoading(false);
			}
		};
		init();
		checkLastProcessed();
	}, []);

	const setDocumentHighlights = async (highlights: IHighlights) => {
		if (!isLoading)
			setIsLoading(true);
		const editor = getEditor();
		// Set highlight settings against document
		await editor.Settings.setDocumentHighlights(JSON.stringify(highlights));
		if (documentUUID) {
			// Toggle highlights
			await editor.Highlights.toggleHighlighting(documentUUID, highlights);
		}
		// Update user preference
		await UserPreferenceService.updateUserPreference({ highlights });

		_setDocumentHighlights({
			referenceOn: highlights.referenceOn,
			definitionTipOn: highlights.definitionTipOn,
			searchMatchOn: highlights.searchMatchOn
		});

		setIsLoading(false);
	};

	const highlightDocument = () => {
		const editor = getEditor();
		return editor.Highlights.toggleHighlighting(documentUUID, documentHighlights);
	};

	return { isLoading, documentHighlights, setDocumentHighlights, highlightDocument };
}


const defaultSettings: IHighlights = {
	definitionTipOn: false,
	referenceOn: false,
	searchMatchOn: false
};

async function getUserPreferences() {
	try {
		const userPreferences = await UserPreferenceService.getPreferences();
		if (userPreferences !== "" && userPreferences.highlights) {
			return userPreferences.highlights as IHighlights;
		}
	} catch (e) {
		console.error(e);
	}
	console.log('No user preferences found');
	return defaultSettings;
}

async function getDocumentHighlights() {
	const editor = getEditor();
	const docHighlights = await editor.Settings.getDocumentHighlights();
	if (docHighlights && docHighlights !== "undefined") {
		return JSON.parse(docHighlights);
	}
	console.log('No document highlights found');
	return defaultSettings;
}


function shouldProcessDocument(docHighlights: IHighlights, userHighlights: IHighlights) {
	return (
		docHighlights.definitionTipOn !== userHighlights.definitionTipOn ||
		docHighlights.referenceOn !== userHighlights.referenceOn ||
		docHighlights.searchMatchOn !== userHighlights.searchMatchOn);
}
