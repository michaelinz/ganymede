import React from 'react';
import { SettingsRowWrapper, Top, ColorSwatch, Separator, ToggleSwitch, SpinnerWrapper, Spinner, Paragraph } from './styles';
import { faSpinnerThird } from '@fortawesome/pro-duotone-svg-icons';

interface IHighlightSettingRowProps {
	label: string;
	checked: boolean;
	onChange: () => void;
	disabled?: boolean;
	spinning: boolean;
	description?: React.ReactChild;
	color?: string;
}

export default function ({ label, checked, onChange, disabled = false, spinning, description, color }: IHighlightSettingRowProps) {
	return (
		<SettingsRowWrapper>
			<Top>
				<span>
					{color && <ColorSwatch color={color} checked={checked}/>}
					<Paragraph checked={checked} onClick={() => !disabled && onChange()}>{label}</Paragraph>
				</span>							
				<SpinnerWrapper>
					{spinning ?
						<Spinner icon={faSpinnerThird} spin />
						: 
						<ToggleSwitch
							onClick={() => !disabled && onChange()}
							checked={checked}
							disabled={disabled}
						/> 
					}
				</SpinnerWrapper>																
			</Top>
			{description}
            <Separator />
		</SettingsRowWrapper>
	);
};