import styled from 'styled-components';
import { Paragraph as ParagraphBase } from '@mccarthyfinch/author-ui-components/dist/module/atoms/Typography';
import McfSwitch from '@mccarthyfinch/author-ui-components/dist/module/molecules/McfSwitch';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const ColorSwatch = styled.div<{ checked: boolean }>`
  min-width: 0.875rem;
  height: 0.875rem;
  margin-left: 0.5rem;
  margin-right: 0.4375rem;
  border-radius: 0.4375rem;
  border: ${({color}) => {
    return color === 'transparent' ? 'solid 0.0625rem transparent' : 'solid 0.0625rem #e3e6eb';
  }};
  background-color: ${({ color, theme }) => theme.color[color]};
  align-self: center;
  opacity: ${({checked}) => {
    return checked ? '100%' : '50%';
  }};
`;

export const SettingsRowWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 1rem 0.5rem 0 0;
  margin-left: 1rem;
  margin-right: 1rem;
  height: 65px;
  padding: 15px 0 0;
  align-content: center;
`;

export const Top = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  user-select: none;
  transition: all 0.2s;
  margin-bottom: 0.875rem;

	> span {
		display: flex;
		flex-direction: row;
	}
`;

export const Separator = styled.div`
  height: 1px;
  opacity: 0.75;
  background-color: #e8eaee;
`;

export const ToggleSwitch = styled(McfSwitch)<{ checked: boolean }>`
  > div {
    height: 1.125rem;
    width: 2rem;
    border-radius: 0.625rem;
    outline-color: transparent;
    background-color: ${({checked}) => {
      return checked ? '#222222' : '#a4afbf';
    }};
    padding-left: 0;

    > input {
      margin: 1px;
    }

    > div {
      height: 1rem;
      width: 1rem;
    }
  }
`;

export const SpinnerWrapper = styled.span`
  width: 1.75rem;
  height: 1.75rem;
  margin: 0 0.625rem 0.4375rem 3.75rem;
  padding: 2px 0 0 2px;
`;

export const Spinner = styled(FontAwesomeIcon)`
  font-size: 1.5rem;
  .my-icon .fa-primary {
    fill: #222222;
  }
  .my-icon .fa-secondary {
      fill: #222222;
      opacity: 0.4;
  }
`;

export const Paragraph = styled(ParagraphBase)<{ checked: boolean }>`
  color: ${({checked}) => {
    return checked ? '#222222' : '#939dab';
  }};
`;