import React, { useState } from 'react';
import HighlightSettingRow from '../HighlightSettingRow';
import { Section, Content } from './styles';
import useDocumentHighlights from '../useDocumentHighlights';
import LoadingOverlay from 'src/components/LoadingOverlay';

export enum HighlightSetting {
	ALL = 'ALL',
	DEFINITION_REFERENCE = 'DEFINITION_REFERENCE',
	SEARCH_ALERT = 'SEARCH_ALERT',
}

export default function HighlightsPage() {
	const { documentHighlights, setDocumentHighlights } = useDocumentHighlights();
	const [allLoading, setAllLoading] = useState(false);
	const [referenceLoading, setReferenceLoading] = useState(false);
	const [searchLoading, setSearchLoading] = useState(false);

	if (!documentHighlights) {
		return <LoadingOverlay />;
	}
	async function toggleHighlighter(highlightSetting: HighlightSetting) {
		switch (highlightSetting) {
			case HighlightSetting.ALL:
				setAllLoading(true);
				setReferenceLoading(true);
				setSearchLoading(true);
				if (documentHighlights.definitionTipOn && documentHighlights.searchMatchOn) {
					documentHighlights.searchMatchOn = false;
					documentHighlights.definitionTipOn = false;
					documentHighlights.referenceOn = false;
				} else {
					documentHighlights.searchMatchOn = true;
					documentHighlights.definitionTipOn = true;
					documentHighlights.referenceOn = true;
				}
				await setDocumentHighlights(documentHighlights);
				setAllLoading(false);
				setReferenceLoading(false);
				setSearchLoading(false);
				break;
			case HighlightSetting.DEFINITION_REFERENCE:
				setReferenceLoading(true);
				if (documentHighlights.definitionTipOn) {
					documentHighlights.definitionTipOn = false;
					documentHighlights.referenceOn = false;
				} else {
					documentHighlights.definitionTipOn = true;
					documentHighlights.referenceOn = true;
				}
				await setDocumentHighlights(documentHighlights);
				setReferenceLoading(false);
				break;
			case HighlightSetting.SEARCH_ALERT:
				setSearchLoading(true);
				documentHighlights.searchMatchOn = !documentHighlights.searchMatchOn;
				await setDocumentHighlights(documentHighlights);
				setSearchLoading(false);
				break;
		}
	}

	const isChecked = (highlight: HighlightSetting) => {
		switch (highlight) {
			case HighlightSetting.ALL:
				return documentHighlights.definitionTipOn && documentHighlights.searchMatchOn;
			case HighlightSetting.DEFINITION_REFERENCE:
				return documentHighlights.definitionTipOn;
			case HighlightSetting.SEARCH_ALERT:
				return documentHighlights.searchMatchOn;
		}
	};

	const getHighlightButtonProps = (highlight: HighlightSetting) => ({
		checked: isChecked(highlight),
		onChange: () => toggleHighlighter(highlight),
	});

	return (
		<Content>
			<Section>Remove highlights before forwarding a document or manage which highlights you’d like to see.</Section>
			<HighlightSettingRow label="All" color="transparent" {...getHighlightButtonProps(HighlightSetting.ALL)} spinning={allLoading} />
			<HighlightSettingRow
				label="Defined terms & document references"
				color="tertiary"
				{...getHighlightButtonProps(HighlightSetting.DEFINITION_REFERENCE)}
				spinning={referenceLoading}
			/>
			<HighlightSettingRow label="Alert language" color="danger" {...getHighlightButtonProps(HighlightSetting.SEARCH_ALERT)} spinning={searchLoading} />
		</Content>
	);
}
