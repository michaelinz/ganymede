import styled from 'styled-components';
import { Content as HighlightContent } from 'src/components/Layout/Page';

export const Section = styled.div`
	margin: 0.75rem 1rem 0.75rem 1rem;
`;

export const Content = styled(HighlightContent)`
	border-radius: 0;
`;