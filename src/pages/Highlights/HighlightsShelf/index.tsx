import React from 'react';
import HighlightsPage from './HighlightsPage';
import McfPanel from 'src/components/McfPanel/McfPanel';
import { HR } from '@mccarthyfinch/author-ui-components';

interface IHighlightShelfProps {
	isOpen: boolean;
	onDismiss: () => void;
}

export default function HighlightShelf({ isOpen, onDismiss }: IHighlightShelfProps) {
	return (
		<McfPanel isOpen={isOpen} onDismiss={onDismiss} heading="Manage highlights">
			<HR />
			<HighlightsPage />
		</McfPanel>
	);
}
