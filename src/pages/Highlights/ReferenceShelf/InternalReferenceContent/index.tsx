import React from 'react';
import { IconButtonBorderless } from '@mccarthyfinch/author-ui-components';
import { ContentWrapper, RowWrapper, Row, Title, ButtonWrapper, Content } from '../styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCrosshairs } from '@fortawesome/pro-regular-svg-icons';
import { goToClauseInDoc } from 'src/pages/Checklist/ChecklistIssueEditorPage/DocumentReferenceSection/utils';

interface IInternalReferenceContentProps {
	text: string;
	sourceText: string[];
}

export default function ({ text, sourceText }: IInternalReferenceContentProps) {
	return (
		<ContentWrapper>
			<RowWrapper>
				<Row style={{ flex: '1' }}>
					<Title>{text}</Title>
				</Row>
				<Row style={{ width: '2.75rem', flex: '0 0 2.75rem', minWidth: '2.75rem' }}>
					<ButtonWrapper style={{ marginRight: '0.25rem' }}>
						<IconButtonBorderless style={{ paddingRight: '0' }}>
							<FontAwesomeIcon icon={faCrosshairs} style={{ fontSize: '0.9375rem' }} 
								onClick={() => {goToClauseInDoc(sourceText);}}/>
						</IconButtonBorderless>
					</ButtonWrapper>		
				</Row>
			</RowWrapper>			
			<Row>
				<Content style={{whiteSpace: 'pre-wrap'}}>{sourceText.join('\n\n')}</Content>
			</Row>
		</ContentWrapper>
	);
}
