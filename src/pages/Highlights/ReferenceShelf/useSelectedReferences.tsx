import { IHighlightReferences } from '@mccarthyfinch/public-api-client';
import { useCallback, useEffect, useState } from 'react';
import { ISelectedReferences } from 'src/editor/base/EditorHighlights';
import { getEditor } from 'src/editor/EditorFactory';
import HighlightService from 'src/models/service/highlightService/HighlightService';

export default function useSelectedReferences(documentUUID: string) {
	const [isLoading, setIsLoading] = useState(false);
	const [isLoadingHighlightReferences, setIsLoadingHighlightReferences] = useState(true);
	const [ highlightReferences, setHighlightReferences ] = useState<IHighlightReferences>();
	const [selectedReferences, setSelectedReferences] = useState<ISelectedReferences>(null);

	const editor = getEditor();
	useEffect(() => {
		HighlightService.getHighlightReferences(documentUUID).then(data => {
			setHighlightReferences(data);
			setIsLoadingHighlightReferences(false);
		});
	}, [documentUUID]);

	useEffect(() => {
		if (editor && documentUUID && !isLoadingHighlightReferences) {
			editor.Document.onDocumentSelectionChange(onDocumentSelectionChanged);
		}
		return (() => {
			// Clean up
			if (documentUUID) {
				editor.Document.onDocumentSelectionChange(onDocumentSelectionChanged, true);
			}
		});
	}, [editor, documentUUID, isLoadingHighlightReferences]);


	const onDocumentSelectionChanged = useCallback(async () => {
		if (await editor.Highlights.isClickAction()) {
			console.log("CLICK EVENT REGISTERED");
			setIsLoading(true);
			const selectedReferences = await editor.Highlights.getSelectedReferences(highlightReferences);
			setSelectedReferences(selectedReferences);
			setIsLoading(false);
		} else {
			console.log("User has selected text, skipping definition/internal contents reference pop-up");
			return;
		}
	}, [highlightReferences]);

	const clearSelectedReferences = () => {
		setSelectedReferences(null);
	};

	return { isLoading: isLoading || !highlightReferences, selectedReferences, clearSelectedReferences };
}
