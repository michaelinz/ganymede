import React from 'react';
import { IconButtonBorderless } from '@mccarthyfinch/author-ui-components';
import { ContentWrapper, RowWrapper, Row, Title, ButtonWrapper, Content } from '../styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCrosshairs } from '@fortawesome/pro-regular-svg-icons';
import { goToDefinitionInDoc } from 'src/pages/Checklist/ChecklistIssueEditorPage/DocumentReferenceSection/utils';

interface IDefinitionContentProps {
	text: string;
	definition: string;
	missingDefinition: boolean;
}

export default function ({ text, definition, missingDefinition }: IDefinitionContentProps) {
	return (
		<ContentWrapper>
			<RowWrapper>
				<Row style={{ flex: '1' }}>
					<Title>{text}</Title>
				</Row>
				<Row style={{ width: '2.75rem', flex: '0 0 2.75rem', minWidth: '2.75rem' }}>
					<ButtonWrapper style={{ marginRight: '0.25rem' }}>
						<IconButtonBorderless style={{ paddingRight: '0' }}>
							<FontAwesomeIcon icon={faCrosshairs} style={{ fontSize: '0.9375rem' }} 
								onClick={() => {!missingDefinition ? goToDefinitionInDoc(definition) : {};}}/>
						</IconButtonBorderless>
					</ButtonWrapper>		
				</Row>
			</RowWrapper>			
			<Row>
				<Content style={{whiteSpace: 'pre-wrap'}}>
					{missingDefinition ? (
						<React.Fragment>
							We did not detect a definition for this phrase.
							<br />
							You may wish to define this.
						</React.Fragment>
					) : (
						definition
					)}
				</Content>
			</Row>
		</ContentWrapper>
	);
}
