import { IHighlightDefinition, IHighlightInternalContentReference } from '@mccarthyfinch/public-api-client';
import React from 'react';
import McfPanel from 'src/components/McfPanel/McfPanel';
import DefinitionContent from './DefinitionContent';
import InternalReferenceContent from './InternalReferenceContent';
import { Menu, Separator } from './styles';
import { groupWith } from 'ramda';
import { HR } from '@mccarthyfinch/author-ui-components';
import useSelectedReferences from './useSelectedReferences';
import LoadingOverlay from 'src/components/LoadingOverlay';
import { ISelectedReferences } from 'src/editor/base/EditorHighlights';

interface PanelProps {
	heading: React.ReactChild;
	content: React.ReactChild;
	type: string;
	item?: IHighlightDefinition | IHighlightInternalContentReference;
}

interface IReferenceShelfProps {
	documentUUID?: string;
}

export default function ReferenceShelf({ documentUUID }: IReferenceShelfProps) {
	const selectedReferenceData = useSelectedReferences(documentUUID);
	const panelProps = getProps(selectedReferenceData?.selectedReferences);
	const isOpen = Boolean(selectedReferenceData?.selectedReferences && (selectedReferenceData?.selectedReferences?.definitions.length || selectedReferenceData?.selectedReferences?.internalContentReferences.length));
	const onDismiss = () => {
		selectedReferenceData?.clearSelectedReferences();
	};
	return (
		<McfPanel
			isOpen={isOpen}
			onDismiss={onDismiss}
			heading={panelProps?.heading ? panelProps.heading : null}
		>
			<HR />
			{
				selectedReferenceData?.isLoading ?
					<LoadingOverlay /> :
					panelProps?.content
			}
		</McfPanel>
	);
}

function getProps(selectedReferences: ISelectedReferences) {
	if (selectedReferences) {
		return itemsToPanelProps([
			selectedReferences?.definitions?.map(showDefinition).filter(i => i)[0],
			selectedReferences?.internalContentReferences?.map(showInternalContentReference).filter(i => i)[0],
		].filter(i => i));
	}
	return null;
}

const showDefinition = (definition: IHighlightDefinition) => {
	if (definition) {
		const props = {
			heading: (definition.missingDefinition ? `Undefined Terms` : `Defined Term`),
			type: 'Dictionary',
			content: <DefinitionContent text={definition.text} missingDefinition={definition.missingDefinition} definition={definition.definition} />,
			item: definition
		};
		return props;
	}
};

const showInternalContentReference = (internalContentReference: IHighlightInternalContentReference) => {
	if (internalContentReference) {
		const props = {
			heading: `Clause reference`,
			type: 'CrossConcept',
			content: (
				<InternalReferenceContent
					text={internalContentReference.text}
					sourceText={internalContentReference.clauseText}
				/>
			),
			item: internalContentReference,
		};
		return props;
	}
};

function itemsToPanelProps(items: PanelProps[]) {
	if (items.length === 0) {
		return null;
	}
	const getContent = () => {
		const content = [];
		const groupedByType: PanelProps[][] = groupWith((a, b) => a.type === b.type, items);
		const typesWithContentGrouped = groupedByType.map(group => {
			return {
				heading: group[0].heading,
				content: <React.Fragment key={group[0].item.uuid}>{group.map(g => <React.Fragment key={`${g.item.uuid}_content`}>{g.content}</React.Fragment>)}</React.Fragment>
			};
		});
		typesWithContentGrouped.forEach((item, i) => {
			if (i > 0) {
				content.push(<Menu>{item.heading}</Menu>);
			}
			content.push(item.content);
		});
		return (
			<React.Fragment>
				{content.map((c, i) => {
					return (
						<React.Fragment key={i}>
							{c}
						</React.Fragment>
					);
				})}
			</React.Fragment>
		);
	};
	return {
		heading: items[0].heading,
		item: items,
		// type: items[0].type,
		content: getContent(),
	};
}
