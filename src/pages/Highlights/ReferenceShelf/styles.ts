import styled from 'styled-components';
import { IconButtonBorderless, Paragraph } from '@mccarthyfinch/author-ui-components';
import { Row as RowBase } from 'src/components/Layout';

export const ContentWrapper = styled.div`
	padding: 1rem;
`;

export const RowWrapper = styled(RowBase)`
`;

export const Row = styled(RowBase)`
	margin: 0.5rem 0;
`;

export const Menu = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	padding: 0.5rem;
	height: 2.75rem;
`;

export const Content = styled(Paragraph)`
	font-size: 0.8125rem;
	font-weight: 300;
	font-stretch: normal;
	font-style: normal;
	line-height: 1.23;
	letter-spacing: normal;
	color: #222222;
`;

export const Title = styled(Paragraph)`
	font-size: 0.8125rem;
	font-weight: 600;
	font-stretch: normal;
	font-style: normal;
	line-height: 1.23;
	letter-spacing: normal;
	color: #222222;
	padding-bottom: 0.625rem;
	text-transform: capitalize;
`;

export const Separator = styled.div`
	height: 1px;
	opacity: 0.75;
	background-color: #e8eaee;
	margin: 0.75rem 1rem 0 1rem;
`;

export const ButtonWrapper = styled.div`
	min-width: 20px;
	max-width: 20px;
	transition: 0.3s all;

	${IconButtonBorderless} {
		svg {
			color: #a4afbf;
		}
		:hover {
			svg {
				color: #222222;
			}
		}
	}
`;