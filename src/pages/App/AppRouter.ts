import React from 'react';
import SiteUrl from '../../components/SiteUrl/SiteUrl';
import CheckInRouter from '../CheckIn/CheckInRouter';
import OnitLoginRouter from '../CheckIn/OnitPages/OnitLoginRouter';
import ChecklistRouter from '../Checklist/ChecklistRouter';
import ClauseLibraryRouter from '../ClauseLibrary/ClauseLibraryRouter';
import DashboardRouter from '../Dashboard/DashboardRouter';

const Playground = React.lazy(() => import('../Playground'));

export const AppRoutes = {
	Checklist: ChecklistRouter,
	CheckIn: CheckInRouter,
	OnitLogin: OnitLoginRouter,
	ClauseLibrary: ClauseLibraryRouter,
	Dashboard: DashboardRouter,
  
	Playground: new SiteUrl({
		name: 'Playground',
		path: '/playground',
		component: Playground,
	})
};

const AppRouter = new SiteUrl({
	name: 'App',
	path: '/',
	childRoutes: AppRoutes,
	isPrivate: false,
});

export default AppRouter;
