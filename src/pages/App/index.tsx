import React, { Suspense } from 'react';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';
import LoadingOverlay from 'src/components/LoadingOverlay';
import PrivateRoute from 'src/components/PrivateRoute';
import AppRouter, { AppRoutes } from './AppRouter';
import { AppWrapper, ContentWrapper, FrameWrapper } from './styles';
import ErrorBoundary from './ErrorBoundary';

function App() {
	const match = useRouteMatch();
	return (
		<AppWrapper>
			<ContentWrapper>
				<ErrorBoundary>
					{/* <FrameWrapper> */}
					<Suspense fallback={<LoadingOverlay />}>
						<Switch>
							{Object.values(AppRouter.childRoutes).map((siteUrl) => {
								const RouteComponent = siteUrl.isPrivate ? PrivateRoute : Route;
								return (
									<RouteComponent
										key={siteUrl.path}
										path={`${match?.url === '/' ? '' : match?.url}${siteUrl.path}`}
										component={siteUrl.component}
										exact={siteUrl.path === '/'}
									/>
								);
							})}
							<Route exact path="/" render={() => <Redirect to={AppRoutes.Dashboard.getHref()} />} />
						</Switch>
					</Suspense>
				</ErrorBoundary>
				{/* </FrameWrapper> */}
			</ContentWrapper>
		</AppWrapper>
	);
}

export default App;

// Hot Module Replacement (HMR) - Remove this snippet to remove HMR.
// Learn more: https://www.snowpack.dev/#hot-module-replacement
if (import.meta.hot) {
	import.meta.hot.accept();
}
