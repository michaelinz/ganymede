import styled from 'styled-components';

export const AppWrapper = styled.div`
	display: flex;
	flex-direction: row;
	height: 100%;
	max-width: 100vw;
	max-height: 100vh;
	position: relative;
	overflow: hidden;
`;

export const ContentWrapper = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	width: 100%;
	height: 100%;
	min-height: 100vh;
	max-height: 100vh;
	overflow-y: auto;
	position: relative;
`;

export const FrameWrapper = styled.div`
	border: solid 1px black;
	width: 352px;
	max-height: 830px;
	display: flex;
	flex: 1 0;
`;

export const PageWrapper = styled.div`
	height: 100%;
	width: 100%;
	max-width: 22rem;
`;
