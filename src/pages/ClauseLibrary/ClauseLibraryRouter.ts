import React from 'react';
import { generatePath } from 'react-router-dom';
import SiteUrl from 'src/components/SiteUrl/SiteUrl';

const ClauseLibraryPage = React.lazy(() => import('./ClauseLibraryPage'));
const ClauseLibrary = React.lazy(() => import('./index'));

export const ClauseLibraryRoutes = {
	ClauseLibrary: new SiteUrl({
		name: 'ClauseLibraryPage',
		path: '/',
		component: ClauseLibraryPage,
	}),
};

const ClauseLibraryRouter = new SiteUrl({
	name: 'ClauseLibrary',
	path: '/clause-library',
	component: ClauseLibrary,
	childRoutes: ClauseLibraryRoutes,
});

export default ClauseLibraryRouter;
