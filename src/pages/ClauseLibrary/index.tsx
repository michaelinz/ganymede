import React, { Suspense } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import LoadingOverlay from '../../components/LoadingOverlay';
import ClauseLibraryRouter from './ClauseLibraryRouter';

export default function ClauseLibrary() {
	const match = useRouteMatch();
	return (
		<Suspense fallback={<LoadingOverlay />}>
			<Switch>
				{Object.values(ClauseLibraryRouter.childRoutes).map((siteUrl) => {
					return <Route key={siteUrl.path} path={`${match?.url || ''}${siteUrl.path}`} component={siteUrl.component} exact={siteUrl.path === '/'} />;
				})}
			</Switch>
		</Suspense>
	);
}
