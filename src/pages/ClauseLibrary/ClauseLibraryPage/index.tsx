import React, { useEffect } from 'react';
import { ClauseLibraryPageUIProvider, ClauseLibraryUIView, useClauseLibraryPageUI } from 'src/modules/ClauseLibrary/useClauseLibraryUI';
import ClauseLibrary from 'src/modules/ClauseLibrary';
import ClauseLibraryPageHeader from 'src/modules/ClauseLibrary/ClauseLibraryPageHeader';

export default function ClauseLibraryPage() {
	return (
		<ClauseLibraryPageUIProvider>
			<ClauseLibraryPageWithContext />
		</ClauseLibraryPageUIProvider>
	);
}

function ClauseLibraryPageWithContext() {
	const clauseLibraryUI = useClauseLibraryPageUI();
	useEffect(() => {
		if (clauseLibraryUI?.view?.clauseLibraryUIView === ClauseLibraryUIView.Closed) {
			clauseLibraryUI?.view?.setClauseLibraryUIView(ClauseLibraryUIView.Browse);
		}
	}, []);
	return <ClauseLibrary clauseLibraryUI={clauseLibraryUI} renderHeader={(clauseLibraryUI) => <ClauseLibraryPageHeader clauseLibraryUI={clauseLibraryUI} />} />;
}
