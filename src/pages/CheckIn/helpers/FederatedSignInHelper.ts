import Auth, { CognitoUser } from "@aws-amplify/auth";
import { Hub } from '@aws-amplify/core';
import { CognitoUserAttribute } from "amazon-cognito-identity-js";
import AuthStore from "src/models/store/AuthStore";
import {getCognitoConfigs} from "src/models/service/auth/CognitoService";
// import OrgStore from "src/models/store/";
import AuthCognitoProvider, { CognitoUserData } from "../../../models/service/auth/AuthCognitoProvider";

// https://github.com/aws-amplify/amplify-js/issues/1316

export type IdentityProvider =
	'Facebook' |
	'Google' |
	'McCarthyFinchMicrosoft' |
	'Okta' |
	'Onit';

export enum AuthSource {
	Cognito = 'Cognito',
	CognitoFacebook = 'CognitoFacebook',
	CognitoGoogle = 'CognitoGoogle',
	CognitoOkta = 'CognitoOkta',
	CognitoOnitTest = 'CognitoOnitTest',
}


const LOGIN_POLL_MS = 500;

export class FederatedSignInError extends Error {
	errorCode: string;
	data: any;
	cognitoUserData: CognitoUserData;

	constructor(error, cognitoUserData?: CognitoUserData) {
		super(error.message);

		if (Error.captureStackTrace) {
			Error.captureStackTrace(this, FederatedSignInError);
		}
		this.errorCode = error.errorCode;
		this.name = 'FederatedSignInError';
		this.cognitoUserData = cognitoUserData;
		this.data = error.data;
	}
}

export const FederatedSignInErrors = {
	NO_EMAIL: {
		errorCode: 'NO_EMAIL',
		message: 'No email',
	},
	USER_EMAIL_EXISTS: {
		errorCode: 'USER_EMAIL_EXISTS',
		message: 'Email already exists',
	},
	ORG_KEY_NOT_SET_IN_STATE: {
		errorCode: 'ORG_KEY_NOT_SET_IN_STATE',
		message: 'Org key was not set in state',
	},
	NO_ORG_ACCESS: {
		errorCode: 'NO_ORG_ACCESS',
		message: 'You do not have access to that organisation',
	},
	EMAIL_NOT_VERIFIED: {
		errorCode: 'EMAIL_NOT_VERIFIED',
		message: 'Your email address is not verified'
	}
}


export function getFederationLink({ identityProvider }: { identityProvider: IdentityProvider | string }): string {

	const configuration = AuthCognitoProvider.Configuration; 

	if (!identityProvider) {
		throw new Error('identity provider is required');
	}

	const loginProps = {
		domain: configuration.oauth.domain,
		identity_provider: `${identityProvider}`,
		redirect_uri: configuration.oauth.redirectSignIn,
		response_type: configuration.oauth.responseType,
		client_id: configuration.Auth.userPoolWebClientId,
		scope: ['aws.cognito.signin.user.admin', 'email', 'openid', 'phone', 'profile']
	};

	const queryParams = Object.keys(loginProps).map(key => {
		if (key === 'domain') {
			return null;
		}
		let value = loginProps[key];
		if (Array.isArray(value)) {
			value = value.join('%20');
		}
		return `${key}=${value}`;
	})
		.filter(s => s)
		.join('&');
	const loginUrl = `https://${loginProps.domain}/oauth2/authorize?${queryParams}`;
	return loginUrl;
}

interface IdentityProviderProps {
	authStore: AuthStore;
}

export default class FederatedSignInHelper {
	authStore: AuthStore;

	private currentSignInRequest;
	cognitoUser: CognitoUserData;

	constructor(props: IdentityProviderProps) {
		this.authStore = props.authStore;
	}

	getFederationLink = getFederationLink;

	// async startFederatedSignIn({ identityProvider }: { identityProvider: IdentityProvider }): Promise<CognitoUserData> {
	// 	this.authStore.clearAuthStorage();
	// 	const loginUrl = this.getFederationLink({ identityProvider });
	// 	if (identityProvider === 'Onit') {
	// 		const onitDomainPickerURL = `${location.hostname}/onit`;
	// 		window.open(onitDomainPickerURL, 'OAuth', "height=640,width=960,toolbar=no,menubar=no,scrollbars=no,location=no,status=no");
	// 	} else {
	// 		window.open(loginUrl, 'OAuth', "height=640,width=960,toolbar=no,menubar=no,scrollbars=no,location=no,status=no");
	// 	}

	// 	const cognitoUserData = await this.startPollForLogin();
	// 	return cognitoUserData;
	// }

	// public async loginThroughFederation() {
	// 	const cognitoUserData = await this.startPollForLogin();
	// 	if (cognitoUserData) {
	// 		return this.authStore.loginToAuthor(cognitoUserData, true);
	// 	}
	// }

	public startPollForLogin(): Promise<CognitoUserData> {
		return new Promise((resolve, reject) => {
			const interval = setInterval(() => {
				if (!this.currentSignInRequest) {
					const request = Auth.currentAuthenticatedUser()
						.then(async (user: CognitoUser) => {
							if (interval) {
								clearInterval(interval);
							}
							this.currentSignInRequest = null;
							try {
								const cognitoUserData = await this.loginUser(user);
								this.authStore.tokenSetUp()
								resolve(cognitoUserData);
							} catch (e) {
								reject(e);
							}
						})
						.catch((e) => {
							this.currentSignInRequest = null;
						});
					this.currentSignInRequest = request;
				}
			}, LOGIN_POLL_MS);
		})
	}


	private async loginUser(user: CognitoUser): Promise<CognitoUserData> {
		// @ts-ignore
		const attributes: CognitoUserAttribute[] = await new Promise((resolve, reject) => {
			user.getUserAttributes((err, result) => {
				if (err) {
					reject(err);
				}
				resolve(result);
			});
		});
		const inAuthor = attributes.find(attribute => {
			return attribute.getName() === "custom:inAuthor";
		});

		const session = user.getSignInUserSession();
		const cogUser = new CognitoUserData(
			session.getAccessToken(),
			session.getRefreshToken(),
			session.getIdToken(),
		);
		this.cognitoUser = cogUser;

		// Dont bother with checking if an email is verified since okta lies about this value

		// const getIsEmailVerified = (attributes: CognitoUserAttribute[]) => {
		// 	const isEmailVerifiedAttribute = attributes.find(attribute => {
		// 		return attribute.getName() === "email_verified"
		// 	});
		// 	return isEmailVerifiedAttribute?.getValue() === "true";
		// }
		// const isEmailVerified = getIsEmailVerified(attributes);
		// if (!isEmailVerified) {
		// 	throw new FederatedSignInError(FederatedSignInErrors.EMAIL_NOT_VERIFIED);
		// }

		if (!inAuthor || !inAuthor.getValue()) {
			console.log('User not in author');
			const email = attributes.find(attribute => {
				return attribute.getName() === "email";
			});
			if (email && email.getValue()) {
				// try {
				// 	await this.authStore.registerFederatedUser(cogUser);
				// } catch (e) {
				// 	if (e && e.response && e.response.data && e.response.data.errorCode) {
				// 		throw new FederatedSignInError(e.response.data, cogUser);
				// 	}
				// 	throw e;
				// }
			} else {
				console.log('No email');
				throw new FederatedSignInError(FederatedSignInErrors.NO_EMAIL, cogUser);
			}
		}
		return cogUser;
	}
}

export function configureAuth() {
	const config =  getCognitoConfigs()
	try {
		AuthCognitoProvider.ConfigureAuth(null, config.Auth.region, config.Auth.userPoolId, config.Auth.userPoolWebClientId, config.oauth.domain);
	} catch (e) {
		console.error('Error occurred during AuthStore.ConfigureAuth', e);
	}
}

export interface AuthState {
	orgKey?: string,
	disableAutoClose?: boolean,
}

// export function getFederatedState() {
// 	const urlParams = new URLSearchParams(window.location.search);
// 	const stateParam = urlParams.get('state');
// 	let state: AuthState = {};
// 	if (stateParam) {
// 		try {
// 			state = JSON.parse(atob(stateParam)) as AuthState;
// 		} catch (e) {
// 			console.error('Could not parse state', stateParam, e);
// 		}
// 	}
// 	return state;
// }

// export function autoCloseWindowForFederatedLogin(onClose = () => window.close()) {
// 	const urlParams = new URLSearchParams(window.location.search);
// 	const federatedSignIn = urlParams.get('federatedSignIn');
// 	if (federatedSignIn !== undefined && federatedSignIn !== null) {
// 		const state = getFederatedState();
// 		let signInTimeout = null;
// 		const onSignIn = () => {
// 			if (signInTimeout) {
// 				clearTimeout(signInTimeout);
// 			}
// 			if (!state?.disableAutoClose) {
// 				onClose();
// 			}
// 		}
// 		Hub.listen("auth", ({ payload: { event, data } }) => {
// 			switch (event) {
// 				case "signIn":
// 					onSignIn();
// 					break;
// 			}
// 		});
// 		const checkLogin = async () => {
// 			try {
// 				await Auth.currentAuthenticatedUser();
// 				onSignIn();
// 			} catch (error) {
// 				signInTimeout = setTimeout(checkLogin, LOGIN_POLL_MS);
// 			}
// 		}
// 		checkLogin();
// 	}
// }

// export function autoCloseWindowForFederatedLogout(onClose = () => window.close()) {
// 	const urlParams = new URLSearchParams(window.location.search);
// 	const federatedSignOut = urlParams.get('federatedSignOut');
// 	if (federatedSignOut !== undefined && federatedSignOut !== null) {
// 		const state = getFederatedState();
// 		if (!state?.disableAutoClose) {
// 			onClose();
// 		}
// 	}
// }

// export function autoCloseWindowForFederatedAuth(onClose = () => window.close()) {
// 	autoCloseWindowForFederatedLogin(onClose);
// 	autoCloseWindowForFederatedLogout(onClose);
// }
