import { faLongArrowRight, faLongArrowLeft } from '@fortawesome/pro-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IInvitation, IOrganisation } from '@mccarthyfinch/public-api-client';
import { observer } from 'mobx-react';
import { ascend, sortWith } from 'ramda';
import React, { useEffect, useState } from 'react';
import CircularProgress from 'src/components/CircularProgress';
import { LoginStatus, useAuthStore } from 'src/models/store/AuthStore';
import {  LineButton, DifferentOrganisation, Line, OrganisationHeader, OrganisationList } from '../styles';
import { SUPPORT_URL } from '../../../links';

function cleanOrganisations(organisations: IOrganisation[]) {
	if (organisations && organisations.length) {
		const sortedOrgs = sortWith<IOrganisation>([
			ascend((org) => (org.isPublic ? 1 : 0)),
			ascend((org) => (org.isPrimaryOrg ? -1 : 0)),
			ascend((org) => org.name),
		])(organisations);

		return sortedOrgs;
	}
	return [];
}

function cleanInvitations(invitations: IInvitation[]) {
	if (invitations && invitations.length) {
		return invitations;
	}
	return [];
}

function getUserEmail(data) {
	if (data.attributes){
		return data.attributes.email
	}
	return ''
}

function OrganisationSelector() {
	const authStore = useAuthStore();
	const [username, setUsername] = useState('')
	const [loading, setLoading] = useState(true);
	const [fetched, setFetched] = useState(false);
	const [error, setError] = useState('');
	const [invitations, setInvitations] = useState<IInvitation[]>([]);
	const [organisations, setOrganisations] = useState<IOrganisation[]>([]);


	useEffect(() => {
		if (!authStore.loginStatus || authStore.loginStatus === LoginStatus.NotLoggedIn) {
			return;
		}
		Promise.all([
			authStore.currentUserCredentials().then((data)=> setUsername(getUserEmail(data))),
			authStore.OrganisationSelector.getInvitations().then((data) => setInvitations(cleanInvitations(data))),
			authStore.OrganisationSelector.getOrganisations().then((data) => setOrganisations(cleanOrganisations(data))),
		]).then(() => {
			setFetched(true)
			setLoading(false);
		}).catch(e => {
			console.error(e);
		});
	}, []);

	const onSelectOrg = async (orgKey: string) => {
		setLoading(true);
		try {
			await authStore.logInAuthor(orgKey);
		} catch (error) {
			console.error(error)
			error.Error ? setError(error.Error) : setError('Could not log in.')
			setLoading(false);
		}
	};

	const signOut = async ()=>{
		authStore.logOut()
	}

	if (!fetched || loading) {
		return (
			<div style={{marginTop:'2rem'}}>
				<CircularProgress />
			</div>
		);
	}

	return (

		<div >
			<OrganisationHeader>Choose an organisation</OrganisationHeader>
			{error && <div style={{ margin: '0.625rem  2.25rem' }} className="error">{error}</div>}

			<OrganisationList>
				{
					organisations.map((org) => {
						return (
							<LineButton key={org.orgKey} onClick={() => onSelectOrg(org.orgKey)}>
									{org.isPublic ? 'Personal Account' : org.name} <FontAwesomeIcon style={{margin:'3px 2px 0px'}} color={'#A4AFBF'} icon={faLongArrowRight} />
							</LineButton>
						);
					})
					}

				{invitations.map((inv) => {
					return (
						<div key={inv.uuid}>
							<LineButton onClick={() => authStore.OrganisationSelector.acceptInvitation(inv.uuid)}>Join {inv.org.name}
							</LineButton>
						</div>
					);
				})}
				<LineButton onClick={()=>{
					signOut()
					}}> <FontAwesomeIcon style={{margin:'3px 2px 0px'}} color={'#A4AFBF'} icon={faLongArrowLeft} /> Back to sign in </LineButton>

			</OrganisationList>

			<Line margin={'1.125rem 0rem'} />
			<DifferentOrganisation>
				Looking for a different organisation?
					<br /><br />
					Check you are signing in with the correct email
					<a >{username&&username}</a>
					or ask your administrator for an invitation.
				</DifferentOrganisation>
			<Line margin={'1.125rem 0rem'} />
			<div style={{ fontSize: '0.625rem' }}>
				 <a target="blank" href={SUPPORT_URL}>Contact support</a>
			</div>
		</div>
	);
}
export default observer(OrganisationSelector);
