import React, { useState } from 'react';
import { inject, observer } from 'mobx-react';

import { SocialButton } from '../styles';
import OktaImage from '../CheckInImages/ico-brand-okta.svg';
import GoogleImage from '../CheckInImages/ico-brand-google.png';
import FacebookImage from '../CheckInImages/ico-brand-facebook.svg';
import OnitImage from '../CheckInImages/ico-brand-onit.svg';

import EditorFactory, { getEditor } from 'src/editor/EditorFactory';
import FederatedSignInHelper, { getFederationLink, configureAuth } from 'src/pages/CheckIn/helpers/FederatedSignInHelper';
import FeatureFlags from 'src/config/FeatureFlags';
import AuthStore from 'src/models/store/AuthStore';
import { CognitoUserData } from 'src/models/service/auth/AuthCognitoProvider';
import getOptimizedWindowSize from 'src/utils/getOptimizedWindowSize';

interface FederatedProps {
    onLogin: (cognitoUserData: CognitoUserData, provider: string) => any,
    authStore?: AuthStore,
    onLoading: (onLoading: boolean, message?: string|JSX.Element) => any,
    onLoginError: (error: string, provider: string) => any
}

export default function FederatedSignIn({ onLoginError, onLogin, authStore, onLoading }: FederatedProps) {

    const setLoading = (isLoading: boolean, message?:string|JSX.Element) => {
        onLoading(isLoading, message)
        if (isLoading) {
            setTimeout(() => {
                onLoading(false, )
            }, 9 * 1 * 1000)
        }
    }

    return (
        <div>
            {/* <FederatedButton onLoginError={onLoginError} onLoading={setLoading} authStore={authStore} onLogin={onLogin} image={OnitImage} identityProvider='Onit' /> */}
            <FederatedButton onLoginError={onLoginError} onLoading={setLoading} authStore={authStore} onLogin={onLogin} image={OktaImage} identityProvider='Okta' />
            <FederatedButton onLoginError={onLoginError} onLoading={setLoading} authStore={authStore} onLogin={onLogin} image={GoogleImage} identityProvider='Google' />
            {/* <FederatedButton onLoginError={onLoginError} onLoading={setLoading} authStore={authStore} onLogin={onLogin} image={FacebookImage} identityProvider='Facebook' /> */}
        </div>
    )
}

const FederatedButton = ({ onLoginError, onLoading, authStore, identityProvider, image, onLogin }:
    {
        onLoading: (onLoading: boolean, message?:string|JSX.Element) => any,
        authStore: AuthStore,
        identityProvider: string,
        image: string,
        onLogin: (userData: any, identityProvider: any) => any,
        onLoginError: (error: string, provider: string) => any
    }) => {

    configureAuth()

    const federatedSignInHelper = new FederatedSignInHelper({ authStore });

    const openWithMSPopup = async (loginUrl: string) => {
        // const loginUrlParam = btoa(loginUrl);
        // const url = `${window.location.protocol}//${window.location.host}/?redirect=${loginUrlParam}`;

        EditorFactory.instance['Editor'].openPopupWindow(loginUrl)

        const user = await federatedSignInHelper.startPollForLogin();

        return onLogin(user, identityProvider);
    }

    const openWithBrowserPopup = async (loginUrl: string) => {
        const size = getOptimizedWindowSize(1024, 768);

        window.open(loginUrl, 'AuthorDOCS', `width=${size.width},height=${size.height},menubar=no,toolbar=no,location=no,resizable=yes,scrollbars=yes,status=no`);
        const user = await federatedSignInHelper.startPollForLogin();

        return onLogin(user, identityProvider);
    }

    const FederatedButtonClicked = async () => {
        onLoading(true, PopupOpenedDisplay())

        try {
            if (identityProvider === 'Onit') {
                const loginDomainPickerURL = `${window.location.protocol}//${window.location.host}/#/onit-login`;

                if (FeatureFlags.openSSOPopupWithMSPopup || EditorFactory.instance['Editor']) {
                    return openWithMSPopup(loginDomainPickerURL);

                } else{
                    return openWithBrowserPopup(loginDomainPickerURL);
                }
            }

            const loginUrl = getFederationLink({ identityProvider });

            if (FeatureFlags.openSSOPopupWithMSPopup || EditorFactory.instance['Editor']) {
                return openWithMSPopup(loginUrl);

            } else {
                return openWithBrowserPopup(loginUrl);
            }

        } catch (error) {
            console.error(error)
            onLoading(false)
            onLoginError('Something went wrong', identityProvider)
        }
        onLoading(false)
    }

    return (
        <SocialButton onClick={() => FederatedButtonClicked()}>
            <img style={{verticalAlign:'sub'}} src={image} /> {identityProvider}
        </SocialButton>
    )
}

const PopupOpenedDisplay = () => {
    return (
    <div style={{ margin: '1rem' }}>
        Please login via the popup window.<br /><br />
        If you were asked to enable popups, please allow popups to be opened, and reload the add-in to try again.
    </div>
    )
}