import { observer } from 'mobx-react';
import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import LoadingOverlay from 'src/components/LoadingOverlay';
import { LoginStatus, useAuthStore } from 'src/models/store/AuthStore';
import { AppRoutes } from 'src/pages/App/AppRouter';
import { Container, Header, MainContent } from '../styles';

import InitialLogin from './InitialLogin';
import OrganisationSelector from './OrganisationSelector';

import LoginHeader from '../CheckInImages/login-header.png'

function LoginPage() {
	const authStore = useAuthStore();

	const checkinStatus = authStore.loginStatus;

	const pageDisplay = () => {
		if (!checkinStatus) {
			return <LoadingOverlay />;
		}

		if (checkinStatus === LoginStatus.NotLoggedIn) {
			return <InitialLogin  />;
		}

		if (checkinStatus === LoginStatus.WaitingForOrganisation) {
			return <OrganisationSelector  />;
		}

		if (checkinStatus === LoginStatus.LoggedIn) {
			return <Redirect to={AppRoutes.Checklist.getHref()} />;
		}

		throw new Error(`LoginStatus not implemented: ${checkinStatus}`);
	}

	return (
		<Container >
			<Header backgroundImage={LoginHeader} />
			<div style={{position:'relative'}}>
			<MainContent>
				{pageDisplay()}</MainContent>
				</div>
		</Container>
	)

}

export default observer(LoginPage);