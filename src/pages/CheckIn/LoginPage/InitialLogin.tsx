import { observer } from 'mobx-react';
import React, { useState } from 'react';
import { useAuthStore } from 'src/models/store/AuthStore';
import { Button, Input, Terms, Line } from '../styles';
import { PRIVACY_URL, SUPPORT_URL } from '../../../links'
import FederatedSignIn from './FederatedSignIn';
import CircularProgress from 'src/components/CircularProgress';
import { Link, useLocation } from 'react-router-dom';

import { EmailValidate, PasswordValidate } from '../utils';
import theme from 'src/styles/scTheme';

function InitialLogin() {
	const authStore = useAuthStore();
	const [email, setEmail] = useState(new URLSearchParams(useLocation().search).get('email') || '');
	const [password, setPassword] = useState('');
	const [formError, setFormErorr] = useState('');
	const [apiError, setApiError] = useState('');
	const [loading, setLoading] = useState(false);
	const [loadingMessage, setLoadingMessage] = useState(null);

	const onLogin = async (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();

		if (loginFormValidate(email, password)) {
			setFormErorr('Incorrect username or password.')
		}
		else {
			setFormErorr(null)
			setApiError(null)
			setLoading(true)
			try {
				await authStore.logInCognito(email.toLowerCase(), password);
			}
			catch (error) {
				console.error(error)
				setApiError(error.message ? error.message : 'Oops, something went wrong! Please try again later.')
				setLoading(false)
				setPassword('')
			}
		}
	};

	const selectOrgInvitation = () => {
		setFormErorr('')
		setApiError('')
		setLoading(false)
	}

	const loginFormValidate = (email, password) => {
		return (EmailValidate(email) || PasswordValidate(password)) ? true : false
		// return (EmailValidate(email)) ? true : false
	}
	
	if (loading) {
		return (
			<div style={{marginTop:'2rem'}}>
				<CircularProgress />
				{loadingMessage && loadingMessage}
			</div>
		);
	}
	
	return (
		<div >
			<h2 >Sign in</h2>
			{apiError && <div style={{ margin: '0.625rem 2.25rem ' }} className="error">{apiError}</div>}

			<FederatedSignIn
				onLoginError={(error, provider) => { setApiError(error) }}
				onLogin={(cognitoUserData, sso: string) => {
					// GA.event({
					// category: EventCategory.USER,
					// action: EventAction.LOGGED_IN,
					// label: sso,
					// });
					selectOrgInvitation();
				}}
				onLoading={(load, loadingMessage) => (setLoading(load), setLoadingMessage(loadingMessage))}
				authStore={authStore}
			/>

			<div className="flex1">
				<Line />
				<div style={{ color: theme.colorPalette.smoke.base, margin: '-0.5rem 0.688rem 0rem 0.9rem' }}>or</div>
				<Line />
			</div>
			<form ref={form => form} noValidate onSubmit={onLogin} autoComplete="on" >
				{formError && <span style={{ marginTop: '1.2rem' }} className="error"> {formError}</span>}
				<Input className={formError && 'errorBorder'} id="email" placeholder="Email" name="email" value={email} onChange={(e) => setEmail(e.target.value)} type="email" autoComplete="username" />
				<Input className={formError && 'errorBorder'} id="password" placeholder="Password" name="password" value={password} onChange={(e) => setPassword(e.target.value)} type="password" autoComplete="current-password" />
				<Button type="submit">Sign in</Button>
			</form>
			<Link to="check-in/forgot-password">Forgot your password?</Link>
			{/* <a ></a> */}
			<div style={{ margin: '1.25rem 0rem' }}> <a target="blank" href={SUPPORT_URL} >Contact support</a></div>
			<Line />
			<Terms textAlign={'center'} >
					By signing in to Precedent, you acknowledge and agree that your use of Precedent and any output of Precedent is at your own risk. Onit does not practice law and Precedent is a drafting and reviewing tool and not a legal service. Your use of Precedent is subject to our
							<br/><a target="blank" style={{ color: 'inherit', fontWeight: 'inherit' }} href={PRIVACY_URL}> terms and conditions</a>.
			</Terms>
		 </div>
	);
}

export default observer(InitialLogin);
