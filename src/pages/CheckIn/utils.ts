
// Returns true if error
export const EmailValidate = (email) =>{
    const emailPattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return (!email || !emailPattern.test(email)) ? true : null
}

// Returns true if error
// Validates for minimum of 8 characters, at least one lowercase and uppercase letter, at least one number, at least one special character
export const PasswordValidate = (password) =>{
    // const passwordPattern = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    // return (!password || !passwordPattern.test(password)) ? true : null
    return !password?true:null
}

