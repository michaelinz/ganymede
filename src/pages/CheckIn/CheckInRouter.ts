import React from 'react';
import SiteUrl from 'src/components/SiteUrl/SiteUrl';

const CheckIn = React.lazy(() => import('./index'));
const LoginPage = React.lazy(() => import('./LoginPage/index'));
const ForgotPasswordPage = React.lazy(()=>import('./ForgotPasswordPage/index'))

// Routes
export const CheckInRoutes = {
	LoginPage: new SiteUrl({
		name: 'LoginPage',
		path: '/',
		component: LoginPage,
		isPrivate: false,
	}),
	ForgotPassword: new SiteUrl({
		name: 'ForgotPassword',
		path: '/forgot-password',
		component: ForgotPasswordPage,	
		isPrivate: false,
	})
};

const CheckInRouter = new SiteUrl({
	name: 'CheckIn',
	path: '/check-in',
	component: CheckIn,
	childRoutes: CheckInRoutes,
	isPrivate: false,
});

export default CheckInRouter;
