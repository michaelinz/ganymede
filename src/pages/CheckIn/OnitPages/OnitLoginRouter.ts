import React from 'react';
import SiteUrl from 'src/components/SiteUrl/SiteUrl';

const OnitLogin = React.lazy(() => import('./index'));
const LoginPage = React.lazy(() => import('./OnitDomainPicker/index'));
const OnitPostLogin = React.lazy(()=>import('./OnitPostLogin/index'))

// Routes
export const OnitLoginRoutes = {
	LoginPage: new SiteUrl({
		name: 'LoginPage',
		path: '/',
		component: LoginPage,
		isPrivate: false,
	}),
	PostLogin: new SiteUrl({
		name: 'PostLogin',
		path: '/success',
		component: OnitPostLogin,	
		isPrivate: false,
	})
};

const OnitLoginRouter = new SiteUrl({
	name: 'OnitLogin',
	path: '/onit-login',
	component: OnitLogin,
	childRoutes: OnitLoginRoutes,
	isPrivate: false,
});


export default OnitLoginRouter;
