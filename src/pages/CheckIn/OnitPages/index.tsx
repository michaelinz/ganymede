import React, { Suspense } from 'react';
import LoadingOverlay from '../../../components/LoadingOverlay';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import OnitLoginRouter from './OnitLoginRouter';
import PrivateRoute from 'src/components/PrivateRoute';




export default function OnitPages() {
	const match = useRouteMatch();

	return (
		<Suspense fallback={<LoadingOverlay />}>
			<Switch>
				{
				(Object.values(OnitLoginRouter.childRoutes).map((siteUrl) => {
					const RouteComponent = siteUrl.isPrivate ? PrivateRoute : Route;
					return (
						<RouteComponent
							key={siteUrl.path}
							path={`${match?.url === '/' ? '' : match?.url}${siteUrl.path}`}
							component={siteUrl.component}
							exact={siteUrl.path === '/'}
						/>
					);
				}))}
			</Switch>
		</Suspense>
	);


}
