import styled from 'styled-components';

export const OnitDomainPickerWrapper = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
	align-items: center;

	form {
		margin-top: 0.5rem;
	}

	button {
		margin-top: 1rem;
		padding: 0.5rem;
		font-weight: ${({ theme }) => theme.fontWeight.light};
		width: 100%;
		svg:not(.MuiCircularProgress-svg) {
			margin-left: 0.5rem;
		}
	}
`;

export const DomainInput = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	min-width: 17rem;
`;

export const TextInputWrapper = styled.div`
	input {
		padding: 0.45rem;
	}
`;

export const SelectWrapper = styled.div`
	width: 7rem;
	font-size: 0.7rem;
`;
export const ErrorWrapper = styled.div`
	width: 17rem;
	font-size: ${({ theme }) => theme.typography.paragraph.fontSize};
	color: ${({theme}) => theme.color.alert};
	margin-bottom: 1rem;
`;
