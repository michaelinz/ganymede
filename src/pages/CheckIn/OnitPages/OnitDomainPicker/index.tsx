import React, { useState } from 'react';
import onitLogo from './onit-logo.png';
import { OnitDomainPickerWrapper, DomainInput, SelectWrapper, ErrorWrapper, TextInputWrapper } from './styles';
import { faArrowRight } from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { CONTACT_SUPPORT_LINK } from 'src/links';
import { CircularProgress } from '@material-ui/core';
import { ONIT_PROVIDER_NAME_KEY } from '../utils';

import { Caption, McfTextInput, PrimaryButton } from '@mccarthyfinch/author-ui-components';
import MCFSimpleSelect from 'src/components/MCFFormElements/MCFFormElements';
import Provider from 'src/models/base/Provider';
import axios from 'axios';


interface IOnitProvider {
	subdomain: string,
	orgKey: string,
	providerName: string,
}




class OnitDomainService extends Provider {
	getSSOProvider = async (subdomain: string): Promise<IOnitProvider> => {
		try {
			const data = await OnitDomainService.API.get(`/auth/sso-provider?subdomain=${subdomain}&provider=onit`);
			console.log(data)
			return data.data;
		} catch (e) {
			console.error(e);
			return null;
		}
	}
}

export default function OnitDomainPicker() {
	const onitDomainService = new OnitDomainService();

	const domainOptions = [
		{ value: '.onit.com', label: '.onit.com', key: '.onit.com', },
		{ value: '.onitcorp.com', label: '.onitcorp.com', key: '.onitcorp.com', }
	];

	const [subdomain, setSubdomain] = useState('');
	const [error, setError] = useState<React.ReactNode>(null);
	const [loading, setLoading] = useState(false);
	const [domain, setDomain] = useState(domainOptions[0].value);
	const onLoginClick = async (e?) => {
		e?.preventDefault();
		if (!subdomain) {
			setError(
				<>
					Please enter an Onit login URL.
				</>
			)
			return;
		}
		setLoading(true);
		setError(null);
		const provider = await onitDomainService.getSSOProvider(subdomain);

		if (provider?.providerName) {
			/**
			 * If we know we can login to this onit subdomain
			 * We navigate to Onit's login with Onit's redirection logic.
			 * We ask Onit to redirect to to this authorDOCS website.
			 * Onit needs to have us whitelisted in their redirect so if the redirect doesn't work talk to them.
			 * It first redirects to Onit's word addin which then redirects to the authorRedirect url.
			 * Onit will use the domain from the authorRedirect and redirect it to, eg, https://authordocs.app/assets/dialog.html
			 * If you go to /assets/dialog.html you can see we simply have it redirect back to this website as, eg, https://authordocs.app/?onit=true&loginSuccess=true
			 *
			 * We need to make sure we store the providerName in localStorage so we can use it for Cognito SSO.
			 *
			 * This should be resolved in the src/pages/OnitPages/index.tsx
			 */
			const onitCustomerLoginURL = `https://${subdomain}${domain}`;

			const authorRedirect = `${window.location.protocol}//${window.location.host}`;
			const onitSSOURL = `${onitCustomerLoginURL}/spa_redirect?no_flash=&return_to=${onitCustomerLoginURL}/office365/word_login_redirect?${authorRedirect}`;



			// Set the provider name in localStorage
			window.localStorage.setItem(ONIT_PROVIDER_NAME_KEY, provider.providerName);

			window.location.href = onitSSOURL;

		} else {
			setError(
				<>
					We couldn&apos;t find your Onit login URL in our records at Author. Please <a target='_blank' rel='noopener noreferrer' href={CONTACT_SUPPORT_LINK}>contact support</a>.
				</>
			);
			setLoading(false);
		}
	}
	return (
		<OnitDomainPickerWrapper>
			<a href="https://localhost:8080/assets/dialog.html">ax</a>
			<img src={onitLogo} alt='Onit' />
			<form onSubmit={onLoginClick}>
				{
					error &&
					<ErrorWrapper>
						{error}
					</ErrorWrapper>
				}
				<Caption fontWeight='medium'>Onit login URL</Caption>

				<DomainInput>
					<TextInputWrapper>
						<McfTextInput
							autoFocus
							debounceDelay={0}
							onChange={(e => setSubdomain(e.target.value))}
							value={subdomain}
							disabled={loading}
							placeholder='your-subdomain'
						/>
					</TextInputWrapper>
					<SelectWrapper>
						<MCFSimpleSelect
							disabled={loading}
							value={domain}
							onChange={setDomain}
							options={domainOptions}
						/>
					</SelectWrapper>

				</DomainInput>
				<PrimaryButton type='submit' disabled={loading}>
					{
						loading ?
							<CircularProgress size={24} thickness={2} /> :
							<>Continue to Login <FontAwesomeIcon icon={faArrowRight} /></>
					}
				</PrimaryButton>
			</form>
		</OnitDomainPickerWrapper>
	)
}

