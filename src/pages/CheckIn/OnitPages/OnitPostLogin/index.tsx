import React from 'react';
import FederatedSignInHelper, { getFederationLink, configureAuth } from 'src/pages/CheckIn/helpers/FederatedSignInHelper';
import LoadingOverlay from 'src/components/LoadingOverlay';
import {  ONIT_PROVIDER_NAME_KEY } from "../utils";
import { useAuthStore } from 'src/models/store/AuthStore';
import EditorFactory from 'src/editor/EditorFactory';
import { Redirect } from 'react-router-dom';


export default function OnitPostLogin() {

    /**
     * Are we logged into Onit? If so login via Cognito as usual using the provider
     * stored in localStorage (this is set in src/pages/OnitPages/OnitDomainPicker/index.tsx)
     */
    const onitProviderName = window.localStorage.getItem(
        ONIT_PROVIDER_NAME_KEY
    );
    if (!onitProviderName) {
        return  <Redirect to="/onit-login" />

    } else {
        

        const authStore = useAuthStore();

        configureAuth()

        const loginUrl = getFederationLink({ identityProvider: onitProviderName });

        const fedHelper = new FederatedSignInHelper({ authStore });

        if (EditorFactory.instance['Editor']) {

             // This may lead to a error in the newly opened window on Online office environment, with office.js unable to instantiate. 
            EditorFactory.instance['Editor'].openPopupWindow(loginUrl);
        } else {
            window.open(loginUrl);
        }


        fedHelper.startPollForLogin().then(
            (res)=>{

            if (EditorFactory.instance['Editor']) {
                EditorFactory.instance['Editor'].closeThisPopupWindow();
            } else {
                window.close();
            }
        })

        return (
            <>
                <LoadingOverlay />
            </>
        );
    }



}
