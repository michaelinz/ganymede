import React, { Suspense } from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import PrivateRoute from 'src/components/PrivateRoute';
import LoadingOverlay from '../../components/LoadingOverlay';
import CheckInRouter from './CheckInRouter';


export default function CheckIn() {
	const match = useRouteMatch();

	return (
		<Suspense fallback={<LoadingOverlay />}>
			<Switch>
				{
				(Object.values(CheckInRouter.childRoutes).map((siteUrl) => {
					const RouteComponent = siteUrl.isPrivate ? PrivateRoute : Route;
					return (
						<RouteComponent
							key={siteUrl.path}
							path={`${match?.url === '/' ? '' : match?.url}${siteUrl.path}`}
							component={siteUrl.component}
							exact={siteUrl.path === '/'}
						/>
					);
				}))}
			</Switch>
		</Suspense>
	);
}

