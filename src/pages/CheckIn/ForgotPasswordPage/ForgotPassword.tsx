
import { observer } from 'mobx-react';
import React, { useState } from 'react';
import {  SUPPORT_URL } from 'src/links';
import AuthStore from 'src/models/store/AuthStore';
import theme from 'src/styles/scTheme';
import { Button, ForgotHeading, Input } from '../styles';

import { EmailValidate } from '../utils';
import { useHistory } from 'react-router-dom';
import CircularProgress from 'src/components/CircularProgress';

function ForgotPassword({ GoNext, authStore }: { GoNext: (email:string) => any, authStore:AuthStore }) {
	const history = useHistory();
	const [email, setEmail] = useState('');
	const [formError, setFormError] = useState('');
	const [apiError, setApiError] = useState('');
	const [loading, setLoading] = useState(false);

	const onNext = async () => {
		if (EmailValidate(email)) {
			setFormError('Incorrect Email')
		} else {
			setLoading(true)
			setFormError(null)
			try {
				const res = await authStore.forgotPassword(email)
				GoNext(email)
			}
			catch (error) {
				setLoading(false)
				console.error(error)
				setApiError(error.message ? error.message : 'Oops, something went wrong! Please try again later.')
			}
		}
	}

	const goBack = () => {
		history.push("/check-in");
	}

	if (loading) {
		return (
			<div style={{marginTop:'2rem'}}>
				<CircularProgress />
			</div>
		);
	}


	return (
		<div >
			<ForgotHeading>Forgot your password?</ForgotHeading>
			<div style={{margin:'0.063rem 2.4rem '}}>Type the address you are trying to sign in with.</div>
			{apiError && <div style={{ margin: '0.625rem 2.25rem' }} className="error">{apiError}</div>}
			{formError && <div style={{ margin: '0.625rem 2.25rem' }} className="error">{formError}</div>}

			<Input style={{ marginTop: "1.25rem" }} className={formError && 'errorBorder'} id="email" placeholder="Email" name="email" value={email} onChange={(e) => setEmail(e.target.value)} type="email" autoComplete="username" />

			<div style={{ display: 'flex' }}>
				<Button  margin={'1.25rem 0.5rem 0.25rem 0rem'} backgroundColor={'white'} color={theme.color.fontPrimary} onClick={goBack}>Back</Button>
				<Button  margin={'1.25rem 0rem 0.25rem 0.5rem'} onClick={onNext}>Next</Button>
			</div>
			<div style={{ margin:'1.25rem 0rem'}}> <a target="blank" href={SUPPORT_URL} >Contact support</a></div>
		</div>
	);
}

export default observer(ForgotPassword);
