import { observer } from 'mobx-react';
import React, { useState } from 'react';
import LoadingOverlay from 'src/components/LoadingOverlay';
import { Container, Header, MainContent } from '../styles';

import LoginHeader from '../CheckInImages/login-header.png'
import ForgotPassword from './ForgotPassword';
import PasswordReset from './PasswordReset';
import { LoginStatus, useAuthStore } from 'src/models/store/AuthStore';
import { Redirect } from 'react-router-dom';
import { AppRoutes } from 'src/pages/App/AppRouter';

export enum ForgotPasswordStatus {
        ForgotPassword = 'ForgotPassword',
        ResetPassword = 'ResetPassword'
}

function ForgotPasswordPage() {
        const authStore = useAuthStore()
        const checkinStatus = authStore.loginStatus;

        const [FPS, setFPS] = useState(ForgotPasswordStatus.ForgotPassword);
        const [email, setEmail] = useState('');

        const pageDisplay = () => {
                if (!checkinStatus) {
                        return <LoadingOverlay />;
                }
                if (checkinStatus === LoginStatus.LoggedIn) {
                        return <Redirect to={AppRoutes.Checklist.getHref()} />;
                } else {
                        if (FPS === ForgotPasswordStatus.ForgotPassword) {
                                return <ForgotPassword authStore={authStore} GoNext={(email) => { setFPS(ForgotPasswordStatus.ResetPassword); setEmail(email) }} />;
                        }

                        if (FPS === ForgotPasswordStatus.ResetPassword) {
                                return <PasswordReset authStore={authStore} email={email} GoBack={() => setFPS(ForgotPasswordStatus.ForgotPassword)} />;
                        }
                }
        }

        return (
                <Container>
                        <Header backgroundImage={LoginHeader} />
			<div style={{position:'relative'}}>
                        <MainContent>
                                {pageDisplay()}</MainContent>
                                </div>
                </Container>
        )
}

export default ForgotPasswordPage