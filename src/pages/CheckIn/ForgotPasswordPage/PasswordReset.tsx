import { observer } from 'mobx-react';
import React, { useState } from 'react';
import CircularProgress from 'src/components/CircularProgress';
import {  SUPPORT_URL } from 'src/links';
import AuthStore from 'src/models/store/AuthStore';
import theme from 'src/styles/scTheme';
import { Button, Input, Terms } from '../styles';
import { PasswordValidate } from '../utils';
import { useHistory } from 'react-router-dom';

function PasswordReset({ GoBack, email, authStore }: { GoBack: () => any, email: string, authStore:AuthStore }) {
	const history = useHistory();

	const [resetCode, setResetCode] = useState('');
	const [newPassword, setNewPassword] = useState('');
	const [newPasswordConfirm, setNewPasswordConfirm] = useState('');
	const [loading, setLoading] = useState(false);

	const [formError, setFormError] = useState('');
	const [apiError, setApiError] = useState('');
	const [resetCodeError, setResetCodeError] = useState(false)
	const [passwordError, setPasswordError] = useState(false)

	const formValidate = (form) => {
		if ( !form.resetCode ){
			setFormError('Please enter reset code')
			setResetCodeError(true)
		}
		else if (PasswordValidate(form.newPassword)) {
			console.log('Password must have a minimum length of 8 characters with number, upper and lower case letter and special character')
			setFormError('Password not strong enough ')
			setPasswordError(true)
		}
		else if (form.newPassword !== form.newPasswordConfirm) {
			console.log('Password does not match')
			setFormError('Password does not match')
			setPasswordError(true)
		} else {
			return true ;
		}
	}

	const resetPassword = async (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		setPasswordError(false)
		setResetCodeError(false)

		if (!formValidate({ resetCode, newPassword, newPasswordConfirm }) ) {
			return ;
		} else {
			setLoading(true)
			setFormError('')
			try {
				const res = await authStore.forgotPasswordChangePassword(email, resetCode, newPassword)
				console.log(res)
				history.push(`/check-in?email=${email}`);
			} catch (err) {
				console.error(err)
				setLoading(false)
				setApiError(err.message ? err.message : 'Oops, something went wrong! Please try again later.')
			}
		}
	}

	const resendEmail = async () => {
		setLoading(true)
		try {
			await authStore.forgotPassword(email)
			setLoading(false)
		}catch(err){
			setLoading(false)
			setApiError(err.message ? err.message : 'Oops, something went wrong! Please try again later.')
		}
	}

	if (loading) {
		return (
			<div style={{marginTop:'2rem'}}>
				<CircularProgress />
			</div>
		);
	}

	return (
		<div >
			{apiError && <div style={{ margin: '0.625rem 2.25rem ' }} className="error">Oops, something went wrong! <br/> {apiError}</div>}
			{formError && <div style={{ margin: '0.625rem 2.25rem ' }} className="error">{formError}</div>}

			<p>If the address <b>{email}</b> matches one of our accounts, we'll email you a password reset code.</p>

			<form ref={form => form} noValidate onSubmit={resetPassword} >

				<Input  id="email" placeholder="Email" name="email" value={email} type="email" disabled />
				<Input  className={resetCodeError && 'errorBorder'} id="resetCode" placeholder="Password reset code" name="resetCode" value={resetCode} onChange={(e) => setResetCode(e.target.value)} required />
				<Input  className={passwordError && 'errorBorder'} id="newPassword" placeholder="New password" name="newPassword" value={newPassword} onChange={(e) => setNewPassword(e.target.value)} type="password" required />
				<Input  className={passwordError && 'errorBorder'} id="newPasswordConfirm" placeholder="Confirm password" name="newPasswordConfirm" value={newPasswordConfirm} onChange={(e) => setNewPasswordConfirm(e.target.value)} type="password" required/>

				<Button margin={'1.25rem 0rem 0.9rem'} type="submit">Reset password</Button>
			</form>

			<div style={{ display: 'flex' }}>
				<Button fontSize={'0.75rem'} height={'1.75rem'} margin={'0rem 0.5rem 0rem 0rem'} backgroundColor={'white'} color={theme.color.fontPrimary} onClick={GoBack}>Back</Button>
				<Button fontSize={'0.75rem'} height={'1.75rem'} margin={'0rem 0rem 0rem 0.5rem'} backgroundColor={'white'} color={theme.color.fontPrimary} onClick={resendEmail}>Resend email</Button>
			</div>
			<Terms color={theme.color.fontPrimary} textAlign={'center'}>
				Haven't received our email? <br />
					Try your spam/junk or  <a target="blank" href={SUPPORT_URL} >contact support</a></Terms>
		</div>
	);
}

export default observer(PasswordReset);
