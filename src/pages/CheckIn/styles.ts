import styled from 'styled-components';
import { McfTextArea, McfTextInput } from '@mccarthyfinch/author-ui-components';

export const Header = styled.div<{ backgroundImage?: string }>`
    font-family: 'Montserrat' !important;
    padding-top: 65.9%;
    background-position: center top;
    width:100%;
    ${({ backgroundImage }) => (backgroundImage ? `background-image: url(${backgroundImage});` : '')}
    background-size:contain;
    background-repeat: no-repeat;
`

export const MainContent = styled.div`
    background-color: white; 
    position: absolute; 
    top: -1.5rem; 
    width:100%;
    border-radius: 16px;
    padding  2rem;
    overflow:auto;
    height: calc(100vh - 65vw);
    min-height:44vh;
`


export const Container = styled.div`
    *{
        font-family: 'Montserrat' !important;
    }
    position:relative;
    margin: 0 auto;
    max-width:52rem;
    min-width:15rem;
    width:100%;
    text-align:center;
    h2{
        font-size:0.9375rem;
        font-weight:500;
        color: ${({ theme }) => theme.color.fontPrimary};
        margin: 0rem 0rem 1.063rem;
    }

    p{
        font-size:0.813rem;
        color: ${({ theme }) => theme.color.fontPrimary};
    }

        a{
            font-size:0.625rem;
            font-weight:600;
            color:${({ theme }) => theme.color.fontPrimary};
        }

        .flex1{
            margin-top:0.938rem;
            margin-bottom:0.625rem;
            display:flex;
        }

        .error{
            color: ${({ theme }) => theme.color.danger};
        }

        .errorBorder{
            border-color: ${({ theme }) => theme.color.danger};
        }

        FontAwesomeIcon{
            width:0.9375rem;
            height:0.9375rem;
            margin-right:4px;
            font-weight:normal;
        }
        button{
            cursor:pointer;
        }
        font-weight:300;
    `;

export const ForgotHeading = styled.div`
    font-size: 16px;
    font-weight: 600;
    color: ${({ theme }) => theme.color.fontPrimary};
`

export const Input = styled.input`
    width:100%;
    height:40px;
    margin-top:10px;
    padding: 12px 10px 12px 16px;
    border-radius: 4px;
    border: solid 1px ${({ theme }) => theme.color.border};
    background-color: white;
    ::placeholder{
        font-size: 13px;
        font-weight: 300;
        font-family
        color: ${({ theme }) => theme.colorPalette.smoke.base};
    }
    &:required:before {
        content: "*";
        color: #ff7452;
    }
    &:focus{
        outline:none;
        border: solid 2px ${({ theme }) => theme.colorPalette.primary['200']} ;
    }
`

export const Button = styled.button<{ fontSize?:string, height?:string, margin?: string, backgroundColor?: string, color?: string }>`
    width:100%;
    ${({ height }) => `height:` + (height ? height : `2.5rem `) + ';'}

    border-radius: 6px;
    font-weight: 600;
    border: solid 2px ${({ backgroundColor, theme }) => backgroundColor ? theme.color.border : theme.colorPalette.secondary['800']};
    text-align:center;
    ${({ color }) => `color:` + (color ? color : `white`) + ';'}
    ${({ backgroundColor, theme }) => `background-color: ${backgroundColor ? backgroundColor : theme.colorPalette.secondary['800']};`}
    ${({ fontSize }) => `font-size:` + (fontSize ? fontSize : `0.9375rem `) + ';'}
    ${({ margin }) => `margin:` + (margin ? margin : `1.25rem 0rem 1.25rem 0rem `) + ';'}
    &:hover {
        ${({ backgroundColor, theme }) => `background-color: ${backgroundColor == 'white' ? theme.color.background : 'black'};`}
    }
`

export const LineButton = styled.button`
    width:100%;
    height:32px;
    border:solid 2px ${({ theme }) => theme.color.border};
    border-radius:6px;
    background-color:white;
    margin-top:10px;
    margin-bottom:10px;
    font-size:0.8125rem;
    font-weight:600;
    color:${({ theme }) => theme.color.fontPrimary};
    &:hover {
        background-color:#e8eaee;
    }
`


export const Line = styled.div<{ margin?: string }>`
    width:100%;
    height: 1px;
    background-color: ${({ theme }) => theme.color.border};
    ${({ margin }) => (margin ? `margin:${margin}` : '')}
`

export const SocialButton = styled.button`
    width:100%;
    height: 32px;
    margin-top:10px;
    margin-bottom:10px;
    border-radius: 6px;
    border: solid 2px  ${({ theme }) => theme.color.border};
    background-color:white;
    color: ${({ theme }) => theme.color.fontPrimary};
    font-size: 13px;
    font-weight: 600;
    cursor:pointer;
    img {
        width:16px;
        height:16px;
    }
`

export const DifferentOrganisation = styled.div`
    font-size:0.75rem;
    font-weight: 300;   
    a{
        text-decoration:none;
        display:block;
    }
`
export const OrganisationHeader = styled.div`
    font-size:1.125rem;
    font-family: 'Roboto', sans-serif !important;
    font-weight: 500;
    color: ${({ theme }) => theme.color.fontPrimary} ;
    margin-bottom:0.938rem;
    font-family: inherent;
`

export const OrganisationList = styled.div`
    display: flex;
    flex-direction: column;
`

export const Terms = styled.div<{ color?: string, textAlign?: string }>`
    margin:1.25rem 2.5rem;
    ${({ color, theme }) => `color:` + (color ? color : theme.colorPalette.smoke.base) + ';'}
    ${({ textAlign }) => `text-align:` + (textAlign ? textAlign : `left`) + ';'}
    font-size:10px;
    font-stretch: normal;
    line-height: 1.3;
}
`