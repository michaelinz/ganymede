import { uniq } from 'ramda';

export default function getLabelsForTypes(items: Array<{ name: string }>) {
	if (!items?.length) {
		return '';
	}
	return uniq(items.map(i => i.name)).sort().join(', ');
}
