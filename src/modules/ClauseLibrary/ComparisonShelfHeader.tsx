import { faArrowFromLeft } from '@fortawesome/pro-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Heading } from '@mccarthyfinch/author-ui-components';
import React from 'react';
import Header, { HeaderColumn, PrimaryHeaderBar, SecondaryHeaderBar } from 'src/components/Layout/Header';
import { IconButtonBorderless } from 'src/components/molecules/Buttons';
import background from '../../../public/assets/clauseBank/header-clausebank-01.svg';
import { ClauseLibraryUIView, IClauseLibraryUI } from './useClauseLibraryUI';

interface IClauseLibraryProps {
	clauseLibraryUI: IClauseLibraryUI;
	onDismiss: () => void;
}

export default function ComparisonShelfHeader({ clauseLibraryUI, onDismiss }: IClauseLibraryProps) {
	const view = clauseLibraryUI.view.clauseLibraryUIView;
	return (
			<Header background={background}>
				<PrimaryHeaderBar>
					<HeaderColumn>
						<IconButtonBorderless onClick={onDismiss} >
							<FontAwesomeIcon icon={faArrowFromLeft} size='xs' />
						</IconButtonBorderless>
					</HeaderColumn>
					<HeaderColumn align="center">
						<Heading>Clause Library</Heading>
					</HeaderColumn>
					<HeaderColumn align="right"></HeaderColumn>
				</PrimaryHeaderBar>
				{
					view === ClauseLibraryUIView.Browse &&
					<SecondaryHeaderBar>

					</SecondaryHeaderBar>
				}
			</Header>
	);
}
