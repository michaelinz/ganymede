import { Caption } from '@mccarthyfinch/author-ui-components';
import { IConceptBankItem, IConceptType, IDocumentType, ISuperTag, ITag } from '@mccarthyfinch/public-api-client';
import { uniq } from 'ramda';
import React from 'react';
import styled from 'styled-components';

interface IConceptBankItemTags {
	conceptBankItem: IConceptBankItem;
	conceptTypes: IConceptType[];
	documentTypes: IDocumentType[];
	tags: ITag[];
	superTags: ISuperTag[];
}

export default function ConceptBankItemTags({ conceptBankItem, conceptTypes, documentTypes, tags, superTags }: IConceptBankItemTags ) {
	const tagLabels = uniq(tags.map(tag => tag.name)).sort();
	const text = [...tagLabels].join(', ');
	return (
		<TagList>
			{text}
		</TagList>
	);
}

const TagList = styled(Caption)`
	margin-top: 0.5em;
	color: ${({ theme }) => theme.color.fontSecondary};
	font-style: italic;
`;
