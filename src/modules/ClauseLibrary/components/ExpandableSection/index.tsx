import React, { useState } from 'react';
import { SectionContent, SectionTitle, SectionWrapper } from './styles';

interface IExpandableSection {
	renderTitle: ({isExpanded: boolean}) => React.ReactNode;
	isNested?: boolean;
	overrideColor?: string;
	onToggleExpansion?: () => void;
	renderContent: () => React.ReactNode;
	isExpandedByDefault?: boolean;
	children?: React.ReactChild;
}

export default function ExpandableSection({
	renderTitle,
	isNested = false,
	overrideColor,
	onToggleExpansion,
	renderContent,
	isExpandedByDefault,
	children,
}: IExpandableSection) {
	const [isExpanded, setIsExpanded] = useState(Boolean(isExpandedByDefault));
	const toggleExpansion = () => {
		if (onToggleExpansion) {
			onToggleExpansion();
		}
		setIsExpanded(!isExpanded);
	};
	return (
		<SectionWrapper isExpanded={isExpanded} isNested={isNested} overrideColor={overrideColor}>
			<SectionTitle onClick={toggleExpansion} isExpanded={isExpanded} isNested={isNested} overrideColor={overrideColor}>
				{renderTitle({ isExpanded })}
			</SectionTitle>

			{isExpanded ?  (
				<SectionContent isNested={isNested}>
					{renderContent()}
					{children}
				</SectionContent>
			) : null}
		</SectionWrapper>
	);
}
