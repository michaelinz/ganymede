import styled, { css } from 'styled-components';

const leftMargin = 0.5;

export const SectionWrapper = styled.div<{ isExpanded: boolean; isNested: boolean; overrideColor?:string }>`
  font-size: ${({ theme }) => theme.typography.paragraph.fontSize};
  position: relative;

  ${({ isExpanded, isNested, theme, overrideColor }) => isExpanded && !isNested &&
    css`
    border-left: 1px solid ${ overrideColor || theme.color.border};

    &::before {
      content: '';
      position: absolute;
      width: 1px;
      height: 1rem;
      top: 0;
      left: -1px;
      background: white;
    }
  `};
`;
export const SectionTitle = styled.div<{ isExpanded: boolean; isNested: boolean; overrideColor?: string }>`
  padding: 0.5rem 0;
  padding-left: ${({ isNested }) => isNested ? leftMargin * 2 : leftMargin}rem;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  position: relative;
  font-weight: ${({ isExpanded, theme }) => isExpanded ? theme.fontWeight.bold : theme.fontWeight.regular};
  /* background-color: ${({ theme, isExpanded }) => isExpanded ? theme.color.background : ''}; */
	color: ${({ theme }) => theme.color.fontPrimary};

  border-top: 1px solid ${({ theme }) => theme.color.border};

  transition: all 0.2s;
  &:hover {
    cursor: pointer;
    color: ${({ theme, overrideColor }) => overrideColor || theme.color.border};
  }
`;

export const SectionContent = styled.div<{ isNested: boolean }>`
  margin-left: ${({ isNested }) => isNested ? leftMargin * 2 : leftMargin}rem;
   ${({ isNested, theme }) => isNested &&
    css`
    padding: 0.5rem 0;
    margin-left: -${leftMargin * 1.1}rem;
    margin-right: -${leftMargin * 1.1}rem;
  `};
`;
