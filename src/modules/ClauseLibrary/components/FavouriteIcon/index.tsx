import { faStar } from '@fortawesome/pro-light-svg-icons';
import { faStar as faStarSolid } from '@fortawesome/pro-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconButtonBorderless } from '@mccarthyfinch/author-ui-components';
import React from 'react';
import styled from 'styled-components';


interface IFavouriteIconProps {
	favourite: boolean;
	onClick?: () => Promise<void>;
}

export default function FavouriteIcon({favourite, onClick}: IFavouriteIconProps) {
	return (
		<FavouriteIconWrapper favourite={favourite} onClick={onClick} disabled={!onClick}>
			<FontAwesomeIcon icon={favourite ? faStarSolid : faStar} />
		</FavouriteIconWrapper>
	);
}

// TODO chase up brooke about colors! why is 12c39a not in the colour palette?
export const FavouriteIconWrapper = styled(IconButtonBorderless)<{favourite: boolean}>`
	padding: 0;

	svg {
		color: ${({ theme, favourite }) => favourite ? theme.color.success : theme.color.fontSecondary};

	}

	&:hover:not(:disabled), &:focus {
		svg{
			color: #12c39a;
			opacity: ${({ favourite }) => favourite ? 0.7 : 1};
		}
	}
`;
