import { faHome } from '@fortawesome/pro-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Heading } from '@mccarthyfinch/author-ui-components';
import React from 'react';
import Header, { HeaderColumn, PrimaryHeaderBar, SecondaryHeaderBar } from 'src/components/Layout/Header';
import Zelda from 'src/components/Zelda';
import { AppRoutes } from 'src/pages/App/AppRouter';
import background from '../../../public/assets/clauseBank/header-clausebank-01.svg';
import { ClauseLibraryUIView, IClauseLibraryUI } from './useClauseLibraryUI';


interface IClauseLibraryProps {
	clauseLibraryUI: IClauseLibraryUI;
}

export default function ClauseLibraryPageHeader({ clauseLibraryUI }: IClauseLibraryProps) {
	const view = clauseLibraryUI.view.clauseLibraryUIView;
	return (
			<Header background={background}>
				<PrimaryHeaderBar>
					<HeaderColumn>
						<Zelda to={AppRoutes.Dashboard.getHref()}>
							<FontAwesomeIcon icon={faHome} />
						</Zelda>
					</HeaderColumn>
					<HeaderColumn align="center">
						<Heading>Clause Library</Heading>
					</HeaderColumn>
					<HeaderColumn align="right"></HeaderColumn>
				</PrimaryHeaderBar>
				{
					view === ClauseLibraryUIView.Browse &&
					<SecondaryHeaderBar>

					</SecondaryHeaderBar>
				}
			</Header>
	);
}
