import React from 'react';
import { Content } from './styles';
import { ClauseLibraryUIView, IClauseLibraryUI } from './useClauseLibraryUI';

import ClauseLibraryList from './pages/ClauseLibraryList';
import ConceptBankItemDetailed from './pages/ConceptBankItemDetailed';
import { PageWrapper, Content as PageContent } from 'src/components/Layout/Page';
import Header, { HeaderColumn, PrimaryHeaderBar, SecondaryHeaderBar } from 'src/components/Layout/Header';
import background from '../../../public/assets/clauseBank/header-clausebank-01.svg';
import Zelda from 'src/components/Zelda';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Heading } from '@mccarthyfinch/author-ui-components';
import { faHome } from '@fortawesome/pro-solid-svg-icons';
import { AppRoutes } from 'src/pages/App/AppRouter';

interface IClauseLibraryProps {
	clauseLibraryUI: IClauseLibraryUI;
	renderHeader: (clauseLibraryUI: IClauseLibraryUI) => React.ReactChild;
}

export default function ClauseLibrary({ clauseLibraryUI, renderHeader }: IClauseLibraryProps) {
	const view = clauseLibraryUI.view.clauseLibraryUIView;
	return (
		<PageWrapper>
			{renderHeader(clauseLibraryUI)}
			<PageContent>
				<Content variant={view === ClauseLibraryUIView.Browse ? 'default' : 'hidden'}>
					<ClauseLibraryList clauseLibraryUI={clauseLibraryUI} />
				</Content>
				{view === ClauseLibraryUIView.Detailed && (
					<Content>
						<ConceptBankItemDetailed clauseLibraryUI={clauseLibraryUI} />
					</Content>
				)}
			</PageContent>
		</PageWrapper>
	);
}
