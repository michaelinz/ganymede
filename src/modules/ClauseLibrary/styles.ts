
import styled, { css } from 'styled-components';

export const Content = styled.div<{ variant?: 'default' | 'hidden' }>`
width: 100%;
height: 100%;
display: flex;
flex-direction: column;
overflow: auto;
opacity: 1;
position: relative;

${({ variant }) => {
	if (variant === 'hidden') {
		return css`
		display: none;
	`;
	}
}}
`;

Content.defaultProps = {
variant: 'default',
};
