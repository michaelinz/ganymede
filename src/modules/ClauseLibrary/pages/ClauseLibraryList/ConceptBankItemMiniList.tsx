import React from 'react';
import ConceptBankItemService from '../../service/ConceptBankItemService';
import QueryStatusWrapper from 'src/components/QueryStatusWrapper';
import { IConceptBankItem } from '@mccarthyfinch/public-api-client';
import ConceptBankItemMiniView from './ConceptBankItemMiniView';
import { IClauseLibraryUI } from '../../useClauseLibraryUI';


interface IConceptBankItemMiniList {
	conceptTypeUUIDs: string[];
	clauseLibraryUI: IClauseLibraryUI;
}

export default function ConceptBankItemMiniList({ conceptTypeUUIDs, clauseLibraryUI }: IConceptBankItemMiniList) {
	const queryResult = ConceptBankItemService.useConceptBankItemsByConceptTypeUUIDs(conceptTypeUUIDs);
	return (
		<QueryStatusWrapper
			queryResults={[queryResult]}
			data-ted="checklist-list"
		>
			<ConceptBankItemMiniListView conceptBankItems={queryResult.data} clauseLibraryUI={clauseLibraryUI} />
		</QueryStatusWrapper>
	);
}

function ConceptBankItemMiniListView({ conceptBankItems, clauseLibraryUI }: { conceptBankItems: IConceptBankItem[]; clauseLibraryUI: IClauseLibraryUI}) {
	return (
		<>
			{conceptBankItems.map(item => <ConceptBankItemMiniView key={item.uuid} conceptBankItem={item} clauseLibraryUI={clauseLibraryUI} />)}
		</>
	);
}
