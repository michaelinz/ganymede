import styled, { css } from 'styled-components';
import { Caption, Paragraph } from '@mccarthyfinch/author-ui-components';

export const ViewByWrapper = styled.div`
  margin: 1rem;
  position: relative;
`;

export const HideTopBorder = styled.div`
  width: 100%;
  height: 1px;
  position: absolute;
  background: white;
  z-index: 1;
`;




export const ConceptHeader = styled.div<{ isNested: boolean; isExpanded?: boolean }>`
	display: flex;
	align-items: center;
	${({ theme, isExpanded }) => {
		return css`
			font-weight: ${theme.fontWeight.bold};

			${Paragraph} {
				font-weight: inherit;
			}
		`;
	}}
`;

export const Counter = styled(Caption)<{ isPublicConcept?: boolean }>`
	padding-left: 3px;
	font-weight: inherit;
`;
/* color: ${({ theme, isPublicConcept }) => isPublicConcept ? theme.color.publicConcept : theme.color.primary}; */


export const ConceptList = styled.div`
  & > *:not(:first-child) {
    margin-top: 1rem;
  }
`;
