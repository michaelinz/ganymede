import { Paragraph } from '@mccarthyfinch/author-ui-components';
import { IConceptType, IConceptTypeCounts } from '@mccarthyfinch/public-api-client';
import React from 'react';
import ExpandableSection from '../../components/ExpandableSection';
import { ConceptHeader, Counter, HideTopBorder, ViewByWrapper } from './styles';
import ConceptBankItemMiniList from './ConceptBankItemMiniList';
import { Filters, IClauseLibraryUI } from '../../useClauseLibraryUI';
import { Content } from 'src/components/Layout/Page';

const leftPad = (number: number) => {
	return (number < 10 ? '0' : '') + number;
};

interface IClauseLibraryListViewProps {
	conceptTypes: IConceptType[];
	conceptTypeCounts: IConceptTypeCounts[];
	clauseLibraryUI: IClauseLibraryUI;
}

function combineCountByLabel(items: IConceptTypeCounts[]) {
	const groupedByLabel = new Map<string, {
		total: number;
		items: IConceptTypeCounts[];
	}>();
	items.forEach(item => {
		const label = item.name;
		if (groupedByLabel.has(label)) {
			const group = groupedByLabel.get(label);
			group.total += item.conceptBankItemCount;
			group.items.push(item);
		} else {
			groupedByLabel.set(label, {
				total: item.conceptBankItemCount,
				items: [item],
			});
		}
	});
	const labels = Array.from(groupedByLabel.keys()).sort();
	return labels.map(label => {
		const value = groupedByLabel.get(label);
		return {
			name: label,
			total: value.total,
			items: value.items,
		};
	});
}

function filterCounts(conceptTypeCounts: IConceptTypeCounts[], conceptTypes: IConceptType[], filters: Filters) {
	if (!filters?.clauseUUIDs?.length) {
		return conceptTypeCounts;
	}
	const labels = conceptTypes.filter(conceptType => filters.clauseUUIDs.includes(conceptType.uuid)).map(count => count.name?.toLowerCase());
	// return counts based on labels of directlyAssociatedCounts
	return conceptTypeCounts.filter(count => labels.includes(count.name.toLowerCase()));
}


export default function ClauseLibraryListView({ conceptTypes, conceptTypeCounts, clauseLibraryUI }: IClauseLibraryListViewProps) {
	const isPublicConcept = false;
	const filters = clauseLibraryUI?.filters?.filters;
	const filteredCounts = filterCounts(conceptTypeCounts, conceptTypes, filters);
	const items = combineCountByLabel(filteredCounts);

	return (
		<Content style={{marginTop: '1rem'}}>
			<ViewByWrapper>
				<HideTopBorder />
				{
					items.map(item => {
						return (
							<ExpandableSection
								key={item.name}
								isExpandedByDefault={Boolean(filters?.clauseUUIDs?.length)}
								renderTitle={({ isExpanded }: { isExpanded: boolean}) => (
									<ConceptHeader
										isNested={false}
										isExpanded={isExpanded}
									>
										<Paragraph>{item.name}</Paragraph>
										<Counter isPublicConcept={isPublicConcept}>({item.total})</Counter>
									</ConceptHeader>
								)}
								renderContent={() => (
									<ConceptBankItemMiniList conceptTypeUUIDs={item.items.map(item => item.uuid)} clauseLibraryUI={clauseLibraryUI} />
								)}
							/>
						);
					})
				}
			</ViewByWrapper>
		</Content>
	);
}
