import { faPlusCircle } from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Paragraph } from '@mccarthyfinch/author-ui-components';
import { IConceptBankItem } from '@mccarthyfinch/public-api-client';
import React, { useRef } from 'react';
import { Row } from 'src/components/Layout';
import QueryStatusWrapper from 'src/components/QueryStatusWrapper';
import ConceptTypeService from 'src/models/service/author/ConceptTypeService';
import DocumentTypeService from 'src/models/service/author/DocumentTypeService';
import SuperTagService from 'src/models/service/author/SuperTagService';
import TagService from 'src/models/service/author/TagService';
import ConceptBankItemTags from 'src/modules/ClauseLibrary/components/ConceptBankItemTags';
import { ClauseLibraryUIView, IClauseLibraryUI } from 'src/modules/ClauseLibrary/useClauseLibraryUI';
import { AddTo, ConceptMiniViewContent, ConceptMiniViewWrapper } from './styles';
import TextMiniView from './TextMiniView';
import FavouriteIcon from 'src/modules/ClauseLibrary/components/FavouriteIcon';
import getLabelsForTypes from 'src/modules/ClauseLibrary/utils/getLabelsForTypes';

interface IConceptBankItemMiniView {
	conceptBankItem: IConceptBankItem;
	clauseLibraryUI: IClauseLibraryUI;
}

export default function ConceptBankItemMiniView({ conceptBankItem, clauseLibraryUI }: IConceptBankItemMiniView) {
	const textRef = useRef(null);
	const conceptTypesResult = ConceptTypeService.useConceptTypes();
	const documentTypesResult = DocumentTypeService.useDocumentTypes();
	const tagsResult = TagService.useTags();
	const superTagsResult = SuperTagService.useSuperTags();

	const onView = () => {
		clauseLibraryUI.view.setSelectedConceptBankItemUUID(conceptBankItem.uuid);
		clauseLibraryUI.view.setClauseLibraryUIView(ClauseLibraryUIView.Detailed);
		// GA.pageview(`/conceptbank/browse/${this.props.conceptBankItem.uuid}`);
	};

	const onAddTo = async () => {
		await clauseLibraryUI.onAdd.get.action(textRef?.current?.outerHTML, conceptBankItem?.content?.plainText);
		// GA.event({
		// 	category: EventCategory.DRAFT,
		// 	action: EventAction.INSERTED_CONCEPT,
		// 	label: conceptBankItem.conceptTypes.map(t => t.nameFormatted).join(','),
		// });
	};

	const conceptTypes = conceptTypesResult?.data?.filter((ct) => conceptBankItem.conceptTypeUUIDs.includes(ct.uuid));
	const documentTypes = documentTypesResult?.data?.filter((ct) => conceptBankItem.documentTypeUUIDs.includes(ct.uuid));
	const tags = tagsResult?.data?.filter((ct) => conceptBankItem.tagUUIDs.includes(ct.uuid));
	const superTags = superTagsResult?.data?.filter((ct) => conceptBankItem.superTagUUIDs.includes(ct.uuid));

	return (
		<QueryStatusWrapper queryResults={[conceptTypesResult, documentTypesResult, tagsResult, superTagsResult]}>
			<ConceptMiniViewWrapper>
				<Row>
					<FavouriteIcon favourite={conceptBankItem.favourite} />
					<Paragraph fontWeight="bold" style={{ paddingLeft: '0.25rem' }}>
						{conceptBankItem.name || getLabelsForTypes(conceptTypes)}
					</Paragraph>
				</Row>

				<ConceptBankItemTags
					conceptBankItem={conceptBankItem}
					conceptTypes={conceptTypes}
					documentTypes={documentTypes}
					tags={tags}
					superTags={superTags}
				/>

				<ConceptMiniViewContent onClick={onView}>
					<div ref={textRef} style={{ marginBottom: '1.15em' }}>
						<TextMiniView conceptBankItem={conceptBankItem} />
					</div>
				</ConceptMiniViewContent>

				<AddTo onClick={onAddTo}>
					<FontAwesomeIcon icon={faPlusCircle} />
					<Paragraph style={{ paddingLeft: '0.25rem' }}>Add to document</Paragraph>
				</AddTo>
			</ConceptMiniViewWrapper>
		</QueryStatusWrapper>
	);
}
