import { IconButtonBorderless, Paragraph } from '@mccarthyfinch/author-ui-components';
import styled from 'styled-components';

export const ConceptMiniViewWrapper = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
	color: ${({ theme }) => theme.color.fontPrimary};

	border-top: 1px solid ${({ theme }) => theme.color.border};
	margin-left: 1rem;
	padding: 1rem 0;
`;

export const ConceptMiniViewActions = styled.div`
  padding: 0.5rem 0.5rem;
  display: flex;
  flex-direction: column;
  & > *:not(:first-child) {
    margin-top: 0.5rem;
  }

  svg {
    color: ${({ theme }) => theme.color.fontPrimary};
  }
`;

export const ConceptMiniViewContent = styled.div<{isPublic?: boolean}>`
  display: flex;
  flex-direction: column;
  padding: 1rem;
  padding-left: 0;
  padding-top: 0;
  transition: all 0.3s;
  width: 100%;
	color: inherit;

  &:hover {
    cursor: pointer;
    color: ${({theme, isPublic})=> isPublic ? theme.color.publicConcept : theme.color.primary };
  }
`;

const lines = 8;
export const ConceptMiniViewText = styled.div`
  max-height: ${lines}rem;
  overflow: hidden;
  display: -webkit-box;
  line-clamp: ${lines};
  -webkit-line-clamp: ${lines};
  -webkit-box-orient: vertical;
  width: 100%;
  word-break: normal;
  font-size: ${({ theme }) => theme.typography.paragraph.fontSize};
	color: inherit;
`;



export const TextMiniViewWrapper = styled.div``;

// Based on https://hungyi.net/blog/css-truncate/
export const TextMiniViewContent = styled.div`
  line-height: 1.15;

  width: 100%;
  font-size: ${({ theme }) => theme.typography.paragraph.fontSize};
	color: inherit;
  position: relative;

  padding-bottom: 1.15em;

  max-height: ${lines}rem;
  overflow: hidden;
  margin-bottom: -1.15em;
  overflow: hidden;
  mask-image: linear-gradient(
    to bottom,
    black 80%,
    transparent 93.33333%
  );

  &::after {
    content: " ";
    display: block;
    height: 1.15em;
  }


  *[data-slate-highlight="true"] {
    background-color: ${({ theme }) => theme.color.highlight};
  }
`;

export const ConceptMiniViewInfo = styled.div`
  margin-top: 1rem;
  font-size: 0.6rem;
  font-weight: ${({ theme }) => theme.fontWeight.bold};
  width: 100%;
  word-break: normal;
	color: inherit;
`;

export const DraftingPreview = styled.div`
	margin: 1em 0 -0.6em 0;
	display: flex;
	flex-direction: row;
	align-items: baseline;
	& > div:not(:first-child){
		margin-left: 0.5em;
	}
	color: inherit;
`;

export const HighlightSpan = styled.span<{isHighlight: boolean}>`
	background-color: ${({theme, isHighlight}) => ( isHighlight ? theme.color.highlight : '')};
`;



export const AddTo = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	position: relative;

	&:hover {
		cursor: pointer;
		color: ${({ theme }) => theme.color.primary};
		${Paragraph} {
			color: ${({ theme }) => theme.color.primary};
		}
	}
`;
