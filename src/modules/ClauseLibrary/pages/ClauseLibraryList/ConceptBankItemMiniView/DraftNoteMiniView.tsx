import React, { Component, useState } from "react";
import { DraftingPreview } from "./styles";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStickyNote } from "@fortawesome/pro-light-svg-icons";
import { theme } from 'src/styles/scTheme';
import styled, { css } from 'styled-components';

export default function DraftNoteMiniView({ draftingNote }: { draftingNote: string }) {
	return (
		<React.Fragment>
			<DraftingPreview>
				<DottedCircleBorder color={draftingNote ? 'primary' : 'fontSecondary'} diameter={'20px'}>
					<FontAwesomeIcon icon={faStickyNote} size={'sm'}
						color={draftingNote ? theme.color.primary + '' : theme.color.fontSecondary + ''}
					/>
				</DottedCircleBorder>
				<IconLabel fontSize={'caption'} color={draftingNote ? 'primary' : 'fontSecondary'}
					fontWeight={'normal'}>
					{
						draftingNote ? 'View drafting note' : 'No drafting note'
					}
				</IconLabel>
			</DraftingPreview>
		</React.Fragment>
	);
}


export const IconLabel = styled.div<{ fontSize: string; color: string; fontWeight: string }>`
	margin-left: 0.2em;
	color: ${({ theme, color }) => theme.color[color]};
  font-size: ${({ theme, fontSize }) => theme.typography[fontSize].fontSize};
  font-weight: ${({ theme, fontWeight }) => theme.fontWeight[fontWeight]};
  :hover {
    text-decoration: underline;
	}
`;

export const DottedCircleBorder = styled.div<{ color: string; diameter: string }>`
	display: flex;
	align-items: center;
	justify-items: center;
	justify-content: center;
	border: 1px dashed ${({ theme, color }) => theme.color[color]};
	border-radius: 50%;
	width: ${({ diameter }) => diameter};
	height: ${({ diameter }) => diameter};
`;
