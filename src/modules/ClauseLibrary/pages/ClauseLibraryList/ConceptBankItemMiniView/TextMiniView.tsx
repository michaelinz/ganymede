import React from 'react';
// import { compare2Words } from 'src/utils/searchAndHighlightUtils';
import { TextMiniViewContent, TextMiniViewWrapper } from './styles';
import { convertFormattedStringifiedJSON, McfRichTextDisplay } from '@mccarthyfinch/author-ui-components';
// import { SearchHighlightExtension } from '@mccarthyfinch/author-ui-components/dist/compounds/McfRichText/Remirror/Extensions/SearchHighlightExtension';
// import SearchControl from '@mccarthyfinch/author-ui-components/dist/compounds/McfRichText/Remirror/Extensions/SearchHighlightExtension/SearchControl';
import { IConceptBankItem } from '@mccarthyfinch/public-api-client';

interface ITextMiniView {
  conceptBankItem: IConceptBankItem;
  searchValues?: string[];
}

function TextMiniView({ conceptBankItem, searchValues = [] }: ITextMiniView) {
	// const searchExtension = new SearchHighlightExtension({
  //   equalityFunction: compare2Words,
  // });

  return (
    <TextMiniViewWrapper>
      <TextMiniViewContent>
        <McfRichTextDisplay
          value={convertFormattedStringifiedJSON(conceptBankItem?.content?.formattedText, conceptBankItem?.content?.plainText)}
          // EditorProps={{
          //   renderAboveEditor: () => <SearchControl searchStrings={searchValues} />,
          //   extensions: [searchExtension],
          // }}
        />
      </TextMiniViewContent>
    </TextMiniViewWrapper>
  );
}

export default TextMiniView;
