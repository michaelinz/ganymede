import React from 'react';
import ConceptBankItemService from 'src/modules/ClauseLibrary/service/ConceptBankItemService';
import QueryStatusWrapper from 'src/components/QueryStatusWrapper';
import ClauseLibraryListView from './ClauseLibraryListView';
import { IClauseLibraryUI } from 'src/modules/ClauseLibrary/useClauseLibraryUI';
import ConceptTypeService from 'src/models/service/author/ConceptTypeService';
import DocumentTypeService from 'src/models/service/author/DocumentTypeService';
import TagService from 'src/models/service/author/TagService';
import SuperTagService from 'src/models/service/author/SuperTagService';

interface IClauseLibraryListProps {
	clauseLibraryUI: IClauseLibraryUI;
}

export default function ClauseLibraryList({clauseLibraryUI}: IClauseLibraryListProps) {
	const queryResult = ConceptBankItemService.useCountByConceptType();

	// Start pre-fetching author types to reduce load times
	const conceptTypesResult = ConceptTypeService.useConceptTypes();
	const documentTypesResult = DocumentTypeService.useDocumentTypes();
	const tagsResult = TagService.useTags();
	const superTagsResult = SuperTagService.useSuperTags();

	const getAllowedConceptTypes = () => {
		if (queryResult.data && conceptTypesResult?.data) {
			const allowedConceptTypeUUIDs = conceptTypesResult?.data.map(ct => ct.uuid);
			return queryResult.data.filter(item => allowedConceptTypeUUIDs.includes(item.uuid));
		}
	};

	return (
		<QueryStatusWrapper
			queryResults={[queryResult, conceptTypesResult]}
			renderError={() => <></>}
			data-ted="checklist-list"
		>
			<ClauseLibraryListView conceptTypes={conceptTypesResult.data} conceptTypeCounts={getAllowedConceptTypes()} clauseLibraryUI={clauseLibraryUI} />
		</QueryStatusWrapper>
	);
}
