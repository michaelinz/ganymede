import { faArrowLeft } from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { convertFormattedStringifiedJSON, HR, McfRichTextDisplay, Paragraph, Subtitle } from '@mccarthyfinch/author-ui-components';
import { IConceptBankItem, IConceptType, IDocumentType, ISuperTag, ITag } from '@mccarthyfinch/public-api-client';
import format from 'date-fns/format';
import React, { useRef } from 'react';
import Drawer, { DrawerPageContentWrapper, DrawerPageWrapper } from 'src/components/Drawer';
import { Row } from 'src/components/Layout';
import { PrimaryButton } from 'src/components/molecules/Buttons';
import ConceptBankItemTags from 'src/modules/ClauseLibrary/components/ConceptBankItemTags';
import FavouriteIcon from 'src/modules/ClauseLibrary/components/FavouriteIcon';
import { ClauseLibraryUIView, IClauseLibraryUI } from 'src/modules/ClauseLibrary/useClauseLibraryUI';
import getLabelsForTypes from 'src/modules/ClauseLibrary/utils/getLabelsForTypes';
import { BackButton, ConceptBankItemDetailedViewWrapper, DrawerContent } from './styles';

interface IConceptBankItemDetailedViewProps {
	clauseLibraryUI: IClauseLibraryUI;
	conceptBankItem: IConceptBankItem;
	conceptTypes: IConceptType[];
	documentTypes: IDocumentType[];
	tags: ITag[];
	superTags: ISuperTag[];
}

export default function ConceptBankItemDetailedView({
	clauseLibraryUI,
	conceptBankItem,
	conceptTypes,
	documentTypes,
	tags,
	superTags,
}: IConceptBankItemDetailedViewProps) {
	const textRef = useRef(null);

	const onBack = () => {
		clauseLibraryUI.view.setClauseLibraryUIView(ClauseLibraryUIView.Browse);
		clauseLibraryUI.view.setSelectedConceptBankItemUUID(null);
	};

	const onAddTo = async () => {
		await clauseLibraryUI.onAdd.get.action(textRef?.current?.outerHTML, conceptBankItem?.content?.plainText);
		// GA.event({
		// 	category: EventCategory.DRAFT,
		// 	action: EventAction.INSERTED_CONCEPT,
		// 	label: conceptBankItem.conceptTypes.map(t => t.nameFormatted).join(','),
		// });
	};


	return (
		<DrawerPageWrapper>
			<DrawerPageContentWrapper>
			<ConceptBankItemDetailedViewWrapper>
				<Row style={{ minHeight: '1.5rem', justifyContent: 'space-between', marginBottom: '1rem' }}>
					<Row>
						<BackButton onClick={onBack}>
							<FontAwesomeIcon icon={faArrowLeft} />
						</BackButton>
					</Row>
					<Row style={{ width: 'auto' }}>
						<FavouriteIcon favourite={conceptBankItem?.favourite} />
					</Row>
				</Row>
				<div>
					<Subtitle fontWeight='bold'>{conceptBankItem.name || getLabelsForTypes(conceptTypes)}</Subtitle>
					<ConceptBankItemTags
						conceptBankItem={conceptBankItem}
						conceptTypes={conceptTypes}
						documentTypes={documentTypes}
						tags={tags}
						superTags={superTags}
					/>
					<br />
					<Paragraph fontWeight="bold">Clause</Paragraph>
					<div ref={textRef} style={{ marginBottom: '1.15em' }}>
						<McfRichTextDisplay
							value={convertFormattedStringifiedJSON(conceptBankItem?.content?.formattedText, conceptBankItem?.content?.plainText)}
						/>
					</div>
					<br />
					<Paragraph fontWeight='bold'>Guidance</Paragraph>
					<McfRichTextDisplay
						value={convertFormattedStringifiedJSON(conceptBankItem?.draftingNoteContent?.formattedText, conceptBankItem?.draftingNoteContent?.plainText)}
						placeholder='No guidance...'
					/>
					<br />
					<HR />
					<br />

					<MetaRow label='Used' value={`${conceptBankItem?.used || 0} times`} />
					<MetaRow label='Clause Type' value={getLabelsForTypes(conceptTypes)} />
					{
						Boolean(tags?.length) &&
						<MetaRow label='Keywords' value={getLabelsForTypes(tags)} />
					}
					{
						Boolean(documentTypes?.length) &&
						<MetaRow label='Document Type' value={getLabelsForTypes(documentTypes)} />
					}
					{
						Boolean(superTags?.length) &&
						<MetaRow label='Team' value={getLabelsForTypes(superTags)} />
					}
					{
						Boolean(conceptBankItem?.createdAt) &&
						<MetaRow label='Created' value={`${conceptBankItem?.createdBy?.email ? `${conceptBankItem?.createdBy?.email}, `: ''}${format(conceptBankItem?.createdAt, 'MMM D, YYYY')}`} />
					}
					{
						Boolean(conceptBankItem?.lastUpdatedAt) &&
						<MetaRow label='Modified' value={`${conceptBankItem?.lastUpdatedBy?.email ? `${conceptBankItem?.lastUpdatedBy?.email}, `: ''}${format(conceptBankItem?.lastUpdatedAt, 'MMM D, YYYY')}`} />
					}
				</div>
			</ConceptBankItemDetailedViewWrapper>
			</DrawerPageContentWrapper>
			<Drawer isOpen={true} hideHandle={true}>
				<DrawerContent>
					<PrimaryButton onClick={onAddTo}>Insert</PrimaryButton>
				</DrawerContent>
			</Drawer>
		</DrawerPageWrapper>
	);
}



function MetaRow({label, value}: {label: string; value: string}) {
	return (
		<Paragraph style={{marginTop: '0.5em'}}>
			<strong style={{fontWeight: 600, marginRight: '0.25em'}}>{label}</strong>{value}
		</Paragraph>
	);
}
