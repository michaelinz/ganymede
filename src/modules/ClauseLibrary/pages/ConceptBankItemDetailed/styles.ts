import { IconButtonBorderless } from '@mccarthyfinch/author-ui-components';
import styled from 'styled-components';

export const ConceptBankItemDetailedViewWrapper = styled.div`
	padding: 0.5rem 1rem;
	flex-grow: 1;
`;

export const BackButton = styled(IconButtonBorderless)`
	padding: 0;
	color: ${({ theme }) => theme.colorPalette.smoke[600]};
`;

export const DrawerContent = styled.div`
	display: flex;
	flex-direction: row;
	flex-flow: row-reverse;
	padding: 1rem;
`;
