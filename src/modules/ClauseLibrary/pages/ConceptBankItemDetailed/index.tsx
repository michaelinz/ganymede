import { IConceptBankItem } from '@mccarthyfinch/public-api-client';
import React from 'react';
import QueryStatusWrapper from 'src/components/QueryStatusWrapper';
import ConceptTypeService from 'src/models/service/author/ConceptTypeService';
import DocumentTypeService from 'src/models/service/author/DocumentTypeService';
import SuperTagService from 'src/models/service/author/SuperTagService';
import TagService from 'src/models/service/author/TagService';
import ConceptBankItemService from 'src/modules/ClauseLibrary/service/ConceptBankItemService';
import { IClauseLibraryUI } from 'src/modules/ClauseLibrary/useClauseLibraryUI';
import ConceptBankItemDetailedView from './ConceptBankItemDetailedView';

interface IConceptBankItemDetailedProps {
	clauseLibraryUI: IClauseLibraryUI;
}

export default function ConceptBankItemDetailed({clauseLibraryUI}: IConceptBankItemDetailedProps) {
	const queryResult = ConceptBankItemService.useConceptBankItem(clauseLibraryUI.view.selectedConceptBankItemUUID);
	return (
		<QueryStatusWrapper
			queryResults={[queryResult]}
			data-ted="checklist-list"
		>
			<ConceptBankItemFetch conceptBankItem={queryResult.data} clauseLibraryUI={clauseLibraryUI} />
		</QueryStatusWrapper>
	);
}

function ConceptBankItemFetch({ clauseLibraryUI, conceptBankItem }: { clauseLibraryUI: IClauseLibraryUI; conceptBankItem: IConceptBankItem }) {
	const conceptTypesResult = ConceptTypeService.useConceptTypes();
	const documentTypesResult = DocumentTypeService.useDocumentTypes();
	const tagsResult = TagService.useTags();
	const superTagsResult = SuperTagService.useSuperTags();

	const conceptTypes = conceptTypesResult?.data?.filter((item) => conceptBankItem.conceptTypeUUIDs.includes(item.uuid));
	const documentTypes = documentTypesResult?.data?.filter((item) => conceptBankItem.documentTypeUUIDs.includes(item.uuid));
	const tags = tagsResult?.data?.filter((item) => conceptBankItem.tagUUIDs.includes(item.uuid));
	const superTags = superTagsResult?.data?.filter((item) => conceptBankItem.superTagUUIDs.includes(item.uuid));

	return (
		<QueryStatusWrapper queryResults={[conceptTypesResult, documentTypesResult, tagsResult, superTagsResult]}>
			<ConceptBankItemDetailedView
				clauseLibraryUI={clauseLibraryUI}
				conceptBankItem={conceptBankItem}
				conceptTypes={conceptTypes}
				documentTypes={documentTypes}
				tags={tags}
				superTags={superTags}
			/>
		</QueryStatusWrapper>
	);
}
