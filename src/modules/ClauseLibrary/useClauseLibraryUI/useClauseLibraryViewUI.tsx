import React, { useState } from 'react';

export enum ClauseLibraryUIView {
	Browse = 'Browse',
	Detailed = 'Detailed',
	Create = 'Create',
	Edit = 'Edit',
	Closed = 'Closed',
}

export default function useClauseLibraryViewUI() {

	const [ onClauseLibraryUIViewChange, setOnClauseLibraryUIViewChange ] = useState<(clauseLibraryUIView: ClauseLibraryUIView, ignoreBlock?: boolean) => boolean>();

	const [ clauseLibraryUIView, _setClauseLibraryUIView ] = useState(ClauseLibraryUIView.Closed);
	const setClauseLibraryUIView = (view: ClauseLibraryUIView, ignoreBlock = false) => {
		if (!ignoreBlock && onBlockUIViewChange) {
			setBlockedUIViewChange(view);
			onBlockUIViewChange();
			return false;
		}
		if (onClauseLibraryUIViewChange) {
			if (!onClauseLibraryUIViewChange(view, ignoreBlock)) {
				return false;
			}
		}
		_setClauseLibraryUIView(view);
		setBlockedUIViewChange(null);
		return true;
	};

	const [ blockedUIViewChange, setBlockedUIViewChange ] = useState<ClauseLibraryUIView>();
	const [ onBlockUIViewChange, setOnBlockUIViewChange ] = useState<() => void>();
	const goToBlockedTabChange = () => {
		if (blockedUIViewChange) {
			setClauseLibraryUIView(blockedUIViewChange, true);
		}
	};

	const [ selectedConceptBankItemUUID, setSelectedConceptBankItemUUID ] = useState<string>();

	return {
		setOnClauseLibraryUIViewChange,
		clauseLibraryUIView, setClauseLibraryUIView,
		onBlockUIViewChange, setOnBlockUIViewChange, goToBlockedTabChange,
		selectedConceptBankItemUUID, setSelectedConceptBankItemUUID,
	};
}
