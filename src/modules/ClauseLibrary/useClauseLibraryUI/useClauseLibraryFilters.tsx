import { useState } from 'react';
import LocalStorage from 'src/models/utils/appState/localStorage';


export enum QuickFilter {
	TEAM = 'TEAM',
	MINE = 'MINE',
	PRIVATE = 'PRIVATE',
	PUBLIC = 'PUBLIC',
}

export type Filters = {
	quick: QuickFilter[];
	clauseUUIDs: string[];
};

const defaultFilterState: Filters = {
	quick: [],
	clauseUUIDs: [],
};

const FILTERS_CACHE_KEY = 'clause-library-filters';

export default function useClauseLibraryFilters() {
	const [ filters, _setFilters ] = useState(LocalStorage.getAll(FILTERS_CACHE_KEY) || defaultFilterState);

	const cacheFilters = () => {
		LocalStorage.setAll(FILTERS_CACHE_KEY, filters);
	};

	const setFilters = (filters: Filters) => {
		_setFilters(filters);
		// cacheFilters(); // TODO change useClauseLibraryFilters to detect if it's comparison shelf or clause library so that we use a different cache key
	};

	return {
		filters,
		setFilters,
	};
}
