import React, { useState } from 'react';
import { insertAtSelection } from '../editorUtils';
import useClauseLibraryFilters from './useClauseLibraryFilters';
import useClauseLibraryViewUI from './useClauseLibraryViewUI';

export { ClauseLibraryUIView } from './useClauseLibraryViewUI';
export { QuickFilter } from './useClauseLibraryFilters';
export type { Filters } from './useClauseLibraryFilters';

interface IClauseLibraryItemOnAdd {
	label: string;
	action: (html: string, text: string) => Promise<void>;
}


function useClauseLibraryUI() {
	const [ onAdd, setOnAdd ] = useState<IClauseLibraryItemOnAdd>({ label: 'Add to Document', action: insertAtSelection });
	const view = useClauseLibraryViewUI();
	const filters = useClauseLibraryFilters();

	return {
		onAdd: {
			get: onAdd,
			set: setOnAdd,
		},
		view,
		filters,
	};
}

export type IClauseLibraryUI = ReturnType<typeof useClauseLibraryUI>;

// Comparison Shelf
const ComparisonShelfUIContext = React.createContext<IClauseLibraryUI | undefined>(undefined);
export function ComparisonShelfUIProvider({ children }: { children: React.ReactChild | React.ReactChildren }) {
	const clauseLibraryUI = useClauseLibraryUI();
	return (
		<ComparisonShelfUIContext.Provider value={clauseLibraryUI}>
			{children}
		</ComparisonShelfUIContext.Provider>
	);
}
export function useComparisonShelfUI() {
	return React.useContext(ComparisonShelfUIContext);
}


// Page
const ClauseLibraryPageUIContext = React.createContext<IClauseLibraryUI | undefined>(undefined);
export function ClauseLibraryPageUIProvider({ children }: { children: React.ReactChild | React.ReactChildren }) {
	const clauseLibraryUI = useClauseLibraryUI();
	return (
		<ClauseLibraryPageUIContext.Provider value={clauseLibraryUI}>
			{children}
		</ClauseLibraryPageUIContext.Provider>
	);
}
export function useClauseLibraryPageUI() {
	return React.useContext(ClauseLibraryPageUIContext);
}
