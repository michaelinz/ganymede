import { ConceptBankItemApiFactory } from '@mccarthyfinch/public-api-client';
import { QueryClient, useQueryClient } from 'react-query';
import getBffAPIProvider, { getBffBasePath } from 'src/models/base/getBffProvider';
import { getUseFetch, getUseOptimisticCreate, getUseOptimisticUpdate } from 'src/models/utils/reactQuery/useOptimisticMutation';

export function getConceptBankItemApi() {
	return ConceptBankItemApiFactory(undefined, getBffBasePath(), getBffAPIProvider().getInstance());
}

export const ConceptBankItemQueryKey = 'concept-bank-items';

const ConceptBankItemService = {
	useCountByConceptType: (options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		const cacheKey = [`${ConceptBankItemQueryKey}-count-by-concept-type`];
		return getUseFetch({
			client,
			cacheKey,
			fetch: () =>
				getConceptBankItemApi()
					.getCountByConceptType()
					.then(data => data.data),

			options: {
				refetchInterval: 1000 * 60,
				refetchOnMount: false,
				refetchOnWindowFocus: false,
				refetchOnReconnect: false,
			}
		});
	},
	useConceptBankItemsByConceptTypeUUIDs: (conceptTypeUUIDs: string[], options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		const cacheKey = [`${ConceptBankItemQueryKey}-by-concept-type`, conceptTypeUUIDs.sort().join(', ')];
		return getUseFetch({
			client,
			cacheKey,
			fetch: () =>
				getConceptBankItemApi()
					.getByConceptTypeUUIDs(conceptTypeUUIDs)
					.then(data => data.data),
		});
	},
	useConceptBankItem: (conceptBankItemUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		const cacheKey = [ConceptBankItemQueryKey, conceptBankItemUUID];
		return getUseFetch({
			client,
			cacheKey,
			fetch: () =>
				getConceptBankItemApi()
					.getConceptBankItem(conceptBankItemUUID)
					.then(data => data.data),
		});
	}

};

export default ConceptBankItemService;
