import { getEditor } from 'src/editor/EditorFactory';

export async function insertAtSelection(html: string, text: string) {
	const editor = getEditor();
	try {
		const cleanedHTML = cleanHtml(html);
		await editor.Selection.insertHtmlAtSelection(cleanedHTML);
	} catch (e) {
		console.warn(e);
		await editor.Selection.insertTextAtSelection(text);
	}
}


export function cleanHtml(html: string): string {
	console.log(html);
	const dom = new DOMParser().parseFromString(html, 'text/html');
	// const dom = document.createRange().createContextualFragment(html);
	const element = dom.querySelector('div.ProseMirror');
	const removeClasses = (element: Element) => {
		if (element instanceof Element) {
			if (element.classList.length) {
				element.classList.value = '';
			}
		}
		for (let i = 0; i < element.children.length; i++) {
			const child = element.children.item(i);
			removeClasses(child);
		}
		return;
	};
	removeClasses(element);
	return element.outerHTML;
}
