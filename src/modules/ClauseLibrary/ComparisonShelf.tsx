import React from 'react';
import McfPanel from 'src/components/McfPanel/McfPanel';
import { ClauseLibraryUIView, ComparisonShelfUIProvider, useComparisonShelfUI } from 'src/modules/ClauseLibrary/useClauseLibraryUI';
import ClauseLibrary from 'src/modules/ClauseLibrary';
import ComparisonShelfHeader from './ComparisonShelfHeader';


function ComparisonShelfView() {
	const clauseLibraryUI = useComparisonShelfUI();
	const isOpen = clauseLibraryUI?.view?.clauseLibraryUIView !== ClauseLibraryUIView.Closed;
	const onDismiss = () => {
		clauseLibraryUI?.view?.setClauseLibraryUIView(ClauseLibraryUIView.Closed);
	};
	return (
		<McfPanel isOpen={isOpen} onDismiss={onDismiss}>
			<ClauseLibrary
				clauseLibraryUI={clauseLibraryUI}
				renderHeader={clauseLibraryUI => <ComparisonShelfHeader clauseLibraryUI={clauseLibraryUI} onDismiss={onDismiss} />}
			/>
		</McfPanel>
	);
}


// Provider is provided in ChecklistEditorBasePage instead
export default function ComparisonShelf(props) {
	return (
		// <ComparisonShelfUIProvider>
			<ComparisonShelfView {...props}/>
		// </ComparisonShelfUIProvider>
	);
}
