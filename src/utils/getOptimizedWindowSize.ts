export default function getOptimizedWindowSize(desiredWidth: number, desiredHeight: number) {
	function maxSize(value: number, max: number) {
		return value < max - 30 ? value : max - 30;
	}

	function percentage(value: number, max: number) {
		return (value * 100) / max;
	}

	const { width: screenWidth, height: screenHeight } = window.screen;
	const width = maxSize(desiredWidth, screenWidth);
	const height = maxSize(desiredHeight, screenHeight);
	const widthPercentage = percentage(width, screenWidth);
	const heightPercentage = percentage(height, screenHeight);
	return { widthPercentage, heightPercentage, width, height };
}
