import React, { ReactNode } from 'react';
import AppDependencyProvider from './AppDependencyProvider';
import StylesDependencyProvider from './StylesDependencyProvider';

// Cannot use properly until this is fixed https://github.com/snowpackjs/snowpack/discussions/1312
// You can work around this if you really need to by installing react-query-dev-tools then going into
// the node_modules/react-query-devtools/index file and making it only export once
import { ReactQueryDevtools } from 'react-query-devtools';
import AppConfigs from 'src/config/AppConfigs';

export default function DependencyProvider({ children }: { children: ReactNode }) {
	return (
		<AppDependencyProvider>
			<StylesDependencyProvider>
				{children}
				{AppConfigs.SHOW_REACT_QUERY_DEV_TOOLS && <ReactQueryDevtools initialIsOpen={false} />}
			</StylesDependencyProvider>
		</AppDependencyProvider>
	);
}
