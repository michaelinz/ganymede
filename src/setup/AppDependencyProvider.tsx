import { configure } from 'mobx';
import React, { ReactNode, useEffect, useState } from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
/**
 * We have to use HashRouter instead of BrowserRouter because Office does some funky stuff
 * https://github.com/OfficeDev/office-js/issues/429
 */
import { HashRouter as Router } from 'react-router-dom';
import LoadingOverlay from 'src/components/LoadingOverlay';
import FeatureFlags from 'src/config/FeatureFlags';
import Editor, { ISubscriber } from 'src/editor/base/Editor';
import EditorFactory from 'src/editor/EditorFactory';

import { autoCloseWindowForFederatedLogin, autoCloseWindowForFederatedLogout } from 'src/models/store/auth/autoCloseWindowForFederatedLogin';
import { PanelStoreProvider } from 'src/models/store/PanelStore';
import { PreferenceStoreProvider } from 'src/models/store/PreferenceStore';
import { hydrateQueryCacheFromStorage } from 'src/models/utils/reactQuery/localStorage';
import polyfillOffice from './polyfillOffice';
import composeWrappers from 'src/utils/composeWrappers';
import { AuthStoreProvider } from 'src/models/store/AuthStore';
import { UIStoreProvider } from 'src/models/store/UIStore';
import Provider from 'src/models/base/Provider';
import APIProvider from 'src/models/base/APIProvider';
import AppConfigs  from 'src/config/AppConfigs';


configure({
	useProxies: 'never',
	enforceActions: 'observed',
});

polyfillOffice();

export default function AppDependencyProvider({ children }: { children: ReactNode }) {
	const [isEditorInitialized, setIsEditorInitialized] = useState(false);

	Provider.API = new APIProvider(AppConfigs.API_URL_BASE).getInstance();

	const queryClient = new QueryClient();
	useEffect(() => {
		// Hydrate queryCache from local storage
		hydrateQueryCacheFromStorage(queryClient);
		initEditor({
			key: 'setInit',
			onEvent: async () => {
				setIsEditorInitialized(true);
			},
		});
	}, []);

	if (!isEditorInitialized) {
		return <LoadingOverlay />;
	}

	return (
		<QueryClientProvider client={queryClient}>
			<Router>
				<ContextProviders>{children}</ContextProviders>
			</Router>
		</QueryClientProvider>
	);
}

const ContextProviders = composeWrappers([
	AuthStoreProvider,
	PreferenceStoreProvider,
	PanelStoreProvider,
	UIStoreProvider,
]);


function initEditor(onLoadSubscriber: ISubscriber) {

	const setOnLoadSubscribers = (editor: Editor) => {
		// Close this window if necessary
		if (FeatureFlags.openSSOPopupWithMSPopup || editor['Editor']) {
			editor.addSubscriber('onLoad', {
				key: 'autoCloseWindow',
				onEvent: async () => {
					autoCloseWindowForFederatedLogin(() => {
						editor['Editor'].closeThisPopupWindow();
					});
					autoCloseWindowForFederatedLogout(() => {
						editor['Editor'].closeThisPopupWindow();
					});
				}
			});
		} else {
			autoCloseWindowForFederatedLogin(() => {
				window.close();
			});
			autoCloseWindowForFederatedLogout(() => {
				window.close();
			});
		}

		/**
		 * Setup application after Office initializes
		 */
		editor.addSubscriber('onLoad', onLoadSubscriber);
	}

	EditorFactory.generateInstance(setOnLoadSubscribers);
}
