import React, { ReactNode } from 'react';
import 'normalize.css';
import { ThemeProvider as SCThemeProvider } from 'styled-components';
import GlobalStyle from 'src/styles/GlobalStyle';
import scTheme from 'src/styles/scTheme';

export default function StylesDependencyProvider({ children }: { children: ReactNode }) {
	return (
		<SCThemeProvider theme={scTheme}>
			<GlobalStyle />
			{children}
		</SCThemeProvider>
	);
}
