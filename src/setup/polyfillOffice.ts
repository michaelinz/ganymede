/**
 * Office in all their wisdom decided to remove window.history functions
 * https://github.com/OfficeDev/office-js/issues/429
 *
 * We are storing these function in window._historyCache in public/index.html
 */
export default function polyfillOffice() {
	window.history.replaceState = (window as any)._historyCache.replaceState;
	window.history.pushState = (window as any)._historyCache.pushState;
}
