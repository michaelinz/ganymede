import { QueryClient, QueryKey } from 'react-query';

// TODO: replace with https://react-query.tanstack.com/plugins/persistQueryClient when it is no longer experimental

const LOCAL_STORAGE_CACHE_KEY = 'queries';

/**
 * Cache data to localStorage
 * @param queryKey
 * @param data
 */
export async function writeToStorage(queryKey: QueryKey, data: any) {
	const _queryKey = JSON.stringify(queryKey);
	let storageData = await localStorage.getItem(LOCAL_STORAGE_CACHE_KEY);

	storageData = {
		...JSON.parse(storageData ?? '{}'),
		[_queryKey]: data,
	};

	localStorage.setItem(LOCAL_STORAGE_CACHE_KEY, JSON.stringify(storageData));
}

/**
 * Load cache from localStorage
 * @param queryClient
 */
export async function hydrateQueryCacheFromStorage(queryClient: QueryClient) {
	const storageData = await localStorage.getItem(LOCAL_STORAGE_CACHE_KEY);

	if (storageData !== null) {
		const queriesWithData = JSON.parse(storageData);

		for (const queryKey in queriesWithData) {
			const data = queriesWithData[queryKey];
			const queryKeyParsed = JSON.parse(queryKey);
			queryClient.setQueryData(queryKeyParsed, data);
		}
	}
}

/**
 * Clear cache in localStorage
 */
export async function clearStorage() {
	await localStorage.removeItem(LOCAL_STORAGE_CACHE_KEY);
}
