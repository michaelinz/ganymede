import { QueryClient, QueryKey, UseMutateAsyncFunction, useMutation, UseMutationResult, useQuery, UseQueryOptions } from 'react-query';
import { writeToStorage } from './localStorage';

interface IUseFetch<T> {
	client: QueryClient;
	cacheKey: string[];
	fetch: (props?: any) => Promise<T>;
	options?: UseQueryOptions<T>;
}
export function getUseFetch<T>({ client, cacheKey, fetch, options = {}}: IUseFetch<T>) {
	return useQuery(
		cacheKey,
		fetch,
		{
			onSuccess: (data) => writeToStorage(cacheKey, data),
			...options,
		},
	);
}


interface IOptimisticCreate<T> {
	client: QueryClient;
	cacheKey: string[];
	createItem: (item: T) => Promise<T>;
}

// Optimistic update returns UseMutationResult<T, unknown, Partial<T>, unknown> rather than UseMutationResult<T, unknown, T, unknown>
// Type error was fixed by putting T | Partial<T> in the type below but the need for Partial<T> may be incorrect
type OptimisticMutationTuple<T> = [UseMutateAsyncFunction<T, unknown, T | Partial<T>, unknown>, UseMutationResult<T, unknown, T | Partial<T>, unknown>];

export function getUseOptimisticCreate<T>({ client, cacheKey, createItem }: IOptimisticCreate<T>): OptimisticMutationTuple<T> {
	const mutation = useMutation(createItem, {
		// Optimistically update the cache value on mutate, but store
		// the old value and return it so that it's accessible in case of
		// an error
		onMutate: (item) => {
			// Cancel any outgoing refetches (so they don't overwrite our optimistic update)
			client.cancelQueries(cacheKey);
			// Snapshot the previous value
			const previousValue = client.getQueryData(cacheKey);

			// Optimistically update to the new value
			client.setQueryData<T[]>(cacheKey, (old) => {
				if (old) {
					return [...old, item];
				}
				return [item];
			});

			// Return the snapshotted value
			return previousValue;
		},
		onError: (err, variables, previousValue: T) => {
			// On failure
			// roll back to the previous value
			client.setQueryData(cacheKey, previousValue);
			// then refetch
			client.invalidateQueries(cacheKey);
		},
		onSuccess: (item) => {
			// On success, update storage
			writeToStorage(cacheKey, item);
		},
	});
	return [mutation.mutateAsync, mutation];
}

interface IOptimisticUpdate<T> {
	client: QueryClient;
	cacheKey?: [string, string | number];
	getCacheKey?: (item: Partial<T>) => [string, string | number];
	updateItem: (item: Partial<T>) => Promise<T>;
}
export function getUseOptimisticUpdate<T>({ client, cacheKey, getCacheKey, updateItem }: IOptimisticUpdate<T>): OptimisticMutationTuple<T> {
	const mutation = useMutation(updateItem, {
		// Optimistically update the cache value on mutate, but store
		// the old value and return it so that it's accessible in case of
		// an error
		onMutate: (item) => {
			const _cacheKey = cacheKey || getCacheKey(item);
			// Cancel any outgoing refetches (so they don't overwrite our optimistic update)
			client.cancelQueries(_cacheKey || getCacheKey(item));
			// Snapshot the previous value
			const previousValue = client.getQueryData<T>(_cacheKey);
			const newValue: T = {
				...previousValue,
				...item,
			};
			// Optimistically update to the new value
			client.setQueryData<T>(_cacheKey, newValue);

			function updateParent() {
				const parentCacheKey = [_cacheKey[0]];
				const itemID = _cacheKey[1];

				if (parentCacheKey[0] === undefined || itemID === undefined) {
					return null;
				}
				const previousParentValue = client.getQueryData(parentCacheKey, { active: true, inactive: true, exact: true });

				client.setQueryData<T[]>(parentCacheKey, (old) => {
					if (old) {
						// @ts-ignore
						const index = old.findIndex((item) => item.uuid === itemID);
						const newOld = [...old];
						if (index >= 0) {
							newOld[index] = {
								...old[index],
								...newValue,
							};
							return newOld;
						}
					}
					return [newValue];
				});

				return [parentCacheKey, previousParentValue];
			}

			const parentTuple = updateParent();

			return [[_cacheKey, previousValue], parentTuple];
		},
		// On failure, roll back to the previous value and refetch
		onError: (err, variables, context) => {
			if (context[0]) {
				const tuple = context[0];
				client.setQueryData(tuple[0], tuple[1]);
				client.invalidateQueries(tuple[0]);
			}
			if (context[1]) {
				const tuple = context[1];
				client.setQueryData(tuple[0], tuple[1]);
				client.invalidateQueries(tuple[0]);
			}
		},
		onSuccess: (item) => {
			const _cacheKey = cacheKey || getCacheKey(item);
			writeToStorage(_cacheKey, item);
		},
		// // After success or failure, refetch the query
		// onSettled: (data) => {
		// 	const _cacheKey = cacheKey || getCacheKey(data);
		// 	cache.invalidateQueries(_cacheKey);
		// 	const parentCacheKey = _cacheKey[0];
		// 	cache.invalidateQueries(parentCacheKey);
		// },
	});
	return [mutation.mutateAsync, mutation];
}

interface IOptimisticDelete {
	client: QueryClient;
	cacheKey?: [string, string | number];
	deleteItem: () => Promise<void>;
}

export function getUseOptimisticDelete<T extends { uuid?: string }>({ client, cacheKey, deleteItem }: IOptimisticDelete):
	[UseMutateAsyncFunction<void, unknown, void, unknown>,  UseMutationResult<void, unknown, void, unknown>] {
	const mutation = useMutation(deleteItem, {
		// Optimistically delete the cache value on mutate, but store
		// the old value and return it so that it's accessible in case of
		// an error
		onMutate: () => {
			const _cacheKey = cacheKey;
			// Cancel any outgoing refetches (so they don't overwrite our optimistic update)
			client.cancelQueries(_cacheKey);
			// Snapshot the previous value
			const previousValue = client.getQueryData<T>(_cacheKey);
			// Optimistically remove value
			client.removeQueries( _cacheKey);

			function updateParent() {
				const parentCacheKey = [_cacheKey[0]];
				const itemID = _cacheKey[1];

				if (parentCacheKey[0] === undefined || itemID === undefined) {
					return null;
				}
				const previousParentValue = client.getQueryData<T[]>(parentCacheKey, { active: true, inactive: true, exact: true });

				const newParentValue = previousParentValue.filter(item => item.uuid !== itemID);
				client.setQueryData<T[]>(parentCacheKey, newParentValue);

				return [parentCacheKey, previousParentValue];
			}

			const parentTuple = updateParent();

			return [[_cacheKey, previousValue], parentTuple];
		},
		// On failure, roll back to the previous value and refetch
		onError: (err, variables, context) => {
			if (context[0]) {
				const tuple = <[QueryKey, T]>context[0];
				client.setQueryData(tuple[0], tuple[1]);
				client.invalidateQueries(tuple[0]);
			}
			if (context[1]) {
				const tuple = <[QueryKey, T[]]>context[1];
				client.setQueryData(tuple[0], tuple[1]);
				client.invalidateQueries(tuple[0]);
			}
		},
	});
	return [mutation.mutateAsync, mutation];
}

// Not updated for react-query@3

// interface IOptimisticCreateNested<P, I> {
// 	cache: QueryCache;
// 	cacheKey: [string, string | number];
// 	setItemOnParent: (parent: P, item: I) => P;
// 	createItem: (item: I) => Promise<I>;
// }

// export function getUseOptimisticCreateNested<P, I>({
// 	cache,
// 	cacheKey, // cache key should be key of list
// 	setItemOnParent,
// 	createItem,
// }: IOptimisticCreateNested<P, I>) {
// 	return useMutation(createItem, {
// 		// Optimistically update the cache value on mutate, but store
// 		// the old value and return it so that it's accessible in case of
// 		// an error
// 		onMutate: (item) => {
// 			// Cancel any outgoing refetches (so they don't overwrite our optimistic update)
// 			cache.cancelQueries(cacheKey);

// 			const previousParent = cache.getQueryData<P>(cacheKey);
// 			const newParent = setItemOnParent(previousParent, item);
// 			// Optimistically update to the new value
// 			cache.setQueryData<P, unknown>(cacheKey, newParent);
// 			return previousParent;
// 		},
// 		// On failure, roll back to the previous value
// 		onError: (err, variables, previousParent: P) => cache.setQueryData(cacheKey, previousParent),
// 		// After success or failure, refetch the query
// 		onSettled: (data) => {
// 			cache.invalidateQueries(cacheKey);
// 		},
// 	});
// }

// interface IOptimisticUpdateNested<P, I> {
// 	cache: QueryCache;
// 	cacheKey: [string, string | number];
// 	getItemFromParent: (parent: P, item: Partial<I> & Required<{ uuid: string }>) => I;
// 	setItemOnParent: (parent: P, item: I) => P;
// 	updateItem: (item: Partial<I> & Required<{ uuid: string }>) => Promise<I>;
// }

// export function getUseOptimisticUpdateNested<P, I>({ cache, cacheKey, getItemFromParent, setItemOnParent, updateItem }: IOptimisticUpdateNested<P, I>) {
// 	// export function getUseOptimisticUpdateMutationNested<T, V>(
// 	// 	cache: QueryCache,
// 	// 	cacheKey: [string, string | number],
// 	// 	getItemFromParent: (parent: V, item: Partial<T>) => T,
// 	// 	setItemOnParent: (parent: V, item: Partial<T>) => V,
// 	// 	mutationFunction: (item: Partial<T>) => Promise<T>,
// 	// ) {
// 	return useMutation(updateItem, {
// 		// Optimistically update the cache value on mutate, but store
// 		// the old value and return it so that it's accessible in case of
// 		// an error
// 		onMutate: (item) => {
// 			// Cancel any outgoing refetches (so they don't overwrite our optimistic update)
// 			cache.cancelQueries(cacheKey);
// 			// Snapshot the previous value
// 			const previousParent = cache.getQueryData<P>(cacheKey);
// 			const previousValue = getItemFromParent(previousParent, item);
// 			const newValue: I = {
// 				...previousValue,
// 				...item,
// 			};

// 			const newParent = setItemOnParent(previousParent, newValue);
// 			// Optimistically update to the new value
// 			cache.setQueryData<P, unknown>(cacheKey, newParent);
// 			return previousParent;
// 		},
// 		// On failure, roll back to the previous value
// 		onError: (err, variables, previousParent: P) => cache.setQueryData(cacheKey, previousParent),
// 		// After success or failure, refetch the query
// 		onSettled: (data) => {
// 			cache.invalidateQueries(cacheKey);
// 		},
// 	});
// }
