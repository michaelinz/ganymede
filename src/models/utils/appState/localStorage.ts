function getData<T>(cacheKey: string): T {
	const data = localStorage.getItem(cacheKey);
	return JSON.parse(data ?? '{}');
}

function setData<T>(cacheKey: string, data: T): void {
	// const oldValue = localStorage.getItem(cacheKey);
	localStorage.setItem(cacheKey, JSON.stringify(data));
	// // Local storage events only fire if changed from a different window/tab, so manually firing the event here
	// window.dispatchEvent(new StorageEvent('storage', { key: cacheKey, newValue: JSON.stringify(data), oldValue }));
}

function getAll<T>(cacheKey: string): T {
	return getData<T>(cacheKey);
}

function setAll<T>(cacheKey: string, data: T): void {
	return setData<T>(cacheKey, data);
}

function clearAll(cacheKey): void {
	return localStorage.removeItem(cacheKey);
}

export default {
	getAll,
	setAll,
	clearAll,
};
