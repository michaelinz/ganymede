function getData<T>(cacheKey: string): T {
	const data = sessionStorage.getItem(cacheKey);
	return JSON.parse(data ?? '{}');
}

function setData<T>(cacheKey: string, data: T): void {
	sessionStorage.setItem(cacheKey, JSON.stringify(data));
}

function getAll<T>(cacheKey: string): T {
	return getData<T>(cacheKey);
}

function setAll<T>(cacheKey: string, data: T): void {
	return setData<T>(cacheKey, data);
}

function clearAll(cacheKey): void {
	return sessionStorage.removeItem(cacheKey);
}

export default {
	getAll,
	setAll,
	clearAll,
};
