import React, { createContext, useContext, useState } from 'react';

interface IStoreProviderProps<T> {
	store?: T;
	children: JSX.Element;
}

export default function contextStoreFactory<T>(createStore: () => T): [() => T, (props: IStoreProviderProps<T>) => JSX.Element, React.Context<T>] {
	const StoreContext = createContext<T>(null);
	const StoreProvider = ({ children, store }: IStoreProviderProps<T>) => {
		const [defaultStore] = useState(createStore());
		return <StoreContext.Provider value={store || defaultStore}>{children}</StoreContext.Provider>;
	};

	const useStore = () => useContext(StoreContext);
	return [useStore, StoreProvider, StoreContext];
}
