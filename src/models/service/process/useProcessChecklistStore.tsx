import React from 'react';
import useProcessChecklistTemplate from './useProcessChecklistTemplate';
export { isProcessing, ProcessingState } from './useProcessChecklistTemplate';

export type IProcessChecklistStore = ReturnType<typeof useProcessChecklistTemplate>;

const ProcessChecklistStoreContext = React.createContext<IProcessChecklistStore | undefined>(undefined);

export function ProcessChecklistStoreProvider({ children }: { children: React.ReactChild | React.ReactChildren }) {
	const processChecklistData = useProcessChecklistTemplate();
	return (
		<ProcessChecklistStoreContext.Provider value={processChecklistData}>
			{children}
		</ProcessChecklistStoreContext.Provider>
	);
}

export default function useProcessChecklistStore() {
	return React.useContext(ProcessChecklistStoreContext);
}
