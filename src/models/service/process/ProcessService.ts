import { ProcessApiFactory } from '@mccarthyfinch/public-api-client';
import getBffAPIProvider, { getBffBasePath } from 'src/models/base/getBffProvider';
import { AxiosResponse } from 'axios';

export function getProcessApi() {
	return ProcessApiFactory(undefined, getBffBasePath(), getBffAPIProvider().getInstance());
}

const ProcessService = {
	processChecklistTemplate: async (checklistTemplateUUID: string, previousFileUUID: string, blob: Blob, filename: string): Promise<{ documentSetUUID: string; fileUUID: string}> => {
		const form = new FormData();
		form.append('checklistTemplateUUID', checklistTemplateUUID);
		form.append('previousFileUUID', previousFileUUID);
		form.append('file', blob, filename);
		const response = await getBffAPIProvider().getInstance().post('/process/checklist-template', form);
		return response.data;
	},
	getProcessingStatus: async (documentSetUUID: string) => {
		const response = await getProcessApi().getProcessingStatus(documentSetUUID);
		return response.data;
	},
	downloadProcessedFile: async (documentUUID: string) => {
		const response = await getProcessApi().downloadProcessedFile(documentUUID, { responseType: 'blob' });
		return response.data;
	},
	// processChecklistTemplate: (checklistTemplateUUID: string, fileUUID: string | null, blob: Blob, filename: string) {

	// }
};



export default ProcessService;
