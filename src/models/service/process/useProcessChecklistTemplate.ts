import { getEditor } from 'src/editor/EditorFactory';
import ProcessService from './ProcessService';
import { useEffect, useState } from 'react';
import ChecklistService from '../checklistService/ChecklistService';
import useDocumentHighlights from 'src/pages/Highlights/HighlightsShelf/useDocumentHighlights';

async function sleep(ms: number): Promise<void> {
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve();
		}, ms);
	});
}

async function blobToBase64(blob): Promise<string> {
	return new Promise((resolve, reject) => {
		const reader = new FileReader();
		reader.readAsDataURL(blob);
		reader.onerror = (err) => {
			reject(err);
		};
		reader.onloadend = () => {
			const dataUrl: any = reader.result;
			const base64Document = dataUrl.split(',')[1];
			resolve(base64Document);
		};
	});
}

export enum ProcessingState {
	NotStarted = 'NotStarted',
	Uploading = 'Uploading',
	Processing = 'Processing',
	Fetching = 'Fetching',
	Highlighting = 'Highlighting',
	Complete = 'Complete',
	Error = 'Error',
}

export function isProcessing(processingState: ProcessingState) {
	return [ ProcessingState.Uploading, ProcessingState.Processing, ProcessingState.Fetching, ProcessingState.Highlighting ].includes(processingState);
}

export interface IUseProcessChecklistTemplate {
	processingState: ProcessingState;
	startProcessing: (checklistTemplateUUID: string) => Promise<void>;
	documentUUID: string;
	checklistUUID: string;
	shouldRedirectToChecklistOnComplete?: boolean;
	setShouldRedirectToChecklistOnComplete?: (shouldRedirectToChecklistOnComplete: boolean) => void;
}

export default function useProcessChecklistTemplate(): IUseProcessChecklistTemplate {
	const [processingState, setProcessingState] = useState(ProcessingState.NotStarted);
	const [documentUUID, setDocumentUUID] = useState<string>();
	const [checklistUUID, setChecklistUUID] = useState<string>();
	const [shouldRedirectToChecklistOnComplete, setShouldRedirectToChecklistOnComplete] = useState(true);

	const { highlightDocument } = useDocumentHighlights();

	useEffect(() => {
		console.log('Processing State', processingState);
	}, [processingState]);

	const uploadDocumentForProcessing = async (checklistTemplateUUID: string) => {
		try {
			setProcessingState(ProcessingState.Uploading);
			setShouldRedirectToChecklistOnComplete(true);
			const editor = getEditor();
			const binary = await editor.Document.getDocumentBinary();
			const [filename, previousFileUUID] = await Promise.all([
				editor.Document.getDocumentFilename(),
				editor.Settings.getFileUUID(),
			]);
			const documentSetUUID = await ProcessService.processChecklistTemplate(checklistTemplateUUID, previousFileUUID, binary, filename);
			setProcessingState(ProcessingState.Processing);
			return documentSetUUID;
		} catch (e) {
			console.error(e);
			setProcessingState(ProcessingState.Error);
		}
	};

	const pollProcessing = (documentSetUUID: string): Promise<string> => {
		const query = async (resolve, reject) => {
			try {
				const pollStatus = await ProcessService.getProcessingStatus(documentSetUUID);
				const pollTimeout = 1000; // TODO - we can update this polling to be smarter potentially eg with exponential backoffs etc
				if (!pollStatus.isComplete) {
					await sleep(pollTimeout);
					query(resolve, reject);
					return;
				}
				if (pollStatus.isError) {
					throw new Error('Document processing error');
				}
				if (pollStatus.isComplete) {
					setDocumentUUID(pollStatus.documentUUID);
					resolve(pollStatus.documentUUID);
				}
			} catch (e) {
				console.error(e);
				setProcessingState(ProcessingState.Error);
				reject(e);
			}
		};
		return new Promise<string>(query);
	};

	const fetchAndReplaceDocument = async (documentUUID: string) => {
		try {
			setProcessingState(ProcessingState.Fetching);
			const newDocumentBinary = await ProcessService.downloadProcessedFile(documentUUID);
			const editor = getEditor();
			const base64 = await blobToBase64(newDocumentBinary);
			return editor.Document.loadBase64ToDocument(base64);
		} catch (e) {
			console.error(e);
			setProcessingState(ProcessingState.Error);
		}
	};

	const fetchChecklistUUID = async (documentUUID: string) => {
		const checklist = await ChecklistService.getChecklistByDocumentUUID(documentUUID);
		if (!checklist?.uuid) {
			throw new Error(`No checklist found for document ${documentUUID}`);
		}
		return checklist.uuid;
	};

	const setSettingsOnDocument = async ({ documentUUID, fileUUID, checklistUUID }: { documentUUID: string; fileUUID: string; checklistUUID: string }): Promise<void> => {
		console.log('Setting on document', { documentUUID, fileUUID, checklistUUID })
		const editor = getEditor();
		await Promise.all([
			editor.Settings.setFileUUID(fileUUID),
			editor.Settings.setDocumentUUID(documentUUID),
			editor.Settings.setChecklistUUID(checklistUUID),
		]);
		console.log('Settings have been set');
	};

	const fetchAndInsertChecklist = async(fileUUID: string, documentUUID: string) => {
		console.log('documentUUID', documentUUID);
		await Promise.all([
			fetchAndReplaceDocument(documentUUID),
			fetchChecklistUUID(documentUUID).then((checklistUUID) => {
				setChecklistUUID(checklistUUID);
				return Promise.all([
					setSettingsOnDocument({ documentUUID, fileUUID, checklistUUID }),
				]);
			})
		]);
	};

	const highlight = async () => {
		try {
			setProcessingState(ProcessingState.Highlighting);
			await highlightDocument();
		} catch (e) {
			console.error(e);
		}
	};

	const startProcessing = async (checklistTemplateUUID: string) => {
		console.log('Start processing document');
		try {
			const { documentSetUUID, fileUUID } = await uploadDocumentForProcessing(checklistTemplateUUID);
			console.log({ documentSetUUID, fileUUID });
			const documentUUID = await pollProcessing(documentSetUUID);
			await fetchAndInsertChecklist(fileUUID, documentUUID);
			await highlight();
			setProcessingState(ProcessingState.Complete);
			console.log('Processing done');
		} catch (e) {
			console.error(e);
			setProcessingState(ProcessingState.Error);
		}
	};

	return {
		processingState,
		startProcessing,
		documentUUID,
		checklistUUID,
		shouldRedirectToChecklistOnComplete,
		setShouldRedirectToChecklistOnComplete
	};
}
