import { ChecklistTemplateApiFactory, IChecklistTemplate } from '@mccarthyfinch/public-api-client';
import { QueryClient, useQueryClient } from 'react-query';
import getBffAPIProvider, { getBffBasePath } from 'src/models/base/getBffProvider';
import { getUseFetch, getUseOptimisticDelete, getUseOptimisticUpdate } from 'src/models/utils/reactQuery/useOptimisticMutation';

export function getChecklistTemplateApi() {
	return ChecklistTemplateApiFactory(undefined, getBffBasePath(), getBffAPIProvider().getInstance());
}

export const ChecklistTemplatesQueryKey = 'checklistTemplates';

const ChecklistTemplateService = {
	useAllChecklistTemplates: (options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		const cacheKey = [ChecklistTemplatesQueryKey];
		return getUseFetch({
			client,
			cacheKey,
			fetch: () =>
				getChecklistTemplateApi()
					.getAllChecklistTemplates()
					.then((data) => data.data),
		});
	},

	useChecklistTemplate: (checklistTemplateUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		const cacheKey = [ChecklistTemplatesQueryKey, checklistTemplateUUID];
		return getUseFetch({
			client,
			cacheKey,
			fetch: () =>
				getChecklistTemplateApi()
					.getChecklistTemplate(checklistTemplateUUID)
					.then((data) => data.data),
		});
	},

	useUpdateChecklistTemplate: (checklistTemplateUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		return getUseOptimisticUpdate<IChecklistTemplate>({
			client,
			cacheKey: [ChecklistTemplatesQueryKey, checklistTemplateUUID],
			updateItem: (item) =>
				getChecklistTemplateApi()
					.updateChecklistTemplate(checklistTemplateUUID, item)
					.then((data) => data.data),
		});
    },

	useDeleteChecklistTemplate: (checklistTemplateUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		return getUseOptimisticDelete<IChecklistTemplate>({
			client,
			cacheKey: [ChecklistTemplatesQueryKey, checklistTemplateUUID],
			deleteItem: () =>
				getChecklistTemplateApi()
					.deleteChecklistTemplate(checklistTemplateUUID)
					.then(),
		});
	},
};

export default ChecklistTemplateService;
