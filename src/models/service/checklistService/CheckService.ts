import { CheckApiFactory, ICheck } from '@mccarthyfinch/public-api-client';
import { QueryClient, QueryObserverResult, useQueryClient } from 'react-query';
import getBffAPIProvider, { getBffBasePath } from 'src/models/base/getBffProvider';
import { getUseFetch } from 'src/models/utils/reactQuery/useOptimisticMutation';

export function getCheckApi() {
	return CheckApiFactory(undefined, getBffBasePath(), getBffAPIProvider().getInstance());
}

export const ChecksQueryKey = 'checks';

const CheckService = {

	useCheck: (checkUUID: string, options?: { client?: QueryClient }): QueryObserverResult<ICheck, unknown> => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;

		if (!checkUUID) {
			return getUseFetch({
				client,
				cacheKey: [],
				fetch: () => null,
			});
		}

		const cacheKey = [ChecksQueryKey, checkUUID];

		return getUseFetch({
			client,
			cacheKey,
			fetch: () =>
				getCheckApi()
					.getCheck(checkUUID)
					.then((data) => data.data),

			options: {
				refetchInterval: 1000 * 60,
				refetchOnMount: false,
				refetchOnWindowFocus: false,
				refetchOnReconnect: false,
			}
		});
	},

};

export default CheckService;
