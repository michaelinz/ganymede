import { DocumentReferenceApiFactory, IDocumentReference } from '@mccarthyfinch/public-api-client';
import { QueryClient, QueryObserverResult, useQueryClient } from 'react-query';
import getBffAPIProvider, { getBffBasePath } from 'src/models/base/getBffProvider';
import { getUseFetch, getUseOptimisticCreate, getUseOptimisticUpdate } from 'src/models/utils/reactQuery/useOptimisticMutation';

export function getDocumentReferenceApi() {
	return DocumentReferenceApiFactory(undefined, getBffBasePath(), getBffAPIProvider().getInstance());
}

export const DocumentReferencesQueryKey = 'document-references';

const getQueryKey = (issueUUID: string) => `${DocumentReferencesQueryKey}-issueUUID-${issueUUID}`;

const DocumentReferenceService = {
	useDocumentReferencesForIssue: (issueUUID: string, options?: { client?: QueryClient }): QueryObserverResult<IDocumentReference[], unknown> => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		const cacheKey = [getQueryKey(issueUUID)];
		return getUseFetch({
			client,
			cacheKey,
			fetch: () =>
				getDocumentReferenceApi()
					.getDocumentReferencesByIssueUUID(issueUUID)
					.then((data) => data.data),
			options: {
				refetchInterval: 1000 * 60,
				refetchOnMount: false,
				refetchOnWindowFocus: false,
				refetchOnReconnect: false,
			}
		});
	},

	useCreateDocumentReference: (issueUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		return getUseOptimisticCreate<IDocumentReference>({
			client,
			cacheKey: [getQueryKey(issueUUID)],
			createItem: (item) =>
				getDocumentReferenceApi()
					.createDocumentReference({ issueUUID, ...item })
					.then((data) => data.data),
		});
	},

	useUpdateDocumentReference: (issueUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		return getUseOptimisticUpdate<IDocumentReference>({
			client,
			getCacheKey: (item) => [getQueryKey(issueUUID), item.uuid],
			updateItem: (item) =>
				getDocumentReferenceApi()
					.updateDocumentReference(item.uuid, item)
					.then((data) => data.data),
		});
	},
};

export default DocumentReferenceService;
