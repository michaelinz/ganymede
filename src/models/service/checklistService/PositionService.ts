import { IPosition, PositionApiFactory } from '@mccarthyfinch/public-api-client';
import { QueryClient, useQueryClient } from 'react-query';
import getBffAPIProvider, { getBffBasePath } from 'src/models/base/getBffProvider';
import { getUseFetch, getUseOptimisticCreate, getUseOptimisticUpdate } from 'src/models/utils/reactQuery/useOptimisticMutation';

export function getPositionApi() {
	return PositionApiFactory(undefined, getBffBasePath(), getBffAPIProvider().getInstance());
}

export const PositionsQueryKey = 'positions';

const getQueryKey = (issueUUID: string) => `${PositionsQueryKey}-issueUUID-${issueUUID}`;

const PositionService = {
	usePositionsForIssue: (issueUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		const cacheKey = [getQueryKey(issueUUID)];
		return getUseFetch({
			client,
			cacheKey,
			fetch: () =>
				getPositionApi()
					.getPositionsByIssueUUID(issueUUID)
					.then((data) => data.data),
		});
	},

	useCreatePosition: (issueUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		return getUseOptimisticCreate<IPosition>({
			client,
			cacheKey: [getQueryKey(issueUUID)],
			createItem: (item) =>
				getPositionApi()
					.createPosition({ issueUUID, ...item })
					.then(async (data) => {
						return data.data;
					}),
		});
	},

	useUpdatePosition: (issueUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		return getUseOptimisticUpdate<IPosition>({
			client,
			getCacheKey: (item) => [getQueryKey(issueUUID), item.uuid],
			updateItem: (item) =>
				getPositionApi()
					.updatePosition(item.uuid, item)
					.then((data) => data.data),
		});
	},
};

export default PositionService;
