import { IIssueCategory, IssueCategoryApiFactory } from '@mccarthyfinch/public-api-client';
import { QueryClient, useQueryClient } from 'react-query';
import getBffAPIProvider, { getBffBasePath } from 'src/models/base/getBffProvider';
import { getUseFetch, getUseOptimisticCreate, getUseOptimisticUpdate } from 'src/models/utils/reactQuery/useOptimisticMutation';

export function getIssueCategoryApi() {
	return IssueCategoryApiFactory(undefined, getBffBasePath(), getBffAPIProvider().getInstance());
}

export const IssueCategoriesQueryKey = 'issue-categories';

const getQueryKey = (checklistUUID: string) => `${IssueCategoriesQueryKey}-checklistUUID-${checklistUUID}`;

const IssueCategoryService = {
	useIssueCategoriesForChecklist: (checklistUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		const cacheKey = [getQueryKey(checklistUUID)];
		return getUseFetch<IIssueCategory[]>({
			client,
			cacheKey,
			fetch: () =>
				getIssueCategoryApi()
					.getIssueCategoryByChecklistUUID(checklistUUID)
					.then((data) => data.data),

			options: {
				refetchInterval: 1000 * 60,
				refetchOnMount: false,
				refetchOnWindowFocus: false,
				refetchOnReconnect: false,
			}
		});
	},

	useCreateIssueCategory: (checklistUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		return getUseOptimisticCreate<IIssueCategory>({
			client,
			cacheKey: [getQueryKey(checklistUUID)],
			createItem: (item) =>
				getIssueCategoryApi()
					.createIssueCategory({ checklistUUID, ...item })
					.then(async (data) => {
						return data.data;
					}),
		});
	},

	useUpdateIssueCategory: (checklistUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		return getUseOptimisticUpdate<IIssueCategory>({
			client,
			getCacheKey: (item) => [getQueryKey(checklistUUID), item.uuid],
			updateItem: (item) =>
				getIssueCategoryApi()
					.updateIssueCategory(item.uuid, item)
					.then((data) => data.data),
		});
	},
};

export default IssueCategoryService;
