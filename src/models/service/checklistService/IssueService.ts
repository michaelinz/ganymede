import { IIssue, IssueApiFactory } from '@mccarthyfinch/public-api-client';
import { QueryClient, useQueryClient } from 'react-query';
import getBffAPIProvider, { getBffBasePath } from 'src/models/base/getBffProvider';
import { getUseFetch, getUseOptimisticCreate, getUseOptimisticUpdate } from 'src/models/utils/reactQuery/useOptimisticMutation';

export function getIssueApi() {
	return IssueApiFactory(undefined, getBffBasePath(), getBffAPIProvider().getInstance());
}

export const IssuesQueryKey = 'issues';

const getQueryKey = (checklistUUID: string) => `${IssuesQueryKey}-checklistUUID-${checklistUUID}`;

const IssueService = {
	useIssuesForChecklist: (checklistUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		const cacheKey = [getQueryKey(checklistUUID)];
		return getUseFetch({
			client,
			cacheKey,
			fetch: () =>
				getIssueApi()
					.getIssueByChecklistUUID(checklistUUID)
					.then((data) => data.data),

			options: {
				refetchInterval: 1000 * 60,
				refetchOnMount: false,
				refetchOnWindowFocus: false,
				refetchOnReconnect: false,
			}
		});
	},

	useIssue: (checklistUUID: string, issueUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		const cacheKey = [getQueryKey(checklistUUID), issueUUID];
		return getUseFetch({
			client,
			cacheKey,
			fetch: () =>
				getIssueApi()
					.getIssue(issueUUID)
					.then((data) => data.data),
		});
	},

	useCreateIssue: (checklistUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		return getUseOptimisticCreate<IIssue>({
			client,
			cacheKey: [getQueryKey(checklistUUID)],
			createItem: (item) =>
				getIssueApi()
					.createIssue(item)
					.then(async (data) => {
						return data.data;
					}),
		});
	},

	useUpdateIssue: (checklistUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		return getUseOptimisticUpdate<IIssue>({
			client,
			getCacheKey: (item) => [getQueryKey(checklistUUID), item.uuid],
			updateItem: (item) =>
				getIssueApi()
					.updateIssue(item.uuid, item)
					.then((data) => data.data),
		});
	},
};

export default IssueService;
