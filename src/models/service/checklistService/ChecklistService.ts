import { ChecklistApiFactory, IChecklist } from '@mccarthyfinch/public-api-client';
import { QueryClient, useQueryClient } from 'react-query';
import getBffAPIProvider, { getBffBasePath } from 'src/models/base/getBffProvider';
import { getUseFetch, getUseOptimisticCreate, getUseOptimisticUpdate } from 'src/models/utils/reactQuery/useOptimisticMutation';

export function getChecklistApi() {
	return ChecklistApiFactory(undefined, getBffBasePath(), getBffAPIProvider().getInstance());
}

export const ChecklistsQueryKey = 'checklists';

const ChecklistService = {
	getChecklistByDocumentUUID: async (documentUUID: string): Promise<IChecklist> => {
		const result = await getChecklistApi().getChecklistsByDocumentUUID(documentUUID); // TODO - update api to return 1;
		if (result?.data?.length === 1) {
			return result.data[0];
		}
		if (result?.data?.length > 1) {
			console.error('Returning multiple checklists', result.data);
		}
		return null;
	},
	useChecklists: (fileUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		const cacheKey = [ChecklistsQueryKey];
		return getUseFetch({
			client,
			cacheKey,
			fetch: () =>
				getChecklistApi()
					.getChecklistsByFileUUID(fileUUID)
					.then((data) => data.data),
		});
	},

	useChecklist: (checklistUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		const cacheKey = [ChecklistsQueryKey, checklistUUID];
		return getUseFetch({
			client,
			cacheKey,
			fetch: () =>
				getChecklistApi()
					.getChecklist(checklistUUID)
					.then((data) => data.data),
		});
	},

	useCreateChecklist: (options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		return getUseOptimisticCreate<IChecklist>({
			client,
			cacheKey: [ChecklistsQueryKey],
			createItem: (item) =>
				getChecklistApi()
					.createChecklist(item)
					.then((data) => data.data),
		});
	},

	useUpdateChecklist: (itemUUID: string, options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		return getUseOptimisticUpdate<IChecklist>({
			client,
			cacheKey: [ChecklistsQueryKey, itemUUID],
			updateItem: (item) =>
				getChecklistApi()
					.updateChecklist(itemUUID, item)
					.then((data) => data.data),
		});
	},
};

export default ChecklistService;
