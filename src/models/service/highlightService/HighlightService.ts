import { IHighlightReferences, IToggleHighlightsParams, WordHighlightApiFactory } from '@mccarthyfinch/public-api-client';
import { QueryClient } from 'react-query';
import getBffAPIProvider, { getBffBasePath } from 'src/models/base/getBffProvider';

export function getHighlightApi() {
	return WordHighlightApiFactory(undefined, getBffBasePath(), getBffAPIProvider().getInstance());
}

export const HighlightReferences = 'highlight-references';

const HighlightService = {
	getHighlightReferences: async (documentUUID: string) => {
		const blank: IHighlightReferences = {
			definitions: [],
			internalContentsReferences: [],
		};
		if (documentUUID) {
			try {
				const result = await getHighlightApi().getHighlightReferences(documentUUID);
				if (result.status === 200) {
					return result.data;
				}
				return blank;
			} catch (e) {
				console.error(e);
				return blank;
			}
		}
		return blank;
	},

	// Use axios
	toggleHighlights: async (iToggleHighlightsParams: IToggleHighlightsParams) => {
		try {
			const result = await getHighlightApi().toggleHighlights(iToggleHighlightsParams);
			if (result.status === 200) {
				return result.data;
			}
		} catch (e) {
			console.warn(e);
		}
		throw new Error('API Error - toggleHighlights');
	},
};

export default HighlightService;
