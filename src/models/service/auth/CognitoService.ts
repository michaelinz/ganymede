import Auth, { CognitoUser } from '@aws-amplify/auth';
import { OAuthOpts } from '@aws-amplify/auth/lib-esm/types';

export default class CognitoService {
	static async logIn(email: string, password: string) {
		const cognitoUser: CognitoUser = await Auth.signIn(email, password);
		return cognitoUser;
	}

	static async getCognitoIDToken() {
		const currentSession = await Auth.currentSession();
		return currentSession.getIdToken().getJwtToken();
	}

	static async refreshCognitoToken(): Promise<void> {
		// eslint-disable-next-line no-async-promise-executor
		return new Promise(async (resolve) => {
			const cognitoUser = await Auth.currentAuthenticatedUser();
			const currentSession = cognitoUser.signInUserSession;
			cognitoUser.refreshSession(currentSession.refreshToken, () => {
				resolve();
			});
		});
	}

	static async signOut() {
		try {
			await Auth.signOut();
		}
		catch(error){
			console.error(error)
		}
	}

	static initCognito(configs?) {
		const configuration = configs || getCognitoConfigs();
		Auth.configure(configuration);
	}

	static async sendPasswordRestCode(email) {
		try {
			 await Auth.forgotPassword(email);
		} catch (e) {
			console.log("sendPasswordRestCode Failed", e);
			throw e;
		}
	}

	static async changePasswordWithResetCode(email, resetCode, newPassword) {
		try {
			return await Auth.forgotPasswordSubmit(email, resetCode, newPassword);
		} catch (e) {
			console.log("changePasswordWithResetCode Failed", e);
			throw e;
		}
	}

	static async currentUserCredentials(){
			return await Auth.currentUserInfo()
	}

}

export const getCognitoConfigs = () => {
	const config: {
		Auth: any;
		oauth: OAuthOpts;
	} = {
		Auth: {
			mandatorySignIn: false,
			region: 'us-east-1',
			userPoolId: 'us-east-1_xa8wF4OTk',
			userPoolWebClientId: '7s7uo9b1k0ng1f04saqd4dg1n0', // Author App Client NO_SRP
			authenticationFlowType: 'USER_PASSWORD_AUTH',
		},
		oauth: {
			domain: `author.auth.us-east-1.amazoncognito.com`,
			scope: ['email', 'profile'],
			redirectSignIn: `${window.location.origin}?federatedSignIn`,
			redirectSignOut: `${window.location.origin}?federatedSignOut`,
			responseType: 'code',
			urlOpener: async (url: string, redirectUrl: string) => {
				const windowProxy = window.open(url, 'OAuth', 'height=640,width=960,toolbar=no,menubar=no,scrollbars=no,location=no,status=no');
				if (windowProxy) {
					return Promise.resolve(windowProxy);
				} else {
					return Promise.reject();
				}
			},
		},
	};

	return config;
};
