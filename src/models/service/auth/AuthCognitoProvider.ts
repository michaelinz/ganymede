import Auth from '@aws-amplify/auth';
// import jwt from 'jsonwebtoken';
import Cookies from 'js-cookie';

// import { sha256 } from 'js-sha256';
import {
	CognitoUser,
} from 'amazon-cognito-identity-js';
import { OAuthOpts } from '@aws-amplify/auth/lib/types';
import EditorFactory from 'src/editor/EditorFactory';

const isLocalHost = location.hostname === "localhost" || location.hostname === "127.0.0.1";

export enum CognitoUserTokenValidityState {
	Valid = "Valid",
	Invalid = "Invalid",
	Checking = "Checking",
}

export class CognitoUserTokens {
	public accessToken: string = null;
	public idToken: string = null;
	public refreshToken: string = null;

	constructor(cognitoAccessToken, cognitoRefreshToke, cognitoIDToken) {
		this.accessToken = cognitoAccessToken;
		this.refreshToken = cognitoRefreshToke;
		this.idToken = cognitoIDToken;
	}
}

export class CognitoUserData {

	public userTokens: CognitoUserTokens = null;

	public name: string = null;
	public cognitoSubjectID: string = null;
	public accessData: any = null;
	public idData: any = null;

	private tokenValid: CognitoUserTokenValidityState = null;


	/**
	 * this.idData example:
	 * aud: "ulcjbjsaqvvn5mnh17i8fctgd"
	 * auth_time: 1543376042
	 * cognito:username: "24a8668d-0929-4cf5-84b5-424afd9d1458"
	 * email: "thi+11@mccarthyfinch.com"
	 * email_verified: false
	 * event_id: "73321460-f2be-11e8-98fc-13f2cc68cf8c"
	 * exp: 1543379642
	 * iat: 1543376042
	 * iss: "https://cognito-idp.us-east-1.amazonaws.com/us-east-1_o4lduUWH8"
	 * name: "bob!!!"
	 * sub: "24a8668d-0929-4cf5-84b5-424afd9d1458"
	 * token_use: "id"
	 */
	public get valid() {
		return this.tokenValid;
	}

	constructor(private _cognitoAccessTokenResponse, private _cognitoRefreshTokeResponse, private _cognitoIDTokenResponse) {
		this.accessData = _cognitoAccessTokenResponse.payload;
		this.idData = _cognitoIDTokenResponse.payload;
		this.name = this.idData.name;
		this.cognitoSubjectID = this.idData.sub;
		this.userTokens = new CognitoUserTokens(_cognitoAccessTokenResponse.jwtToken, _cognitoRefreshTokeResponse.token, _cognitoIDTokenResponse.jwtToken);
	}

	setTokenValidity(validity: CognitoUserTokenValidityState) {
		this.tokenValid = validity;
	}
}

class AuthCognitoProvider {
	static Configuration;

	constructor() {

		if (!AuthCognitoProvider.Configuration) {
			throw new Error('You must run AuthCognitoProvider.ConfigureAuth(configs) first');
		}
	}

	// Should only be used in AuthStore
	static ConfigureAuth = (configParam, cognitoRegion, cognitoUserPoolId, cognitoUserPoolWebClientId, oauthDomain) => {
		let configuration: {
			Auth: any,
			oauth: OAuthOpts,
		} = configParam;

		if (configuration === null) {
			// // useSRP
			// // please ensure that the client ID supports non SRP flow if this is disabled
			// config.Auth.userPoolWebClientId = '1196h84cq3cdurcgsls3s7bee4'; // Author App Client
			// config.Auth.authenticationFlowType = 'USER_SRP_AUTH';
			configuration = {
				Auth: {
					mandatorySignIn: false,
					region: cognitoRegion,
					userPoolId: cognitoUserPoolId,
					userPoolWebClientId: cognitoUserPoolWebClientId, // Author App Client NO_SRP
					authenticationFlowType: 'USER_PASSWORD_AUTH',
				},
				oauth: {
					domain: oauthDomain,
					scope: ['email', 'profile'],
					redirectSignIn: `${window.location.origin}?federatedSignIn`,
					redirectSignOut: `${window.location.origin}?federatedSignOut`,
					responseType: 'code',
					urlOpener: async (url: string, redirectUrl: string) => {
						const isOfficeEditorInit = EditorFactory.instance['Editor']

						const windowProxy = isOfficeEditorInit ? isOfficeEditorInit.openPopupWindow(url) : window.open(url, 'OAuth', 'height=640,width=960,toolbar=no,menubar=no,scrollbars=no,location=no,status=no');
						if (windowProxy) {
							return Promise.resolve(windowProxy);
						} else {
							return Promise.reject();
						}
					}
				}
			}

			Auth.configure(configuration);
			AuthCognitoProvider.Configuration = configuration;
		}
	}

	// private getSessionExpiryFromToken(token): Date {
	// 	if (!token || !token.jwtToken) {
	// 		return null;
	// 	}
	// 	const decodeToken = jwt.decode(token.jwtToken);
	// 	return new Date(decodeToken.exp * 1000);
	// }

	/**
	 * Refresh current Cognito Token
	 * because cognito/amplify documentation sucks and aws cannot be trusted to do simple tasks they say they do (like refreshing a damn token)...
	 * thanks stranger on the interwebs: https://github.com/aws-amplify/amplify-js/issues/2560#issuecomment-481707972
	 */
	public async forceRefreshCognitoToken() {
		try {
			const currentSession = await Auth.currentSession();
			const cognitoUser = await Auth.currentAuthenticatedUser();
			await new Promise((resolve, reject) => {
				try {
					cognitoUser.refreshSession(currentSession.getRefreshToken(), (err, session) => {
						if (err) {
							reject(err);
						} else {
							resolve();
						}
					});
				} catch (e) {
					reject(e);
				}
			});
		} catch (e) {
			console.error("forceRefreshCognitoToken fail", e);
		}
	}

	public getCognitoRefreshedSession = async (): Promise<CognitoUserData> => {
		try {
			await this.forceRefreshCognitoToken();
			const res = await Auth.currentSession();
			if (!res) {
				return null;
			}
			//console.log("Cognito session refreshed, new expiry:", this.getSessionExpiryFromToken(res.getAccessToken()));
			return new CognitoUserData(res.getAccessToken(), res.getRefreshToken(), res.getIdToken());
		} catch (e) {
			console.log("getCognitoCurrentSession fail", e);
		}
		return null;
	};

	public getCognitoCurrentUser = async (): Promise<CognitoUser> => {
		try {
			const cognitoUser = await Auth.currentUserPoolUser();
			console.log("getCognitoCurrentUser response", cognitoUser);
			if (!cognitoUser) {
				return null;
			}
			return cognitoUser;
		} catch (e) {
			console.log("getCognitoCurrentUser fail", e);
		}
		return null;
	};

	public async resendActivation(email) {
		try {
			const res = await Auth.resendSignUp(email);
			console.log("resendSignUp response", res);
			return res;
		} catch (e) {
			console.log("resendSignUp fail", e);
			throw e;
		}
	}

	public async login(email, password) {
		try {
			const res = await Auth.signIn(email, password);
			console.log("loginCognito response", res);
			return res;
		} catch (e) {
			console.log("loginCognito fail", e);
			throw e;
		}
	}


	public async signUp(email, password, name) {
		const response = {
			success: false,
			data: null,
			message: null,
		};
		try {
			const res = await Auth.signUp({
				username: email,
				password: password,
				attributes: {
					name: name
				}
			});
			response.success = true;
			response.data = res;
			// console.log("signUpCognito response", res);
		} catch (e) {
			// console.log("signUpCognito fail", e);
			response.success = false;
			if (e && e.message) {
				response.message = e.message;
			}
			response.data = e;
			throw e;
		}
		return response;
	}

	public async signOut() {
		try {
			const res = await Auth.signOut();
			console.log("signOut response", res);
			return res;
		} catch (e) {
			console.log("signOut fail", e);
			throw e;
		}

	}

	public async sendPasswordRestCode(email) {
		try {
			return await Auth.forgotPassword(email);
		} catch (e) {
			console.log("sendPasswordRestCode Failed", e);
			throw e;
		}

	}

	public async changePasswordWithResetCode(email, resetCode, newPassword) {
		try {
			return await Auth.forgotPasswordSubmit(email, resetCode, newPassword);
		} catch (e) {
			console.log("changePasswordWithResetCode Failed", e);
			throw e;
		}

	}

	public async changePasswordLoggedInUser(oldPass, newPass) {
		try {
			const currentUser = await Auth.currentAuthenticatedUser();
			if (currentUser) {
				return await Auth.changePassword(currentUser, oldPass, newPass);
			} else {
				throw Error("currentAuthenticatedUser is null");
			}
		} catch (e) {
			console.log("changePasswordLoggedInUser Failed", e);
			throw e;
		}
	}

	/**
	 * Depricated?
	 * gets the cognito tokens saved in the cookies, then validate those with the hashes from the local session/token
	 * return null if tokens are not valid,
	 * return a token collection if the are validated
	 * @param validateCognitoAccessTokenSHA256
	 * @param validateCognitoRefreshTokenSHA256
	 */
	// getCognitoTokensFromLocalStorage(validateCognitoAccessTokenSHA256, validateCognitoRefreshTokenSHA256): CognitoUserTokens {
	// 	const cognitoAccessToken = Cookies.get("cognitoAccessToken");
	// 	const cognitoAccessTokenValid = sha256(cognitoAccessToken) === validateCognitoAccessTokenSHA256;
	// 	const cognitoRefreshToken = Cookies.get("cognitoRefreshToken");
	// 	const cognitoRefreshTokenValid = sha256(cognitoRefreshToken) === validateCognitoRefreshTokenSHA256;
	// 	const cognitoIDToken = Cookies.get("cognitoIDToken");
	// 	if (cognitoAccessToken && cognitoRefreshToken && cognitoIDToken && cognitoAccessTokenValid && cognitoRefreshTokenValid) {
	// 		return new CognitoUserTokens(cognitoAccessToken, cognitoRefreshToken, cognitoIDToken);
	// 	}
	// 	return null;
	// }
}

export default AuthCognitoProvider;
