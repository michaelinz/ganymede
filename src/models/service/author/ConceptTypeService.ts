import { ConceptTypeApiFactory } from '@mccarthyfinch/public-api-client';
import { QueryClient, useQueryClient } from 'react-query';
import getBffAPIProvider, { getBffBasePath } from 'src/models/base/getBffProvider';
import { getUseFetch, getUseOptimisticCreate, getUseOptimisticUpdate } from 'src/models/utils/reactQuery/useOptimisticMutation';

export function getConceptTypeApi() {
	return ConceptTypeApiFactory(undefined, getBffBasePath(), getBffAPIProvider().getInstance());
}

export const ConceptTypeQueryKey = 'concept-types';

const ConceptTypeService = {
	useConceptTypes: (options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		const cacheKey = [ConceptTypeQueryKey];
		return getUseFetch({
			client,
			cacheKey,
			fetch: () =>
			getConceptTypeApi()
					.getConceptTypes()
					.then(data => data.data),
			options: {
				refetchInterval: 1000 * 60,
				refetchOnMount: false,
				refetchOnWindowFocus: false,
				refetchOnReconnect: false,
			}
		});
	},

};

export default ConceptTypeService;
