import { SuperTagApiFactory } from '@mccarthyfinch/public-api-client';
import { QueryClient, useQueryClient } from 'react-query';
import getBffAPIProvider, { getBffBasePath } from 'src/models/base/getBffProvider';
import { getUseFetch, getUseOptimisticCreate, getUseOptimisticUpdate } from 'src/models/utils/reactQuery/useOptimisticMutation';

export function getSuperTagApi() {
	return SuperTagApiFactory(undefined, getBffBasePath(), getBffAPIProvider().getInstance());
}

export const SuperTagQueryKey = 'supertags';

const SuperTagService = {
	useSuperTags: (options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		const cacheKey = [SuperTagQueryKey];
		return getUseFetch({
			client,
			cacheKey,
			fetch: () =>
			getSuperTagApi()
					.getSuperTags()
					.then(data => data.data),
			options: {
				refetchInterval: 1000 * 60,
				refetchOnMount: false,
				refetchOnWindowFocus: false,
				refetchOnReconnect: false,
			}
		});
	},

};

export default SuperTagService;
