import { DocumentTypeApiFactory } from '@mccarthyfinch/public-api-client';
import { QueryClient, useQueryClient } from 'react-query';
import getBffAPIProvider, { getBffBasePath } from 'src/models/base/getBffProvider';
import { getUseFetch, getUseOptimisticCreate, getUseOptimisticUpdate } from 'src/models/utils/reactQuery/useOptimisticMutation';

export function getDocumentTypeApi() {
	return DocumentTypeApiFactory(undefined, getBffBasePath(), getBffAPIProvider().getInstance());
}

export const DocumentTypeQueryKey = 'document-types';

const DocumentTypeService = {
	useDocumentTypes: (options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		const cacheKey = [DocumentTypeQueryKey];
		return getUseFetch({
			client,
			cacheKey,
			fetch: () =>
				getDocumentTypeApi()
					.getDocumentTypes()
					.then((data) => data.data),
			options: {
				refetchInterval: 1000 * 60,
				refetchOnMount: false,
				refetchOnWindowFocus: false,
				refetchOnReconnect: false,
			},
		});
	},
};

export default DocumentTypeService;
