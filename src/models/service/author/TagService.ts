import { TagApiFactory } from '@mccarthyfinch/public-api-client';
import { QueryClient, useQueryClient } from 'react-query';
import getBffAPIProvider, { getBffBasePath } from 'src/models/base/getBffProvider';
import { getUseFetch, getUseOptimisticCreate, getUseOptimisticUpdate } from 'src/models/utils/reactQuery/useOptimisticMutation';

export function getTagApi() {
	return TagApiFactory(undefined, getBffBasePath(), getBffAPIProvider().getInstance());
}

export const TagQueryKey = 'tags';

const TagService = {
	useTags: (options?: { client?: QueryClient }) => {
		const defaultClient = useQueryClient();
		const client = options?.client || defaultClient;
		const cacheKey = [TagQueryKey];
		return getUseFetch({
			client,
			cacheKey,
			fetch: () =>
			getTagApi()
					.getTags()
					.then(data => data.data),
			options: {
				refetchInterval: 1000 * 60,
				refetchOnMount: false,
				refetchOnWindowFocus: false,
				refetchOnReconnect: false,
			}
		});
	},

};

export default TagService;
