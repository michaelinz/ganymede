import { UserPreferenceApiFactory, PreferenceCategory, PreferencePrecedentDocs } from '@mccarthyfinch/public-api-client';
import getBffAPIProvider, { getBffBasePath } from 'src/models/base/getBffProvider';

export function getUserPreferenceApi() {
	return UserPreferenceApiFactory(undefined, getBffBasePath(), getBffAPIProvider().getInstance());
}

const UserPreferenceService = {
	getPreferences: async () => {
		try {
			const result = await getUserPreferenceApi().getUserPreferenceForCategory(PreferenceCategory.PrecedentDocs);
			return result.data;
		} catch (e) {
			console.warn(e);
		}
		throw new Error('API Error - usePreferences');
	},

	updateUserPreference: async (userPreference: PreferencePrecedentDocs) => {
		try {
			const result = await getUserPreferenceApi().updateUserPreferenceForPrecedentDocs(userPreference);
			if (result.status === 200) {
				return result.data;
			}
		} catch (e) {
			console.warn(e);
		}
		throw new Error('API Error - usePreferences');
	},
};

export default UserPreferenceService;
