import React from 'react';

type RenderPanel = (state: PanelState, dispatch: Dispatch) => React.ReactNode;

type PanelState = {
	isOpen: boolean;
	renderPanel?: RenderPanel;
};

const PanelContext = React.createContext<PanelState | undefined>(undefined);

/**
 * actions to perform upon the state
 */
type Action = { type: 'open' } | { type: 'close' } | { type: 'setRenderPanel'; renderPanel: RenderPanel };
function panelReducer(state: PanelState, action: Action): PanelState {
	switch (action.type) {
		case 'open': {
			return { ...state, isOpen: true };
		}
		case 'close': {
			return { ...state, isOpen: false };
		}
		case 'setRenderPanel': {
			return { ...state, renderPanel: action.renderPanel };
		}
		default: {
			// @ts-ignore
			throw new Error(`Unhandled action type: ${action.type}`);
		}
	}
}

/**
 * dispatcher dispatches actions upon the state
 */
type Dispatch = (action: Action) => void;
const PanelDispatchContext = React.createContext<Dispatch | undefined>(undefined);

function PanelProvider({ children }: { children: React.ReactNode }) {
	const [state, dispatch] = React.useReducer(panelReducer, { isOpen: false });
	return (
		<PanelContext.Provider value={state}>
			<PanelDispatchContext.Provider value={dispatch}>{children}</PanelDispatchContext.Provider>
		</PanelContext.Provider>
	);
}

function usePanelState() {
	const context = React.useContext(PanelContext);
	if (context === undefined) {
		throw new Error('usePanelState must be used within a PanelProvider');
	}
	return context;
}

function usePanelDispatch() {
	const context = React.useContext(PanelDispatchContext);
	if (context === undefined) {
		throw new Error('usePanelDispatch must be used within a PanelProvider');
	}
	return context;
}

function usePanel(): [PanelState, Dispatch] {
	return [usePanelState(), usePanelDispatch()];
}

export { PanelProvider, usePanel };
