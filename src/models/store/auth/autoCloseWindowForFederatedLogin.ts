import { Hub } from '@aws-amplify/core';
import Auth, { CognitoUser } from '@aws-amplify/auth';

export interface AuthState {
	orgKey?: string;
	disableAutoClose?: boolean;
}

const LOGIN_POLL_MS = 500;

export function getFederatedState() {
	const urlParams = new URLSearchParams(window.location.search);
	const stateParam = urlParams.get('state');
	let state: AuthState = {};
	if (stateParam) {
		try {
			state = JSON.parse(atob(stateParam)) as AuthState;
		} catch (e) {
			console.error('Could not parse state', stateParam, e);
		}
	}
	return state;
}

export function autoCloseWindowForFederatedLogin(onClose = () => window.close()) {
	const urlParams = new URLSearchParams(window.location.search);
	const federatedSignIn = urlParams.get('federatedSignIn');
	if (federatedSignIn !== undefined && federatedSignIn !== null) {
		const state = getFederatedState();
		let signInTimeout = null;
		const onSignIn = () => {
			if (signInTimeout) {
				clearTimeout(signInTimeout);
			}
			if (!state?.disableAutoClose) {
				onClose();
			}
		};
		Hub.listen('auth', ({ payload: { event, data } }) => {
			switch (event) {
				case 'signIn':
					onSignIn();
					break;
			}
		});
		const checkLogin = async () => {
			try {
				await Auth.currentAuthenticatedUser();
				onSignIn();
			} catch (error) {
				signInTimeout = setTimeout(checkLogin, LOGIN_POLL_MS);
			}
		};
		checkLogin();
	}
}

export function autoCloseWindowForFederatedLogout(onClose = () => window.close()) {
	const urlParams = new URLSearchParams(window.location.search);
	const federatedSignOut = urlParams.get('federatedSignOut');
	if (federatedSignOut !== undefined && federatedSignOut !== null) {
		const state = getFederatedState();
		if (!state?.disableAutoClose) {
			onClose();
		}
	}
}