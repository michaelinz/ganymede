import { makeAutoObservable } from 'mobx';

export default class AppStore {
	app = 'AuthorDOCS2';

	constructor() {
		makeAutoObservable(this);
	}
}

// https://mobx-react.js.org/recipes-migration
