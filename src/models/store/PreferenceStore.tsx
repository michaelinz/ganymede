import { action, computed, makeObservable, observable, runInAction } from 'mobx';
import LocalStorage from '../utils/appState/localStorage';
import contextStoreFactory from '../utils/contextStoreFactory';

export default class PreferenceStore {
	ready: boolean = false;

	constructor() {
		makeObservable<PreferenceStore>(this, {
			ready: observable,
		});
		this.init();
	}

	private async init() {
		runInAction(() => {
			this.ready = true;
		});
	}
}

// https://mobx-react.js.org/recipes-migration
const [_usePreferenceStore, _PreferenceStoreProvider] = contextStoreFactory<PreferenceStore>(() => new PreferenceStore());
export const usePreferenceStore = _usePreferenceStore;
export const PreferenceStoreProvider = _PreferenceStoreProvider;
