import { makeAutoObservable } from 'mobx';
import contextStoreFactory from '../utils/contextStoreFactory';

export default class PanelStore {
	isOpen = false;
	renderPanel: (props: { isOpen: boolean }) => JSX.Element;

	constructor() {
		makeAutoObservable(this);
	}

	close() {
		this.isOpen = false;
	}

	open() {
		this.isOpen = true;
	}

	setRenderPanel(renderPanel: (props: { isOpen: boolean }) => JSX.Element) {
		this.renderPanel = renderPanel;
	}
}

const [_usePanelStore, _PanelStoreProvider] = contextStoreFactory<PanelStore>(() => new PanelStore());
export const usePanelStore = _usePanelStore;
export const PanelStoreProvider = _PanelStoreProvider;
