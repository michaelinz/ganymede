import { AuthApiFactory, ISession } from '@mccarthyfinch/public-api-client';
import { action, computed, makeObservable, observable, runInAction } from 'mobx';
import getBffAPIProvider, { getBffBasePath } from '../base/getBffProvider';
import CognitoService from '../service/auth/CognitoService';
import contextStoreFactory from '../utils/contextStoreFactory';
import { clearStorage } from '../utils/reactQuery/localStorage';
import FeatureFlags from '../../config/FeatureFlags';

import AuthCognitoProvider from 'src/models/service/auth/AuthCognitoProvider'


const SESSION_KEY = 'auth-session';
const REFRESH_AUTH_SESSION_INTERVAL_MINUTES = 10;

export function getAuthApi() {
	return AuthApiFactory(undefined, getBffBasePath(), getBffAPIProvider().getInstance());
}

export enum LoginStatus {
	NotLoggedIn = 'NotLoggedIn',
	LoggedIn = 'LoggedIn',
	WaitingForOrganisation = 'WaitingForOrganisation'
}

export type AuthSubscriber = (loginStatus: LoginStatus) => Promise<void>;

const COGNITO_ID_TOKEN = 'cognitoIDToken';
const AUTH_TOKEN = 'authToken'; // JWT version of SESSION_KEY

/**
 * Add cognitoIDToken to localStorage
 * If no cognitoIDToken is provided then remove cognitoIDToken from localStorage
 * @param cognitoIDToken
 */
function updateCognitoIDTokenToCache(cognitoIDToken: string) {
	if (cognitoIDToken) {
		return localStorage.setItem(COGNITO_ID_TOKEN, cognitoIDToken);
	}
	return localStorage.removeItem(COGNITO_ID_TOKEN);
}


/**
 * Add cognitoIDToken to localStorage
 * If no cognitoIDToken is provided then remove cognitoIDToken from localStorage
 * @param cognitoIDToken
 */
function updateAuthTokenToCache(authToken: string) {
	if (authToken) {
		return localStorage.setItem(AUTH_TOKEN, authToken);
	}
	return localStorage.removeItem(AUTH_TOKEN);
}


export function getCognitoIDTokenFromCache() {
	return localStorage.getItem(COGNITO_ID_TOKEN);
}

export function getAuthTokenFromCache() {
	return localStorage.getItem(AUTH_TOKEN);
}

export default class AuthStore {
	ready = false;

	private session: ISession = null;
	private cognitoIDToken: string = null;

	private subscribers: AuthSubscriber[] = [];


	constructor() {
		// Add session and cognitoIDToken to the type since they are private variables
		makeObservable<AuthStore, 'session' | 'cognitoIDToken'>(this, {
			ready: observable,
			session: observable,
			cognitoIDToken: observable,
			loginStatus: computed,
			user: computed,
			logInAuthor: action,
			logInCognito: action,
			logOut: action
		});
		this.init();
	}

	get user() {
		return this.session.user;
	}

	private async initSession() {
		const getSessionFromCache = () => {
			const session = localStorage.getItem(SESSION_KEY);
			if (session) {
				try {
					return JSON.parse(session) as ISession;
				} catch (e) {
					console.warn(e);
				}
			}
			return null;
		};
		const getSessionFromServer = async () => {
			try {
				const authAPI = getAuthApi();
				const result = await authAPI.getCurrentSession();
				if (!FeatureFlags.AUTH_USE_COOKIES) {
					updateAuthTokenToCache(result.headers['x-auth-token']);
				}
				if (result.status === 200) {
					return result.data;
				}
			} catch (e) {
				console.warn(e);
			}
			return null;
		};

		const cachedSession = getSessionFromCache();
		if (!cachedSession) {
			return null;
		}
		const serverSession = await getSessionFromServer();
		if (!serverSession) {
			return null;
		}
		if (cachedSession.user.uuid === serverSession.user.uuid && cachedSession.organisation.uuid === serverSession.organisation.uuid) {
			this.setSession(serverSession);
			return serverSession;
		}
		// Cached session is not the same as the server session so we should logout
		await this.logOut();
		return null;
	}

	private async init() {
		CognitoService.initCognito();
		await this.updateCognitoIDToken();
		await this.initSession();
		runInAction(() => {
			this.ready = true;
		});
		if (this.loginStatus === LoginStatus.LoggedIn) {
			this.onLoginAuthor();
		}
	}

	private refreshInterval: number;
	private onLoginAuthor() {
		this.refreshInterval = setInterval(() => this.refreshToken(), REFRESH_AUTH_SESSION_INTERVAL_MINUTES * 60 * 1000);
		this.subscribers.forEach((subscriber) => {
			subscriber(this.loginStatus);
		});
	}

	get loginStatus() {
		if (!this.ready) {
			return;
		}
		const isLoggedIntoCognito = Boolean(this.cognitoIDToken);
		const isLoggedIntoAuthor = Boolean(this.session);

		if (!isLoggedIntoCognito) {
			// We aren't logged into cognito so we should be logged out
			if (isLoggedIntoAuthor) {
				try {
					console.warn('Logged into author but not cognito, forcing log out');
					this.logOut();
				} catch (e) {
					console.error(e);
				}
			}
			return LoginStatus.NotLoggedIn;
		}

		// Logged into cognito and author
		if (isLoggedIntoAuthor) {
			return LoginStatus.LoggedIn;
		}

		// Logged into cognito but not author
		return LoginStatus.WaitingForOrganisation;
	}

	async logInCognito(email: string, password: string) {
		await CognitoService.logIn(email, password);
		await this.updateCognitoIDToken();
	}

	async logInAuthor(orgKey: string, app = 'authorDOCS2') {
		const login = async () => {
			try {
				const result = await getAuthApi().login({
					cognitoIDToken,
					orgKey,
					app,
				});
				if (!FeatureFlags.AUTH_USE_COOKIES) {
					updateAuthTokenToCache(result.headers['x-auth-token']);
				}
				if (result.status === 200) {
					return result.data;
				}
			} catch (e) {
				console.warn(e);
			}
			throw new Error('Could not log in.');
		};
		const cognitoIDToken = await CognitoService.getCognitoIDToken();
		this.setSession(await login());
		this.onLoginAuthor();
		return this.session;
	}

	async logOut() {
		await Promise.all([
			CognitoService.signOut(),
			getAuthApi()
				.logout()
				.then((result) => result.data),
		]);
		this.setSession(null);
		this.updateCognitoIDToken();
		if (this.refreshInterval) {
			clearInterval(this.refreshInterval);
			this.refreshInterval = null;
		}
		clearStorage();
	}

	private async updateCognitoIDToken() {
		let cognitoIDToken: string = null;
		try {
			cognitoIDToken = await CognitoService.getCognitoIDToken();
		} catch (e) {
			// Do nothing
		}
		runInAction(() => {
			this.cognitoIDToken = cognitoIDToken;
		});
		updateCognitoIDTokenToCache(cognitoIDToken);
	}

	private async setSession(session: ISession) {
		if (session) {
			localStorage.setItem(SESSION_KEY, JSON.stringify(session));
		} else {
			localStorage.removeItem(SESSION_KEY);
		}
		runInAction(() => {
			this.session = session;
		});
	}

	private async refreshToken() {
		await CognitoService.refreshCognitoToken();
		await this.updateCognitoIDToken();
		if (!this.cognitoIDToken) {
			console.log('Could not find cognitoIDToken, logging out');
			this.logOut();
		}
	}

	OrganisationSelector = {
		async getOrganisations() {
			const cognitoIDToken = await CognitoService.getCognitoIDToken();
			const result = await getAuthApi().getOrganisations({ cognitoIDToken });
			return result.data;
		},

		async getInvitations() {
			const cognitoIDToken = await CognitoService.getCognitoIDToken();
			const result = await getAuthApi().getInvitations({ cognitoIDToken });
			return result.data;
		},

		async acceptInvitation(invitationUUID: string) {
			const cognitoIDToken = await CognitoService.getCognitoIDToken();
			const result = await getAuthApi().acceptInvitation(invitationUUID, { cognitoIDToken });
			return result.data;
		},

		async rejectInvitation(invitationUUID: string) {
			const cognitoIDToken = await CognitoService.getCognitoIDToken();
			const result = await getAuthApi().rejectInvitation(invitationUUID, { cognitoIDToken });
			return result.data;
		},
	};

	async tokenSetUp (){
		return await this.updateCognitoIDToken()
	}

	async forgotPassword(email){
		return await CognitoService.sendPasswordRestCode(email);
	}

	async forgotPasswordChangePassword (email, resetCode, newPassword) {
		return await CognitoService.changePasswordWithResetCode(email, resetCode, newPassword);
	};

	async currentUserCredentials(){
		return await CognitoService.currentUserCredentials()
	}



}

const authStore = new AuthStore();
const [_useAuthStore, _AuthStoreProvider] = contextStoreFactory<AuthStore>(() => authStore);
export const useAuthStore = _useAuthStore;
export const AuthStoreProvider = _AuthStoreProvider;
