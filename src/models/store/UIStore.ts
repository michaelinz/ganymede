import { action, computed, makeObservable, observable, runInAction } from 'mobx';
import LocalStorage from '../utils/appState/localStorage';
import contextStoreFactory from '../utils/contextStoreFactory';

export enum QuickFilter {
	TODO = 'TODO',
	MINE = 'MINE',
	IMPORTANT = 'IMPORTANT',
	DONE = 'DONE',
}

export interface Filters {
	quick: QuickFilter[];
}

export const defaultFilterState: Filters = {
	quick: [],
};

const FILTERS_CACHE_KEY = 'filters';

/**
 * TODOs
 * - change this to CHECKLIST ui store
 * - use context and hooks instead of mobx
 *
 */

export default class UIStore {
	ready: boolean = false;

	constructor() {
		makeObservable<UIStore>(this, {
			ready: observable,
			...this._observableFilters,
		});
		this.init();
	}

	private async init() {
		await this._loadFilters();

		runInAction(() => {
			this.ready = true;
		});
	}


	/** FILTERS */
	// Explicity initialised to undefined because mobx won't allow you to decorate non existent properties and properties without
	// initialisation are stripped from classes at compilation :( the typescript-based fix described below did not seem to work
	// See points 4 and 5 from "Getting Started": https://mobx.js.org/migrating-from-4-or-5.html#getting-started
	private _filters: Filters = undefined;

	get filters() {
		return this._filters;
	}

	setFilters = (filters: Filters, doNotCache?: boolean) => {
		this._filters = filters;
		if (!doNotCache) {
			this._cacheFilters(filters);
		}
	}

	private _cacheFilters(filters: Filters) {
		LocalStorage.setAll(FILTERS_CACHE_KEY, filters);
	}

	private async _loadFilters() {
		const filters = await LocalStorage.getAll<Filters>(FILTERS_CACHE_KEY);
		if (Object.keys(filters).length > 0) {
			// No need to cache, we just got the data from there
			this.setFilters(filters, true);
		} else {
			this.setFilters(defaultFilterState);
		}
	}

	private _observableFilters = {
		_filters: observable,
		filters: computed,
		setFilters: action,
	};
}

const uiStore = new UIStore();
const [_useUIStore, _UIStoreProvider] = contextStoreFactory<UIStore>(() => uiStore);
export const useUIStore = _useUIStore;
export const UIStoreProvider = _UIStoreProvider;
