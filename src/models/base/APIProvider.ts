import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import { getAuthTokenFromCache, getCognitoIDTokenFromCache } from '../store/AuthStore';

class APIInterceptionObservable<T> {
	observers: any[] = [];
	// add the ability to subscribe to a new object / DOM element
	// essentially, add something to the observers array
	subscribe(subscribeFunction: (payload: T) => Promise<T | any>) {
		this.observers.push(subscribeFunction);
	}

	// add the ability to unsubscribe from a particular object
	// essentially, remove something from the observers array
	unsubscribe(unSubscribeFunction) {
		this.observers = this.observers.filter((subscriber) => subscriber !== unSubscribeFunction);
	}

	// update all subscribed objects / DOM elements
	// and pass some data to each of them
	// notify(data) {
	//   this.observers.forEach(observer => observer(data));
	// }
}

export default class APIProvider {
	public readonly requestInterceptors: APIInterceptionObservable<AxiosRequestConfig> = new APIInterceptionObservable();
	public readonly responseInterceptors: APIInterceptionObservable<AxiosResponse> = new APIInterceptionObservable();
	private readonly axiosInstance: AxiosInstance = null;

	constructor(private readonly apiBaseURL: string) {
		this.axiosInstance = axios.create({
			baseURL: this.apiBaseURL,
			headers: { 'Content-Type': 'application/json' },
			withCredentials: true,
		});
		this.requestInterceptors.subscribe(this.cognitoIDTokenInterceptor);
		this.requestInterceptors.subscribe(this.authTokenInterceptor);
		this.initInterceptors(this.axiosInstance);
	}

	public getInstance = () => {
		return this.axiosInstance;
	};

	private initInterceptors = (axiosInstance: AxiosInstance) => {
		axiosInstance.interceptors.request.use(
			async (originalConfig: AxiosRequestConfig) => {
				for (const interceptor of this.requestInterceptors.observers) {
					try {
						originalConfig = await interceptor(originalConfig);
					} catch (e) {
						console.error('APIProvider request intercept execution fail', e);
						// move on...
					}
				}
				return originalConfig;
			},
			(error) => Promise.reject(error),
		);

		axiosInstance.interceptors.response.use(
			async (response: AxiosResponse) => {
				for (const interceptor of this.responseInterceptors.observers) {
					try {
						response = await interceptor(response);
					} catch (e) {
						console.error('APIProvider response intercept execution fail', e);
						// move on...
					}
				}
				return response;
			},
			(error) => Promise.reject(error),
		);
	};

	private cognitoIDTokenInterceptor = async (config: AxiosRequestConfig): Promise<AxiosRequestConfig> => {
		const cognitoIDToken = getCognitoIDTokenFromCache();
		if (cognitoIDToken) {
			config.headers.common['x-cognito-id-token'] = cognitoIDToken;
		}
		return config;
	};

	private authTokenInterceptor = async (config: AxiosRequestConfig): Promise<AxiosRequestConfig> => {
		const authToken = getAuthTokenFromCache();
		if (authToken) {
			config.headers.common['x-auth-token'] = authToken;
		}
		return config;
	};
}
