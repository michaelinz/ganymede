import APIProvider from './APIProvider';
import AppConfigs from '../../config/AppConfigs';

export function getBffBasePath() {
	return AppConfigs.API_URL_BASE;
}

const BffAPIProvider = new APIProvider(getBffBasePath());

// BffAPIProvider.setDefaultOrgKey(AppConfigs.ORG_KEY);
// BffAPIProvider.setDefaultUserUUID(AppConfigs.USER_UUID);

export default function getBffAPIProvider() {
	return BffAPIProvider;
}
