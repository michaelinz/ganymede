import { AxiosInstance } from 'axios';

export default abstract class Provider {
	static API: AxiosInstance;
}
