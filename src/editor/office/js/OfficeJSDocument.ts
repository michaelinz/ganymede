import EditorDocument from '../../base/EditorDocument';

const DEFAULT_DOCUMENT_TITLE = 'Document';

export default class OfficeJSDocument extends EditorDocument {
	private getSlice(file: Office.File, sliceIndex: number): Promise<Office.AsyncResult<Office.Slice>> {
		return new Promise<Office.AsyncResult<Office.Slice>>((resolve) => {
			file.getSliceAsync(sliceIndex, (result) => resolve(result));
		});
	}

	private closeFile = (file: Office.File): Promise<void> => {
		return new Promise<void>((resolve) => {
			file.closeAsync(() => resolve());
		});
	};

	private getFile(fileType: Office.FileType, options?: any): Promise<Office.AsyncResult<Office.File>> {
		return new Promise<Office.AsyncResult<Office.File>>((resolve) => {
			Office.context.document.getFileAsync(fileType, options, (result) => resolve(result));
		});
	}
	async getDocumentBinary() {
		try {
			const fileResult = await this.getFile(Office.FileType.Compressed, null);
			const status = fileResult.status;
			if (status !== Office.AsyncResultStatus.Succeeded) {
				const message = 'Failed reading document file ' + JSON.stringify(fileResult);
				throw new Error(message);
			}

			const file = fileResult.value as Office.File;
			const totalDocumentSlices = file.sliceCount;
			let currentDocumentSlice = 0;
			let documentSlices = [];

			while (currentDocumentSlice !== totalDocumentSlices) {
				const sliceResult = await this.getSlice(file, currentDocumentSlice);

				if (sliceResult.status !== Office.AsyncResultStatus.Succeeded) {
					const message = 'Failed reading document file slice ' + JSON.stringify(sliceResult);
					throw new Error(message);
				}

				documentSlices = documentSlices.concat(sliceResult.value.data);

				currentDocumentSlice += 1;
			}
			const documentBlob = new Blob([Uint8Array.from(documentSlices)], { type: 'application/octet-stream' });

			await this.closeFile(file);

			return documentBlob;
		} catch (e) {
			throw new Error(e);
		}
	}

	async getDocumentFilename(): Promise<string> {
		let title = DEFAULT_DOCUMENT_TITLE;
		const url = Office.context.document.url;
		if (url) {
			const paths = url.split(/[\\\/]/);
			if (paths.length) {
				title = paths[paths.length - 1];
			}
		}
		title = title.replace(/%20/g, ' ').trim();
		return title;
	}

	async loadBase64ToDocument(base64File: string) {
		return Word.run(async (context) => {
			await context.document.body.insertFileFromBase64(base64File, Word.InsertLocation.replace);
			await context.sync();
		});
	}

	/**
	 * Save the file
	 */
	saveDocument = async (): Promise<void> => {
		await Word.run(async (context) => {
			await context.sync();
			context.document.save();
			await context.sync();
		});
		return;
	};

	onDocumentSelectionChange(callback: () => void, removeHandler = false): void {
		if (removeHandler) {
			Office.context.document.removeHandlerAsync(
				Office.EventType.DocumentSelectionChanged,
				callback
			);
			return;
		}

		Office.context.document.addHandlerAsync(
			Office.EventType.DocumentSelectionChanged,
			callback,
			null
		);
	}
}
