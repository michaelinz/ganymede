export enum ClassName {
	DefinitionAlert = "mcf-class-definition-alert",
	InternalDictionaryDefinition = "mcf-class-internal-dictionary-definition",
	InternalContentsReference = "mcf-class-internal-contents-reference"
}
