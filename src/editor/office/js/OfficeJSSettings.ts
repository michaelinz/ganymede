import EditorSettings from '../../base/EditorSettings';
import { getCustomProperty } from './utils/CustomXML';
export default class OfficeJSSettings extends EditorSettings {
	protected async getProperty(key: string): Promise<any> {
		try {
			return Office.context.document.settings.get(key);
		} catch (e) {
			return null;
		}
	}

	protected async setProperty(key: string, value: any): Promise<void> {
		Office.context.document.settings.set(key, value);
		await Office.context.document.settings.saveAsync();
		return;
	}

	protected async removeProperty(key: string): Promise<void> {
		Office.context.document.settings.remove(key);
		await Office.context.document.settings.saveAsync();
		return;
	}

	public async setDocumentForAutoShowTaskPane() {
		try {
			Office.context.document.settings.set('Office.AutoShowTaskpaneWithDocument', true);
			Office.context.document.settings.saveAsync();
		} catch (e) {
			console.log(e);
		}
	}

	/**
	 * Approve - custom xml
	 */
	public async getLastProcessedTimeApprove(): Promise<number> {
		const unixTime = await getCustomProperty('LastProcessedByApprove');
		if (unixTime) {
			return parseInt(unixTime, 10);
		}
		return undefined;
	}

	public async getLastProcessedDocumentUUIDApprove(): Promise<string> {
		return getCustomProperty('LastRunApproveDocumentUUID');
	}

}
