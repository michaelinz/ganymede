import EditorSelection, { IEditorParagraph } from '../../base/EditorSelection';
import * as OfficeJSSelectionHelper from './OfficeJSSelectionHelper';

export default class OfficeJSSelection extends EditorSelection {
	async getSelectionHtml(): Promise<string> {
		const data = await Word.run(async (context) => {
			const selection = context.document.getSelection();
			const html = selection.getHtml();

			await context.sync();
			return html.value;
		});
		return data;
	}

	async getSelectionParagraphs() {
		const data = await Word.run(async (context) => {
			const paragraphs = context.document.getSelection().paragraphs;
			context.load(paragraphs);
			await context.sync();
			return paragraphs.items.map((paragraph, i) => ({
				text: paragraph.text,
				index: i,
			}));
		});
		return data;
	}

	async insertHtmlAtSelection(html: string): Promise<void> {
		await Word.run(async (context) => {
			const selection = context.document.getSelection();
			selection.insertHtml(html, Word.InsertLocation.replace);
			return context.sync();
		});
		return;
	}

	async insertTextAtSelection(text: string): Promise<void> {
		await Word.run(async (context) => {
			const selection = context.document.getSelection();
			selection.insertText(text, Word.InsertLocation.replace);
			return context.sync();
		});
		return;
	}

	async selectContentControl(tag: string): Promise<boolean> {
		return Word.run(async (context) => {
			const cc = context.document.contentControls.getByTag(tag).getFirstOrNullObject();
			await context.sync();
			if (cc && !cc.isNullObject) {
				cc.select(Word.SelectionMode.select);
				return true;
			}
			return false;
		});
	}

	async selectParagraphs(paragraphs: IEditorParagraph[]): Promise<boolean> {
		return Word.run(async (context) => {
			return OfficeJSSelectionHelper.selectParagraphs(context, paragraphs);
		});
	}

	async selectTextInParagraphs(text: string, paragraphs: IEditorParagraph[]): Promise<boolean> {
		return Word.run(async (context) => {
			return OfficeJSSelectionHelper.selectTextInParagraphs(context, text, paragraphs);
		});
	}

}
