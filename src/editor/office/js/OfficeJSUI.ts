import getOptimizedWindowSize from '../../../utils/getOptimizedWindowSize';
import EditorUI from '../../base/EditorUI';

export default class OfficeJSUI extends EditorUI {
	async openPopupWindow(startAddress: string) {
		const size = getOptimizedWindowSize(1024, 768);
		try {
			Office.context.ui.displayDialogAsync(startAddress, { width: size.widthPercentage, height: size.heightPercentage }, (asyncResult) => {
				const dialog = asyncResult.value;
				dialog.addEventHandler(Office.EventType.DialogMessageReceived, (dialogEvent) => {
					const event = dialogEvent as { message: string };
					let message;
					try {
						message = JSON.parse(event?.message);
					} catch (e) {
						message = event.message;
					}
					if (message?.close) {
						dialog.close();
					}
				});
			});
		} catch (e) {
			console.error(e);
			window.open(
				startAddress,
				'AuthorDOCS',
				`width=${size.widthPercentage},height=${size.heightPercentage},menubar=no,toolbar=no,location=no,resizable=yes,scrollbars=yes,status=no`,
			);
		}
	}

	async closeThisPopupWindow() {
		try {
			Office.context.ui.messageParent(JSON.stringify({ close: true }));
		} catch (e) {
			console.error(e);
		}
		window.close();
	}
}
