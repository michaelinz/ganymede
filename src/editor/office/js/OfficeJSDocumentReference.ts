import EditorDocumentReference from '../../base/EditorDocumentReference';
import { DocumentReferenceType, IDocumentReference } from '@mccarthyfinch/public-api-client';
import * as OfficeJSSelectionHelper from './OfficeJSSelectionHelper';
import { IEditorParagraph } from 'src/editor/base/EditorSelection';

export default class OfficeJSDocumentReference extends EditorDocumentReference {
	async createDocRefFromSelection(tag: string) {
		await Word.run(async (context) => {
			const selection = context.document.getSelection();
			const contentControl = selection.insertContentControl();
			contentControl.set({
				tag,
			});
			await context.sync();
		});
	}

	async goToDocRef(docRef: IDocumentReference) {
		console.log('goToDocRef', docRef.uuid);
		console.log(docRef);
		await Word.run(async (context) => {
			let cc: Word.ContentControl;
			if (docRef.tag) {
				cc = context.document.contentControls.getByTag(docRef.tag).getFirstOrNullObject();
				await context.sync();
			}
			if (cc && !cc.isNullObject) {
				cc.select(Word.SelectionMode.select);
			} else if (docRef.content?.plainText && docRef.content.plainText !== null) {
				// const text = docRef.content.plainText.split('\n');
				if (docRef.relatedLabel === 'Concept') {
					return OfficeJSSelectionHelper.selectParagraphs(context, docRef.paragraphs);
				}
				if ([DocumentReferenceType.ConceptSearch, DocumentReferenceType.DetailSearch, DocumentReferenceType.DocumentSearch].includes(docRef.type)) {
					return OfficeJSSelectionHelper.selectParagraphs(context, docRef.paragraphs);
				}

				const text = docRef.content?.plainText ||  docRef.paragraphs.map(p => p.text).join('\n');
				return OfficeJSSelectionHelper.selectTextInParagraphs(context, text, docRef.paragraphs);
			}
		});
	}

	async goToDefinition(text: string) {
		console.log('goToDefinition');
		const paras = [];
		// Strip off all CRLN - we are interested only in the first para
		paras.push(text.match(/[^\r\n]+/g)[0]);
		await Word.run(async (context) => {
			const indexedParas = paras.map((txt, index) => ({
				index,
				text: txt,
			} as IEditorParagraph));

			return OfficeJSSelectionHelper.selectParagraphs(context, indexedParas);
		});
	}

	async goToClause(text: string[]) {
		console.log('goToClause');
		await Word.run(async (context) => {
			const indexedParas = text.map((txt, index) => ({
				index,
				text: txt,
			} as IEditorParagraph));

			return OfficeJSSelectionHelper.selectParagraphs(context, indexedParas);
		});
	}

	async deleteDocRef(tag: string) {
		await Word.run(async (context) => {
			const cc = context.document.contentControls.getByTag(tag).getFirstOrNullObject();
			await context.sync();
			if (cc && !cc.isNullObject) {
				cc.delete(true);
			}
		});
	}

}
