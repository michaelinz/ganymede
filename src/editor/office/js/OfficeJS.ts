import Editor from '../../base/Editor';
import OfficeJSDocument from './OfficeJSDocument';
import OfficeJSSettings from './OfficeJSSettings';
import OfficeJSUI from './OfficeJSUI';
import OfficeJSSelection from './OfficeJSSelection';
import OfficeJSDocumentReference from './OfficeJSDocumentReference';
import OfficeJSHighlights from './OfficeJSHighlights';

export default class OfficeJS extends Editor {
	Document = new OfficeJSDocument();
	Settings = new OfficeJSSettings();
	Editor = new OfficeJSUI();
	Selection = new OfficeJSSelection();
	DocumentReference = new OfficeJSDocumentReference();
	Highlights = OfficeJSHighlights;

	async onLoad() {
		this.notifySubscribers('onLoad');
	}
}
