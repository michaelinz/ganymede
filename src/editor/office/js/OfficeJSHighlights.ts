import { IHighlightReferences, IHighlightDefinition, IHighlightInternalContentReference, IHighlights } from '@mccarthyfinch/public-api-client';
import HighlightService from 'src/models/service/highlightService/HighlightService';
import EditorHighlights, { ISelectedReferences, TrackChangeStatus } from '../../base/EditorHighlights';
import { ClassName } from './ClassName';
import { LoadOption } from "./OfficeJSBaseActions";
import OfficeJSDocument from './OfficeJSDocument';
import { isLatestWord, isTextSimilar, isRunningWordOnline } from './utils';

const OfficeJSHighlights: EditorHighlights = {

	async toggleHighlighting(documentUUID: string, highlightSettings: IHighlights): Promise<{ success: boolean, trackedChangesStatus: TrackChangeStatus }> {
		if (isRunningWordOnline()) {
			console.warn('Highlights disabled due to being online');
			// Highlights are disabled for word online due to performance issues
			return { success: false, trackedChangesStatus: TrackChangeStatus.UNKNOWN };
		} else {
			const officeJSDocument = new OfficeJSDocument();
			const currentDocumentBinary = await officeJSDocument.getDocumentBinary(); // TODO - port this to a util?
			const styledDocument = await getDocumentMarkupXml(currentDocumentBinary, documentUUID, highlightSettings);

			if (styledDocument.trackedChangesEnabled) {
				console.log("Skipping Highlights XML refresh as tracked changes are enabled."); // This will destroy the document
				return {
					success: false, trackedChangesStatus: TrackChangeStatus.ENABLED,
				};
			} else {
				await _loadOOXMLToDocument(styledDocument.xml);
				return {
					success: true, trackedChangesStatus: TrackChangeStatus.DISABLED,
				};
			}
		}
	},

	async isClickAction(): Promise<boolean> {
		const result = await Word.run(async (context) => {
			const selection = context.document.getSelection().load([LoadOption.Load, LoadOption.Text]);
			await context.sync();
			if (selection.text.length > 1) {
				return false;
			}
			return true;
		});
		return result as boolean;
	},

	async getSelectedReferences(definitionClauseReferences: IHighlightReferences): Promise<ISelectedReferences> {
		if (!definitionClauseReferences || (definitionClauseReferences.definitions.length === 0 && definitionClauseReferences.internalContentsReferences.length === 0)) {
			console.warn('no definitionClauseReferences');
			return {
				definitions: [],
				internalContentReferences: [],
			};
		}
		return await Word.run(async (context) => {
			const selection = context.document.getSelection().load([LoadOption.Load, LoadOption.Text]);
			const cc = selection.contentControls.load([LoadOption.Tag, LoadOption.Text]);
			await context.sync();

			// get the content controls around the selection
			const contentControls: Word.ContentControl[] = [];
			contentControls.push(...cc.items);
			try {
				const parentContentControl = selection.parentContentControl.load([LoadOption.Tag, LoadOption.Text]);
				await context.sync();
				if (parentContentControl) {
					contentControls.push(parentContentControl);
				}
			} catch (err) {
				console.warn(err);
				// not much we can do...
			}

			// get the paragraph around the selection
			const paragraphs = selection.paragraphs.load(LoadOption.Text);
			await context.sync();
			const paragraphsItems = paragraphs.items;
			const selectedParagraph = paragraphs && paragraphsItems.length
				? selection.paragraphs.items[0].load([LoadOption.Text])
				: null;

			await context.sync();

			const selectedItems: ISelectedReferences = {
				definitions: [],
				internalContentReferences: [],
			};
			const selectedDefinitions = _findDefinitionsFromContentControls(definitionClauseReferences?.definitions, contentControls);
			if (selectedDefinitions?.length) {
				selectedItems.definitions.push(...selectedDefinitions);
			}

			const selectedInternalContentReferences = _findInternalContentReferancesFromContentControls(definitionClauseReferences.internalContentsReferences, contentControls);
			if (selectedInternalContentReferences?.length) {
				selectedItems.internalContentReferences.push(...selectedInternalContentReferences);
			}
			const selectedItemsCount = selectedItems.definitions.length + selectedItems.internalContentReferences.length;
			if (selectedItemsCount) {
				return selectedItems;
			}
			// fallback logic for finding selected items based on string comparison
			if (!selectedItemsCount) {
				const isLatestWordVersion = isLatestWord();
				const options = isLatestWordVersion ? 'text, isListItem, listItemOrNullObject/listString' : 'text';
				const docParagraphs = context.document.body.paragraphs.load(options);
				await context.sync();

				const stringMatchSelectedItems = await _findSelectedItemsBasedOnStringSearch(context, selection, selectedParagraph, docParagraphs.items, definitionClauseReferences.definitions, definitionClauseReferences.internalContentsReferences);
				if (stringMatchSelectedItems) {
					if (stringMatchSelectedItems.selectedDefinitions.length) {
						stringMatchSelectedItems.selectedDefinitions.forEach(selectedDefinition => {
							if (!selectedItems.definitions.find(definition => definition.uuid === selectedDefinition.uuid)) {
								selectedItems.definitions.push(selectedDefinition);
							}
						});
					}
					if (stringMatchSelectedItems.selectedInternalContentReferences.length) {
						stringMatchSelectedItems.selectedInternalContentReferences.forEach(i => {
							if (!selectedItems.internalContentReferences.find(s => s.uuid === i.uuid)) {
								selectedItems.internalContentReferences.push(i);
							}
						});
					}
				}
			}
			return selectedItems;
		});
	},


};

export default OfficeJSHighlights;

export enum TagIdPrefix {
	BlockUuid = "block-uuid",
	Reference = "reference-uuid"
}

async function getContentControlTagsAtSelection() {
	return Word.run(async context => {
		const selection = context.document.getSelection().load([LoadOption.Load, LoadOption.Text]);
		const cc = selection.contentControls.load([LoadOption.Tag, LoadOption.Text]);
		await context.sync();

		// get the content controls around the selection
		const contentControls: Word.ContentControl[] = [];
		contentControls.push(...cc.items);
		try {
			const parentContentControl = selection.parentContentControl.load([LoadOption.Tag, LoadOption.Text]);
			await context.sync();
			if (parentContentControl) {
				contentControls.push(parentContentControl);
			}
		} catch (err) {
			console.warn(err);
			// not much we can do...
		}
		const tags: string[] = [];
		for (const contentControl of contentControls) {
			if ((!contentControl) || (!contentControl.tag)) {
				return null;
			}
			tags.push(...contentControl.tag.split(","));
		}
		return tags;
	});
}

async function getDocumentMarkupXml(currentDocumentBinary: Blob, documentUuid: string, highlightsSettings: IHighlights): Promise<any> {
	console.log('currentDocumentBinary.size', currentDocumentBinary.size);
	console.log(`getting XML markup from API`);

	const wordDocBinaryBase64 = await convertBinaryToBase64(currentDocumentBinary);

	const payload = {
		documentUUID: documentUuid,
		wordDocBinaryBase64,
		highlights: highlightsSettings,
	};

	const response = await HighlightService.toggleHighlights(payload);
	const xml = response.xml as string;
	if (!xml) {
		throw Error("Invalid xml returned " + xml);
	}
	console.log('markup returned, markup length', xml.length);
	const trackedChangesEnabled = response.trackedChangesEnabled;

	return { xml, trackedChangesEnabled };
}

async function convertBinaryToBase64(binaryDocument: Blob) {
	return new Promise<string>((resolve, reject) => {
		const reader = new FileReader();
		reader.onload = () => {
			const dataUrl: any = reader.result;
			const base64Document = dataUrl.split(',')[1];
			resolve(base64Document);
		};

		reader.readAsDataURL(binaryDocument);
	});
}

async function _loadOOXMLToDocument(xml: string): Promise<any> {
	return Word.run(async (context) => {
		// const beforeInsertTime = new Date();
		await context.document.body.insertOoxml(xml, Word.InsertLocation.replace);
		await context.sync();
	});
}




function _findDefinitionsFromContentControls(allDefinitions: IHighlightDefinition[], contentControls: Array<Word.ContentControl>): IHighlightDefinition[] {
	const definitionUUIDs: string[] = [];

	for (const contentControl of contentControls) {
		const dictionaryId = _getResourceUUIDsFromContentControls(contentControl, ClassName.InternalDictionaryDefinition);
		if (dictionaryId !== null) {
			definitionUUIDs.push(dictionaryId);
		}
	}
	return allDefinitions.filter(d => definitionUUIDs.includes(d.uuid));
}

function _findInternalContentReferancesFromContentControls(allICRs: IHighlightInternalContentReference[], contentControls: Array<Word.ContentControl>): IHighlightInternalContentReference[] {
	const internalContentsReferenceUUIDs: string[] = [];
	for (const contentControl of contentControls) {
		const internalContentsReferenceId = _getResourceUUIDsFromContentControls(contentControl, ClassName.InternalContentsReference);
		if (internalContentsReferenceId !== null) {
			internalContentsReferenceUUIDs.push(internalContentsReferenceId);
		}
	}
	return allICRs.filter(d => internalContentsReferenceUUIDs.includes(d.uuid));
}

async function _findSelectedItemsBasedOnStringSearch(context: Word.RequestContext, selection: Word.Range, paragraph: Word.Paragraph, blocks: Word.Paragraph[], definitions: IHighlightDefinition[], internalContentReferences: IHighlightInternalContentReference[]): Promise<{ selectedDefinitions: IHighlightDefinition[]; selectedInternalContentReferences: IHighlightInternalContentReference[] }> {

	if (isLatestWord() && paragraph && !paragraph.isNullObject) {
		const rangeExpandTo = paragraph.getRange(Word.SelectionMode.start);
		await context.sync();
		const range = selection.expandToOrNullObject(rangeExpandTo);
		await context.sync();
		const paragraphStartToCursorRange = range.load([LoadOption.Text]);
		await context.sync();
		// await context.sync();

		const relatedBlock = findBlockForParagraph(paragraph, blocks);
		const paragraphText = paragraph.text.toLowerCase();
		const selectionStringPos = paragraphStartToCursorRange.text.length;

		const relevantDefinitions = definitions.filter(def => {
			if (blocks.indexOf(relatedBlock) === -1) {
				return false;
			}
			const text = def.text.toLowerCase().trim();
			const stringMatchIndex = paragraphText.indexOf(text, selectionStringPos - text.length);
			if (stringMatchIndex !== -1) {
				if (stringMatchIndex < selectionStringPos && stringMatchIndex + text.length > selectionStringPos) {
					return true;
				}
			}
			return false;
		}).sort((a, b) => b.text.length - a.text.length); // order by longest string


		const relevantICR = internalContentReferences.filter(icr => {
			if (blocks.indexOf(relatedBlock) === -1) {
				return false;
			}
			const text = icr.text.toLowerCase().trim();
			const stringMatchIndex = paragraphText.indexOf(text, selectionStringPos - text.length);
			if (stringMatchIndex !== -1) {
				if (stringMatchIndex < selectionStringPos && stringMatchIndex + text.length > selectionStringPos) {
					return true;
				}
			}
			return false;
		}).sort((a, b) => b.text.length - a.text.length); // order by longest string
		return {
			selectedDefinitions: relevantDefinitions,
			selectedInternalContentReferences: relevantICR,
		};
	}

	return null;
}

function _getResourceUUIDsFromContentControls(contentControl: Word.ContentControl, className: ClassName) {
	if ((!contentControl) || (!contentControl.tag)) {
		return null;
	}
	const tags = contentControl.tag.split(",");
	if (tags.includes(className)) {
		return _getUUIDFromTagWithPrefix(contentControl.tag, TagIdPrefix.Reference);
	}
	return null;
}

function _getUUIDFromTagWithPrefix(tagString: string, idPrefix: TagIdPrefix): string {
	const composeIdTag = (idPrefix: TagIdPrefix, tagId: string): string => {
		const idTagValueSeparator = "_";
		const tagIdGlobalPrefix = "mcf-id-";
		const result = tagIdGlobalPrefix + idPrefix + idTagValueSeparator + tagId;
		return result;
	};

	if (!tagString) {
		return null;
	}
	const idTagValueSeparator = "_";

	const tagPrefix = composeIdTag(idPrefix, "");
	const multiTagSeparator = ",";
	const tags = tagString.split(multiTagSeparator);

	const idTag = tags.find(t => t.startsWith(tagPrefix));
	if (!idTag) {
		return null;
	}

	const tagParts = idTag.split(idTagValueSeparator);

	if (tagParts.length <= 1) {
		return null;
	} else if (tagParts.length === 2) {
		return tagParts[1];
	} else {
		return tagParts.slice(1).join(idTagValueSeparator);
	}
}


function findBlockForParagraph(paragraph: Word.Paragraph, blocks: Word.Paragraph[]) {
	return blocks.find(block => paragraph && isTextSimilar(paragraph.text, block.text));
}
