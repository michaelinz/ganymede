import { areParagraphsSimilarEnough } from '../OfficeJSSelectionHelper';

describe('OfficeJSSelectionHelper', () => {
	describe('areParagraphsSimilarEnough', () => {
		test('4/5 paragraphs match', () => {
			const similarities = [1, 1, 1, 1, 0];
			expect(areParagraphsSimilarEnough(similarities)).toEqual(true);
		});

		test('1/2 paragraphs do not match', () => {
			const similarities = [1, 0];
			expect(areParagraphsSimilarEnough(similarities)).toEqual(false);
		});

		test('1 paragraph does match', () => {
			const similarities = [1];
			expect(areParagraphsSimilarEnough(similarities)).toEqual(true);
		});
	});
});
