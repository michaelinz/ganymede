/**
 * Magic strings
 * These are the options when .load run of Word API
 */
export enum LoadOption {
	Text = 'text',
	Tag = 'tag',
	Load = 'load'
}