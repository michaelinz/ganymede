import { IEditorParagraph } from 'src/editor/base/EditorSelection';
import { isTextSimilar, getSimilarity } from './utils/isTextSimilar';
import { isLatestWord } from './utils/isLatestWord';

export async function selectParagraphs(context: Word.RequestContext, paragraphs: IEditorParagraph[]): Promise<boolean> {
	const range = await findRangeForParagraphs(context, paragraphs);
	if (range) {
		range.select(Word.SelectionMode.select);
		return true;
	}
	return false;
}

export async function selectTextInParagraphs(context: Word.RequestContext, text: string, paragraphs: IEditorParagraph[]): Promise<boolean> {
	const range = await findRangeForParagraphs(context, paragraphs);
	if (range) {
		const selection = range.search(text, { ignorePunct: true }).getFirstOrNullObject();
		await context.sync();
		if (selection && !selection.isNullObject) {
			selection.select();
			return true;
		}
	}
	return false;
}

export async function findRangeForParagraphs(context: Word.RequestContext, paragraphs: IEditorParagraph[]): Promise<Word.Range | Word.Paragraph> {
	if (!paragraphs || !paragraphs.length) {
		console.warn('No paragraphs found');
		return null;
	}
	// const text = docRef.relatedLabel === 'Concept' ? docRef.paragraphs.map(p => p.text).join('\n') : docRef.content.plainText; // TODO - change later when concept plainText includes new lines
	const wordParagraphs = await findWordParagraphs(context, paragraphs);
	if (wordParagraphs.length) {
		const firstParagraph = wordParagraphs[0];
		if (isLatestWord()) {
			//console.log("utilizing latest version of words features to select full range");
			const lastParagraph = wordParagraphs[wordParagraphs.length - 1];
			const range: Word.Range | Word.Paragraph = (wordParagraphs.length > 1 && lastParagraph)
				? firstParagraph.getRange().expandTo(lastParagraph.getRange())
				: firstParagraph;

			await context.sync();
			return range;
		}
		//console.log("Word version isn't latest, returning first paragraph");
		return firstParagraph;
	}
	return null;
}

async function findWordParagraphs(context: Word.RequestContext, paragraphs: IEditorParagraph[]) {
	const isLatestWordVersion = isLatestWord();
	const options = isLatestWordVersion ? 'text, isListItem, listItemOrNullObject/listString' : 'text';
	const wordParagraphs = context.document.body.paragraphs.load(options);
	await context.sync();

	const indexedParas = wordParagraphs.items.map((para, index) => ({
		index,
		text: isLatestWordVersion && para.isListItem ? para.listItemOrNullObject.listString + " " + para.text : para.text,
	} as IEditorParagraph));

	const foundIndexedParas = findIndexedParagraphs(indexedParas, paragraphs);
	const matchingParagraphs: Word.Paragraph[] = [];
	for (const foundPara of foundIndexedParas) {
		matchingParagraphs.push(wordParagraphs.items[foundPara.index]);
	}
	return matchingParagraphs;
}


function findIndexedParagraphs(wordParagraphs: IEditorParagraph[], paragraphsToFind: IEditorParagraph[]): IEditorParagraph[] {
	const text = paragraphsToFind.map(p => p.text);
	if (!text.length) {
		return [];
	}

	let matchingParagraphs: IEditorParagraph[] = [];
	let currentFirstParagraphIndex = 0;
	while (currentFirstParagraphIndex < wordParagraphs.length) {
		// Clean up line breaks and match the first para for reference index
		const cleanedParas = text[0].match(/[^\r\n]+/g);
		const result = getFirstMatchingParagraphForText(wordParagraphs, cleanedParas[0], currentFirstParagraphIndex);
		if (!result) {
			return [];
		}

		currentFirstParagraphIndex = result.index;

		matchingParagraphs = findMatchingWordParagraphsFromTextArray(wordParagraphs, cleanedParas, currentFirstParagraphIndex);
		if (matchingParagraphs.length) {
			// found a match, yey!
			break;
		}
		currentFirstParagraphIndex++;
	}
	return matchingParagraphs;
}

function getFirstMatchingParagraphForText
	(allParagraphs: IEditorParagraph[], text: string, startFromIndex: number): IEditorParagraph {

	let paragraph: IEditorParagraph = null;
	let i = startFromIndex;
	while (i < allParagraphs.length) {
		paragraph = allParagraphs[i];
		if (paragraph && isTextSimilar(paragraph.text, text)) {
			return {
				index: i,
				text: paragraph.text,
			};
		}
		i++;
	}
	return null;
}

function findMatchingWordParagraphsFromTextArray
	(wordParagraphs: IEditorParagraph[], textArrayToMatch: string[], startParagraphIndex: number): IEditorParagraph[] {

	// get the word paragraphs we care about. remove all the empties
	const relevantWordParagraphTexts = wordParagraphs
		.slice(startParagraphIndex)
		.filter(para => para && para.text && para.text.trim() !== "");
	// remove the empties on the matching text array too
	const nonEmptyItemsToMatch = textArrayToMatch.filter(text => text && text.trim() !== "");

	if (!nonEmptyItemsToMatch.length || !relevantWordParagraphTexts.length || nonEmptyItemsToMatch.length > relevantWordParagraphTexts.length) {
		// if there's nothing to match, then nothing to do here
		return [];
	}

	// iterate through all the text matches and try to consecutive fuzzy matches

	// Get pure similarities per paragraph
	const similarities: number[] = [];
	for (let i = 0; i < nonEmptyItemsToMatch.length; i++) {
		const paragraphText = relevantWordParagraphTexts[i];
		const text = nonEmptyItemsToMatch[i];
		const similarity = getSimilarity(paragraphText?.text, text);
		similarities.push(similarity);
	}

	if (!areParagraphsSimilarEnough(similarities)) {
		return [];
	}

	const firstParagraph = relevantWordParagraphTexts[0]; // stash the first paragraph
	const lastParagraph = relevantWordParagraphTexts[nonEmptyItemsToMatch.length - 1];

	const firstParagraphIndex = wordParagraphs.findIndex(para => para === firstParagraph);
	const lastParagraphIndex = wordParagraphs.findIndex(para => para === lastParagraph);

	// find the paragraph array based on the first and the last match

	const foundIndexedParas: IEditorParagraph[] = [];
	for (let i = firstParagraphIndex; i <= lastParagraphIndex; i++) {
		foundIndexedParas.push({
			index: i,
			text: wordParagraphs[i].text,
		} as IEditorParagraph);
	}
	return foundIndexedParas;
}


export function areParagraphsSimilarEnough(similarities: number[]) {
	/**
	 * See OfficeJSSelectionHelper.test.ts
	 */
	const PARAGRAPH_LEVEL_SIMILARITY_THRESHOLD = 0.85;
	const GROUP_SIMILARITY_THRESHOLD = 0.80; // one out of 5 paragraphs allowed to not pass threshold
	const passedCount = similarities.filter((similarity) => similarity >= PARAGRAPH_LEVEL_SIMILARITY_THRESHOLD).length;
	return passedCount/similarities.length >= GROUP_SIMILARITY_THRESHOLD;
}
