export function isRunningWordOnline(): boolean {
	const officePlatformType = Office.context.platform;
	return officePlatformType === Office.PlatformType.OfficeOnline;
}
