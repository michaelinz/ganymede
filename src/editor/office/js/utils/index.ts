export * from './CustomXML';
export * from './isLatestWord';
export * from './isRunningWordOnline';
export * from './isTextSimilar';
