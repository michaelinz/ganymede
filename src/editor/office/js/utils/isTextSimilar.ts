import { compareTwoStrings } from 'src/components/utils/compareStrings';

export function isTextSimilar(text1: string, text2: string): boolean {
	const TEXT_SIMILARITY_THRESHOLD = 0.85;
	return getSimilarity(text1, text2) >= TEXT_SIMILARITY_THRESHOLD;
}

export function getSimilarity(text1: string, text2: string): number {
	if (text1 && text2) {
		const textNumbersRegex = /[^a-zA-Z0-9]/g;

		const textToLettersNumbersOnly = (text: string) => {
			return text ? text.replace(textNumbersRegex, '').trim().toLowerCase() : '';
		};

		const preparedText2 = textToLettersNumbersOnly(text2);
		const preparedText1 = textToLettersNumbersOnly(text1);

		const similarity = compareTwoStrings(preparedText2, preparedText1);
		return similarity;
	}
	if (!text1 && !text2) {
		return 1;
	}
	return 0;
}
