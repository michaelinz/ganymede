export function isLatestWord() {
	return Office.context.requirements.isSetSupported('WordApi', '1.3');
}
