import convert from 'xml-js';

const CUSTOM_XML_NAME_SPACE = "https://www.mccarthyfinch.com";
const ROOT_FIELD_NAME = 'Root';


export async function getCustomProperty(key: string) {
	try {
		const customXMLs: string[] = await getCustomXMLs(CUSTOM_XML_NAME_SPACE);
		return findValueByKeyInCustomXMLStrings(customXMLs, ROOT_FIELD_NAME, key);
	} catch (e) {
		console.error(e);
		return null;
	}
}

async function findValueByKeyInCustomXMLStrings(customXMLs: string[], rootFieldName: string, key: string) {
	// reverse loop, because the data is appended at bottom in approve
	for (let i = customXMLs.length - 1; i >= 0; i--) {
		// TODO: this needs to be validated since i've swapped xml2js to xml-js
		const xmlObj = convert.xml2js(customXMLs[i], { compact: true });
		const value = xmlObj[rootFieldName][key];
		if (value !== undefined) {
			// the child's value will be parsed as an array?
			return value._text;
		}
	}
	return null;
}

/**
 * Custom xml is a solution to used in both C# and JS
 * Settings in JS SDK is different to the settings in C# SDK
 * https://docs.microsoft.com/en-us/javascript/api/office/office.settings?view=word-js-preview
 */
async function getCustomXMLs(namespace: string): Promise<Array<string>> {
	const calls = [];

	const getResult = (customXmlPart: Office.CustomXmlPart) =>
		new Promise((resolve, reject) => {
			customXmlPart.getXmlAsync((asyncResult: Office.AsyncResult<string>) => {
				if (asyncResult.status === Office.AsyncResultStatus.Succeeded) {
					return resolve(asyncResult.value);
				}
				return reject();
			});
		});

	((await getCustomXMLsResult(namespace)).value as Array<Office.CustomXmlPart>).map((customXMLPart) => {
		calls.push(getResult(customXMLPart));
	});

	return Promise.all(calls);
}

async function getCustomXMLsResult(namespace: string): Promise<any> {
	try {
		return new Promise((resolve, reject) => {
			Office.context.document.customXmlParts.getByNamespaceAsync(namespace, (asyncResult: Office.AsyncResult<any>) => {
				if (asyncResult.status === Office.AsyncResultStatus.Succeeded) {
					return resolve(asyncResult);
				}
				return reject('Cannot get the custom xml parts');
			});
		});
	} catch (e) {
		console.error('Cannot get the custom xml parts');
	}
	return { value: [] };
}
