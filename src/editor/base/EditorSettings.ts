export default abstract class EditorSettings {
	protected abstract getProperty(key: string): Promise<any>;
	protected abstract setProperty(key: string, value: any): Promise<void>;
	protected abstract removeProperty(key: string): Promise<void>;

	public abstract setDocumentForAutoShowTaskPane(): Promise<void>;

	/**
	 * documentUUID
	 */
	public async getDocumentUUID(): Promise<string> {
		return await this.getProperty('documentUUID');
	}

	public async setDocumentUUID(documentUUID: string): Promise<void> {
		return this.setProperty('documentUUID', documentUUID);
	}

	/**
	 * fileUUID
	 */
	public async getFileUUID(): Promise<string> {
		return await this.getProperty('fileUUID');
	}

	public async setFileUUID(fileUUID: string): Promise<void> {
		return this.setProperty('fileUUID', fileUUID);
	}

	/**
	 * checklistUUID
	 */
	public async getChecklistUUID(): Promise<string> {
		return await this.getProperty('checklistUUID');
	}

	public async setChecklistUUID(checklistUUID: string): Promise<void> {
		return this.setProperty('checklistUUID', checklistUUID);
	}

	public async getLastProcessedTimeChecklist(): Promise<number> {
		const unixTime = await this.getProperty('LastProcessedByDocs');
		if (unixTime) {
			return parseInt(unixTime, 10);
		}
		return undefined;
	}

	public async setLastProcessedDocumentUUIDChecklist(dateTime: Date): Promise<void> {
		return this.setProperty('LastProcessedByDocs', dateTime.getTime().toString());
	}


	/**
	 * Approve
	 */
	public async getLastProcessedTimeApprove(): Promise<number> {
		const unixTime = await this.getProperty('LastProcessedByApprove');
		if (unixTime) {
			return parseInt(unixTime, 10);
		}
		return null;
	}

	public async getLastProcessedDocumentUUIDApprove(): Promise<string> {
		return this.getProperty('LastRunApproveDocumentUUID');
	}

	/**
	 * documentHighlights
	 */
	public async getDocumentHighlights(): Promise<string> {
		return await this.getProperty('documentHighlights');
	}

	public async setDocumentHighlights(documentHighlights: string): Promise<void> {
		return this.setProperty('documentHighlights', documentHighlights);
	}

}
