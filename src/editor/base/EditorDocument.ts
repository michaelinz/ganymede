export default abstract class EditorDocument {
	abstract getDocumentBinary(): Promise<Blob>;
	abstract getDocumentFilename(): Promise<string>;
	abstract saveDocument(): Promise<void>;
	abstract loadBase64ToDocument(base64: string): Promise<void>;
	// subscribe for on document change event
	abstract onDocumentSelectionChange(callback: () => void, removeHandler?: boolean): void;
}
