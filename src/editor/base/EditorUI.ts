export default abstract class EditorUI {
	abstract async openPopupWindow(startAddress: string): Promise<void>;
	abstract async closeThisPopupWindow(): Promise<void>;
}
