import EditorDocument from './EditorDocument';
import EditorSettings from './EditorSettings';
import EditorUI from './EditorUI';
import EditorSelection from './EditorSelection';
import EditorDocumentReference from './EditorDocumentReference';
import EditorHighlights from './EditorHighlights';

export type ISubscriptionEvent = 'onLoad';

export interface ISubscriber {
	key: string;
	onEvent: <T = any>(args?: T) => Promise<void>;
}

export default abstract class Editor {
	Document: EditorDocument;
	Settings: EditorSettings;
	UI: EditorUI;
	Selection: EditorSelection;
	DocumentReference: EditorDocumentReference;
	Highlights: EditorHighlights;

	/**
	 * Subscription
	 */
	subscribers = new Map<string, ISubscriber[]>();

	addSubscriber(eventName: ISubscriptionEvent, subscriber: ISubscriber) {
		if (!this.subscribers.has(eventName)) {
			this.subscribers.set(eventName, []);
		}
		this.subscribers.get(eventName).push(subscriber);
	}

	removeSubscribers(eventName: ISubscriptionEvent, subscriber: ISubscriber) {
		if (!this.subscribers.has(eventName)) {
			return;
		}
		this.subscribers.set(
			eventName,
			this.subscribers.get(eventName).filter((sub) => subscriber.key === sub.key),
		);
	}

	async notifySubscribers<T = any>(eventName: ISubscriptionEvent, args?: T) {
		if (!this.subscribers.has(eventName)) {
			return;
		}
		const events = this.subscribers.get(eventName).map((eventName) => eventName.onEvent(args));
		return Promise.all(events);
	}

	abstract onLoad(): Promise<void>;
}
