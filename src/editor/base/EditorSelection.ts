export interface IEditorParagraph {
	uuid?: string;
	text: string;
	index?: number;
}

export default abstract class EditorSelection {
	abstract getSelectionHtml(): Promise<string>;
	abstract getSelectionParagraphs(): Promise<IEditorParagraph[]>;
	abstract insertHtmlAtSelection(html: string): Promise<void>;
	abstract insertTextAtSelection(text: string): Promise<void>;

	abstract selectContentControl(tag: string): Promise<boolean>; // returns true if we selected
	abstract selectParagraphs(paragraphs: IEditorParagraph[]): Promise<boolean>;
	abstract selectTextInParagraphs(text: string, paragraphs: IEditorParagraph[]): Promise<boolean>;
}
