import { IDocumentReference } from '@mccarthyfinch/public-api-client';

export default abstract class EditorDocumentReference {
	abstract createDocRefFromSelection(tag: string): Promise<void>;
	abstract goToDocRef(docRef: IDocumentReference): Promise<void>;
	abstract goToDefinition(text: string): Promise<void>;
	abstract goToClause(text: string[]): Promise<void>;
	abstract deleteDocRef(tag: string): Promise<void>;
}
