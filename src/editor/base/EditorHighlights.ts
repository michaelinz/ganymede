import { IHighlights, IHighlightReferences, IHighlightDefinition, IHighlightInternalContentReference } from '@mccarthyfinch/public-api-client';

export enum TrackChangeStatus {
	LOADING = -2,
	UNKNOWN = -1,
	DISABLED = 0,
	ENABLED = 1,
}


export interface ISelectedReferences {
	definitions: IHighlightDefinition[];
	internalContentReferences: IHighlightInternalContentReference[];
}

export default interface EditorHighlights {
	toggleHighlighting(documentUUID: string, highlightsSettings: IHighlights): Promise<{ success: boolean; trackedChangesStatus: TrackChangeStatus }>;

	isClickAction(): Promise<boolean>;
	getSelectedReferences(definitionClauseReferences: IHighlightReferences): Promise<ISelectedReferences>;
}
