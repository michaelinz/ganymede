import EditorDocument from 'src/editor/base/EditorDocument';

export default class NoneDocument extends EditorDocument {
	async getDocumentBinary(): Promise<Blob> {
		return null;
	}

	async getDocumentFilename(): Promise<string> {
		return 'Document.docx';
	}

	async saveDocument(): Promise<void> {
		return;
	}

	async loadBase64ToDocument(base64: string): Promise<void> {
		return;
	}
	// subscribe for on document change event
	onDocumentSelectionChange(callback: () => void, removeHandler?: boolean): void {
		return;
	}

}
