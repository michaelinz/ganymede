import EditorDocumentReference from 'src/editor/base/EditorDocumentReference';
import { IDocumentReference } from '@mccarthyfinch/public-api-client';

export default class NoneDocumentReference extends EditorDocumentReference {
	async createDocRefFromSelection(tag: string) {
		console.log('createDocRefFromSelection', tag);
	}

	async goToDocRef(docRef: IDocumentReference) {
		console.log('goToDocRef', docRef.tag);
	}

	async goToDefinition(text: string) {
		console.log('goToDefinition', text);
	}

	async goToClause(text: string[]) {
		console.log('goToClause', text);
	}	

	async deleteDocRef(tag: string) {
		console.log('deleteDocRef', tag);
	}
}
