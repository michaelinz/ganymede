import Editor from 'src/editor/base/Editor';
import NoneDocumentReference from './NoneDocumentReference';
import NoneSettings from './NoneSettings';
import NoneSelection from './NoneSelection';
import NoneDocument from './NoneDocument';
import NoneHighlights from './NoneHighlights';

export default class NoneEditor extends Editor {
	Settings = new NoneSettings();
	Document = new NoneDocument();
	DocumentReference = new NoneDocumentReference();
	Selection = new NoneSelection();
	Highlights = NoneHighlights;

	async onLoad() {
		this.notifySubscribers('onLoad');
	}
}
