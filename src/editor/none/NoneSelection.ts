import EditorSelection, { IEditorParagraph } from 'src/editor/base/EditorSelection';
import { IHighlightReferences, IHighlightDefinition, IHighlightInternalContentReference } from '@mccarthyfinch/public-api-client';

function getSelectionHTML() {
	return `<div>${window.getSelection().toString()}</div>`;
}

export default class NoneSelection extends EditorSelection {
	async getSelectionHtml(): Promise<string> {
		const html = getSelectionHTML();
		return html;
	}

	async getSelectionParagraphs() {
		return [{
			text: window.getSelection().toString(),
			index: 0,
		}];
	}

	async insertHtmlAtSelection(html: string): Promise<void> {
		console.log('insertHtmlAtSelection', html);
	}

	async insertTextAtSelection(text: string): Promise<void> {
		console.log('insertTextAtSelection', text);
	}

	async selectContentControl(tag: string): Promise<boolean> {
		return false;
	}

	async selectParagraphs(paragraphs: IEditorParagraph[]): Promise<boolean> {
		return false;
	}

	async selectTextInParagraphs(text: string, paragraphs: IEditorParagraph[]): Promise<boolean> {
		return false;
	}


	async isClickAction(): Promise<boolean> {
		console.log('isClickAction', false);
		return false;
	}

	async getSelectedReferences(definitionClauseReferences: IHighlightReferences): Promise<{definitions: IHighlightDefinition[], internalContentReferences: IHighlightInternalContentReference[]}>{
		console.log('getSelectedReferences');
		return {definitions: [], internalContentReferences : []};
	}
}
