import { IHighlightReferences, IHighlights } from '@mccarthyfinch/public-api-client';
import EditorHighlights, { TrackChangeStatus, ISelectedReferences } from '../base/EditorHighlights';

const NoneHighlights: EditorHighlights = {

	async toggleHighlighting(documentUUID: string, highlightsSettings: IHighlights): Promise<{ success: boolean; trackedChangesStatus: TrackChangeStatus }> {
		return {
			success: true,
			trackedChangesStatus: TrackChangeStatus.ENABLED,
		};
	},
	async isClickAction() {
		return true;
	},
	async getSelectedReferences(definitionClauseReferences: IHighlightReferences): Promise<ISelectedReferences> {
		return {
			definitions: [],
			internalContentReferences: [],
		};
	},

};

export default NoneHighlights;
