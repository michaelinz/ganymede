import EditorSettings from 'src/editor/base/EditorSettings';

const NONE_SETTINGS_CACHE_PREFIX = 'none-settings-';

export default class NoneSettings extends EditorSettings {
	public setDocumentForAutoShowTaskPane(): Promise<void> {
		return;
	}

	protected async getProperty(key: string): Promise<any> {
		return localStorage.getItem(`${NONE_SETTINGS_CACHE_PREFIX}${key}`);
	}

	protected async setProperty(key: string, value: any) {
		localStorage.setItem(`${NONE_SETTINGS_CACHE_PREFIX}${key}`, value);
	}

	protected async removeProperty(key: string) {
		localStorage.removeItem(`${NONE_SETTINGS_CACHE_PREFIX}${key}`);
	}

}
