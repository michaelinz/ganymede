import Editor, { ISubscriber, ISubscriptionEvent } from './base/Editor';
import EditorDocument from './base/EditorDocument';
import NoneEditor from './none/NoneEditor';
import OfficeJSEditor from './office/js/OfficeJS';

export default class EditorFactory {
	static instance: Editor;

	static onLoadOfficeJS(onInitialise: () => void) {
		try {
			/**
			 * window.isOfficeInitialised set by public/scripts/initOffice.js which should be loaded in index.html
			 * (the public folder is copied to https://url.com/ by the build tool)
			 * initOffice.js is what runs Office.onReady/Office.initialize
			 *
			 * We are running Office.onReady there as it will load faster than app.js
			 * The concern that this solves is that Office may run Office.onReady before app.js is loaded and thus crashing the app.
			 * This happens frequently on new deploys. Office.js is cached so loads immediately, but the new app.js is new.
			 * This app.js is yet to be cached and so still needs to be downloaded, and since it takes a while to download,
			 * Office.js will run Office.onReady before our app loads to set Office.onReady.
			 * This causes our app to crash as Office.onReady needs to be called and our app needs to know when Office is ready.
			 *
			 * If we are worried that initOffice.js takes too long to load,
			 * we can just copy its contents to index.html in a <script> tag instead
			 */
			if ((window as any).isOfficeInitialised) {
				console.log('Initialised office');
				onInitialise();
			} else {
				throw new Error('Waiting for window.isOfficeInitialised...');
			}
		} catch (e) {
			/**
			 * We want to retry even if it isn't the expected error above because our app will fail to load if onLoadCallback() isn't called.
			 * Because of this we are being very aggressive with retries.
			 */
			console.log('Error initializing office:\n' + e);
			setTimeout(() => this.onLoadOfficeJS(onInitialise), 500);
		}
	}

	static generateInstance(setOnLoadSubscribers?: (editor: Editor) => void) {
		const applicationMode = (window as any).applicationMode;
		switch (applicationMode) {
			// case 'OfficeVSTO':
			// 	EditorFactory.instance = new OfficeVSTOEditor();
			// 	break;
			case 'OfficeJS':
			default:
				function isInWord() {
					try {
						return Office.context.host === Office.HostType.Word;
					} catch {
						return false;
					}
				}

				return EditorFactory.onLoadOfficeJS(() => {
					if (isInWord()) {
						EditorFactory.instance = new OfficeJSEditor();
					} else {
						EditorFactory.instance = new NoneEditor();
					}
					setOnLoadSubscribers(EditorFactory.instance);
					EditorFactory.instance.onLoad();
				});
		}
	}

	static getInstance() {
		if (!EditorFactory.instance) {
			throw new Error('Error: Could not get editor!!!');
		}
		return EditorFactory.instance;
	}
}

export const getEditor = EditorFactory.getInstance;
