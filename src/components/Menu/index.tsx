import React from 'react';
import styled from 'styled-components';

export const MenuItemWrapper = styled.div`
	min-width: 6.875rem;
	font-size: ${({ theme }) => theme.typography.paragraph.fontSize};
	padding: 0.5rem;

	&:hover {
		cursor: pointer;
		opacity: 0.75;
	}
`;

interface IMenuItemProps {
	renderContent: () => React.ReactChild;
	onClick: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
}

export function MenuItem({ renderContent, onClick }: IMenuItemProps) {
	return <MenuItemWrapper onClick={onClick}>{renderContent()}</MenuItemWrapper>;
}

export const MenuList = styled.div`
	border-radius: 2px;
	border: solid 1px #e3e6eb;

	background: white;
`;
