import { matchPath, match } from 'react-router-dom';

export interface IPathRoutes {
	[x: string]: SiteUrl;
}

interface ISiteUrl {
	path: string;
	component?: React.ComponentType;
	name: string;
	parentUrl?: SiteUrl;
	childRoutes?: IPathRoutes;
	isPrivate?: boolean;
}

export default class SiteUrl implements ISiteUrl {
	path: string;
	name: string;
	component?: React.ComponentType;
	parentUrl?: SiteUrl;
	childRoutes?: IPathRoutes;
	isPrivate?: boolean;

	constructor({ path, name, component, parentUrl, childRoutes, isPrivate = true }: ISiteUrl) {
		this.path = path;
		this.name = name;
		this.component = component;
		this.parentUrl = parentUrl;
		this.isPrivate = isPrivate;
		if (childRoutes) {
			this.setChildRoutes(childRoutes);
		}
	}

	setParentUrl(parent: SiteUrl) {
		this.parentUrl = parent;
	}

	private getFullUrls() {
		const urls: SiteUrl[] = [];
		// eslint-disable-next-line @typescript-eslint/no-this-alias
		let currentUrl: SiteUrl = this;
		while (currentUrl) {
			urls.unshift(currentUrl);
			currentUrl = currentUrl.parentUrl;
		}
		return urls;
	}

	getHref() {
		const href = this.getFullUrls()
			.map((u) => u.path)
			.join('');
		if (href.startsWith('//')) {
			return href.substring(1);
		}
		return href;
	}

	setChildRoutes(urls: IPathRoutes) {
		Object.values(urls).forEach((url) => {
			url.setParentUrl(this);
		});
		this.childRoutes = urls;
	}
}

export interface ISiteUrlMatch {
	siteUrl: SiteUrl;
	match: match<any>;
}

// We can filter things by the path to optimise this
// But there aren't that many routes so this is fine
// TODO: get this working with variables in href path
export function getSiteUrlFromPath(
	pathRoutes: IPathRoutes,
	path: string,
	options: {
		matchRootPathRoute?: boolean;
	} = {},
): ISiteUrlMatch {
	const currentOptions = {
		matchRootPathRoute: false,
		...options,
	};
	for (const siteUrl of Object.values(pathRoutes)) {
		const match = matchPath(location.pathname, {
			path: siteUrl.getHref(),
			exact: true,
		});

		if (match) {
			return {
				siteUrl,
				match,
			};
		}

		const childMatch = matchPath(location.pathname, {
			path: siteUrl.getHref(),
			exact: false,
		});

		if (currentOptions.matchRootPathRoute && childMatch) {
			return {
				siteUrl,
				match,
			};
		}

		if (childMatch && siteUrl.childRoutes) {
			const childSiteUrl = getSiteUrlFromPath(siteUrl.childRoutes, path, {
				...currentOptions,
			});
			if (childSiteUrl) {
				return childSiteUrl;
			}
		}
	}
	return null;
}
