import React from 'react';
import { LinkProps } from 'react-router-dom';
import { ZeldaA, ZeldaLink } from './styles';

interface IZelda extends LinkProps {
	to: string;
	forceOpenNewWindow?: boolean;
	'data-ted'?: string;
	[x: string]: any;
}

// Because Link does not support external links
export default function Zelda({ to, forceOpenNewWindow, children, 'data-ted': dataTed, ...rest }: IZelda) {
	if (!to) {
		return <>{children}</>;
	}

	const isExternal = /^https?:\/\//.test(to);
	if (isExternal || forceOpenNewWindow) {
		return (
			<ZeldaA href={to} data-ted={`${dataTed || ''}`} target="_blank" rel="noopener noreferrer" {...rest}>
				{children}
			</ZeldaA>
		);
	}
	return (
		<ZeldaLink data-ted={`${dataTed || ''}`} to={to} {...rest}>
			{children}
		</ZeldaLink>
	);
}
