import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const ZeldaA = styled.a`
	text-decoration: none;
`;

export const ZeldaLink = styled(Link)`
	text-decoration: none;
`;
