import React from 'react';
import { DrawerWrapper, HandleWrapper, Handle } from './styles';
export { DrawerPageWrapper, DrawerPageContentWrapper } from './styles';

interface IDrawerProps {
	children: React.ReactChild;
	hideHandle?: boolean;
	isOpen: boolean;
	setIsOpen?: (isOpen: boolean) => void;
}

export default function Drawer({ children, isOpen, setIsOpen, hideHandle, ...rest }: IDrawerProps) {
	return (
		<DrawerWrapper isOpen={isOpen} {...rest}>
			{
				hideHandle ? null :
				<HandleWrapper onClick={setIsOpen ? () => setIsOpen(!isOpen) : undefined}>
					<Handle />
				</HandleWrapper>
			}
			{children}
		</DrawerWrapper>
	);
}
