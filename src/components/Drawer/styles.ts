import styled from 'styled-components';
import { Content } from '../Layout/Page';

export const DrawerWrapper = styled.div<{ isOpen?: boolean }>`
	box-shadow: 0 0 6px 6px rgba(34, 34, 34, 0.1);
	overflow: hidden;
	display: flex;
	flex-direction: column;
	transition: height 0.5s ease-out;
`;

export const HandleWrapper = styled.button`
	padding: 0.75rem 0;
	width: 100%;
	display: flex;
	flex-direction: row;
	justify-content: center;
	align-items: center;
	border: none;
	outline: none;
	background: none;
	cursor: pointer;
	flex-shrink: 0;
`;

export const Handle = styled.div`
	width: 28px;
	height: 4px;
	border-radius: 2px;
	box-shadow: inset 0 1px 3px 0 ${({ theme }) => theme.colorPalette.smoke.base};
	background-color: ${({ theme }) => theme.colorPalette.smoke[100]};
`;




export const DrawerPageWrapper = styled(Content)`
	background: transparent;
	justify-content: space-between;
	min-height: 0;
`;

export const DrawerPageContentWrapper = styled.div`
	display: flex;
	flex-direction: column;
	position: relative;
	flex-grow: 0;
	flex-shrink: 1;
	height: fit-content;
	overflow: hidden;
`;
