import styled from 'styled-components';

export const Row = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	width: 100%;
	position: relative;
`;

export const Column = styled.div`
	display: flex;
	flex-direction: column;
	width: 100%;
	position: relative;
`;
