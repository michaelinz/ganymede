import styled from 'styled-components';

export const SecondaryHeaderBar = styled.div`
	padding: 0.5rem 1rem;
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	margin-top: 1.25rem;
`;

export default SecondaryHeaderBar;