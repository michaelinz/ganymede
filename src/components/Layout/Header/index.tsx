import React from 'react';
import { HeaderWrapper } from './styles';

export { PrimaryHeaderBar } from './PrimaryHeaderBar';
export { SecondaryHeaderBar } from './SecondaryHeaderBar';
export { Column as HeaderColumn } from './styles';

interface HeaderProps {
	children: JSX.Element | JSX.Element[];
	background?: string;
}

/**
 * The main header that appears at the top of each page
 * @param children an array of header row content. Passing more than one row will increase the header height
 * @see PrimaryHeaderBar for the default layout of the top row (import from Header module)
 */
export default function Header({ children, background }: HeaderProps) {
	const filteredChildren = Array.isArray(children) ? children.filter(child => child) : [children];
	return (
		<HeaderWrapper large={filteredChildren.length > 1} backgroundImage={background}>
			{filteredChildren}
		</HeaderWrapper>
	);
}
