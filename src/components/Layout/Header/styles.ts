import styled from 'styled-components';
import { Row } from '../../Layout';

export const HeaderWrapper = styled.div<{ large?: boolean, backgroundImage?: string }>`
	background-color: ${({ theme }) => theme.color.primary};
	${({ backgroundImage }) => (backgroundImage ? `background-image: url(${backgroundImage});` : '')}
	background-position: left top;
	background-repeat: no-repeat;
	background-size: 100%;
	height: ${({ theme, large }) => (large ? theme.layout.headerHeight : theme.layout.headerHeightSmall)};
	width: 100%;
	margin-bottom: -1rem;
	transition: height 0.2s ease-out;
`;

export const BarWrapper = styled.div`
	padding: 0.5rem 2.5rem 0.5rem 1rem;
`;

export const HeaderRow = styled(Row)`
	min-height: 2rem;
`;

export const Column = styled.div<{ align?: 'left' | 'center' | 'right' }>`
	display: flex;
	flex-grow: 1;
	justify-content: ${({ align }) => {
		if (align === 'left') { return 'flex-start'; }
		if (align === 'center') { return 'center'; }
		if (align === 'right') { return 'flex-end'; }
		return 'flex-start';
	}};

	svg {
		font-size: 1rem;
	}

	& > *:not(:last-child) {
		margin-right: 0.5rem;
	}

	& > * {
		color: white;
	}
`;
