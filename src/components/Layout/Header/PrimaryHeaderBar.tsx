import React from 'react';
import { BarWrapper, HeaderRow } from './styles';

interface PrimaryHeaderBarProps {
	children: JSX.Element | JSX.Element[];
}

/**
 * The top row of the main header with default icons on the right side
 * @param children an array of child elements wrapped in the HeaderColumn utility component
 */
export function PrimaryHeaderBar({ children }: PrimaryHeaderBarProps) {
	return (
		<BarWrapper>
			<HeaderRow>
				{children}
			</HeaderRow>
		</BarWrapper>
	);
}

export default PrimaryHeaderBar;
