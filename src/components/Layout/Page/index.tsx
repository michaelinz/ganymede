import styled from 'styled-components';

export const PageWrapper = styled.div`
	display: flex;
	flex-direction: column;
	align-items: stretch;
	flex: 0 0;
	width: 100%;
	height: 100%;
	min-height: 100vh;
	max-height: 100vh;
	position: relative;
	overflow: hidden;
`;

export const Content = styled.div`
	background: white;
	border-radius: 1rem 1rem 0 0;
	width: 100%;
	height: 100%;
	display: flex;
	flex-direction: column;
	flex-grow: 1;
	flex-shrink: 1;
	position: relative;
	overflow: auto;
	min-height: 0;
`;
