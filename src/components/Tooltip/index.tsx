import React from 'react';
import { Theme } from '@material-ui/core'
import MaterialTooltip, { TooltipProps } from '@material-ui/core/Tooltip';
import withStyles, { Styles } from '@material-ui/core/styles/withStyles';
import colors from '../../styles/colors';

/**
 * 26 Feb 2021
 *
 * This Material UI component throws a warning in React strict mode:
 * "Warning: findDOMNode is deprecated in StrictMode. findDOMNode was passed an instance of Transition which is inside StrictMode.
 * Instead, add a ref directly to the element you want to reference. Learn more about using refs safely here:
 * https://fb.me/react-strict-mode-find-node"
 *
 * According to https://stackoverflow.com/questions/61220424/material-ui-drawer-finddomnode-is-deprecated-in-strictmode this will
 * be fixed in @material-ui/core@v5 which is currently in alpha and is unstable. For now, ignore the warning.
 */

const styles: Styles<Theme, {}, "tooltip" | "arrow" | "tooltipPlacementLeft" | "tooltipPlacementRight" | "tooltipPlacementTop" | "tooltipPlacementBottom"> = {
	tooltip: {
		maxWidth: 220,
		backgroundColor: colors.slate.base,
		color: 'white',
		textAlign: 'center',
		fontSize: '11px',
		lineHeight: 1,
		fontWeight: 600,
		padding: '0.5rem',
		fontFamily: "Open Sans, sans-serif",
	},
	tooltipPlacementBottom: {
		marginTop: '0.125rem',
	},
	tooltipPlacementTop: {
		marginBottom: '0.125rem',
	},
	tooltipPlacementLeft: {
		marginRight: '0.125rem',
	},
	tooltipPlacementRight: {
		marginLeft: '0.125rem',
	},
	arrow: {
		color: colors.slate.base,
	},
};

const Tooltip = ({ title, children, className, placement = 'top', enterDelay = 1000, ...rest }: TooltipProps) => (
	<MaterialTooltip title={title} classes={{ tooltip: className }} placement={placement} enterDelay={enterDelay} {...rest}>
		{children}
	</MaterialTooltip>
);

export default withStyles(styles)(Tooltip);
