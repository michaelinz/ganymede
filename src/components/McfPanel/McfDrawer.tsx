import React from 'react';
import { DrawerWrapper, Drawer } from './styles';

interface IProps {
	onDismiss: () => void;
	isOpen: boolean;
	children?: React.ReactNode;
}

function McfDrawer({onDismiss, isOpen, children}: IProps) {
	return (
		<DrawerWrapper isOpen={isOpen} onClick={onDismiss}>
			<Drawer onClick={e => e.stopPropagation()}>
				{children}
			</Drawer>
		</DrawerWrapper>
	);
}

export default McfDrawer;
