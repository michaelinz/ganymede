import styled from 'styled-components';
import { IconButtonBorderless } from '@mccarthyfinch/author-ui-components';

export const DrawerWrapper = styled.div<{isOpen: boolean}>`
	top: 0;
	bottom: 0;
	right: 0;
	left: 0;
	position: fixed;
	z-index: 1000;
	transform: ${({isOpen}) => isOpen ? 'translate(0)' : 'translate(100%)'};
	transition: all 0.2s ease-in-out;
`;

export const Drawer = styled.div`
	height: 100%;
	width: auto;
	margin-left: 1rem;
	box-shadow: -4px 0 6px 0 rgba(66, 82, 110, 0.25);
`;

export const PanelWrapper = styled.div`
	position: relative;
	z-index: 20;
	background: white;
	height: 100vh;
	width: 100%;
	overflow: hidden;
`;

export const Panel = styled.div`
	display: flex;
	flex-direction: column;
	flex-shrink: 0;
	flex-grow: 0;
	overflow: hidden;
	height: 100%;
`;

export const Heading = styled.span`
	font-size: 1rem;
	font-weight: 600;
	font-stretch: normal;
	font-style: normal;
	line-height: 0.94;
	letter-spacing: normal;
	color: #222222;
`;

export const PanelContent = styled.div`
	overflow-y: auto;
  	display: flex;
  	flex-direction: column;
  	flex-grow: 1;
`;

export const Menu = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	padding: 0.5rem;
	height: 2.75rem;
	flex: 0 0 2.75rem;
`;

export const CloseButton = styled(IconButtonBorderless)`
	margin-right: 0.2rem;
	font-size: 0.9375rem;
	color: #a4afbf;
	font-weight: normal;
  	font-stretch: normal;
  	font-style: normal;
  	line-height: 1;
  	letter-spacing: normal;
`;
