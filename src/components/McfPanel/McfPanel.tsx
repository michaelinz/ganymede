import React from 'react';
import McfDrawer from './McfDrawer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowFromLeft } from '@fortawesome/pro-regular-svg-icons';
import { PanelWrapper, Panel, PanelContent, Menu, Heading, CloseButton } from './styles';
interface IProps {
	onDismiss: () => void;
	isOpen: boolean;
	heading?: string | React.ReactNode;
	children?: React.ReactNode;
	arrowToLeft?: boolean;
}

export default function McfPanel({ isOpen, heading, onDismiss, children }: IProps) {
	return (
		<McfDrawer isOpen={isOpen} onDismiss={onDismiss}>
			<PanelWrapper>
				<Panel>
					{heading && (
						<Menu>
							<CloseButton onClick={onDismiss}>
								<FontAwesomeIcon icon={faArrowFromLeft} style={{ fontSize: '0.9375rem' }} />
							</CloseButton>
							<Heading>{heading}</Heading>
						</Menu>
					)}
					<PanelContent>
						{children}
					</PanelContent>
				</Panel>
			</PanelWrapper>
		</McfDrawer>
	);
}
