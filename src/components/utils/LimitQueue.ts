export default class LimitQueue {
	private maxQueueLength: number = null;

	setMaxQueueLength(maxQueueLength: number) {
		this.maxQueueLength = maxQueueLength;
	}

	private running: () => Promise<void> = null;
	private pending: (() => Promise<void>)[] = [];

	private consumeNextIfNotAlreadyRunning() {
		if (this.running) {
			// already running
		} else if (this.pending.length) {
			const newCandidate = this.pending[0];
			this.running = newCandidate;
			this.pending = this.pending.filter((i) => i !== newCandidate);
			newCandidate().then((result) => {
				this.running = null;
				this.consumeNextIfNotAlreadyRunning();
				return result;
			});
		} else {
			// nothing to consume...
		}
	}

	private hasMaxItems() {
		if (this.maxQueueLength !== undefined && this.maxQueueLength !== null) {
			// if running that means we add 1 to the pending count to account for the item that is running
			const totalPending = this.running ? this.pending.length + 1 : this.pending.length;
			return totalPending >= this.maxQueueLength;
		}
		return false;
	}

	push(func: () => Promise<any>) {
		if (this.hasMaxItems) {
			console.log(`Already have max (${this.maxQueueLength}) items in queue`);
			return;
		}
		this.pending.push(func);
		this.consumeNextIfNotAlreadyRunning();
	}
}
