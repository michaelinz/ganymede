// From https://stackoverflow.com/questions/43164554/how-to-implement-authenticated-routes-in-react-router-4
import { observer } from 'mobx-react';
import React from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';
import { LoginStatus, useAuthStore } from 'src/models/store/AuthStore';
import { AppRoutes } from 'src/pages/App/AppRouter';

const LoginPath = AppRoutes.CheckIn.path;

function PrivateRoute({ component: Component, ...rest }: RouteProps) {
	const authStore = useAuthStore();
	if (!authStore.loginStatus) {
		return null;
	}
	const loggedIn = authStore.loginStatus === LoginStatus.LoggedIn;
	return (
		<Route {...rest} render={(props) => (loggedIn ? <Component {...props} /> : <Redirect to={{ pathname: LoginPath, state: { from: props.location } }} />)} />
	);
}
export default observer(PrivateRoute);
