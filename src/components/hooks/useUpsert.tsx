import React, { useEffect, useReducer, useRef, useState } from 'react';
import debounce from 'lodash/debounce';
import { equals } from 'ramda';
import { makeAutoObservable } from 'mobx';
import { observer } from 'mobx-react';

interface Item {
	uuid?: string;
}

interface IUseUpsertProps<T extends Item> {
	item: T;
	createItem: (item: T) => Promise<T>;
	updateItem: (item: Partial<T>) => Promise<T>;
	postUpsert?: (item: T) => Promise<T>;
	debounceDelay?: number;
	isNew?: boolean;
}

// type IUseUpsertOutput<T extends Item> = {
// 	item: T;
// 	onUpsertItem: (item: Partial<T>) => Promise<T>;
// 	isUpserting: boolean;
// };

// TODO: convert this from mobx?
// Would need to do something with reducers urgh

function useUpsert<T extends Item>({
	item: _item,
	createItem,
	updateItem,
	postUpsert,
	debounceDelay = 1000,
	isNew,
}: IUseUpsertProps<T>): [T, (partial: Partial<T>) => Promise<T>, Promise<T>] {
	const [upserter] = useState(new Upserter({ item: _item, createItem, updateItem, postUpsert, debounceDelay, isNew }));
	const _debouncedOnChange = useRef(debounce(upserter.onUpsertItemServer, debounceDelay));

	const [cachedItem, setCachedItem] = useState(_item);
	const [cachedCurrentUpsertRequest, setCachedCurrentUpsertRequest] = useState<Promise<T>>();

	// Debounced
	const onUpsertItem = (partialItem: Partial<T>): Promise<T> => {
		const { item, fieldsToUpsert, setFieldsToUpsert, setItem, createRequest, onUpsertItemServer } = upserter;

		const newFields = Object.keys(partialItem).filter((key) => !fieldsToUpsert.includes(key));
		if (newFields.length) {
			setFieldsToUpsert([...fieldsToUpsert, ...newFields]);
		}
		setItem({ ...item, ...partialItem });
		setCachedItem({ ...item, ...partialItem });

		if (!item.uuid && !createRequest) {
			const request = onUpsertItemServer();
			setCachedCurrentUpsertRequest(request);
			return request;
		}
		const request = _debouncedOnChange.current();
		setCachedCurrentUpsertRequest(request);
		return request;
	};
	return [cachedItem, onUpsertItem, cachedCurrentUpsertRequest];
}

export default useUpsert;

class Upserter<T extends Item> {
	item: T;
	fieldsToUpsert: string[] = [];
	createRequest: Promise<T>;
	currentUpsertRequest: Promise<T>;

	props: IUseUpsertProps<T>;

	constructor(props: IUseUpsertProps<T>) {
		this.props = props;
		this.item = props.item;
		makeAutoObservable(this);
	}

	setItem = (item: T) => {
		this.item = item;
	};
	setFieldsToUpsert = (fieldsToUpsert: string[]) => {
		this.fieldsToUpsert = fieldsToUpsert;
	};
	setCreateRequest = (createRequest: Promise<T>) => {
		this.createRequest = createRequest;
	};
	setCurrentUpsertRequest = (currentUpsertRequest: Promise<T>) => {
		this.currentUpsertRequest = currentUpsertRequest;
	};

	updateItemCache = (partialItem: any) => {
		this.setItem({ ...this.item, ...partialItem });
	};

	onUpsertItemServer = (): Promise<T> => {
		const { fieldsToUpsert, item, updateItemCache, createRequest, setCreateRequest, currentUpsertRequest, setCurrentUpsertRequest } = this;
		const { createItem, updateItem, postUpsert, isNew } = this.props;

		const partialItem: Partial<T> = {};
		fieldsToUpsert.forEach((field) => {
			partialItem[field] = item[field];
		});

		if ((isNew === undefined ? item.uuid : !isNew) || createRequest) {
			// Update
			const getUpdateRequest = async (item: T) => {
				const icData = await updateItem({ uuid: item.uuid, ...partialItem });
				return icData;
			};

			let request: Promise<T>;
			if (currentUpsertRequest) {
				request = currentUpsertRequest.then(getUpdateRequest);
			} else {
				request = getUpdateRequest(item);
			}
			setCurrentUpsertRequest(request);
			return currentUpsertRequest;
		}

		// Create
		const getCreateRequest = async () => {
			const icData = await createItem({ ...item, ...partialItem });
			updateItemCache({
				uuid: icData.uuid,
			});
			if (postUpsert) {
				return postUpsert(icData);
			}
			return icData;
		};
		const newCreateRequest = getCreateRequest();
		setCreateRequest(newCreateRequest);
		setCurrentUpsertRequest(newCreateRequest);
		return currentUpsertRequest;
	};
}
