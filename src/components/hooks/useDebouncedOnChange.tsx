import React, { useEffect, useRef, useState } from 'react';
import debounce from 'lodash/debounce';
import { equals } from 'ramda';

interface IUseDebouncedOnChange<T, V> {
	value: T;
	getValueFromOnChange: (e: V) => T;
	onChange: (e: V) => void;
	debounceDelay?: number;
}

export default function useDebouncedOnChange<T, V>({
	value,
	onChange,
	getValueFromOnChange,
	debounceDelay = 1000,
}: IUseDebouncedOnChange<T, V>): [T, (e: V) => void] {
	const [_value, setValue] = useState(value);
	const _debouncedOnChange = useRef(debounce(onChange, debounceDelay));

	useEffect(() => {
		if (!equals(value, _value)) {
			setValue(value);
		}
	}, [value]);

	useEffect(() => {
		_debouncedOnChange.current = debounce(onChange, debounceDelay);
	}, [onChange]);

	const debouncedOnChange = (e: V) => {
		const value = getValueFromOnChange(e);
		setValue(value);
		_debouncedOnChange.current(e);
	};

	return [_value, debouncedOnChange];
}
