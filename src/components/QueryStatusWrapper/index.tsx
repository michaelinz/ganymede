import React from 'react';
import { UseQueryResult } from 'react-query';
import CircularProgress from '../CircularProgress';
import styled from 'styled-components';
/**
 * Renders different components based on the query status of a react-query
 */

export interface IQueryResultWrapperProps {
	queryResults: UseQueryResult<unknown, unknown>[];
	children: React.ReactChild;
	renderLoading?: () => React.ReactChild;
	renderError?: (errors?: any) => React.ReactChild;
	className?: string;
	['data-ted']?: string;
}

const LoadingWrapper = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	flex: 1;
	width: 100%;
	height: 100%;
	min-height: 3rem;
	max-height: 100vh;
	overflow-y: auto;
	position: relative;
`;
function DefaultLoading() {
	return (
		<LoadingWrapper>
			<CircularProgress />
		</LoadingWrapper>
	);
}

export default function QueryStatusWrapper({
	queryResults,
	children,
	renderLoading,
	renderError,
	className,
	['data-ted']: dataTed,
}: IQueryResultWrapperProps) {
	const hasData = !queryResults.find((result) => !result.data);
	const showLoading = queryResults.find((result) => result.isLoading);
	const errors = queryResults.filter((result) => result.isError);
	if (errors?.length) {
		console.warn(errors);
	}
	return (
		<>
			{hasData && children}
			{showLoading ? (renderLoading ? renderLoading() : <DefaultLoading />) : null}
		</>
	);
}
