import styled, { css } from 'styled-components';

export const Wrapper = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: center;
	align-items: center;
`;

export const Input = styled.input`
	appearance: none;
	opacity: 0;
	width: 0;
	height: 0;
	position: absolute;
`;

export const Label = styled.label<{ checked: boolean, disabled?: boolean }>`
	display: flex;
	flex-direction: row;
	justify-content: center;
	align-items: center;
	transition: 0.2s all;

	${({ disabled }) => {
		if (!disabled) {
			return css`
				&:hover:not(:disabled) {
					opacity: 0.5;
					cursor: pointer;
				}
			`;
		}
	}}
`;
