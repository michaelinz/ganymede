import React from 'react';
import { Wrapper, Input, Label } from './styles';

export interface ICustomCheckboxProps extends React.InputHTMLAttributes<HTMLInputElement> {
	children: (checked: boolean, disabled: boolean) => React.ReactChild;
}

const CustomCheckbox = (props: ICustomCheckboxProps) => {
	const { children, className, style, onClick, disabled = false, checked, ...rest } = props;

	const _id = props.id ? props.id : `${props.name}_${props.value}`;
	const _onClick = (onClick && !disabled)
		? (e) => { e.preventDefault(); return onClick(e); }
		: undefined;

	return (
		<Wrapper
			data-ted={`${props['data-ted']}-checkbox`}
			className={className}
			style={style}
			aria-checked={checked}
			onClick={_onClick}
		>
			<Label htmlFor={_id} checked={checked} disabled={disabled}>
				{children(checked, disabled)}
			</Label>
			<Input
				tabIndex={0}
				id={_id}
				type='checkbox'
				onChange={() => {}}
				disabled={disabled}
				data-ted={`${props['data-ted']}-input`}
				{...rest}
			/>
		</Wrapper>
	);
};

export default CustomCheckbox;
