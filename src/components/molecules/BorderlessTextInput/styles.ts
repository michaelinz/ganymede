import styled, { css } from 'styled-components';
import { Annotation } from '@mccarthyfinch/author-ui-components';

export const TextInputWrapper = styled.div<{ hasError?: boolean; disabled?: boolean }>`
	display: block;
	width: 100%;
	position: relative;

	${({ disabled }) => {
		if (!disabled) {
			return css`

			*[role=input] {
					/* &:hover, &:active {
						margin-right: 5rem;
					/* } */
				}
			}
			`;
		}
	}}

	[contenteditable=true] {
		position: relative;
		display: flex;

		&:empty:before {
			content: attr(placeholder);
			color: ${({ theme }) => theme.color.fontSecondary};
			pointer-events: none;
			position: absolute;
			left: 0;
			right: 0;
			width: auto;
			top: 0;
		}

		// This is only an issue with Edge
		@supports (-ms-ime-align: auto) {
			&:empty {
				padding-top: 1em;
			}
		}

		&[data-is-placeholder='true'] {
			color: ${({ theme }) => theme.color.fontSecondary};
		}
	}

	*[role='input'] {
		border: none;
		box-shadow: none;
		border-radius: 3px;
		outline: none;
		display: flex;
		min-height: 1em;
		width: auto;
	}

	${({ theme, hasError }) => {
		if (hasError) {
			return css`
				${Annotation} {
					color: ${theme.color.danger};
				}

				input {
					border-bottom: 1px solid;
					border-color: ${theme.color.danger};
					color: ${theme.color.danger};
					&::placeholder {
						color: ${theme.color.danger};
					}
				}
			`;
		}
	}}
`;
