import React, { useState, useEffect, useRef } from 'react';
import { TextInputWrapper } from './styles';
import debounce from 'lodash/debounce';
import { Annotation } from '@mccarthyfinch/author-ui-components';
import ContentEditable from 'react-contenteditable';
import useDebouncedOnChange from 'src/components/hooks/useDebouncedOnChange';

export interface IBorderlessTextInput extends React.InputHTMLAttributes<HTMLInputElement> {
	label?: string | React.ReactChild;
	hasError?: boolean;
	debounceDelay?: number;
	allowNewlines?: boolean;
}

function BorderlessTextInput({
	debounceDelay = 1000,
	onChange,
	value,
	label,
	style,
	className,
	hasError = false,
	disabled,
	autoFocus,
	allowNewlines,
	placeholder,
	...rest
}: IBorderlessTextInput) {
	const getValueFromOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		if (e?.persist) {
			e.persist();
		}
		return e.target.value;
	};
	const [_value, onChangeValue] = useDebouncedOnChange({ value, onChange, getValueFromOnChange, debounceDelay });
	const [ isFocused, setIsFocused ] = useState(false);

	const editableRef = useRef<HTMLElement>(null);
	useEffect(() => {
		if (autoFocus) {
			editableRef.current.focus();
		}
	}, []);

	const onPreventNewline = (e: React.KeyboardEvent<HTMLDivElement>) => {
		if (e.key === 'Enter') {
			e.preventDefault();
			editableRef.current.blur();
		}
	};

	const getValueOrPlaceholder = (): { value: string; isPlaceholder?: boolean } => {
		const currentValue = value === undefined ? '' : (debounceDelay ? _value.toString() : value.toString());
		if (currentValue) {
			return {
				value: currentValue,
			};
		}
		if (isFocused) {
			return {
				value: '',
			};
		}
		return {
			value: placeholder,
			isPlaceholder: true,
		};
	};

	const { value: currentValue, isPlaceholder } = getValueOrPlaceholder();
	return (
		<TextInputWrapper style={style} className={className} hasError={hasError} disabled={disabled}>
			{label && <Annotation fontWeight="medium">{label}</Annotation>}
			<ContentEditable
				innerRef={editableRef}
				role="input"
				tagName="span"
				html={currentValue}
				data-is-placeholder={isPlaceholder}
				// onPaste={(e) => {
				// 	e.preventDefault();
				// 	const text = e.clipboardData.getData("text");
				// 	document.execCommand("insertText", false, text);
				// }}
				onChange={onChange === undefined ? undefined : debounceDelay ? onChangeValue : onChange}
				disabled={disabled}
				onFocus={() => setIsFocused(true)}
				onBlur={() => setIsFocused(false)}
				onKeyDown={(e) => {
					if (!allowNewlines) {
						onPreventNewline(e);
					}
				}}
				{...rest}
			></ContentEditable>
		</TextInputWrapper>
	);
}
export default BorderlessTextInput;
