import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusCircle } from '@fortawesome/pro-light-svg-icons';
import { IconButtonBorderless } from '@mccarthyfinch/author-ui-components';

interface IAddItemButtonProps {
	onClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

export default function AddItemButton({ onClick, ...rest }: IAddItemButtonProps) {
	return (
		<IconButtonBorderless onClick={onClick} style={{ paddingRight: '0' }} {...rest}>
			<FontAwesomeIcon icon={faPlusCircle} />
		</IconButtonBorderless>
	);
}
