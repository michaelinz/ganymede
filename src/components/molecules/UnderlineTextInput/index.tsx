import React, { useState, useEffect, useRef } from 'react';
import { UnderlineTextInputWrapper, Underline, UnderlineWrapper } from './styles';
import debounce from 'lodash/debounce';
import { Annotation } from '@mccarthyfinch/author-ui-components';
import ContentEditable from 'react-contenteditable';

export interface IUnderlineTextInput extends React.InputHTMLAttributes<HTMLInputElement> {
	label?: string | React.ReactChild;
	hasError?: boolean;
	debounceDelay?: number;
	preventNewlines?: boolean;
}

function UnderlineTextInput({ debounceDelay = 1000, onChange, value, label, style, className, hasError = false, disabled, preventNewlines, placeholder, ...rest }: IUnderlineTextInput) {
	const [_value, setValue] = useState(value);
	const debouncedOnChange = useRef(debounce(onChange, debounceDelay));
	const [ isFocused, setIsFocused ] = useState(false);
	const editableRef = useRef(null);

	useEffect(() => {
		if (value !== _value) {
			setValue(value);
		}
	}, [value]);

	useEffect(() => {
		debouncedOnChange.current = debounce(onChange, debounceDelay);
	}, [onChange]);

	const onChangeValue = (e: React.ChangeEvent<HTMLInputElement>) => {
		if (e && e.persist) {
			e.persist();
		}
		setValue(e.target.value);
		debouncedOnChange.current(e);
	};

	const onPreventNewline = (e: React.KeyboardEvent<HTMLDivElement>) => {
		if (e.key === 'Enter') {
			e.preventDefault();
			editableRef.current.blur();
		}
	};

	const getValueOrPlaceholder = (): { value: string; isPlaceholder?: boolean } => {
		const currentValue = value === undefined ? '' : (debounceDelay ? _value.toString() : value.toString());
		if (currentValue) {
			return {
				value: currentValue,
			};
		}
		if (isFocused) {
			return {
				value: '<br />',
			};
		}
		return {
			value: placeholder,
			isPlaceholder: true,
		};
	};

	const { value: currentValue, isPlaceholder } = getValueOrPlaceholder();

	return (
		<UnderlineTextInputWrapper style={style} className={className} hasError={hasError} disabled={disabled}>
			{label && <Annotation fontWeight="medium">{label}</Annotation>}
			<ContentEditable
				innerRef={editableRef}
				role="input"
				tagName="span"
				html={currentValue}
				data-is-placeholder={isPlaceholder}
				// onPaste={(e) => {
				// 	e.preventDefault();
				// 	const text = e.clipboardData.getData("text");
				// 	document.execCommand("insertText", false, text);
				// }}
				onChange={onChange === undefined ? undefined : onChangeValue}
				onKeyDown={(e) => {
					if (preventNewlines) {
						onPreventNewline(e);
					}
				}}
				disabled={disabled}
				onFocus={() => setIsFocused(true)}
				onBlur={() => setIsFocused(false)}
				{...rest}
			></ContentEditable>
			<UnderlineWrapper>
				<Underline />
			</UnderlineWrapper>
		</UnderlineTextInputWrapper>
	);
}
export default UnderlineTextInput;
