import styled, { css } from 'styled-components';
import { TextInputWrapper } from '../BorderlessTextInput/styles';

export const UnderlineWrapper = styled.span`
	position: absolute;
	display: block;
	bottom: 0;
	left: 0rem;
	right: 0rem;
	transition: all 0.3s;
`;

export const Underline = styled.span`
	position: absolute;
	display: block;
	background: black;
	transition: all 0.3s;

	bottom: 0;
	// Center
	/* height: 0;
	left: 50%;
	right: 50%; */

	// From Left
	height: 1px;
	left: 0;
	right: 100%;
`;

export const UnderlineTextInputWrapper = styled(TextInputWrapper)`
	${({ disabled }) => {
		if (!disabled) {
			return css`

			*[role=input] {
					/* &:hover, &:active {
						margin-right: 5rem;
					/* } */

					&:hover + ${UnderlineWrapper}, &:focus  + ${UnderlineWrapper}, &:active + ${UnderlineWrapper} {

						${Underline} {
							height: 1px;
							left: 0;
							right: 0;
						}
					}
				}
			}
			`;
		}
	}}

	*[role=input] {
		margin: 0rem;
	}
`;
