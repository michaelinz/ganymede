import styled from 'styled-components';
import {
	PrimaryButton as AuthorPrimaryButton,
	SecondaryButton as AuthorSecondaryButton,
	IconButtonBorderless as AuthorIconButtonBorderless,
} from '@mccarthyfinch/author-ui-components';

export const PrimaryButton = styled(AuthorPrimaryButton)`
	background-color: #222222;
	color: white;
	font-size: 1rem;
	padding: 0.8em;
	border-radius: 0.375em;
	min-width: 4.2em;

	&:hover:not(:disabled) {
		background-color: #222222;
		opacity: 0.8;

		/* background-color: ${({ theme }) => theme.colorPalette.primary['700']}; */
	}
	&:active:not(:disabled), &:focus:not(:disabled) {
		background-color: #222222;
		opacity: 0.6;
		/* background-color: ${({ theme }) => theme.colorPalette.primary['300']}; */
	}
`;


export const SecondaryButton = styled(AuthorSecondaryButton)`
	font-size: 1rem;
	padding: 0.8em;
	border-radius: 0.375em;
	background: transparent;
	border: solid 2px #e8eaee;
	min-width: 4.2em;
`;

export const IconButtonBorderless = styled(AuthorIconButtonBorderless)`
	padding: 0;

	svg {
		color: white;
	}

	&:hover:not(:disabled), &:focus {
		svg {
			color: #8CC3E8;
		}
	}
`;
