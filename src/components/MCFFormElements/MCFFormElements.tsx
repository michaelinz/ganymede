import { withStyles } from "@material-ui/core";
import InputBase from "@material-ui/core/InputBase";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";

import React from "react";
import Select from "@material-ui/core/Select";
import {
	MCFFormsStyledMenuItem,
	MCFFormStyledSelectInput
} from "./styles";
import { faChevronDown } from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styled, { DefaultTheme } from 'styled-components';


interface ISelectOption {
	value: any,
	key: string | number,
	label: any,
}

const SelectInputDropdownIcon = styled(FontAwesomeIcon)`
font-size: 0.6rem;
position: absolute;
top: 50%;
right: 8px;
transform: translate(0, -50%);
color: ${({ theme }) => theme.color.fontSecondary};
pointer-events: none;
`;



const MCFSimpleSelect = ({ onChange, label = null, options, nativeSelect = false, value, disabled, ...other }: { onChange: (value) => void, label?: string, options: ISelectOption[], nativeSelect?: boolean, disabled?: boolean, [key: string]: any }) => {

	return (
		<FormControl style={{ width: "100%" }}>
			<InputLabel style={{ fontSize: '0.75rem' }}>
				{label}
			</InputLabel>
			<Select
				IconComponent={() => (
					<SelectInputDropdownIcon size={"xs"} icon={faChevronDown} />
				)}
				onChange={(e) => onChange(e.target.value)}
				// name={`${label}_options`}
				value={value}
				input={<MCFFormStyledSelectInput />}
				disabled={disabled}
			>
				{options.map(i => {
					return (
						<MCFFormsStyledMenuItem
							key={`${i.value}_options`}
							value={i.value}
						>
							{i.label}
						</MCFFormsStyledMenuItem>
					);
				})}
			</Select>

		</FormControl>

	);
}
export default MCFSimpleSelect
