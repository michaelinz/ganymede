import styled, {DefaultTheme} from 'styled-components';
// import {Subtitle} from '../typography';
import MenuItem from '@material-ui/core/MenuItem';
import {withStyles} from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';
import TextField from '@material-ui/core/TextField';
// import {DatePicker} from "office-ui-fabric-react";


export const MCFFormStyledSelectInput = withStyles(theme => ({
	root: {
		// paddingLeft: "8px",
		width: "100%",
		// 'label + &': {
		// 	marginTop: theme.spacing.unit * 2,
		// },

	},
	input: {
		width: "100%",
		// fontFamily: "Roboto",
		fontSize: "12px",
		fontWeight: "normal",
		fontStyle: "normal",
		fontStretch: "normal",
		lineHeight: "14px",
		letterSpacing: "normal",
		paddingLeft: "8px",
		border: '1px solid #e3e6eb',
		boxSizing: "border-box",
		borderRadius: 3,
		color: "#3b4962",
		backgroundColor: 'white',
	},
}))(InputBase);

export const MCFFormsStyledMenuItem = withStyles(theme => ({
	root: {
		// fontFamily: "Roboto",
		fontSize: "12px",
		fontWeight: "normal",
		fontStyle: "normal",
		fontStretch: "normal",
		padding: "8px",
		width: "100%",
		color: "#3b4962",
		backgroundColor: 'white',
	}
}))(MenuItem);

