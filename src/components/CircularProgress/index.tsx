import React from 'react';

interface CircularProgressProps extends React.HTMLAttributes<HTMLSpanElement> {}

// circular-progress class style is from public/index.html
export default function CircularProgress({ className, ...rest }: CircularProgressProps) {
	return <span className={`circular-progress ${className}`} {...rest} />;
}
