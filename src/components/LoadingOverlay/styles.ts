import styled from 'styled-components';

export const LoadingOverlayWrapper = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	overflow: hidden;
	flex-shrink: 1;
	flex-grow: 0;
	background: transparent;
	display: flex;
	flex-direction: column;
	margin: 0;
	/* padding-top: 5rem; */
	align-items: center;
	z-index: 1000;
	justify-content: center;
`;
