import React from 'react';
import CircularProgress from 'src/components/CircularProgress';
import { LoadingOverlayWrapper } from './styles';

function LoadingOverlay({ message }: { message?: React.ReactChild }) {
	return (
		<LoadingOverlayWrapper>
			<CircularProgress />
			{message}
		</LoadingOverlayWrapper>
	);
}

export default LoadingOverlay;
