import styled from 'styled-components';

export const FloatingMenuWrapper = styled.div`
	position: fixed;
	right: 2.75rem;
	bottom: 1.6rem;

	display: flex;
	flex-direction: column;
	align-items: flex-end;
	z-index: 200;
`;

export const FAB = styled.div`
	cursor: pointer;

	height: 2.75rem;
	width: 2.75rem;
	border-radius: 50%;
	box-shadow: 0 0 7px 0 rgba(34, 44, 99, 0.5);
	color: ${({ theme }) => theme.colorPalette.slate[900]};
	border: solid 1px ${({ theme }) => theme.colorPalette.slate[900]};

	padding: 1rem;

	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	background-color: white;

	svg {
		font-size: 1.3rem;
	}

	&:hover {
		opacity: 0.5;
		transition: all 0.3s;
	}
`;

export const MenuWrapper = styled.div`
	margin-bottom: 0.5rem;
`;
