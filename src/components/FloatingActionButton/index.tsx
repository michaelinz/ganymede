import React, { useRef, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/pro-light-svg-icons';
import { useOnClickOutside } from '@mccarthyfinch/author-ui-components';
import { MenuList, MenuItem } from 'src/components/Menu';
import { FloatingMenuWrapper, FAB, MenuWrapper } from './styles';

export interface IMenuItem {
	key: string;
	renderContent: () => React.ReactChild;
	onClick: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
}

export interface IFloatingActionButton {
	onFABClick?: (isOpen?: boolean) => void;
	menu?: IMenuItem[];
}

export default function FloatingActionButton({ onFABClick, menu }: IFloatingActionButton) {
	const ref = useRef();
	const [isOpen, setIsOpen] = useState(false);

	useOnClickOutside(ref, () => {
		if (isOpen) {
			setIsOpen(false);
		}
	});
	return (
		<>
			<FloatingMenuWrapper ref={ref}>
				{menu && isOpen && (
					<MenuWrapper>
						<MenuList>
							{menu.map((item) => {
								return (
									<MenuItem
										key={item.key}
										renderContent={item.renderContent}
										onClick={(e) => {
											item.onClick(e);
											setIsOpen(false);
										}}
									/>
								);
							})}
						</MenuList>
					</MenuWrapper>
				)}
				<FAB
					onClick={() => {
						onFABClick && onFABClick(isOpen);
						setIsOpen(!isOpen);
					}}
				>
					<FontAwesomeIcon icon={faPlus} />
				</FAB>
			</FloatingMenuWrapper>
		</>
	);
}
