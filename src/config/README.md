The config.override.js file is referred to in index.html to setup configuration. We override the src/config/AppConfigs file with these configs for env specific config.

We refer to prod as demo1 in tangler namespace for some reason which is why the prod config is `config.override.demo1.js` instead of `config.override.prod.js`

https://github.com/McCarthyFinch/McCarthyFinch.Tangler/blob/036db8b8f563f0961b5ad7e100b6c406b7329d7a/tangler-namespaces.yml#L478


During development, the config file is copied from src/config/config.override.js to public/config/config.override.js

Note: because we only copy this file at the start of `npm start`, you will need to either run `npm run prepare` manually or restart `npm start` to get the changes. Or copy the file over manually.

During deployment, the correct config file is copied to to deployment by the main.tf file
