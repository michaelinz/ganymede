const FeatureFlags = {
	openSSOPopupWithMSPopup: false,
	AUTH_USE_COOKIES: false,
	ENABLE_DRAG_N_DROP_DOC_REFS: false, // Placeholder feature flag
};

export default FeatureFlags;
