const fs = require('fs');
const path = require('path');

function copyConfigToOverride(env = 'dev') {
	const srcFile = path.resolve(__dirname, `config.override.${env}.js`);
	const destFile = path.resolve(__dirname, 'config.override.js');
	if (fs.existsSync(destFile)) {
		console.log(`${destFile} already exists`);
		return;
	}
	console.log(`Copying ${srcFile} to ${destFile}`);
	fs.copyFileSync(srcFile, destFile);
}

function copyConfigOverrideToPublic() {
	const srcFile = path.resolve(__dirname, `config.override.js`);
	const destFolder = path.resolve(__dirname, '..', '..', 'public', 'config');
	if (!fs.existsSync(destFolder)) {
		fs.mkdirSync(destFolder);
	}
	const destFile = path.resolve(destFolder, 'config.override.js');
	console.log(`Copying ${srcFile} to ${destFile}`);
	fs.copyFileSync(srcFile, destFile);
}

copyConfigToOverride();
copyConfigOverrideToPublic();
