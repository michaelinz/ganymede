// See https://www.snowpack.dev/#environment-variables
// We populate the .env file for builds via npm run prepare

const APP_CONFIG = {
	API_URL_BASE: 'https://localhost:3002',
	SHOW_REACT_QUERY_DEV_TOOLS: false,
};

const AppConfigs = APP_CONFIG;

/***
 * Merges APP_CONFIG with config.override.js
 */
const ConfigManager = APP_CONFIG;
if (window['APP_CONFIG_OVERRIDE']) {
	Object.keys(window['APP_CONFIG_OVERRIDE']).forEach((appConfigKey) => {
		if (window['APP_CONFIG_OVERRIDE'][appConfigKey] != null || window['APP_CONFIG_OVERRIDE'][appConfigKey] === '') {
			ConfigManager[appConfigKey] = window['APP_CONFIG_OVERRIDE'][appConfigKey];
		}
	});
}

export default AppConfigs;
