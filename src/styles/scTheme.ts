import color from './colors';
import { scTheme } from '@mccarthyfinch/author-ui-components';

const themeOverrides = {
	fontFamily: '"Montserrat", "Roboto", "Helvetica"',
	color: {
		...scTheme.color,
		// Result section colours
		alert: color.red.base,
		concept: color.blue.base,
		detail: color.indigo.base,
		definition: color.teal.base,

		checked: '#12c39a',
		success: '#12c39a',
		fontPrimary: '#222222',

		publicConcept: color.purple.base,
	},
	colorPalette: {
		...scTheme.colorPalette,

		// Result section colours
		alert: color.red,
		concept: color.blue,
		detail: color.indigo,
		definition: color.teal,

		author: color.blue,


		publicConcept: color.purple,
	},
	layout: {
		headerHeight: '9.12rem',
		headerHeightSmall: '4.25rem',
	},
	typography: {
		...scTheme.typography,
		paragraph: {
			fontSize: '13px',
			lineHeight: '16px',
		},
		subtitle: {
			fontSize: '16px',
			lineHeight: '1.1em',
		},
		caption: {
			fontSize: '10px',
			lineHeight: '1.1em',
		},
		categoryTitle: {
			fontSize: '14px',
			lineHeight: '17px',
		}
	},
};

const theme = {
	...scTheme,
	...themeOverrides,
};

export { theme };
export default theme;
