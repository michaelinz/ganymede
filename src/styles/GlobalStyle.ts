import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
	*, *::after, *::before {
		box-sizing: border-box;
		/* transition: 0.3s all; */
	}

	a {
		color: ${({ theme }) => theme.color.primary};
		text-decoration: underline;
		transition: 0.3s all;

		&:hover {
			cursor: pointer;
			color: ${({ theme }) => theme.colorPalette.primary[200]};
		}
	}

	html {
		height: 100%;
		width: 100%;
		max-width: 100%;
	}

	body {
		font-family: ${({ theme }) => theme.fontFamily};
		color: ${({ theme }) => theme.color.fontPrimary};
		font-weight: ${({ theme }) => theme.fontWeight.regular};
		width: 100%;
		max-width: 100%;
		height: 100%;
		font-size: ${({ theme }) => theme.typography.paragraph.fontSize};
	}

	img {
		vertical-align: middle;
	}

	ol, ul {
		padding-inline-start: 0em;
	}

	/** Mac scrollbar from https://gist.github.com/devinrhode2/2573411 */
	::-webkit-scrollbar {
    width: 8px;
    height: 8px;
    background-color: rgba(0,0,0,0);
    -webkit-border-radius: 100px;
	}

	::-webkit-scrollbar:hover {
		background-color: rgba(0, 0, 0, 0.09);
	}

	::-webkit-scrollbar-thumb {
		background: rgba(0,0,0,0.5);
		-webkit-border-radius: 100px;
	}
	::-webkit-scrollbar-thumb:active {
		background: rgba(0,0,0,0.61); /** Some darker color when you click it */
		-webkit-border-radius: 100px;
	}

	/* add vertical min-height & horizontal min-width */
	::-webkit-scrollbar-thumb:vertical {
		min-height: 10px;
	}
	::-webkit-scrollbar-thumb:horizontal {
		min-width: 10px;
	}
`;

export default GlobalStyle;
