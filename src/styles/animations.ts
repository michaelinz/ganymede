import { css } from 'styled-components';

export const loadingPlaceholderAnimation = css`
	@keyframes placeholderAnimate {
    0% { background-position: -650px 0; }
    100% { background-position: 650px 0; }
	}

	animation-duration: 6s;
	animation-fill-mode: forwards;
	animation-iteration-count: infinite;
	animation-timing-function: linear;
	animation-name: placeholderAnimate;
	background: ${({ theme }) => theme.colorPalette.snow[100]}; // Fallback
	background: ${({ theme }) => `linear-gradient(to right, ${theme.colorPalette.snow[200]} 2vw, ${theme.colorPalette.smoke[100]} 18vw, ${theme.colorPalette.snow[200]} 33vw)`};
`;
