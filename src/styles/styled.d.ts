/* eslint-disable @typescript-eslint/no-empty-interface */
// Copied from '@mccarthyfinch/mcf-ui/src/styles/styled.d.ts';
import 'styled-components';
import scTheme from './scTheme';
import { ColorPalette } from '@mccarthyfinch/author-ui-components';

type Theme = typeof scTheme;

declare module 'styled-components' {
	export interface DefaultTheme extends Theme {}
	export type ThemeColor = keyof typeof scTheme.color;
	export type ThemeColorPalette = keyof typeof scTheme.colorPalette;
	export type ThemeColorPaletteShade = keyof ColorPalette;
	export type ThemeFontWeight = keyof typeof scTheme.fontWeight;
	export type ThemeTypography = keyof typeof scTheme.typography;
}
