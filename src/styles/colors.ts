import { colors as defaultColors } from '@mccarthyfinch/author-ui-components';
// import getFilledCustomPalette from "@mccarthyfinch/author-ui-components/dist/theme/getFilledCustomPalette";

const colors = {
	...defaultColors,
};
export default colors;
