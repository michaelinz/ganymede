/* eslint-disable */
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');

const fs = require('fs');


const cwd = process.cwd();
const tempBuildManifest = JSON.parse(
	fs.readFileSync(path.join(cwd, 'package.json'), {
		encoding: 'utf-8',
	}),
);
const presetEnvTargets =
	tempBuildManifest.browserslist || '>0.75%, not ie 11, not UCAndroid >0, not OperaMini all';


/**
 * Include in webpack transpilation
 */
const inclusions = [
	path.resolve('src'),
	/node_modules\/((@mccarthyfinch\/public-api-client|query-string|@remirror|escape-string-regexp|map-obj))/, // starts with eg @remirror
];

module.exports = {
	entry: {
		app: './src/index.tsx',
	},
	mode: 'development',
	devtool: 'none',
	devServer: {
		contentBase: path.resolve('./', 'ie11', 'build'),
		historyApiFallback: true,
		hot: true,
		stats: 'errors-warnings',
		https: {
			key: fs.readFileSync('./snowpack.key'),
			cert: fs.readFileSync('./snowpack.crt'),
			cacert: fs.readFileSync('./snowpack.crt')
		},
	},
	output: {
		path: path.resolve('./', 'ie11', 'build'),
		filename: '[name].[hash].js',
		publicPath: '/',
	},
	resolve: {
		extensions: [".js", ".jsx", ".json", ".ts", ".tsx", ".html"],
		alias: {
			'src': path.resolve('./', './src'),
		},
	},
	module: {
		rules: [
			{
				test: /\.(t|j)sx?$/,
				include: inclusions,
				use: [
					{
						loader: '@open-wc/webpack-import-meta-loader',
					},
					{
						loader: 'babel-loader',
						options: {
						configFile: false,
							babelrc: false,
							compact: true,
							presets: [
								[
									'@babel/preset-env',
									{
										targets: presetEnvTargets,
										bugfixes: true,
									},
								],
								"@babel/preset-typescript",
								"@babel/preset-react"
							],
							plugins: [
								['@babel/plugin-proposal-class-properties', { loose: true }],
							]
						},
					},
					{
						loader: require.resolve('./plugins/proxy-import-resolve.js'),
					},
				],
			},
			{
				test: /\.css$/,
				use: [
					{
						loader: 'css-loader',
					},
				],
			},
			{
				test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
				use: [
					{
						loader: 'url-loader?limit=100000',
					},
				],
			},
			// {
			// 	test: /.*/,
			// 	exclude: [/\.(t|j)sx?$/, /\.json?$/, /\.css$/],
			// 	use: [
			// 		{
			// 			loader: 'file-loader',
			// 			options: {
			// 				name: 'assets/[name]-[hash].[ext]',
			// 			},
			// 		},
			// 	],
			// },
		],
	},
	plugins: [
		new CopyWebpackPlugin({
			patterns: [
				{
					from: './public', to: './'
				}
			]
		}),
		new HtmlWebpackPlugin({
			template: './public/index.html',
			filename: 'index.html',
			chunks: ['app'],
		})
	]
};
