module.exports = {
	alias: {
		"src": "./src",
	},
	mount: {
		public: '/',
		src: '/_dist_',
	},
	devOptions: {
		secure: true,
		hmrErrorOverlay: false,
		port:8080
	},
	plugins: [
		'@snowpack/plugin-react-refresh',
		'@snowpack/plugin-dotenv',
		'@snowpack/plugin-typescript',
		'@snowpack/plugin-webpack'
	],
	installOptions: {
		installTypes: true,
		polyfillNode: true,
	}
};

