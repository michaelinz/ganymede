/* eslint-disable no-undef */

function setOfficeInitialised() {
	window.isOfficeInitialised = true;
	console.log('window.isOfficeInitialised set to true');
}

function initOffice() {
	if (Office) {
		if (Office.onReady) {
			Office.onReady(setOfficeInitialised);
			// eslint-disable-next-line @typescript-eslint/no-empty-function
			Office.initialize = function() {};
		} else {
			Office.initialize = setOfficeInitialised;
		}
	} else {
		setTimeout(initOffice, 500);
	}
}

window.isOfficeInitialised = false;
initOffice();
