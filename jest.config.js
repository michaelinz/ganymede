
module.exports = {
	...require('@snowpack/app-scripts-react/jest.config.js')(),
	"testPathIgnorePatterns": [
		"<rootDir>/src/config"
	],
	"moduleNameMapper": {
		"src/(.*)": "<rootDir>/src/$1"
	}
};
